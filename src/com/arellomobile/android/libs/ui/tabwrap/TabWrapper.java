/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ViewAnimator;

/**
 * @author denis.mirochnik
 *         <p/>
 *         09.07.2012
 */
@SuppressWarnings("deprecation")
public class TabWrapper extends BaseActivityGroup implements ContentActivity
{
	private static final String LAST_ID = "lastId";
	private static final String CHILDS = "childs";
	private static final String CURRENT_CHILD = "currentChild";

	public static final String FIRST_INTENT = "firstIntent";
	private LocalActivityManager mLocalActivityManager;

	List<Content> mContent;

	private int mActivityId;

	private ViewAnimator mAnimator;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mContent = new ArrayList<Content>();
		mAnimator = new ViewAnimator(this);

		mAnimator.setAnimateFirstView(false);

		setContentView(mAnimator);

		mLocalActivityManager = getLocalActivityManager();

		if (savedInstanceState != null && savedInstanceState.containsKey(CHILDS) && savedInstanceState.containsKey(CURRENT_CHILD))
		{
			final int currChild = savedInstanceState.getInt(CURRENT_CHILD);

			mActivityId = savedInstanceState.getInt(LAST_ID, 0);
			mContent = savedInstanceState.getParcelableArrayList(CHILDS);

			for (final Content content : mContent)
			{
				startActivityExisted(content);
			}

			mAnimator.setDisplayedChild(currChild);
		}
		else
		{
			final Intent firstIntent = getIntent().getParcelableExtra(FIRST_INTENT);

			if (firstIntent != null)
			{
				startActivity(firstIntent);
			}
		}
	}

	@Override
	public ChildActivity findChildActivity(String activityId)
	{
		ChildActivity childActivity;
		Activity activity;
		Activity baseActivity;

		for (final Content content : mContent)
		{
			activity = getLocalActivityManager().getActivity(content.getId());

			if (activity instanceof ChildActivity)
			{
				final String id = ((ChildActivity) activity).getActivityId();

				if (id != null && id.equals(activityId))
				{
					return (ChildActivity) activity;
				}
			}
		}

		for (final Content content : mContent)
		{
			baseActivity = getLocalActivityManager().getActivity(content.getId());

			if (baseActivity instanceof BaseActivityGroup)
			{
				childActivity = ((BaseActivityGroup) baseActivity).findChildActivity(activityId);

				if (childActivity != null)
				{
					return childActivity;
				}
			}
		}

		return null;
	}

	@Override
	public ChildActivity getChildActivity(String id)
	{
		return null;
	}

	private String nextActivityId()
	{
		final String activityId = getId(mActivityId);

		mActivityId++;

		return activityId;
	}

	private void addActivity(String id, Intent intent)
	{
		mContent.add(new Content(id, intent));
	}

	private Content getLastActivity()
	{
		if (mContent.size() > 0)
		{
			return mContent.get(mContent.size() - 1);
		}
		else
		{
			return null;
		}
	}

	private String removeLastActivity()
	{
		final int index = mContent.size() - 1;

		final Content content = mContent.get(index);

		mContent.remove(index);

		return content.getId();
	}

	private void removeActivity(Content content)
	{
		mContent.remove(content);
	}

	private Content getActivity(String id)
	{
		for (final Content content : mContent)
		{
			if (content.getId().equals(id))
			{
				return content;
			}
		}

		return null;
	}

	private boolean hasTwoActivity()
	{
		return mContent.size() > 1;
	}

	private String getId(int id)
	{
		return "Activity_" + id;
	}

	private void startActivityExisted(Content content)
	{
		final Intent intent = content.getIntent();
		final String id = content.getId();

		intent.putExtra(TabChildActivity.ACTIVITY_ID, id);

		final FrameLayout decorView = (FrameLayout) mLocalActivityManager.startActivity(id, intent).getDecorView().findViewById(android.R.id.content);

		View contentView = decorView;

		if (decorView.getChildCount() > 0)
		{
			contentView = decorView.getChildAt(0);
			decorView.removeAllViews();
		}

		((TabChildActivity) mLocalActivityManager.getActivity(id)).setRootView(contentView);

		mAnimator.addView(contentView);
	}

	@Override
	public void startActivity(Intent intent)
	{
		final String nextActivity = nextActivityId();

		intent.putExtra(TabChildActivity.ACTIVITY_ID, nextActivity);

		final FrameLayout contentView = (FrameLayout) mLocalActivityManager.startActivity(nextActivity, intent).getDecorView().findViewById(android.R.id.content);

		View content = contentView;

		if (contentView.getChildCount() > 0)
		{
			content = contentView.getChildAt(0);
			contentView.removeAllViews();
		}

		((TabChildActivity) mLocalActivityManager.getActivity(nextActivity)).setRootView(content);
		addActivity(nextActivity, intent);

		mAnimator.addView(content);

		((TabBaseActivity) getParent()).notifyActivityStartedListener(((TabChildActivity) mLocalActivityManager.getActivity(nextActivity)).getActivityId());

		showNext();
	}

	void startActivityOuter(Intent intent)
	{
		if (getParent() != null)
		{
			getParent().startActivity(intent);
		}
		else
		{
			super.startActivity(intent);
		}
	}

	private void destroyLastActivity()
	{
		showPrevious();

		final TabChildActivity activity = (TabChildActivity) mLocalActivityManager.getActivity(mLocalActivityManager.getCurrentId());

		final String id = activity.getActivityId();

		mAnimator.removeView(activity.getRootView());

		mLocalActivityManager.destroyActivity(removeLastActivity(), true);

		mLocalActivityManager.startActivity(getLastActivity().getId(), getLastActivity().getIntent());

		((TabBaseActivity) getParent()).notifyActivityDestroyedListener(id);
	}

	@Override
	public void finishFromChild(Activity child)
	{
		if (!hasTwoActivity())
		{
			return;
		}

		hideKeyBoard();

		final String id = ((TabChildActivity) child).getId();

		final Content content = getActivity(id);

		if (mLocalActivityManager.getCurrentId().equals(id))
		{
			destroyLastActivity();

			return;
		}

		((TabBaseActivity) getParent()).notifyActivityDestroyedListener(((TabChildActivity) child).getActivityId());

		final View contentView = ((TabChildActivity) mLocalActivityManager.getActivity(id)).getRootView();

		mLocalActivityManager.destroyActivity(id, true);

		removeActivity(content);

		if (mAnimator.getCurrentView() == contentView)
		{
			showPrevious();
		}

		mAnimator.removeView(contentView);
	}

	private void showPrevious()
	{
		mAnimator.setInAnimation(getInAnimation(true));
		mAnimator.setOutAnimation(getOutAnimation(true));

		mAnimator.showPrevious();
	}

	private void showNext()
	{
		mAnimator.setInAnimation(getInAnimation(false));
		mAnimator.setOutAnimation(getOutAnimation(false));

		mAnimator.showNext();
	}

	private boolean processKeyUp()
	{
		if (!hasTwoActivity())
		{
			return false;
		}

		destroyLastActivity();

		return true;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getKeyCode() != KeyEvent.KEYCODE_BACK || event.getAction() != KeyEvent.ACTION_UP || (event.getFlags() & KeyEvent.FLAG_SOFT_KEYBOARD) != 0)
		{
			return super.dispatchKeyEvent(event);
		}

		final Activity currentActivity = getLocalActivityManager().getCurrentActivity();

		if (currentActivity != null)
		{
			if (!(currentActivity instanceof TabBaseActivity))
			{
				return processKeyUp();
			}

			if (!currentActivity.dispatchKeyEvent(event))
			{
				return processKeyUp();
			}
			else
			{
				return true;
			}
		}

		return super.dispatchKeyEvent(event);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);

		outState.putInt(CURRENT_CHILD, mAnimator.getDisplayedChild());
		outState.putParcelableArrayList(CHILDS, (ArrayList<? extends Parcelable>) mContent);
		outState.putInt(LAST_ID, mActivityId);
	}

	private static final int IN_ANIM = 0;
	private static final int OUT_ANIM = 1;

	private static final int FORWARD_ANIM = 0;
	private static final int BACK_ANIM = 1;

	private static Animation mAnimtions[][];

	static
	{
		mAnimtions = new Animation[2][2];

		AnimationSet animSet;
		TranslateAnimation transAnim;
		AlphaAnimation alphaAnim;

		animSet = new AnimationSet(true);

		transAnim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_PARENT, 0.5f, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0);
		transAnim.setDuration(200);

		alphaAnim = new AlphaAnimation(0.0f, 1.0f);
		alphaAnim.setDuration(200);

		animSet.addAnimation(transAnim);
		animSet.addAnimation(alphaAnim);

		mAnimtions[IN_ANIM][FORWARD_ANIM] = animSet;

		animSet = new AnimationSet(true);

		transAnim = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_PARENT, -0.5f, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0);
		transAnim.setDuration(200);

		alphaAnim = new AlphaAnimation(0.0f, 1.0f);
		alphaAnim.setDuration(200);

		animSet.addAnimation(transAnim);
		animSet.addAnimation(alphaAnim);

		mAnimtions[IN_ANIM][BACK_ANIM] = animSet;

		animSet = new AnimationSet(true);

		transAnim = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.RELATIVE_TO_PARENT, -0.5f, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0);
		transAnim.setDuration(200);

		alphaAnim = new AlphaAnimation(1.0f, 0.0f);
		alphaAnim.setDuration(200);

		animSet.addAnimation(transAnim);
		animSet.addAnimation(alphaAnim);

		mAnimtions[OUT_ANIM][FORWARD_ANIM] = animSet;

		animSet = new AnimationSet(true);

		transAnim = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.RELATIVE_TO_PARENT, 0.5f, TranslateAnimation.ABSOLUTE, 0, TranslateAnimation.ABSOLUTE, 0);
		transAnim.setDuration(200);

		alphaAnim = new AlphaAnimation(1.0f, 0.0f);
		alphaAnim.setDuration(200);

		animSet.addAnimation(transAnim);
		animSet.addAnimation(alphaAnim);

		mAnimtions[OUT_ANIM][BACK_ANIM] = animSet;
	}

	protected Animation getInAnimation(boolean back)
	{
		if (back)
		{
			return mAnimtions[IN_ANIM][BACK_ANIM];
		}
		else
		{
			return mAnimtions[IN_ANIM][FORWARD_ANIM];
		}
	}

	protected Animation getOutAnimation(boolean back)
	{
		if (back)
		{
			return mAnimtions[OUT_ANIM][BACK_ANIM];
		}
		else
		{
			return mAnimtions[OUT_ANIM][FORWARD_ANIM];
		}
	}

}
