/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         01.08.2012
 */
public interface OnTabChangeListener
{
	void onTabChanged(String tabId);
}
