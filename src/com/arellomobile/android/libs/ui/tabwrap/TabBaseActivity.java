/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.KeyEvent;

/**
 * @author denis.mirochnik
 *         <p/>
 *         13.07.2012
 */
abstract class TabBaseActivity extends TabChildActivity implements OnActivityResultHandler, TabController
{
	private OnTabChangeListener mTabChangeListener;
	private OnActivityChangeListener mActivityChangeListener;

	private Class<? extends TabWrapper> mWrapper;

	private SparseArray<OnActivityResultListener> mResultMap;

	private boolean mRememberSelected = true;

	private List<ChildActivity> mChildActivites;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mResultMap = new SparseArray<OnActivityResultListener>();
		mChildActivites = new ArrayList<ChildActivity>();
	}

	void registerChildActivity(ChildActivity activity)
	{
		mChildActivites.add(activity);
	}

	void unregisterChildActivity(ChildActivity activity)
	{
		mChildActivites.remove(activity);
	}

	@Override
	public ChildActivity getChildActivity(String activityId)
	{
		for (final ChildActivity activity : mChildActivites)
		{
			final String id = activity.getActivityId();

			if (activityId != null && activityId.equals(id))
			{
				return activity;
			}
		}

		for (final ChildActivity activity : mChildActivites)
		{
			if (activity instanceof ContentActivity)
			{
				final ChildActivity childActivity = ((ContentActivity) activity).getChildActivity(activityId);

				if (childActivity != null)
				{
					return childActivity;
				}
			}
		}

		final ChildActivity childActivity = findChildActivity(activityId);

		if (childActivity != null)
		{
			return childActivity;
		}

		return null;
	}

	protected void setup(Bundle savedInstanceState)
	{
		mWrapper = TabWrapper.class;
	}

	protected void setup(Bundle savedInstanceState, Class<? extends TabWrapper> wrapper)
	{
		mWrapper = wrapper;
	}

	@Override
	public void setOnActivityResultListener(OnActivityResultListener listener, int requestCode)
	{
		mResultMap.put(requestCode, listener);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		final OnActivityResultListener listener = mResultMap.get(requestCode);

		if (listener != null)
		{
			mResultMap.remove(requestCode);

			listener.onActivityResultUnreal(requestCode, resultCode, data);
		}
		else
		{
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getKeyCode() != KeyEvent.KEYCODE_BACK || event.getAction() != KeyEvent.ACTION_UP || (event.getFlags() & KeyEvent.FLAG_SOFT_KEYBOARD) != 0)
		{
			return super.dispatchKeyEvent(event);
		}

		final Activity currentActivity = getLocalActivityManager().getCurrentActivity();

		if (currentActivity != null)
		{
			if (!currentActivity.dispatchKeyEvent(event))
			{
				if (getParent() != null)
				{
					return false;
				}

				return super.dispatchKeyEvent(event);
			}
			else
			{
				return true;
			}
		}

		return super.dispatchKeyEvent(event);
	}

	Intent getWrapperIntent(String id, Intent intent)
	{
		final Intent wrapperIntent = new Intent(this, getWrapperClass());

		wrapperIntent.putExtra(TabWrapper.FIRST_INTENT, intent);
		wrapperIntent.putExtra(BaseActivityGroup.ACTIVITY_ID, id);

		return wrapperIntent;
	}

	Class<? extends TabWrapper> getWrapperClass()
	{
		return mWrapper;
	}

	void notifyActivityStartedListener(String id)
	{
		if (mActivityChangeListener != null)
		{
			mActivityChangeListener.onActivityStarted(id);
		}
	}

	void notifyActivityDestroyedListener(String id)
	{
		if (mActivityChangeListener != null)
		{
			mActivityChangeListener.onActivityDestroyed(id);
		}
	}

	void notifyTabChangeListener(String id)
	{
		hideKeyBoard();

		if (mTabChangeListener != null)
		{
			mTabChangeListener.onTabChanged(id);
		}
	}

	@Override
	public void setOnActivityChangeListener(OnActivityChangeListener listener)
	{
		mActivityChangeListener = listener;
	}

	@Override
	public void setOnTabChangeListener(OnTabChangeListener listener)
	{
		mTabChangeListener = listener;
	}

	public boolean isRememberSelected()
	{
		return mRememberSelected;
	}

	public void setRememberSelected(boolean rememberSelected)
	{
		mRememberSelected = rememberSelected;
	}
}
