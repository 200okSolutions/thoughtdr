/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * @author denis.mirochnik
 *         <p/>
 *         09.07.2012
 */
public class TabChildActivity extends BaseActivityGroup implements OnActivityResultListener, InternalChildActivity
{
	private static final int CODE_BIAS = 10000;

	private static final int BASE_CODE_VALUE = 1000000;

	private static final String REQUEST_CODES = "requestCodes";

	private static final String BASE_CODE = "baseCode";

	private View mRootView;

	private static int sCodeBase = 1000000;

	private int mCodeBase;

	private List<Integer> mRequestCodes;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		final TabBaseActivity tabController = (TabBaseActivity) getNearestTabController();

		if (tabController != null)
		{
			tabController.registerChildActivity(this);
		}

		if (savedInstanceState != null && savedInstanceState.containsKey(BASE_CODE))
		{
			mCodeBase = savedInstanceState.getInt(BASE_CODE);
		}
		else
		{
			if (sCodeBase + CODE_BIAS < 0)
			{
				sCodeBase = BASE_CODE_VALUE;
			}

			mCodeBase = sCodeBase + CODE_BIAS;
			sCodeBase += CODE_BIAS;
		}

		if (savedInstanceState != null && savedInstanceState.containsKey(REQUEST_CODES))
		{
			mRequestCodes = savedInstanceState.getIntegerArrayList(REQUEST_CODES);

			final Activity rootActivity = getRootActivity();

			if (rootActivity != this && rootActivity instanceof OnActivityResultHandler)
			{
				for (final Integer code : mRequestCodes)
				{
					((OnActivityResultHandler) rootActivity).setOnActivityResultListener(this, encodeCode(code));
				}
			}
		}
		else
		{
			mRequestCodes = new ArrayList<Integer>();
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy()
	{
		super.onDestroy();

		final TabBaseActivity tabController = (TabBaseActivity) getNearestTabController();

		if (tabController != null)
		{
			tabController.unregisterChildActivity(this);
		}
	}

	@Override
	public void startActivity(Intent intent)
	{
		if (getParent() != null)
		{
			getParent().startActivity(intent);
		}
		else
		{
			super.startActivity(intent);
		}
	}

	public void startActivityOuter(Intent intent)
	{
		if (getParent() != null && getParent() instanceof TabWrapper)
		{
			((TabWrapper) getParent()).startActivityOuter(intent);
		}
		else
		{
			super.startActivity(intent);
		}
	}

	public void startActivityReal(Intent intent)
	{
		super.startActivity(intent);
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode)
	{
		final Activity parent = getRootActivity();

		if (parent == this)
		{
			super.startActivityForResult(intent, requestCode);

			return;
		}

		if (parent instanceof OnActivityResultHandler)
		{
			mRequestCodes.add(requestCode);
			((OnActivityResultHandler) parent).setOnActivityResultListener(this, encodeCode(requestCode));
		}

		parent.startActivityForResult(intent, encodeCode(requestCode));
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);

		outState.putIntegerArrayList(REQUEST_CODES, (ArrayList<Integer>) mRequestCodes);
		outState.putInt(BASE_CODE, mCodeBase);
	}

	public void finishReal()
	{
		final Activity rootActivity = getRootActivity();

		rootActivity.finish();
	}

	protected String getTabId()
	{
		final Activity parent = getParent();

		if (parent != null && parent instanceof TabWrapper)
		{
			final TabWrapper tabWrapper = (TabWrapper) parent;

			return tabWrapper.getId();
		}

		return null;
	}

	@Override
	public TabController getNearestTabController()
	{
		Activity parent = this;

		while (parent.getParent() != null)
		{
			parent = parent.getParent();

			if (parent instanceof TabController)
			{
				return (TabController) parent;
			}
		}

		return null;
	}

	@Override
	public View findViewById(int id)
	{
		if (getRootView() != null)
		{
			return getRootView().findViewById(id);
		}

		return super.findViewById(id);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		onActivityResultReal(requestCode, resultCode, data);
	}

	@Override
	public void onActivityResultUnreal(int requestCode, int resultCode, Intent data)
	{
		final int realCode = decodeCode(requestCode);

		mRequestCodes.remove(mRequestCodes.indexOf(realCode));

		onActivityResultReal(realCode, resultCode, data);
	}

	protected void onActivityResultReal(int requestCode, int resultCode, Intent data)
	{
	}

	private int decodeCode(int requestCode)
	{
		return requestCode - mCodeBase;
	}

	private int encodeCode(int requestCode)
	{
		return requestCode + mCodeBase;
	}

	void setRootView(View rootView)
	{
		mRootView = rootView;
	}

	protected View getRootView()
	{
		return mRootView;
	}

	@Override
	public String getActivityId()
	{
		return getId();
	}

	@Override
	ChildActivity findChildActivity(String activityId)
	{
		return null;
	}
}
