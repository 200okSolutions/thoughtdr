/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import android.content.Intent;

/**
 * @author denis.mirochnik
 *         <p/>
 *         10.07.2012
 */
public interface OnActivityResultListener
{
	void onActivityResultUnreal(int requestCode, int resultCode, Intent data);
}
