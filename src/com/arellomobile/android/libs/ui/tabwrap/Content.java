package com.arellomobile.android.libs.ui.tabwrap;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

class Content implements Parcelable
{
	private final String mId;
	private final Intent mIntent;

	public Content(String id, Intent intent)
	{
		mId = id;
		mIntent = intent;
	}

	public String getId()
	{
		return mId;
	}

	public Intent getIntent()
	{
		return mIntent;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(mId);
		dest.writeParcelable(mIntent, flags);
	}

	public static final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>()
	{
		@Override
		public Content createFromParcel(Parcel in)
		{
			final Content content =
					new Content(in.readString(), (Intent) in.readParcelable(Intent.class.getClassLoader()));

			return content;
		}

		@Override
		public Content[] newArray(int size)
		{
			return new Content[size];
		}
	};
}
