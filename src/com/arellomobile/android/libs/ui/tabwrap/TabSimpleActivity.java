/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author denis.mirochnik
 *         <p/>
 *         13.07.2012
 */
public abstract class TabSimpleActivity extends TabAbsActivity implements OnClickListener
{
	List<TabButton> mTabButtons;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mTabButtons = new ArrayList<TabButton>();
	}

	protected View addTab(int resId, String id, Intent intent)
	{
		if (resId != 0)
		{
			mTabButtons.add(new TabButton(resId, id));

			findViewById(resId).setOnClickListener(this);
		}

		return addTab(id, intent);
	}

	protected View addTab(int resId, String id, Intent intent, boolean useTabWrapper)
	{
		if (resId != 0)
		{
			mTabButtons.add(new TabButton(resId, id));

			findViewById(resId).setOnClickListener(this);
		}

		return addTab(id, intent, useTabWrapper);
	}

	@Override
	protected void removeTab(String id)
	{
		super.removeTab(id);

		final TabButton button = getButton(id);

		mTabButtons.remove(button);

		final View buttonView = findViewById(button.getResId());

		buttonView.setOnClickListener(null);
		buttonView.setSelected(false);
	}

	@Override
	protected void notifyTabChangeListener(String id)
	{
		super.notifyTabChangeListener(id);

		unselectAll();

		final TabButton button = getButton(id);

		if (button != null)
		{
			findViewById(button.getResId()).setSelected(true);
		}
	}

	@Override
	public void onClick(View v)
	{
		selectTab(getButton(v.getId()).getTabId());
	}

	private void unselectAll()
	{
		for (final TabButton button : mTabButtons)
		{
			findViewById(button.getResId()).setSelected(false);
		}
	}

	private TabButton getButton(String tabId)
	{
		for (final TabButton button : mTabButtons)
		{
			if (button.getTabId().equals(tabId))
			{
				return button;
			}
		}

		return null;
	}

	private TabButton getButton(int resId)
	{
		for (final TabButton button : mTabButtons)
		{
			if (button.getResId() == resId)
			{
				return button;
			}
		}

		return null;
	}

	private static class TabButton
	{
		private final int mResId;
		private final String mTabId;

		public TabButton(int resId, String tabId)
		{
			mResId = resId;
			mTabId = tabId;
		}

		public int getResId()
		{
			return mResId;
		}

		public String getTabId()
		{
			return mTabId;
		}
	}
}
