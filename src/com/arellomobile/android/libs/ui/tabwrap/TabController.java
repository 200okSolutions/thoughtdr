/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         13.07.2012
 */
public interface TabController extends ContentActivity, InternalChildActivity
{
	void setSelectedTab(int index);

	void setSelectedTab(String id);

	void setOnTabChangeListener(OnTabChangeListener listener);

	void setOnActivityChangeListener(OnActivityChangeListener listener);
}
