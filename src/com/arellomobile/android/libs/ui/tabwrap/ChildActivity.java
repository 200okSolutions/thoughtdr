/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         01.08.2012
 */
public interface ChildActivity
{
	String getActivityId();
}

interface InternalChildActivity extends ChildActivity
{
	TabController getNearestTabController();
}
