/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         10.07.2012
 */
interface OnActivityResultHandler
{
	void setOnActivityResultListener(OnActivityResultListener listener, int requestCode);
}
