/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author denis.mirochnik
 *         <p/>
 *         09.07.2012
 */
@Deprecated
public class TabHostActivity extends TabBaseActivity implements OnTabChangeListener
{
	private static final String SELECTED_TAB = "selectedTab";

	private List<String> mIds;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mIds = new ArrayList<String>();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);

		int selected = 0;

		if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_TAB))
		{
			selected = savedInstanceState.getInt(SELECTED_TAB, 0);
		}

		getTabHost().setCurrentTab(selected);
	}

	@Override
	protected void setup(Bundle savedInstanceState)
	{
		super.setup(savedInstanceState);

		setupHost(savedInstanceState);
	}

	@Override
	protected void setup(Bundle savedInstanceState, Class<? extends TabWrapper> wrapperClass)
	{
		super.setup(savedInstanceState, wrapperClass);

		setupHost(savedInstanceState);
	}

	private void setupHost(Bundle savedInstanceState)
	{
		getTabHost().setOnTabChangedListener(this);
		getTabHost().setup(getLocalActivityManager());
	}

	protected void addTab(String id, String indicator, Intent intent)
	{
		initTabSpec(getTabHost().newTabSpec(id).setIndicator(indicator), id, intent);
	}

	protected void addTab(String id, View indicator, Intent intent)
	{
		initTabSpec(getTabHost().newTabSpec(id).setIndicator(indicator), id, intent);
	}

	private void initTabSpec(TabHost.TabSpec spec, String id, Intent intent)
	{
		mIds.add(id);

		spec.setContent(getWrapperIntent(id, intent));

		getTabHost().addTab(spec);
	}

	protected TabHost getTabHost()
	{
		return (TabHost) findViewById(android.R.id.tabhost);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);

		if (isRememberSelected())
		{
			outState.putInt(SELECTED_TAB, getTabHost().getCurrentTab());
		}
	}

	@Override
	public void setSelectedTab(int index)
	{
		getTabHost().setCurrentTab(index);
	}

	@Override
	public void setSelectedTab(String id)
	{
		getTabHost().setCurrentTabByTag(id);
	}

	@Override
	public void onTabChanged(String tabId)
	{
		notifyTabChangeListener(tabId);
	}

	@Override
	public ChildActivity findChildActivity(String activityId)
	{
		ChildActivity childActivity;
		Activity activity;
		Activity baseActivity;

		for (final String tabId : mIds)
		{
			activity = getLocalActivityManager().getActivity(tabId);

			if (activity instanceof ChildActivity)
			{
				final String id = ((ChildActivity) activity).getActivityId();

				if (id != null && id.equals(activityId))
				{
					return (ChildActivity) activity;
				}
			}
		}

		for (final String tabId : mIds)
		{
			baseActivity = getLocalActivityManager().getActivity(tabId);

			if (baseActivity instanceof BaseActivityGroup)
			{
				childActivity = ((BaseActivityGroup) baseActivity).findChildActivity(activityId);

				if (childActivity != null)
				{
					return childActivity;
				}
			}
		}

		return null;
	}
}
