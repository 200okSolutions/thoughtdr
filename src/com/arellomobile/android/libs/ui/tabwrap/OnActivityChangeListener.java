/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         02.08.2012
 */
public interface OnActivityChangeListener
{
	void onActivityStarted(String id); //value got from ChildActivity.getActivityId();

	void onActivityDestroyed(String id); //value got from ChildActivity.getActivityId();
}
