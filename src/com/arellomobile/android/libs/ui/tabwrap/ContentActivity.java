/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

/**
 * @author denis.mirochnik
 *         <p/>
 *         03.08.2012
 */
public interface ContentActivity
{
	ChildActivity getChildActivity(String activityId);
}
