/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ViewAnimator;

/**
 * @author denis.mirochnik
 *         <p/>
 *         13.07.2012
 */
public abstract class TabAbsActivity extends TabBaseActivity
{
	private static final String CURRENT_CHILD = "currentChild";

	private List<Content> mContent;

	private Content mSelected;

	private ViewAnimator mAnimator;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mContent = new ArrayList<Content>();
		mAnimator = new ViewAnimator(this);

		mAnimator.setAnimateFirstView(false);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);

		if (mContent.size() == 0)
		{
			return;
		}

		String currId = mContent.get(0).getId();

		if (savedInstanceState != null && savedInstanceState.containsKey(CURRENT_CHILD))
		{
			currId = savedInstanceState.getString(CURRENT_CHILD);
		}

		if (mSelected == null)
		{
			selectTab(currId);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public ChildActivity findChildActivity(String activityId)
	{
		ChildActivity childActivity;
		Activity activity;
		Activity baseActivity;

		for (final Content content : mContent)
		{
			activity = getLocalActivityManager().getActivity(content.getId());

			if (activity instanceof ChildActivity)
			{
				final String id = ((ChildActivity) activity).getActivityId();

				if (id != null && id.equals(activityId))
				{
					return (ChildActivity) activity;
				}
			}
		}

		for (final Content content : mContent)
		{
			baseActivity = getLocalActivityManager().getActivity(content.getId());

			if (baseActivity instanceof BaseActivityGroup)
			{
				childActivity = ((BaseActivityGroup) baseActivity).findChildActivity(activityId);

				if (childActivity != null)
				{
					return childActivity;
				}
			}
		}

		return null;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);

		if (isRememberSelected() && mSelected != null)
		{
			outState.putString(CURRENT_CHILD, mSelected.getId());
		}
	}

	@Override
	protected void setup(Bundle savedInstanceState)
	{
		super.setup(savedInstanceState);

		setupContentView();
	}

	@Override
	protected void setup(Bundle savedInstanceState, Class<? extends TabWrapper> wrapper)
	{
		super.setup(savedInstanceState, wrapper);

		setupContentView();
	}

	private void setupContentView()
	{
		getContentView().addView(mAnimator);
	}

	protected View addTab(String id, Intent intent)
	{
		final Content newTab = new Content(id, getWrapperIntent(id, intent));

		mContent.add(newTab);

		return startTab(newTab);
	}

	protected View addTab(String id, Intent intent, boolean useTabWrapper)
	{
		if (useTabWrapper)
		{
			return addTab(id, intent);
		}

		final Content newTab = new Content(id, intent);

		mContent.add(newTab);

		return startTab(newTab);
	}

	@SuppressWarnings("deprecation")
	protected void removeTab(String id)
	{
		if (mSelected.getId().equals(id))
		{
			mSelected = null;
		}

		getLocalActivityManager().destroyActivity(id, true);

		mAnimator.removeViewAt(mContent.indexOf(getContent(id)));

		removeContent(id);
	}

	private void removeContent(String id)
	{
		mContent.remove(getContent(id));
	}

	@SuppressWarnings("deprecation")
	private View startTab(Content tab)
	{
		final FrameLayout contentView = (FrameLayout) getLocalActivityManager().startActivity(tab.getId(), tab.getIntent()).getDecorView().findViewById(android.R.id.content);

		View content = contentView;

		if (contentView.getChildCount() > 0)
		{
			content = contentView.getChildAt(0);
			contentView.removeAllViews();
		}

		mAnimator.addView(content);

		return content;
	}

	public String getCurrentTabId()
	{
		if (mSelected != null)
		{
			return mSelected.getId();
		}

		return null;
	}

	protected boolean hasTab(String id)
	{
		return getContent(id) != null;
	}

	protected View selectTab(String id)
	{
		int oldInd = -1;

		if (mSelected != null)
		{
			oldInd = mContent.indexOf(mSelected);

			if (mSelected.getId().equals(id))
			{
				return mAnimator.getCurrentView();
			}
		}

		final Content content = getContent(id);

		return openTab(content, oldInd, mContent.indexOf(content));
	}

	protected View selectTab(int index)
	{
		int oldInd = -1;

		if (mSelected != null)
		{
			oldInd = mContent.indexOf(mSelected);

			if (mContent.indexOf(mSelected) == index)
			{
				return mAnimator.getCurrentView();
			}
		}

		return openTab(getContent(index), oldInd, index);
	}

	@SuppressWarnings("deprecation")
	private View openTab(Content content, int oldInd, int newInd)
	{
		getLocalActivityManager().startActivity(content.getId(), content.getIntent());

		mSelected = content;

		moveToTab(oldInd, newInd);

		notifyTabChangeListener(content.getId());

		return mAnimator.getCurrentView();
	}

	private void moveToTab(int oldInd, int newInd)
	{
		mAnimator.setInAnimation(null);
		mAnimator.setOutAnimation(null);

		if (oldInd != -1)
		{
			mAnimator.setInAnimation(getInAnimation(oldInd > newInd));
			mAnimator.setOutAnimation(getOutAnimation(oldInd > newInd));
		}

		mAnimator.setDisplayedChild(newInd);
	}

	private Content getContent(String id)
	{
		for (final Content content : mContent)
		{
			if (content.getId().equals(id))
			{
				return content;
			}
		}

		return null;
	}

	private Content getContent(int index)
	{
		return mContent.get(index);
	}

	@Override
	public void setSelectedTab(int index)
	{
		selectTab(index);
	}

	@Override
	public void setSelectedTab(String id)
	{
		selectTab(id);
	}

	private static final int IN_ANIM = 0;
	private static final int OUT_ANIM = 1;

	private static final int FORWARD_ANIM = 0;
	private static final int BACK_ANIM = 1;

	private static Animation mAnimtions[][];

	static
	{
		mAnimtions = new Animation[2][2];

		AlphaAnimation alphaAnim;

		alphaAnim = new AlphaAnimation(0.0f, 1.0f);
		alphaAnim.setDuration(200);

		mAnimtions[IN_ANIM][FORWARD_ANIM] = alphaAnim;

		alphaAnim = new AlphaAnimation(0.0f, 1.0f);
		alphaAnim.setDuration(200);

		mAnimtions[IN_ANIM][BACK_ANIM] = alphaAnim;

		alphaAnim = new AlphaAnimation(1.0f, 0.0f);
		alphaAnim.setDuration(200);

		mAnimtions[OUT_ANIM][FORWARD_ANIM] = alphaAnim;

		alphaAnim = new AlphaAnimation(1.0f, 0.0f);
		alphaAnim.setDuration(200);

		mAnimtions[OUT_ANIM][BACK_ANIM] = alphaAnim;
	}

	protected Animation getInAnimation(boolean backward)
	{
		if (backward)
		{
			return mAnimtions[IN_ANIM][BACK_ANIM];
		}
		else
		{
			return mAnimtions[IN_ANIM][FORWARD_ANIM];
		}
	}

	protected Animation getOutAnimation(boolean backward)
	{
		if (backward)
		{
			return mAnimtions[OUT_ANIM][BACK_ANIM];
		}
		else
		{
			return mAnimtions[OUT_ANIM][FORWARD_ANIM];
		}
	}

	protected abstract FrameLayout getContentView();
}
