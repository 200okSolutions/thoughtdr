/**
 *
 */
package com.arellomobile.android.libs.ui.tabwrap;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

/**
 * @author denis.mirochnik
 *         <p/>
 *         17.07.2012
 */
@SuppressWarnings("deprecation")
public abstract class BaseActivityGroup extends ActivityGroup
{
	public static final String ACTIVITY_ID = "id";

	private String mId;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mId = getIntent().getStringExtra(ACTIVITY_ID);
	}

	String getId()
	{
		return mId;
	}

	protected void hideKeyBoard()
	{
		final InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		final Window window = getRootActivity().getWindow();

		mgr.hideSoftInputFromWindow(window.getDecorView().getApplicationWindowToken(), 0);
	}

	public Activity getRootActivity()
	{
		Activity parent = this;

		while (parent.getParent() != null)
		{
			parent = parent.getParent();
		}

		return parent;
	}

	abstract ChildActivity findChildActivity(String activityId);
}
