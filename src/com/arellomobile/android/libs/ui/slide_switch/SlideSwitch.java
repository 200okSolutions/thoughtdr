package com.arellomobile.android.libs.ui.slide_switch;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Date: 22.06.12
 * Time: 12:53
 *
 * @author Sergey Grigoriev
 */
public class SlideSwitch extends FrameLayout implements Checkable, View.OnTouchListener
{
	public interface OnCheckedChangeListener
	{
		void onCheckedChanged(SlideSwitch view, boolean checked);
	}

	private Context mContext;

	private final int mSlideSwitchId = 1;
	private final int mViewOnId = 2;
	private final int mViewOffId = 3;
	private final int mButtonId = 4;

	private Thread mThread;
	private Handler mHandler = new Handler();
	private float mAdd = 0.0f;
	private boolean mChecked;
	private TextView mViewBar;
	private View mViewBackgroundOff;
	private View mViewBackgroundOn;
	private FrameLayout mSlideSwitch;
	private int mOrientation = HORIZONTAL;
	String mText;
	int mTextColor;
	float mTextSize;
	int mTextStyle;

	private OnCheckedChangeListener mListener;

	private int mButtonBgOn;
	private int mButtonBgOff;
	private int mBgOn;
	private int mBgOff;

	private static final float ACCELE = 0.05f;
	private static final long INTARVAL = 50L;
	private static final int HORIZONTAL = 0;
	private static final int VERTICAL = 1;

	private int mLeft;
	private int mTop;
	private float mXOrg;
	private float mYOrg;
	private float mXPrev;
	private float mYPrev;
	private long mTPrev;
	private boolean mMoved;

	public SlideSwitch(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		mContext = context;

		/*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SlideSwitch);

				mChecked = a.getBoolean(R.styleable.SlideSwitch_checked, false);
				mOrientation = a.getInt(R.styleable.SlideSwitch_orientation, HORIZONTAL);
				mText = a.getString(R.styleable.SlideSwitch_text);
				mTextColor = a.getColor(R.styleable.SlideSwitch_text_color, 0xFF000000);
				mTextSize = a.getDimension(R.styleable.SlideSwitch_text_size, 0);
				mTextStyle = a.getInt(R.styleable.SlideSwitch_text_style, Typeface.NORMAL);
				mBgOn = a.getResourceId(R.styleable.SlideSwitch_bg_on, android.R.drawable.button_onoff_indicator_on);
				mBgOff = a.getResourceId(R.styleable.SlideSwitch_bg_off, android.R.drawable.button_onoff_indicator_off);
				mButtonBgOn = a.getResourceId(R.styleable.SlideSwitch_button_bg_on, android.R.drawable.title_bar_tall);
				mButtonBgOff = a.getResourceId(R.styleable.SlideSwitch_button_bg_off, android.R.drawable.title_bar_tall);*/

		switch (mOrientation)
		{
			case HORIZONTAL:
				addView(createHorizontalView());
				break;
			case VERTICAL:
				addView(createVerticalView());
				break;
			default:
				throw new AssertionError();
		}

		mViewBackgroundOff = findViewById(mViewOffId);
		mViewBackgroundOff.setBackgroundResource(mBgOff);

		mViewBackgroundOn = findViewById(mViewOnId);
		mViewBackgroundOn.setBackgroundResource(mBgOn);

		int gravity;

		switch (mOrientation)
		{
			case HORIZONTAL:
				if (mChecked)
				{
					gravity = Gravity.RIGHT;
				}
				else
				{
					gravity = Gravity.LEFT;
				}
				break;
			case VERTICAL:
				if (mChecked)
				{
					gravity = Gravity.TOP;
				}
				else
				{
					gravity = Gravity.BOTTOM;
				}
				break;
			default:
				throw new AssertionError();
		}

		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mViewBar.getLayoutParams();
		params.gravity = gravity;
		mViewBar.setLayoutParams(params);
		if (mText != null)
		{
			mViewBar.setText(mText);
		}
		else
		{
			mViewBar.setText("Switch");
		}
		mViewBar.setTextColor(mTextColor);
		mViewBar.setTextSize(mTextSize);
		mViewBar.setTypeface(null, mTextStyle);
		mViewBar.setOnTouchListener(this);
		mViewBar.setBackgroundResource(mButtonBgOff);
		findViewById(mSlideSwitchId).setOnTouchListener(this);
	}

	public boolean isChecked()
	{
		return mChecked;
	}

	public void toggle()
	{
		boolean checkedNew = !mChecked;
		setChecked(checkedNew);
	}

	public void setOnCheckedChangeListener(OnCheckedChangeListener listener)
	{
		mListener = listener;
	}

	public void setText(int resId)
	{
		setText(mContext.getString(resId));
	}

	public void setText(String text)
	{
		mViewBar.setText(text);
	}

	public void setTextColor(int color)
	{
		mViewBar.setTextColor(mContext.getResources().getColor(color));
	}

	public void setTextSize(float size)
	{
		mViewBar.setTextSize(size);
	}

	public void setTextSize(int unit, float size)
	{
		mViewBar.setTextSize(unit, size);
	}

	public void setTypeface(Typeface tf)
	{
		mViewBar.setTypeface(tf);
	}

	public void setTypeface(Typeface tf, int style)
	{
		mViewBar.setTypeface(tf, style);
	}

	private void changeSwitch()
	{
		toggle();
		if (mListener != null)
		{
			mListener.onCheckedChanged(SlideSwitch.this, mChecked);
		}
	}

	public void setButtonBgOn(int resId)
	{
		mButtonBgOn = resId;
	}

	public void setButtonBgOff(int resId)
	{
		mButtonBgOff = resId;
		mViewBar.setBackgroundResource(mButtonBgOff);
	}

	public void setBgOn(int resId)
	{
		mBgOn = resId;
		mViewBackgroundOn.setBackgroundResource(mBgOn);
	}

	public void setBgOff(int resId)
	{
		mBgOff = resId;
		mViewBackgroundOff.setBackgroundResource(mBgOff);
	}

	private View createVerticalView()
	{
		mSlideSwitch = new FrameLayout(mContext);
		mSlideSwitch.setLayoutParams(
				new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		mSlideSwitch.setId(mSlideSwitchId);

		mViewBackgroundOn = new View(mContext);
		mViewBackgroundOn.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		mViewBackgroundOn.setId(mViewOnId);

		mViewBackgroundOff = new View(mContext);
		mViewBackgroundOff.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		mViewBackgroundOff.setId(mViewOffId);

		mViewBar = new TextView(mContext);
		mViewBar.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		mViewBar.setGravity(Gravity.CENTER);
		mViewBar.setId(mButtonId);

		mSlideSwitch.addView(mViewBackgroundOff);
		mSlideSwitch.addView(mViewBackgroundOn);
		mSlideSwitch.addView(mViewBar);

		return mSlideSwitch;
	}

	private View createHorizontalView()
	{
		mSlideSwitch = new FrameLayout(mContext);
		mSlideSwitch.setLayoutParams(
				new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		mSlideSwitch.setId(mSlideSwitchId);

		mViewBackgroundOn = new View(mContext);
		mViewBackgroundOn.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		mViewBackgroundOn.setId(mViewOnId);

		mViewBackgroundOff = new View(mContext);
		mViewBackgroundOff.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		mViewBackgroundOff.setId(mViewOffId);

		mViewBar = new TextView(mContext);
		mViewBar.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
		mViewBar.setGravity(Gravity.CENTER);
		mViewBar.setId(mButtonId);

		mSlideSwitch.addView(mViewBackgroundOff);
		mSlideSwitch.addView(mViewBackgroundOn);
		mSlideSwitch.addView(mViewBar);

		return mSlideSwitch;
	}

	public boolean onTouch(View v, MotionEvent me)
	{
		boolean bResult = false;

		float x = me.getRawX();
		float y = me.getRawY();
		long t = System.currentTimeMillis();

		int action = me.getAction();

		switch (action)
		{
			case MotionEvent.ACTION_DOWN:
			{
				stopTimer();
				mXOrg = x;
				mYOrg = y;
				mLeft = v.getLeft();
				mTop = v.getTop();
				bResult = true;
				mViewBar.setBackgroundResource(mButtonBgOn);
				mMoved = false;
			}
			break;
			case MotionEvent.ACTION_UP:
				startTimer();
				mViewBar.setBackgroundResource(mButtonBgOff);
				if (!mMoved)
				{
					changeSwitch();
				}
				break;
			case MotionEvent.ACTION_MOVE:
			{
				if (v.equals(mViewBar))
				{
					stopTimer();
					switch (mOrientation)
					{
						case HORIZONTAL:
							mAdd = (x - mXPrev) / (t - mTPrev);
							float x_move = x - mXOrg;
							moveBarX(mLeft + (int) x_move);
							break;
						case VERTICAL:
							mAdd = (y - mYPrev) / (t - mTPrev);
							float y_move = y - mYOrg;
							moveBarY(mTop + (int) y_move);
							break;
					}
				}
				if (x - mXOrg != 0 || y - mYOrg != 0)
				{
					mMoved = true;
				}
			}
			break;
			default:
				// do nothing.
		}

		mTPrev = t;
		mXPrev = x;
		mYPrev = y;

		return bResult;
	}

	private void moveBarX(int left)
	{
		int width = mViewBar.getWidth();
		int widthParent = getWidth();
		int height = mViewBar.getHeight();
		int leftMax = widthParent - width;

		int leftNew = left;
		if (leftNew <= 0)
		{
			stopTimer();
			leftNew = 0;
			updateState(false);
		}
		else if (leftMax <= leftNew)
		{
			stopTimer();
			leftNew = leftMax;
			updateState(true);
		}

		int top = mViewBar.getTop();
		int rightNew = leftNew + width;
		int bottom = top + height;
		mViewBar.layout(leftNew, top, rightNew, bottom);

		int centerNew = leftNew + (width / 2);
		mViewBackgroundOn.layout(0, top, centerNew, bottom);
		mViewBackgroundOff.layout(centerNew, top, widthParent, bottom);
	}

	private void moveBarY(int top)
	{
		int height = mViewBar.getHeight();
		int heightParent = getHeight();
		int width = mViewBar.getWidth();
		int topMax = heightParent - height;

		int topNew = top;
		if (topNew <= 0)
		{
			stopTimer();
			topNew = 0;
			updateState(true);
		}
		else if (topMax <= topNew)
		{
			stopTimer();
			topNew = topMax;
			updateState(false);
		}

		int left = mViewBar.getLeft();
		int bottomNew = topNew + height;
		int right = left + width;
		mViewBar.layout(left, topNew, right, bottomNew);

		int centerNew = topNew + (height / 2);
		mViewBackgroundOn.layout(left, centerNew, right, heightParent);
		mViewBackgroundOff.layout(left, 0, right, centerNew);

	}

	private void updateState(boolean checked)
	{
		if (mChecked == checked)
		{
			return;
		}

		mChecked = checked;

		if (mListener != null)
		{
			mListener.onCheckedChanged(SlideSwitch.this, mChecked);
		}
	}

	public void setChecked(boolean checked)
	{
		mChecked = checked;

		switch (mOrientation)
		{
			case HORIZONTAL:
				if (checked)
				{
					int widthParent = getWidth();
					moveBarX(widthParent);
				}
				else
				{
					moveBarX(0);
				}
				break;
			case VERTICAL:
				if (checked)
				{
					moveBarY(0);
				}
				else
				{
					int heightParent = getHeight();
					moveBarY(heightParent);
				}
				break;
			default:
				throw new AssertionError();
		}
	}

	private void stopTimer()
	{
		if (mThread == null)
		{
			return;
		}

		mThread.interrupt();
		mThread = null;
	}

	private void startTimer()
	{
		stopTimer();

		mThread = new Thread(new Runnable()
		{
			public void run()
			{
				while (true)
				{
					try
					{
						Thread.sleep(INTARVAL);
					}
					catch (InterruptedException e)
					{
						return;
					}

					mHandler.post(new Runnable()
					{
						public void run()
						{
							onTimer();
						}
					});
				}
			}
		});

		mThread.start();
	}

	private void onTimer()
	{
		switch (mOrientation)
		{
			case HORIZONTAL:
			{
				int left = mViewBar.getLeft();
				int width = mViewBar.getWidth();
				int widthParent = SlideSwitch.this.getWidth();
				int centerParent = widthParent / 2;
				int center = left + (width / 2);

				if (center < centerParent)
				{
					mAdd -= ACCELE;
				}
				else
				{
					mAdd += ACCELE;
				}

				int move = (int) (mAdd * INTARVAL);
				int leftNew = left + move;
				moveBarX(leftNew);
			}
			break;
			case VERTICAL:
			{
				int top = mViewBar.getTop();
				int height = mViewBar.getHeight();
				int heightParent = SlideSwitch.this.getHeight();
				int centerParent = heightParent / 2;
				int center = top + (height / 2);

				if (center < centerParent)
				{
					mAdd -= ACCELE;
				}
				else
				{
					mAdd += ACCELE;
				}

				int move = (int) (mAdd * INTARVAL);
				int topNew = top + move;
				moveBarY(topNew);
			}
			break;
			default:
				throw new AssertionError();
		}
	}
}