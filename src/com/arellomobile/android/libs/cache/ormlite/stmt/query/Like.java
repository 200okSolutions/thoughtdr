package com.arellomobile.android.libs.cache.ormlite.stmt.query;

import com.arellomobile.android.libs.cache.ormlite.field.FieldType;
import com.arellomobile.android.libs.cache.ormlite.stmt.Where;

import java.sql.SQLException;

/**
 * Internal class handling the SQL 'like' comparison query part. Used by {@link Where#like}.
 *
 * @author graywatson
 */
public class Like extends BaseComparison
{

	public Like(String columnName, FieldType fieldType, Object value) throws SQLException
	{
		super(columnName, fieldType, value);
	}

	@Override
	public StringBuilder appendOperation(StringBuilder sb)
	{
		sb.append("LIKE ");
		return sb;
	}
}
