package com.arellomobile.android.libs.cache.ormlite.stmt.query;

import com.arellomobile.android.libs.cache.ormlite.field.FieldType;
import com.arellomobile.android.libs.cache.ormlite.stmt.StatementBuilder;

import java.sql.SQLException;

/**
 * Internal class handling the SQL SET part used by UPDATE statements. Used by
 * {@link StatementBuilder#updateColumnValue(String, Object)}.
 * <p/>
 * <p>
 * It's not a comparison per se but does have a columnName = value form so it works.
 * </p>
 *
 * @author graywatson
 */
public class SetValue extends BaseComparison
{

	public SetValue(String columnName, FieldType fieldType, Object value) throws SQLException
	{
		super(columnName, fieldType, value);
	}

	@Override
	public StringBuilder appendOperation(StringBuilder sb)
	{
		sb.append("= ");
		return sb;
	}
}
