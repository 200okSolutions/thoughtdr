package com.arello.tdrtest;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.TDRDataManager;
import com.arello.tdrtest.purchase.DownloadManager;
import com.arello.tdrtest.service.PlayService;

public class TDRApplication extends Application {

	TDRDataManager mDataManager;
	Settings mSettings;
	DownloadManager mDownloadManager;

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			PlayService.PlayServiceBinder binder = (PlayService.PlayServiceBinder) service;
			mService = binder.getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mService = null;
		}
	};

	PlayService mService;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public TDRDataManager getDataManager() {
		if (mDataManager == null)
			mDataManager = new TDRDataManager(this);
		return mDataManager;
	}

	public Settings getSettings() {
		if (mSettings == null)
			mSettings = new Settings(this);
		return mSettings;
	}

	public void startService(boolean withMusic, boolean dayMode) {
		if (mService == null) {
			Intent intent = new Intent(this, PlayService.class);
			intent.putExtra(Constants.START_WITH_MUSIC, withMusic);
			intent.putExtra(Constants.DAY_MODE, dayMode);
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
	}

	public void stopService() {
		if (mService != null) {
			mService.setOnCurrentAffirmationChangedListener(null);
			mService.setOnCurrentMusicChangedListenr(null);
			// mService.setServiceStopListener(null);
			unbindService(mConnection);
			mService = null;
		}
	}

	public PlayService getPlayService() {
		return mService;
	}

	public DownloadManager getDownloadManager() {
		if (mDownloadManager == null)
			mDownloadManager = new DownloadManager();
		return mDownloadManager;
	}
}
