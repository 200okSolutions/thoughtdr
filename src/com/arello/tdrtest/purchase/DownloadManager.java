package com.arello.tdrtest.purchase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTask;

import android.content.Context;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;

public class DownloadManager {
	public static interface DownloadManagerTaskListener {
		void onDownloadComplete(String originalUri, String destinationFile,
				DownloadManagerTask task);

		void onDownloadStarted(String originalUri, DownloadManagerTask task);

		void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task);
	}

	public static abstract class DownloadManagerTask extends
			AsyncTask<Void, Void, Void> {
		String mUrl;
		String mEndPath;
		String mErrorMessage;
		private Object tag;
		private WeakReference<DownloadManagerTaskListener> listener;

		public DownloadManagerTask(String url) {
			mUrl = url;
		}

		public String getUrl() {
			return mUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (listener.get() != null)
				listener.get().onDownloadStarted(mUrl, this);
		}

		@Override
		protected Void doInBackground(Void... params) {
			InputStream is = null;
			FTPClient client = new FTPClient();
			try {
				if (isCancelled())
					return null;
				client.connect(Constants.PURCHASE_FTP_HOST, 21);
				client.login(Constants.PURCHASE_LOGIN,
						Constants.PURCHASE_PASSWORD);
				client.enterLocalPassiveMode();

				if (FTPReply.isPositiveCompletion(client.getReplyCode())) {
					is = client.retrieveFileStream(mUrl);
					mEndPath = getFilePath();
					File outputFile = new File(mEndPath);
					FileOutputStream fos = new FileOutputStream(outputFile);
					byte[] buffer = new byte[1024];
					int len1 = 0;
					while ((len1 = is.read(buffer)) > 0) {
						if (isCancelled())
							return null;
						fos.write(buffer, 0, len1);
					}
					fos.close();
					client.logout();
				}
			} catch (Exception e) {
				e.printStackTrace();
				mEndPath = null;
				mErrorMessage = e.getLocalizedMessage();
			}
			try {
				client.disconnect();
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listener != null) {
				if (mErrorMessage == null) {
					listener.get().onDownloadComplete(mUrl, mEndPath, this);
				} else {
					listener.get().onDownloadFailed(mUrl, mErrorMessage, this);
				}
			}
			DownloadManager.taskFinished(this);
		}

		protected abstract String getFilePath();

		public void setListener(DownloadManagerTaskListener listener) {
			this.listener = new WeakReference<DownloadManager.DownloadManagerTaskListener>(
					listener);
		}

		public Object getTag() {
			return tag;
		}

		public void setTag(Object tag) {
			this.tag = tag;
		}

	}

	static Queue<DownloadManagerTask> mTasksQueue = new LinkedList<DownloadManager.DownloadManagerTask>();
	static DownloadManagerTask mCurrentTask;

	public static void addNewLoadTask(DownloadManagerTask task) {

		if (mCurrentTask == null || mCurrentTask.getStatus() == Status.FINISHED) {
			mCurrentTask = task;
			task.execute();
		} else {
			mTasksQueue.add(task);
		}
	}

	protected static void taskFinished(DownloadManagerTask task) {
		mTasksQueue.remove(task);
		DownloadManagerTask newTask = null;
		while ((newTask == null || newTask.getStatus() == Status.FINISHED)
				&& mTasksQueue.size() > 0)
			newTask = mTasksQueue.poll();
		mCurrentTask = newTask;
		if (newTask != null) {
			newTask.execute();
		}
	}

	public static int getQueueSize() {
		return mTasksQueue.size();
	}

}
