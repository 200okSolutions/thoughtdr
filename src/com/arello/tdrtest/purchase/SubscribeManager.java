package com.arello.tdrtest.purchase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Date: 16.05.12 Time: 17:14
 * 
 * @author Yuri Shmakov
 */
public class SubscribeManager extends AsyncTask<String, Integer, Integer> {
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private volatile boolean mIsCancel = false;
	private EnterDialog mEmailEnterDialog;
	private OnSubsribeListener mOnSubsribeListener;

	public SubscribeManager(Context context, EnterDialog emailEnterDialog,
			OnSubsribeListener onSubsribeListener) {
		mContext = context;
		mEmailEnterDialog = emailEnterDialog;
		mOnSubsribeListener = onSubsribeListener;
	}

	protected void onPreExecute() {
		mProgressDialog = new ProgressDialog(mContext);

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage(mContext
				.getString(R.string.send_email_progress));
		mProgressDialog.show();
	}

	@Override
	protected Integer doInBackground(String... emails) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(Constants.SUBSCRIBE_URL + emails[0]);

		int result = HttpURLConnection.HTTP_INTERNAL_ERROR;

		try {
			HttpResponse response = httpClient.execute(httpGet);

			result = response.getStatusLine().getStatusCode();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(Integer result) {
		if (mIsCancel) {
			cancel(true);
			return;
		}

		if (result == 200) {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(
					"main", Context.MODE_PRIVATE).edit();
			editor.putInt("starts", -1);
			editor.commit();

			mEmailEnterDialog.setOnDismissListener(null);
			mEmailEnterDialog.dismiss();

			if (mOnSubsribeListener != null) {
				mOnSubsribeListener.onSubscribe();
			}
		} else {
			new AlertDialog.Builder(mContext)
					.setMessage(mContext.getString(R.string.email_send_wrong))
					.setCancelable(true)
					.setNeutralButton(mContext.getString(R.string.close_alert),
							null).create().show();
		}

		mProgressDialog.dismiss();
	}

	public void cancel() {
		mIsCancel = true;
	}

	public interface OnSubsribeListener {
		void onSubscribe();
	}
}
