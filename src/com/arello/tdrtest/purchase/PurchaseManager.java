package com.arello.tdrtest.purchase;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.FileItem;
import com.arello.tdrtest.data.purchase.PurchaseRecord;
import com.arello.tdrtest.uihelpers.Utils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import xmlwise.Plist;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class PurchaseManager {
	public static final String AFFIRMATIONS_KEY = "Affirmations";
	public static final String SLEEP_SOUNDS_KEY = "SleepSounds";

	private PurchaseManagerDelegate delegate;
	private final Logger log = Logger.getLogger(getClass().getName());
	private List<PurchaseRecord> mAffirmationsList = new ArrayList<PurchaseRecord>();
	private List<PurchaseRecord> mSleepSoundsList = new ArrayList<PurchaseRecord>();

	private Context mContext;
	private boolean mGetFree;
	LoadPurchassesTask mLoadPurchassesTask;
	Fragment mParent;

	public PurchaseManager(Context context) {
		this(context, false);
	}

	public PurchaseManager(Context context, Fragment parent) {
		this(context, false);
		mParent = parent;
	}

	public PurchaseManager(Context context, boolean getFree) {
		mContext = context;
		mGetFree = getFree;
	}

	public void receivePurchaseList() {
		if (Utils.isNetworkAvailable(mContext)) {
			new LoadPurchassesTask().execute();
			mLoadPurchassesTask = new LoadPurchassesTask();

		} else {
			if (delegate != null) {
				delegate.onProductsListLoadingFailed(mContext.getResources()
						.getString(R.string.no_internet_connection_message));
			}
		}
	}

	public boolean cancelPurchaseListReceiving() {
		if (mLoadPurchassesTask != null) {
			// mLoadPurchassesTask.cancel(true);
			mLoadPurchassesTask.cancelLoading();
			delegate.onProductsListLoadingCanceled();
			mLoadPurchassesTask = null;
			return true;
		}
		return false;
	}

	protected String innerReceivePurchaseList() {
		InputStream is = null;
		FTPClient client = new FTPClient();
		try {
			client.connect(Constants.PURCHASE_FTP_HOST, 21);
			client.login(Constants.PURCHASE_LOGIN, Constants.PURCHASE_PASSWORD);
			client.enterLocalPassiveMode();

			if (FTPReply.isPositiveCompletion(client.getReplyCode())) {
				// is =
				// mContext.getResources().getAssets().open("products_android.plist");//
				is = client
						.retrieveFileStream(mGetFree ? Constants.FREE_PURCHASE_LIST_URL
								: Constants.PURCHASE_LIST_URL);

				Writer writer = new StringWriter();
				char[] buffer = new char[1024];
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
				String xml = writer.toString();

				if (mGetFree) {
					Map<String, Object> rootNode = Plist.fromXml(xml);

					getAffirmationsList().clear();
					getSleepSoundsList().clear();

					if (rootNode.containsKey(AFFIRMATIONS_KEY)) {
						PurchaseRecord freeAffirmations = new PurchaseRecord();
						freeAffirmations.setProductID("freeAffirmations");
						freeAffirmations.setPurchased(true);

						List<Map<String, Object>> affList = (List<Map<String, Object>>) rootNode
								.get(AFFIRMATIONS_KEY);
						List<FileItem> freeAffList = new ArrayList<FileItem>();
						for (Map<String, Object> affirmation : affList) {
							if (affirmation.containsKey("files")) {
								List<Map<String, Object>> files = (List<Map<String, Object>>) affirmation
										.get("files");

								for (Map<String, Object> file : files) {
									freeAffList.add(FileItem
											.createItemFromMap(file));
								}
							}
						}
						freeAffirmations.setItems(freeAffList);
						getAffirmationsList().add(freeAffirmations);
					}
					if (rootNode.containsKey(SLEEP_SOUNDS_KEY)) {
						PurchaseRecord freeSleepSounds = new PurchaseRecord();
						freeSleepSounds.setProductID("freeAffirmations");
						freeSleepSounds.setPurchased(true);

						List<Map<String, Object>> sleepList = (List<Map<String, Object>>) rootNode
								.get(SLEEP_SOUNDS_KEY);
						List<FileItem> freeSleepList = new ArrayList<FileItem>();
						for (Map<String, Object> sleepSound : sleepList) {
							if (sleepSound.containsKey("files")) {
								List<Map<String, Object>> files = (List<Map<String, Object>>) sleepSound
										.get("files");

								for (Map<String, Object> file : files) {
									freeSleepList.add(FileItem
											.createItemFromMap(file));
								}
							}
						}
						freeSleepSounds.setItems(freeSleepList);
						getSleepSoundsList().add(freeSleepSounds);
					}
				} else {
					Map<String, Object> rootNode = Plist.fromXml(xml);

					SharedPreferences purchased = mContext
							.getSharedPreferences("purchased_list",
									Context.MODE_PRIVATE);

					getAffirmationsList().clear();
					getSleepSoundsList().clear();

					if (rootNode.containsKey(AFFIRMATIONS_KEY)) {
						for (Map<String, Object> affirmation : (List<Map<String, Object>>) rootNode
								.get(AFFIRMATIONS_KEY)) {
							PurchaseRecord record = PurchaseRecord
									.createPurchaseRecordFromMap(affirmation);
							record.setPurchased(purchased.getBoolean(
									record.getProductID(), false));

							getAffirmationsList().add(record);
						}
					}
					if (rootNode.containsKey(SLEEP_SOUNDS_KEY)) {
						for (Map<String, Object> sleepSound : (List<Map<String, Object>>) rootNode
								.get(SLEEP_SOUNDS_KEY)) {
							PurchaseRecord record = PurchaseRecord
									.createPurchaseRecordFromMap(sleepSound);
							record.setPurchased(purchased.getBoolean(
									record.getProductID(), false));

							getSleepSoundsList().add(record);
						}
					}
				}
				client.logout();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return e.getLocalizedMessage();
		}

		try {
			client.disconnect();
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public PurchaseManagerDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(PurchaseManagerDelegate delegate) {
		this.delegate = delegate;
	}

	public List<PurchaseRecord> getAffirmationsList() {
		return mAffirmationsList;
	}

	public void setAffirmationsList(List<PurchaseRecord> mAffirmationsList) {
		this.mAffirmationsList = mAffirmationsList;
	}

	public List<PurchaseRecord> getSleepSoundsList() {
		return mSleepSoundsList;
	}

	public void setSleepSoundsList(List<PurchaseRecord> mSleepSoundsList) {
		this.mSleepSoundsList = mSleepSoundsList;
	}

	protected class LoadPurchassesTask extends AsyncTask<Void, Void, Void> {
		boolean isSuccess = true;
		String errorMessage;
		boolean mIsCanceled = false;

		public void cancelLoading() {
			mIsCanceled = true;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if (delegate != null) {
				delegate.onProductsListLoadingStarted();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			errorMessage = innerReceivePurchaseList();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (isCancelled() || mIsCanceled
					|| (mParent != null && !mParent.isVisible())) {
				return;
			}
			if (delegate != null) {
				if (errorMessage == null) {
					delegate.onProductsListReceived();
				} else {
					delegate.onProductsListLoadingFailed(errorMessage);
				}
			}

		}
	}

	public interface PurchaseManagerDelegate {
		void onProductsListReceived();

		void onProductsListLoadingStarted();

		void onProductsListLoadingCanceled();

		void onProductsListLoadingFailed(String errorMessage);
	}
}
