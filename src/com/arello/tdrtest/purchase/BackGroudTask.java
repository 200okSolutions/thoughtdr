package com.arello.tdrtest.purchase;

import java.util.List;

import org.apache.http.NameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.arello.tdrtest.data.purchase.WebServiceHandlerSecure;

public class BackGroudTask {
	public final static int GET = 1;
	public final static int POST = 2;
	Context context;
	String title, url;
	List<NameValuePair> pair;
	String response;
	BackGroundTaskListener backGroundTaskListener;
	boolean isshow = true;

	public BackGroudTask(Context context, String title, String url, int method,
			List<NameValuePair> pair,
			BackGroundTaskListener backGroundTaskListener) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.title = title;
		this.url = url;
		this.pair = pair;
		this.backGroundTaskListener = backGroundTaskListener;
		new BackTask().execute();
	}

	public BackGroudTask(Context context, String title, String url, int method,
			List<NameValuePair> pair,
			BackGroundTaskListener backGroundTaskListener, boolean isShow) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.title = title;
		this.url = url;
		this.pair = pair;
		this.backGroundTaskListener = backGroundTaskListener;
		this.isshow = isShow;
		new BackTask().execute();
	}

	public class BackTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog dialog;
		String response;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (isshow) {
				dialog = new ProgressDialog(context);
				dialog.setMessage("Please wait!!!");
				dialog.setCancelable(false);
				dialog.show();
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.cancel();
			}
			backGroundTaskListener.onCompleteTask(response);
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				response = WebServiceHandlerSecure.makeServiceCall(url,
						WebServiceHandlerSecure.GET, pair);
			} catch (Exception e) {
				e.printStackTrace();
				dialog.cancel();
			}
			return null;
		}

	}
}
