package com.arello.tdrtest;

import java.util.HashMap;

import android.content.Context;

import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.model.Products;

public class Constants {
	// #if TEST_BUILD
	public static final int SLEEP_TIME_MULTIPLIER = 10 * 60;
	public static final int REPEAT_AFFIRMATION_COUNT = 20;
	public static final int DEFAULT_SLEEP_TIME = 60;
	public static final int NIGHT_AFFIRAMTIONS_DELAY = 20 * 1000;
	// public static int SNOOZE_TIME = 15 * 1000;
	public static final int MAX_AFFIRMATOIN_COUNT = 5;
	// #else
	// # public static final int SLEEP_TIME_MULTIPLIER = 60 * 1000;
	// # public static final int REPEAT_AFFIRMATION_COUNT = 20;
	// # public static final int DEFAULT_SLEEP_TIME = 60;
	// # public static final int NIGHT_AFFIRAMTIONS_DELAY = 60 * 1000;
	// # public static final int SNOOZE_TIME = 15 * 60 * 1000;
	// # public static final int MAX_AFFIRMATOIN_COUNT = 5;
	// #endif
	public final static String START_WITH_MUSIC = "start_with_mist_key";
	public final static String DAY_MODE = "day_mode_key";

	public static final String PURCHASE_FTP_HOST = "ftp.affirmationexperts.com";// "ftp.awake2000.com";
	public static final String PURCHASE_FTP_BASE_DIRECTORY = "/inapp/";// "/ThoughtDR/";
	public static final String PURCHASE_LOGIN = "access1@affirmationexperts.com";// "awake200";
	public static final String PURCHASE_PASSWORD = "v+{8H;;ABdb.";// "AppVentures2012";
	public static final String PURCHASE_LIST_URL = PURCHASE_FTP_BASE_DIRECTORY
			+ "products_android.plist"; // PURCHASE_FTP_BASE_DIRECTORY +
										// "products.plist";
	public static final String FREE_PURCHASE_LIST_URL = PURCHASE_FTP_BASE_DIRECTORY
			+ "products_android_free.plist"; // PURCHASE_FTP_BASE_DIRECTORY +
												// "freeProducts.plist";

	// public static final String SUBSCRIBE_URL =
	// "http://www.awake2000.com/thought/sendEmail.php?email=";
	public static final String SUBSCRIBE_URL = "http://affirmationexperts.com/api/newsletter.php";

	public static Category[] defaultCategories() {
		return new Category[] { Category.createCategory("Uncategorized"),
				Category.createCategory("Health"),
				Category.createCategory("Relationships"),
				Category.createCategory("Happiness"),
				Category.createCategory("Stress Relief"),
				Category.createCategory("Career") };
	}

	public static SleepMusic[] defaultSleepMusics(Context context) {
		SleepMusic music = SleepMusic.createSleepMusic("Weather", null, null,
				"kenhall_storm_fiverr.mp3");

		return new SleepMusic[] { music };
	}

	public static final String APP_TITLE = "ThoughtsDR";
	public static final String USER_NAME = "userName";
	public static final String PASSWORD = "password";
	public static final String USER_ID = "userId";
	public static final String IS_REMEMBER = "isRemember";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String PHONE = "phone_no";
	public static final String COUNTRY = "country";
	public static final String SERVER_URL = "http://affirmationexperts.com/api/";
	public static final String LOGIN_URL = "login.php";
	public static final String REGISTRATION_URL = "registration.php";
	public static final String FORGOT_PASSWORD_URL = "forget_password.php";
	public static final String CHANGE_PASSWORD_URL = "change_password.php";
	public static final String IS_LOGGED_IN = "isLoggedIn";
	public static final String PRODUCT_BASE_URL = "http://www.affirmationexperts.com/affapp/inapp/";
	public static final String FREE_PRODUCT_URL = "products_android_free.plist";
	public static final String PAID_PRODUCT_URL = "products_android.plist";
	public static final String CONSUMER_KEY = "ck_a906a5ae7636f2fa009c86458804b448";
	public static final String CONSUMER_SECRET = "cs_6b62c06a0ad4343aec4ef33a8f4a0e3c";
	public static final String NEWS_LETTER_URL = "http://affirmationexperts.com/api/newsletter.php";
	public static final String REGISTER_DEVICE_ID = "http://affirmationexperts.com/api/register_device_id.php";
	public static final String CATEGORY_URL = "https://affirmationexperts.com/wc-api/v2/products/categories";
	public static final String CUSTOM_CATEGORY_URL = "http://affirmationexperts.com/api/custom_categories.php";
	public static final String PRODUCTS_URL = "https://affirmationexperts.com/wc-api/v2/products";
	public static final String COINS_URL = "http://affirmationexperts.com/api/get_all_coins.php";
	public static final String BUCKS_URL = "http://affirmationexperts.com/api/get_all_bucks.php";
	public static final String BUCKS_BY_ID_URL = "http://affirmationexperts.com/api/get_brainbuck_coin.php";
	public static final String GET_BRAIN_COIN_URL = "http://affirmationexperts.com/api/get_reward_point.php";
	public static final String SOCIAL_MEDIA_URL = "http://affirmationexperts.com/api/social_login.php";
	public static HashMap<Integer, Products> cart;
	public static final long FIRST_STOP = 8000;
	public static final long SECOND_STOP = 12000;
	public static final long LAST_STOP = 15000;
	public static final String TOTAL_TIME = "total_time";
	public static final String START_TIME = "start_time";
	public static final String STOP_TIME = "stop_time";
	public static final String IS_EIGTH_HOUR = "isEightHour";
	public static final String TOTAL_HOUR = "totalHour";
	public static final String REGISTRATION_ID = "registrationId";
}
