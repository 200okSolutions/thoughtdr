package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Categories;
import com.arello.tdrtest.model.Products;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class BuyAudio extends FragmentActivity implements
		BackGroundTaskListener, OnClickListener {

	Categories[] categories, customCategory;

	ArrayAdapter<String> spinnerAdapter;
	TextView txtBrainBucks, txtCoin;
	String brainBucks = "0", coins = "0";
	int categoryPos = 0;
	ArrayList<Products> products, customProducts;
	ExpandableListView expandableListView;
	ExpandableListAdapter adapter;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	Button first, second, third, fourth;
	LinearLayout layout;
	TextView noData;
	EditText edit;
	int lastPos = 0;
	int lastClick = 0;
	Button spinner;
	String selSlug = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.buy_audio);
		edit = (EditText) findViewById(R.id.editText1);
		if (edit != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
		}
		edit.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					GetCustomProducts(selSlug);
				}
				return false;
			}
		});
		GetPoints();
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(BuyAudio.this, "Getting categories",
				Constants.CATEGORY_URL, WebServiceHandler.GET, pair, this);
	}

	public void GetCategories() {

		new BackGroudTask(BuyAudio.this, "Getting categories",
				Constants.CUSTOM_CATEGORY_URL, WebServiceHandler.GET, null,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						try {
							IntializeComponent();
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							customCategory = new Categories[jsonObject
									.getJSONArray("product_categories")
									.length()];
							customCategory = mapper.readValue(jsonObject
									.getJSONArray("product_categories")
									.toString(), Categories[].class);
							spinnerAdapter = new ArrayAdapter<String>(
									getApplicationContext(),
									R.layout.spinner_item, R.id.categoryName);
							for (int i = 0; i < customCategory.length; i++) {
								if (customCategory[i].getName().contains(
										"&amp;")) {
									customCategory[i].setName(customCategory[i]
											.getName().replace("&amp;", "&"));
								}
								spinnerAdapter.add(customCategory[i].getName());
							}
							/*
							 * for (Categories category : customCategory) {
							 * spinnerAdapter.add(category.getName().replace(
							 * "&amp;", "&")); }
							 */
							// spinner.setAdapter(spinnerAdapter);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
	}

	public void GetPoints() {
		List<NameValuePair> pair1 = new ArrayList<NameValuePair>();
		pair1.add(new BasicNameValuePair("user_id", Utils.getSharedPreferences(
				Constants.USER_ID, getApplicationContext())));
		new BackGroudTask(BuyAudio.this, "Getting categories",
				Constants.GET_BRAIN_COIN_URL, WebServiceHandler.GET, pair1,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						if (response == null || response.equals("")) {
							Toast.makeText(getApplicationContext(),
									"Server Error.", Toast.LENGTH_SHORT).show();
							return;
						} else {
							try {
								JSONObject jsonObject = new JSONObject(response);
								if (jsonObject.has("PointsData")) {
									brainBucks = jsonObject.getJSONObject(
											"PointsData").getString(
											"brain_bucks_point");
									coins = jsonObject.getJSONObject(
											"PointsData").getString("points");
								} else {
									brainBucks = "0";
									coins = "0";
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
	}

	public void IntializeComponent() {
		noData = (TextView) findViewById(R.id.noData);
		layout = (LinearLayout) findViewById(R.id.swipeLayout);
		txtBrainBucks = (TextView) findViewById(R.id.txtBucks);
		txtCoin = (TextView) findViewById(R.id.txtCoin);
		txtBrainBucks.setSelected(true);
		txtCoin.setSelected(true);
		spinner = (Button) findViewById(R.id.spinner1);
		spinner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dialog dialog;
				AlertDialog.Builder builder = new AlertDialog.Builder(
						BuyAudio.this);
				builder.setSingleChoiceItems(spinnerAdapter, -1,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								selSlug = customCategory[whichButton].getSlug();
								spinner.setText(customCategory[whichButton]
										.getName());
								GetCustomProducts(selSlug);
								dialog.cancel();
							}
						});
				dialog = builder.create();
				dialog.show();
			}
		});
		txtBrainBucks.setText(brainBucks);
		txtCoin.setText(coins);
		txtBrainBucks.setSelected(true);
		txtCoin.setSelected(true);
		first = (Button) findViewById(R.id.btn1);
		second = (Button) findViewById(R.id.btn2);
		third = (Button) findViewById(R.id.btn3);
		fourth = (Button) findViewById(R.id.btn4);
		// spinner.setAdapter(spinnerAdapter);
		first.setOnClickListener(this);
		second.setOnClickListener(this);
		third.setOnClickListener(this);
		fourth.setOnClickListener(this);
		first.setSelected(true);
		second.setSelected(true);
		third.setSelected(true);
		fourth.setSelected(true);
		expandableListView = (ExpandableListView) findViewById(R.id.lvExp);
		layout.setOnTouchListener(new TouchGesture(getApplicationContext()) {
			@Override
			public void onSwipeRight() {
				// TODO Auto-generated method stub
				super.onSwipeRight();
				if (categoryPos != 0) {
					System.out.println("cat pos:" + categoryPos
							+ ":slug:from swipe"
							+ categories[categoryPos].getSlug());
					categoryPos -= 4;
					GetProducts(categories[categoryPos].getSlug());
					setBackground(first);
				}
			}

			@Override
			public void onSwipeLeft() {
				// Whatever
				if (categoryPos + 4 < categories.length) {
					categoryPos += 4;
					System.out.println("cat pos:" + categoryPos
							+ ":slug:from swipe"
							+ categories[categoryPos].getSlug());
					GetProducts(categories[categoryPos].getSlug());
					setBackground(first);
				}
			}
		});
		expandableListView.setOnTouchListener(new TouchGesture(
				getApplicationContext()) {
			@Override
			public void onSwipeRight() {
				// TODO Auto-generated method stub
				super.onSwipeRight();
				if (categoryPos != 0) {
					categoryPos -= 4;
					System.out.println("cat pos:" + categoryPos
							+ ":slug:from swipe"
							+ categories[categoryPos].getSlug());
					GetProducts(categories[categoryPos].getSlug());
					setBackground(first);
				}

			}

			@Override
			public void onSwipeLeft() {
				// Whatever
				if (categoryPos + 4 < categories.length) {
					categoryPos += 4;
					System.out.println("cat pos:" + categoryPos
							+ ":slug:from swipe"
							+ categories[categoryPos].getSlug());
					GetProducts(categories[categoryPos].getSlug());

					setBackground(first);
				}
			}
		});
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration
				.createDefault(getApplicationContext()));
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.product_icon)
				.showImageForEmptyUri(R.drawable.product_icon)
				.showImageOnFail(R.drawable.product_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();
		if (categoryPos == 0) {
			setText();
			GetProducts(categories[0].getSlug());
		}
	}

	public void GetCustomProducts(String slug) {
		System.out.println("drop down slug:" + slug);
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[tag_name]", slug));
		if (lastClick == 0) {
			pair.add(new BasicNameValuePair("filter[product_cat]",
					categories[categoryPos].getSlug()));
			System.out.println("category:" + categories[categoryPos].getSlug());
		} else if (lastClick == 1) {
			pair.add(new BasicNameValuePair("filter[product_cat]",
					categories[categoryPos + 1].getSlug()));
			System.out.println("category:"
					+ categories[categoryPos + 1].getSlug());
		} else if (lastClick == 2) {
			pair.add(new BasicNameValuePair("filter[product_cat]",
					categories[categoryPos + 2].getSlug()));
			System.out.println("category:"
					+ categories[categoryPos + 2].getSlug());
		} else if (lastClick == 3) {
			pair.add(new BasicNameValuePair("filter[product_cat]",
					categories[categoryPos + 3].getSlug()));
			System.out.println("category:"
					+ categories[categoryPos + 3].getSlug());
		}
		pair.add(new BasicNameValuePair("filter[q]", edit.getText().toString()));
		System.out.println("Search Text:" + edit.getText().toString());
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));

		new BackGroudTask(BuyAudio.this, "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) { // TODO
						try {
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							products = new ArrayList<Products>();
							products = mapper
									.readValue(
											jsonObject.getJSONArray("products")
													.toString(),
											TypeFactory.defaultInstance()
													.constructCollectionType(
															List.class,
															Products.class));
							if (products.size() > 0) {
								noData.setVisibility(View.GONE);
								expandableListView.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyAudio.this, products);
								expandableListView.setAdapter(adapter);
							} else {
								expandableListView.setVisibility(View.GONE);
								noData.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyAudio.this, products);
								expandableListView.setAdapter(adapter);
							}
						} catch (Exception e) { // TODO Auto-generated catch
												// block
							e.printStackTrace();
						}
					}
				});
	}

	public void setText() {

		first.setSingleLine();

		first.setText(categories[categoryPos].getName().indexOf(" ") > 0 ? categories[categoryPos]
				.getName().replaceFirst(" ", " \n") : categories[categoryPos]
				.getName() + "\n");
		if (categoryPos + 1 < categories.length) {

			second.setSingleLine();

			second.setVisibility(View.VISIBLE);
			second.setText(categories[categoryPos + 1].getName());
		} else {
			second.setVisibility(View.GONE);
		}
		if (categoryPos + 2 < categories.length) {

			third.setSingleLine();

			third.setVisibility(View.VISIBLE);
			third.setText(categories[categoryPos + 2].getName());
		} else {
			third.setVisibility(View.GONE);
		}
		if (categoryPos + 3 < categories.length) {

			fourth.setSingleLine();

			fourth.setVisibility(View.VISIBLE);
			fourth.setText(categories[categoryPos + 3].getName());
		} else {
			fourth.setVisibility(View.GONE);
		}
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(response);
			ObjectMapper mapper = new ObjectMapper();
			categories = mapper.readValue(
					jsonObject.getJSONArray("product_categories").toString(),
					Categories[].class);
			GetCategories();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public class ExpandableListAdapter extends BaseExpandableListAdapter {

		private Context _context;
		ArrayList<Products> products;

		public ExpandableListAdapter(Context context,
				ArrayList<Products> products) {
			this._context = context;
			this.products = products;
		}

		@Override
		public Products getChild(int groupPosition, int childPosititon) {
			return this.products.get(groupPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_item, null);
			}
			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Products getGroup(int groupPosition) {
			return this.products.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return this.products.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, final ViewGroup parent) {
			String headerTitle = products.get(groupPosition).getTitle();
			// String price = products.get(groupPosition).getPrice();
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_group, null);
			}
			RatingBar bar = (RatingBar) convertView
					.findViewById(R.id.ratingBar1);
			bar.setRating(products.get(groupPosition).getAverage_rating());
			final LinearLayout layout = (LinearLayout) convertView
					.findViewById(R.id.descLayout);
			layout.setTag("layout" + String.valueOf(groupPosition));
			TextView lblTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			lblTitle.setText(headerTitle);
			ImageView img = (ImageView) convertView
					.findViewById(R.id.priceimage);
			TextView lblPrice = (TextView) convertView
					.findViewById(R.id.productPrice);
			if (products.get(groupPosition).getPoints_for()
					.equals("brainbucks")) {
				img.setBackground(getResources().getDrawable(R.drawable.bucks));
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else if (products.get(groupPosition).getPoints_for()
					.equals("coins")) {
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else {
				lblPrice.setText("0");
			}
			lblPrice.setSelected(true);

			String desc = products.get(groupPosition).getDescription();
			TextView description = (TextView) convertView
					.findViewById(R.id.productLongDesc);
			description.setText(desc);
			final Button expand = (Button) convertView
					.findViewById(R.id.productExpand);
			expand.setTag("btn" + String.valueOf(groupPosition));
			expand.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (layout.getVisibility() == View.VISIBLE) {
						layout.setVisibility(View.GONE);
						expand.setBackground(getResources().getDrawable(
								R.drawable.down));
						Animation animation = AnimationUtils.loadAnimation(
								getApplicationContext(), R.anim.layout_anim_up);
						layout.startAnimation(animation);
					} else {
						expand.setBackground(getResources().getDrawable(
								R.drawable.up));
						layout.setVisibility(View.VISIBLE);
						Animation animation = AnimationUtils.loadAnimation(
								getApplicationContext(), R.anim.layout_anim);

						layout.startAnimation(animation);
					}
					try {
						if (lastPos != groupPosition) {
							LinearLayout lay = (LinearLayout) parent
									.findViewWithTag("layout"
											+ String.valueOf(lastPos));
							lay.setVisibility(View.GONE);
							Button btn = (Button) parent.findViewWithTag("btn"
									+ String.valueOf(lastPos));
							btn.setBackground(getResources().getDrawable(
									R.drawable.down));
							Animation animation1 = AnimationUtils
									.loadAnimation(getApplicationContext(),
											R.anim.layout_anim_up);
							lay.startAnimation(animation1);
							lastPos = groupPosition;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			final ImageView pic = (ImageView) convertView
					.findViewById(R.id.productImage);
			String url = products.get(groupPosition).getFeatured_src()
					.replace("https", "http");
			imageLoader.displayImage(url, pic, options,
					new SimpleImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							pic.setImageBitmap(loadedImage);
						}
					}, new ImageLoadingProgressListener() {

						@Override
						public void onProgressUpdate(String imageUri,
								View view, int current, int total) {

						}
					});
			convertView.setTag(groupPosition);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == first) {
			setBackground(first);
			GetProducts(categories[categoryPos].getSlug());
		} else if (v == second) {
			setBackground(second);
			GetProducts(categories[categoryPos + 1].getSlug());
		} else if (v == third) {
			setBackground(third);
			GetProducts(categories[categoryPos + 2].getSlug());
		} else if (v == fourth) {
			setBackground(fourth);
			GetProducts(categories[categoryPos + 3].getSlug());
		}
	}

	public void setBackground(Button btn) {
		btn.setBackground(getResources().getDrawable(R.drawable.active_tab));
		if (btn == first) {
			second.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			third.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			fourth.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			lastClick = 0;
		} else if (btn == second) {
			first.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			third.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			fourth.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			lastClick = 1;
		} else if (btn == third) {
			second.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			first.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			fourth.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			lastClick = 2;
		} else if (btn == fourth) {
			second.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			third.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			first.setBackground(getResources().getDrawable(
					R.drawable.normal_tab));
			lastClick = 3;
		}
	}

	public void GetProducts(String slug) {
		System.out.println("slug:" + slug);
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[product_cat] ", slug));
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(BuyAudio.this, "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						try {
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							products = new ArrayList<Products>();
							products = mapper
									.readValue(
											jsonObject.getJSONArray("products")
													.toString(),
											TypeFactory.defaultInstance()
													.constructCollectionType(
															List.class,
															Products.class));
							if (products.size() > 0) {
								noData.setVisibility(View.GONE);
								expandableListView.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyAudio.this, products);
								expandableListView.setAdapter(adapter);
							} else {
								expandableListView.setVisibility(View.GONE);
								noData.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyAudio.this, products);
								expandableListView.setAdapter(adapter);
							}
							setText();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}
					}
				});
	}

}
