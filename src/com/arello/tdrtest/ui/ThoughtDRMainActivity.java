package com.arello.tdrtest.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.ui.tabs.*;

import java.util.HashMap;

/*
 * most of these code copypasted from
 * http://developer.android.com/resources/samples
 * /Support4Demos/src/com/example/android/supportv4/app/FragmentTabs.html
 */

public class ThoughtDRMainActivity extends TDRBaseActivity {

	TabHost mTabHost;
	TabManager mTabManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);

		String dayName = getResources().getString(R.string.day);
		String nightName = getResources().getString(R.string.night);
		String settingsName = getResources().getString(R.string.settings);
		String infoName = getResources().getString(R.string.info);
		mTabManager
				.addTab(mTabHost.newTabSpec(nightName).setIndicator(
						createTabContent(nightName, R.drawable.night_selector)),
						NightTab.class, null);
		mTabManager.addTab(
				mTabHost.newTabSpec(dayName).setIndicator(
						createTabContent(dayName, R.drawable.day_selector)),
				DayTab.class, null);

		mTabManager
				.addTab(mTabHost
						.newTabSpec(
								getResources().getString(
										R.string.buy_affirmations))
						.setIndicator(
								View.inflate(this,
										R.layout.buy_affirmations_button, null)),
						Shop.class, null);

		mTabManager.addTab(
				mTabHost.newTabSpec(settingsName).setIndicator(
						createTabContent(settingsName,
								R.drawable.settings_selector)),
				SettingsTabNew.class, null);
		// mTabManager.addTab(mTabHost.newTabSpec(settingsName).setIndicator(createTabContent(settingsName,
		// R.drawable.settings_selector)), SettingsTab.class, null);
		mTabManager.addTab(
				mTabHost.newTabSpec(infoName).setIndicator(
						createTabContent(infoName, R.drawable.info_selector)),
				InfoTab.class, null);

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	protected View createTabContent(String name, int drawableId) {
		View view = View.inflate(this, R.layout.tab_button, null);
		((ImageView) view.findViewById(R.id.tab_icon))
				.setImageResource(drawableId);
		((TextView) view.findViewById(R.id.tab_title)).setText(name);
		return view;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}

	/**
	 * This is a helper class that implements a generic mechanism for
	 * associating fragments with the tabs in a tab host. It relies on a trick.
	 * Normally a tab host has a simple API for supplying a View or Intent that
	 * each tab will show. This is not sufficient for switching between
	 * fragments. So instead we make the content part of the tab host 0dp high
	 * (it is not shown) and the TabManager supplies its own dummy view to show
	 * as the tab content. It listens to changes in tabs, and takes care of
	 * switch to the correct fragment shown in a separate content area whenever
	 * the selected tab changes.
	 */
	public static class TabManager implements TabHost.OnTabChangeListener {
		private final FragmentActivity mActivity;
		private final TabHost mTabHost;
		private final int mContainerId;
		private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
		TabInfo mLastTab;

		static final class TabInfo {
			private final String tag;
			private final Class<?> clss;
			private final Bundle args;
			private Fragment fragment;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				tag = _tag;
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabManager(FragmentActivity activity, TabHost tabHost,
				int containerId) {
			mActivity = activity;
			mTabHost = tabHost;
			mContainerId = containerId;
			mTabHost.setOnTabChangedListener(this);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mActivity));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			info.fragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(tag);
			if (info.fragment != null && !info.fragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				ft.detach(info.fragment);
				ft.commit();
			}

			mTabs.put(tag, info);
			mTabHost.addTab(tabSpec);
		}

		@Override
		public void onTabChanged(String tabId) {
			TabInfo newTab = mTabs.get(tabId);
			if (mLastTab != newTab) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				if (mLastTab != null) {
					if (mLastTab.fragment != null) {
						ft.detach(mLastTab.fragment);
					}
				}
				if (newTab != null) {
					if (newTab.fragment == null) {
						newTab.fragment = Fragment.instantiate(mActivity,
								newTab.clss.getName(), newTab.args);
						ft.add(mContainerId, newTab.fragment, newTab.tag);
					} else {
						ft.attach(newTab.fragment);
					}
				}

				mLastTab = newTab;
				ft.commit();
				mActivity.getSupportFragmentManager()
						.executePendingTransactions();
			}
		}
	}

	public Fragment getCurrentFragment(String tag) {
		final FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		if (fragment != null && fragment.isVisible()) {
			return fragment;
		}
		return null;
	}

	@Override
	public void onBackPressed() {

		Fragment fragment = getCurrentFragment(getResources().getString(
				R.string.buy_affirmations));
		try {
			if (fragment != null) {
				/*
				 * if (((BuyAffirmationsTab) fragment).onBackPressed())
				 * 
				 * { return; }
				 */
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		mTabHost.setCurrentTab(0);
		// super.onBackPressed();
	}
}