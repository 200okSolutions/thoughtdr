package com.arello.tdrtest.ui;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.data.purchase.FileItem;
import com.arello.tdrtest.data.purchase.PurchaseRecord;
import com.arello.tdrtest.purchase.DownloadManager;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTask;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTaskListener;
import com.arello.tdrtest.purchase.billing.BillingService;
import com.arello.tdrtest.purchase.billing.BillingService.RequestPurchase;
import com.arello.tdrtest.purchase.billing.BillingService.RestoreTransactions;
import com.arello.tdrtest.purchase.billing.Consts.PurchaseState;
import com.arello.tdrtest.purchase.billing.Consts.ResponseCode;
import com.arello.tdrtest.purchase.billing.PurchaseObserver;
import com.arello.tdrtest.purchase.billing.ResponseHandler;
import com.arello.tdrtest.uihelpers.Utils;

import java.util.List;
import java.util.logging.Logger;

public class PurchaseInfoActivity extends TDRBaseActivity implements
		DownloadManagerTaskListener {
	public final static String PURCHASE_TYPE = "PURCHASE_TYPE_KEY";
	public final static String PURCHASE_ITEM = "PURCHASE_ITEM_KEY";

	class DownloadManagerLoadFileTask extends DownloadManagerTask {

		public DownloadManagerLoadFileTask(FileItem item) {
			super(item.getFilePath());
			setTag(item);
			setListener(PurchaseInfoActivity.this);
		}

		@Override
		protected String getFilePath() {
			return Utils.getCachePath(PurchaseInfoActivity.this) + "/"
					+ Uri.parse(getUrl()).getLastPathSegment();
		}

	}

	public class TDRPurchaseObserver extends PurchaseObserver {
		private final Logger log = Logger.getLogger(getClass().getName());

		public TDRPurchaseObserver(TDRBaseActivity activity, Handler handler) {
			super(activity, handler);
		}

		@Override
		public void onBillingSupported(boolean supported) {
			if (!supported) {
				log.warning("Billing not supported");
			}
			mIsBillingSupported = supported;
		}

		@Override
		public void onPurchaseStateChange(PurchaseState purchaseState,
				String itemId, int quantity, long purchaseTime,
				String developerPayload) {
			if (purchaseState == PurchaseState.PURCHASED
					|| purchaseState == PurchaseState.REFUNDED) {
				setPurchased(mRecord);
				mBuyButton.setText(getResources().getString(R.string.download));
				log.warning("Item purchased " + itemId);
			} else if (purchaseState == PurchaseState.CANCELED) {
				log.warning("Purchase canceled " + itemId);
			}
		}

		@Override
		public void onRequestPurchaseResponse(RequestPurchase request,
				ResponseCode responseCode) {
			log.warning("Transaction response received" + request.mProductId);
		}

		@Override
		public void onRestoreTransactionsResponse(RestoreTransactions request,
				ResponseCode responseCode) {
			log.warning("Transaction restored");
			hideProgress();
		}

	}

	TextView mDescriptionText;
	TextView mContentText;
	TextView mPriceText;
	Button mBuyButton;
	boolean mIsPurchaseAffirmation;
	PurchaseRecord mRecord;
	TDRPurchaseObserver mPurchaseObserver;
	BillingService mBillingService;
	private final Logger log = Logger.getLogger(getClass().getName());
	List<Affirmation> mPurchaseAffirmation;
	List<SleepMusic> mPurchasedSleepSounds;
	boolean mIsBillingSupported;
	private String mShowedError;

	@Override
	protected void onCreate(Bundle savedState) {
		super.onCreate(savedState);
		setContentView(R.layout.puchase_info_layout);
		mDescriptionText = (TextView) findViewById(R.id.description_text);
		mContentText = (TextView) findViewById(R.id.content_text);
		mPriceText = (TextView) findViewById(R.id.price_text);
		mBuyButton = (Button) findViewById(R.id.buy_button);

		mIsPurchaseAffirmation = getIntent().getBooleanExtra(PURCHASE_TYPE,
				true);
		mRecord = (PurchaseRecord) getIntent().getSerializableExtra(
				PURCHASE_ITEM);

		StringBuilder tempString = new StringBuilder(getResources().getString(
				R.string.the_package_content) + '\n');

		for (FileItem item : mRecord.getItems()) {
			if (mIsPurchaseAffirmation) {
				tempString.append(String.format("%s / %s \n", item.getTitle(),
						item.getCategory()));
			} else {
				tempString.append(String.format("%s\n", item.getTitle()));
			}
		}

		mDescriptionText.setText(mRecord.getDescription());
		mContentText.setText(tempString);
		mPriceText.setText(mRecord.getValue());

		mBuyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if (AccountManager.get(PurchaseInfoActivity.this)
						.getAccountsByType("com.google").length < 1) {
					new AlertDialog.Builder(PurchaseInfoActivity.this)
							.setMessage(
									PurchaseInfoActivity.this
											.getString(R.string.need_sign_in_alert))
							.setCancelable(true)
							.setNeutralButton(
									PurchaseInfoActivity.this
											.getString(R.string.close_alert),
									null).create().show();
					return;
				}

				if (mRecord.isPurchased()) {
					if (mIsPurchaseAffirmation) {
						mPurchaseAffirmation = getTDRApplication()
								.getDataManager().getAffirmationsWithProductId(
										mRecord.getProductID());
					} else {
						mPurchasedSleepSounds = getTDRApplication()
								.getDataManager().getSleepSoundsWithProductId(
										mRecord.getProductID());
					}
					downloadSounds();
				} else {
					if (!mIsBillingSupported) {
						showAlert(R.string.billing_not_supported,
								android.R.string.ok);
						return;
					}
					if (!mBillingService.requestPurchase(
							mRecord.getProductID(), null)) {
						log.warning("Purchase failed " + mRecord.getProductID());
					}
				}
			}
		});
		if (!mIsPurchaseAffirmation) {
			mBuyButton.setText(R.string.buy_this_sleepsounds);
		}
		mBillingService = new BillingService();
		mBillingService.setContext(this);
		ResponseHandler.register(mPurchaseObserver = new TDRPurchaseObserver(
				this, new Handler()));
		if (mBillingService.checkBillingSupported()) {
			if (!mBillingService.restoreTransactions()) {
				log.warning("restore transactions failed");
			} else if (AccountManager.get(PurchaseInfoActivity.this)
					.getAccountsByType("com.google").length < 1) {
				log.warning("account not signed");
			} else {
				showProgress();
			}
		}
		if (mIsPurchaseAffirmation) {
			mPurchaseAffirmation = getTDRApplication().getDataManager()
					.getAffirmationsWithProductId(mRecord.getProductID());
			if (mPurchaseAffirmation.size() > 0) {
				setPurchased(mRecord);
				mBuyButton.setText(getResources().getString(R.string.download));
			}
		} else {
			mPurchasedSleepSounds = getTDRApplication().getDataManager()
					.getSleepSoundsWithProductId(mRecord.getProductID());
			if (mPurchasedSleepSounds.size() > 0) {
				setPurchased(mRecord);
				mBuyButton.setText(getResources().getString(R.string.download));
			}
		}
	}

	protected void downloadSounds() {
		if (Utils.isNetworkAvailable(this)) {
			Toast.makeText(
					this,
					mIsPurchaseAffirmation ? R.string.downloading_affirmations
							: R.string.downloading_sleep_sounds,
					Toast.LENGTH_LONG).show();
		} else {
			Utils.showAlert(this,
					getString(R.string.no_internet_connection_message));
		}

		if (mBuyButton != null) {
			mBuyButton.setEnabled(false);
		}

		for (FileItem fileItem : mRecord.getItems()) {
			DownloadManager.addNewLoadTask(new DownloadManagerLoadFileTask(
					fileItem));
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mBillingService.unbind();
	}

	@Override
	public void onDownloadComplete(String originalUri, String destinationFile,
			DownloadManagerTask task) {
		FileItem fileItem = (FileItem) task.getTag();

		if (originalUri.equals(fileItem.getFilePath())) {
			if (mIsPurchaseAffirmation) {
				for (Affirmation a : mPurchaseAffirmation) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager().deleteAffirmation(
								a);
						break;
					}
				}

				Affirmation aff = new Affirmation();
				aff.setName(fileItem.getTitle());
				Category cat = getTDRApplication().getDataManager()
						.getCategoryWitnName(fileItem.getCategory());
				if (cat == null) {
					cat = Category.createCategory(fileItem.getCategory());
					getTDRApplication().getDataManager().addNewCategory(cat);
				}
				aff.setCategory(cat);
				aff.setProductId(mRecord.getProductID());
				aff.setSoundPath(destinationFile);
				getTDRApplication().getDataManager().addAffirmation(aff);
			} else {
				for (SleepMusic a : mPurchasedSleepSounds) {
					log.warning("sleep sounds test: " + a.getName() + " || "
							+ fileItem.getTitle());
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager()
								.deleteSleepSound(a);
						break;
					}
				}

				SleepMusic sleepMusic = SleepMusic.createSleepMusic(
						fileItem.getTitle(), fileItem.getCategory(),
						destinationFile);
				sleepMusic.setProductId(mRecord.getProductID());
				getTDRApplication().getDataManager().addSleepSound(sleepMusic);
			}
		}

		if (DownloadManager.getQueueSize() == 0 && mBuyButton != null) {
			mBuyButton.setEnabled(true);
		}
	}

	@Override
	public void onDownloadStarted(String originalUri, DownloadManagerTask task) {
	}

	@Override
	public void onDownloadFailed(String origianlUri, String errorMessage,
			DownloadManagerTask task) {
		/*
		 * if (mShowedError == null ||
		 * !mShowedError.equalsIgnoreCase(errorMessage)) { Utils.showAlert(this,
		 * errorMessage); } mShowedError = errorMessage;
		 */
		if (DownloadManager.getQueueSize() == 0 && mBuyButton != null) {
			mBuyButton.setEnabled(true);
		}
	}

	private void setPurchased(PurchaseRecord record) {
		record.setPurchased(true);

		SharedPreferences.Editor editor = getSharedPreferences(
				"purchased_list", Context.MODE_PRIVATE).edit();

		editor.putBoolean(record.getProductID(), true);
		editor.commit();
	}

	@Override
	public void finish() {
		if (mRecord != null) {
			Intent intent = new Intent();
			intent.putExtra(PURCHASE_ITEM, mRecord);
			setResult(RESULT_OK, intent);
		}

		super.finish();
	}
}
