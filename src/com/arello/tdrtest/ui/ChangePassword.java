package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;

public class ChangePassword extends TDRBaseActivity implements OnClickListener,
		BackGroundTaskListener {

	Button submit;
	EditText editcurrentPass, editNewPass, editconfirm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password_screen);
		IntializeComponent();
	}

	public void IntializeComponent() {
		editcurrentPass = (EditText) findViewById(R.id.editCurrent);
		editNewPass = (EditText) findViewById(R.id.editNew);
		editconfirm = (EditText) findViewById(R.id.editConfirm);
		submit = (Button) findViewById(R.id.btnSubmitChange);
		submit.setOnClickListener(this);
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Change Pass Task Completed:" + response);
		if (response == null || response.equals("")) {
			Toast.makeText(getApplicationContext(), "Server Error.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(response);
				if (jsonObject.getString("flag").equals("1")) {
					if (Utils.getSharedPreferences(Constants.IS_REMEMBER,
							getApplicationContext()) != null) {
						if (Utils.getSharedPreferences(Constants.IS_REMEMBER,
								getApplicationContext()).equals("1")) {
							Utils.saveSharedPreferences(Constants.PASSWORD,
									editNewPass.getText().toString(),
									getApplicationContext());
						}
					}
					Toast.makeText(getApplicationContext(),
							jsonObject.getString("msg"), Toast.LENGTH_SHORT)
							.show();
					Intent intent = new Intent(getApplicationContext(),
							ThoughtDRMainActivity.class);
					startActivity(intent);
					finish();
				} else {
					Toast.makeText(getApplicationContext(),
							jsonObject.getString("msg"), Toast.LENGTH_SHORT)
							.show();
					return;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == submit) {
			String newPass = editNewPass.getText().toString();
			String confirmPass = editconfirm.getText().toString();
			if (editcurrentPass.getText().toString() == null
					|| editcurrentPass.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(),
						"Enter current Password.", Toast.LENGTH_SHORT).show();
				return;
			} else if (editNewPass.getText().toString() == null
					|| editNewPass.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter New Password.",
						Toast.LENGTH_SHORT).show();
				return;
			} else if (editconfirm.getText().toString() == null
					|| editconfirm.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(),
						"Enter Confirm Password.", Toast.LENGTH_SHORT).show();
				return;
			} else if (!newPass.equals(confirmPass)) {
				Toast.makeText(getApplicationContext(),
						"New Password and confirm password doesn't match.",
						Toast.LENGTH_SHORT).show();
				return;
			} else {
				List<NameValuePair> pair = new ArrayList<NameValuePair>();
				pair.add(new BasicNameValuePair("old_password", editcurrentPass
						.getText().toString()));
				pair.add(new BasicNameValuePair("new_password", editNewPass
						.getText().toString()));
				pair.add(new BasicNameValuePair("user_id", Utils
						.getSharedPreferences(Constants.USER_ID,
								getApplicationContext())));
				if (Utils.isNetworkAvailable(getApplicationContext())) {
					new BackGroudTask(ChangePassword.this,
							"Sending Request!!!", Constants.SERVER_URL
									+ Constants.CHANGE_PASSWORD_URL,
							WebServiceHandler.GET, pair, this);
				} else {
					Toast.makeText(getApplicationContext(),
							"Plese connect to internet.", Toast.LENGTH_SHORT)
							.show();
					return;
				}
			}
		}
	}
}
