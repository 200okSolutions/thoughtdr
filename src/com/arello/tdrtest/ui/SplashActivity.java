package com.arello.tdrtest.ui;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.data.purchase.FileItem;
import com.arello.tdrtest.purchase.DownloadManager;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTask;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTaskListener;
import com.arello.tdrtest.purchase.PurchaseManager;
import com.arello.tdrtest.purchase.SubscribeManager;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog.OnDeleteListener;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog.OnSubmitListener;
import com.arello.tdrtest.uihelpers.Patterns;
import com.arello.tdrtest.uihelpers.Utils;

public class SplashActivity extends TDRBaseActivity implements
		PurchaseManager.PurchaseManagerDelegate {
	private SharedPreferences mSharedPreferences;
	private SharedPreferences.Editor mEditor;
	private SubscribeManager mSubscribeManager;
	private PurchaseManager mPurchaseManager;
	private ProgressDialog mProgressDialog;
	private String mShowedError;
	private DialogInterface.OnDismissListener mOnDismissListener;
	private SubscribeManager.OnSubsribeListener mOnSubsribeListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mSharedPreferences = getSharedPreferences("main", Context.MODE_PRIVATE);
		mEditor = mSharedPreferences.edit();

		setContentView(R.layout.splash);

		getWindow().setBackgroundDrawableResource(R.drawable.background);

		findViewById(R.id.start_button).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if (mSharedPreferences.getInt("starts", 5) == 5) {
							mOnDismissListener = new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(
										DialogInterface dialogInterface) {
									skipSplash();
								}
							};
							mOnSubsribeListener = new SubscribeManager.OnSubsribeListener() {
								@Override
								public void onSubscribe() {
									mPurchaseManager.receivePurchaseList();
								}
							};

							showDialog();
							mEditor.putInt("starts", 0);
							mEditor.commit();
						} else {
							if (mSharedPreferences.getInt("starts", -1) >= 0) {
								mEditor.putInt(
										"starts",
										mSharedPreferences.getInt("starts", 0) + 1);
								mEditor.commit();
							}

							skipSplash();
						}
					}
				});
	}

	private void showDialog() {
		final EnterDialog emailEnterDialog = new EnterDialog(this,
				R.string.enter_email, R.style.Theme_ThoughtDR_DialogCenter);
		emailEnterDialog.setEmptyError(R.string.empty_email_alert);
		emailEnterDialog.addRule(Patterns.EMAIL_ADDRESS,
				R.string.invalid_email_alert);
		emailEnterDialog.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

		emailEnterDialog.setOnSubmitListener(new OnSubmitListener() {

			@Override
			public void onSubmit() {
				mPurchaseManager = new PurchaseManager(SplashActivity.this,
						true);
				mPurchaseManager.setDelegate(SplashActivity.this);
				mSubscribeManager = new SubscribeManager(SplashActivity.this,
						emailEnterDialog, mOnSubsribeListener);
				mSubscribeManager.execute(emailEnterDialog.getValue());
				mEditor.remove("starts");
				mEditor.commit();
			}
		});

		emailEnterDialog
				.setOnEditorActionListener(new EnterDialog.OnEditorActionListener() {
					@Override
					public boolean onEditorAction() {
						mPurchaseManager = new PurchaseManager(
								SplashActivity.this, true);
						mPurchaseManager.setDelegate(SplashActivity.this);
						mSubscribeManager = new SubscribeManager(
								SplashActivity.this, emailEnterDialog,
								mOnSubsribeListener);
						mSubscribeManager.execute(emailEnterDialog.getValue());
						mEditor.remove("starts");
						mEditor.commit();
						return false;
					}
				});
		emailEnterDialog.setDeleteListener(new OnDeleteListener() {

			@Override
			public void onDelete() {
				skipSplash();
			}
		});
		emailEnterDialog
				.setOnDismissListener(new EnterDialog.OnDismissListener() {
					@Override
					public void onDismiss() {
						skipSplash();
					}
				});

		emailEnterDialog.show();
		emailEnterDialog.showDeleteButton();
		emailEnterDialog.getDeleteButton().setText("Cancel");
		emailEnterDialog.showSubmitButton();
	}

	@Override
	protected void onPause() {
		if (null != mSubscribeManager) {
			mSubscribeManager.cancel();
		}

		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
		}

		super.onPause();
	}

	@Override
	public void onProductsListReceived() {
		if (mPurchaseManager.getAffirmationsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getAffirmationsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new AffirmationDownloadTask(
						fileItem));
			}
		}

		if (mPurchaseManager.getSleepSoundsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getSleepSoundsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new SleepSoundsDownloadTask(
						fileItem));
			}
		}
	}

	@Override
	public void onProductsListLoadingStarted() {
		Toast.makeText(this, R.string.downloading_free_affirmations,
				Toast.LENGTH_LONG).show();

		skipSplash();
	}

	@Override
	public void onProductsListLoadingCanceled() {
	}

	@Override
	public void onProductsListLoadingFailed(String errorMessage) {
	}

	public class DownloadManagerLoadFileTask extends
			DownloadManager.DownloadManagerTask {

		public DownloadManagerLoadFileTask(FileItem item) {
			super(item.getFilePath());
			setTag(item);
		}

		@Override
		protected String getFilePath() {
			return Utils.getCachePath(SplashActivity.this) + "/"
					+ Uri.parse(getUrl()).getLastPathSegment();
		}

	}

	class AffirmationDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public AffirmationDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<Affirmation> freeAffirmation = getTDRApplication()
						.getDataManager().getAffirmationsWithProductId(
								"freeAffirmation");

				for (Affirmation a : freeAffirmation) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager().deleteAffirmation(
								a);
						break;
					}
				}

				Affirmation aff = new Affirmation();
				aff.setName(fileItem.getTitle());
				Category cat = getTDRApplication().getDataManager()
						.getCategoryWitnName(fileItem.getCategory());
				if (cat == null) {
					cat = Category.createCategory(fileItem.getCategory());
					getTDRApplication().getDataManager().addNewCategory(cat);
				}
				aff.setCategory(cat);
				aff.setProductId("freeAffirmation");
				aff.setSoundPath(destinationFile);
				getTDRApplication().getDataManager().addAffirmation(aff);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
		}
	}

	class SleepSoundsDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public SleepSoundsDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<SleepMusic> freeSleepMusic = getTDRApplication()
						.getDataManager().getSleepSoundsWithProductId(
								"freeSleepSounds");

				for (SleepMusic a : freeSleepMusic) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager()
								.deleteSleepSound(a);
						break;
					}
				}

				SleepMusic sleepMusic = new SleepMusic();
				sleepMusic.setName(fileItem.getTitle());
				sleepMusic.setProductId("freeSleepSounds");
				sleepMusic.setSoundPath(destinationFile);
				sleepMusic.setCategory(fileItem.getCategory());
				getTDRApplication().getDataManager().addSleepSound(sleepMusic);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
		}
	}

	@Override
	public void hideProgress() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.cancel();
		}
	}

	private void skipSplash() {
		Intent intent = new Intent(SplashActivity.this,
				ThoughtDRMainActivity.class);
		startActivity(intent);
	}
}
