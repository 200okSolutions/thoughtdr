package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Products;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductList extends TDRBaseActivity implements
		BackGroundTaskListener, OnItemClickListener {

	ListView categoryListView;
	Products[] products;
	ProducstListAdapter adapter;
	String slug;
	Settings settings;

	HashMap<Integer, Products> cart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_list);
		try {
			slug = getIntent().getStringExtra("slug");
		} catch (Exception e) {
			e.printStackTrace();
		}
		categoryListView = (ListView) findViewById(R.id.productview);
		categoryListView.setOnItemClickListener(this);
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[product_cat] ", slug));
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		settings = new Settings(getApplicationContext());
		cart = settings.getCart();
		if (cart == null) {
			cart = new HashMap<Integer, Products>();
			Constants.cart = new HashMap<Integer, Products>();
		}
		new BackGroudTask(ProductList.this, "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair, this);
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(response);
			ObjectMapper mapper = new ObjectMapper();
			products = mapper.readValue(jsonObject.getJSONArray("products")
					.toString(), Products[].class);
			adapter = new ProducstListAdapter(getApplicationContext(),
					R.layout.category_title, products);
			categoryListView.setAdapter(adapter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), products[position].getTitle(),
				Toast.LENGTH_SHORT).show();

	}

	static class Holder {

		TextView textCategoriesName = null;
		Button buttton = null;
	}

	public class ProducstListAdapter extends ArrayAdapter<Products> {

		private Context context;
		private Products[] products;
		private LayoutInflater inflater = null;

		public ProducstListAdapter(Context context, int resource,
				Products[] products) {
			super(context, resource, products);

			this.context = context;
			this.products = products;

			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@SuppressWarnings("unused")
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			final Holder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.category_title, null);
				holder = new Holder();
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.textCategoriesName = detail(
					convertView,
					R.id.categoryTitle,
					String.valueOf(this.products[position].getTitle() + "\n$"
							+ products[position].getPrice()));
			holder.buttton = (Button) convertView.findViewById(R.id.coinBuckPrice);
			holder.buttton.setVisibility(View.VISIBLE);
			// holder.buttton.setVisibility(View.GONE);
			if (settings.isCartContainsProduct(Integer
					.parseInt(ProductList.this.products[position].getId()))) {
				holder.buttton.setText("View Cart");
			}
			holder.buttton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (settings.isCartContainsProduct(Integer
							.parseInt(ProductList.this.products[position]
									.getId()))) {
						Intent intent = new Intent(ProductList.this, Cart.class);
						startActivityForResult(intent, 123);
					} else {
						settings.addProductToCart(Integer
								.parseInt(ProductList.this.products[position]
										.getId()), products[position]);
						holder.buttton.setText("View Cart");
						/*List<NameValuePair> pair = new ArrayList<NameValuePair>();
						pair.add(new BasicNameValuePair("", ""));
						new BackGroudTask(getApplicationContext(), "", "",
								WebServiceHandler.GET, null,
								new BackGroundTaskListener() {

									@Override
									public void onCompleteTask(String response) {
										// TODO Auto-generated method stub

									}
								});*/
					}
				}
			});
			return convertView;

		}

		private TextView detail(View v, int resId, String text) {
			TextView tv = (TextView) v.findViewById(resId);
			tv.setText(text);
			return tv;
		}
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (arg0 == 123) {
			if (arg1 == RESULT_OK) {
				boolean isCartChange = arg2.getBooleanExtra("change", false);
				if (isCartChange) {
					adapter = new ProducstListAdapter(getApplicationContext(),
							R.layout.category_title, products);
					categoryListView.setAdapter(adapter);
				}
			}
		}
	}

}
