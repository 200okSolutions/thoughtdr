package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.ui.tabs.TDRBaseTab;
import com.arello.tdrtest.uihelpers.Utils;

public class Shop extends TDRBaseTab implements OnClickListener,
		BackGroundTaskListener {

	ImageView audio, ebook, course, game, bucks, coins, backBtn;
	TextView totalCoins, totalBucks;
	LinearLayout layout;
	String strBucks, strCoins;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.shop_menu, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		layout = (LinearLayout) getActivity().findViewById(R.id.newLyou);
		DisplayMetrics metrics = getResources().getDisplayMetrics();

		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		System.out.println("height:" + height + "\nwidth" + width);
		if (currentapiVersion >= 19) {
			// Do something for 14 and above versions
			System.out.println("lolipop");
			if (height <= 1280) {
				getActivity()
						.getWindow()
						.getDecorView()
						.setSystemUiVisibility(
								View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
			}
		}
		GetPoints();
	}

	public void GetPoints() {
		List<NameValuePair> pair1 = new ArrayList<NameValuePair>();
		pair1.add(new BasicNameValuePair("user_id", Utils.getSharedPreferences(
				Constants.USER_ID, getActivity())));
		new BackGroudTask(getActivity(), "Getting categories",
				Constants.GET_BRAIN_COIN_URL, WebServiceHandler.GET, pair1,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						System.out.println("Task Completed:" + response);
						if (response == null || response.equals("")) {
							Toast.makeText(getActivity(), "Server Error.",
									Toast.LENGTH_SHORT).show();
							return;
						} else {
							try {
								JSONObject jsonObject = new JSONObject(response);
								if (jsonObject.has("PointsData")) {
									strCoins = jsonObject.getJSONObject(
											"PointsData").getString(
											"brain_bucks_point");
									strBucks = jsonObject.getJSONObject(
											"PointsData").getString("points");
								} else {
									strBucks = "0";
									strCoins = "0";
								}
								IntializeComponent();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
	}

	public void IntializeComponent() {
		audio = (ImageView) getActivity().findViewById(R.id.btnAudio);
		ebook = (ImageView) getActivity().findViewById(R.id.btnEbooks);
		course = (ImageView) getActivity().findViewById(R.id.btnCourses);
		game = (ImageView) getActivity().findViewById(R.id.btnGames);
		bucks = (ImageView) getActivity().findViewById(R.id.btnBrain_buck);
		coins = (ImageView) getActivity().findViewById(R.id.btnCoins);
		backBtn = (ImageView) getActivity().findViewById(R.id.backBtn);
		totalCoins = (TextView) getActivity().findViewById(R.id.totalAmount);
		totalBucks = (TextView) getActivity().findViewById(R.id.txtCoin);
		totalCoins.setText(strCoins);
		totalBucks.setText(strBucks);
		audio.setOnClickListener(this);
		ebook.setOnClickListener(this);
		course.setOnClickListener(this);
		game.setOnClickListener(this);
		bucks.setOnClickListener(this);
		coins.setOnClickListener(this);
		backBtn.setOnClickListener(this);
		// GetPoints();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == audio) {
			if (Utils.isNetworkAvailable(getActivity())) {
				startActivity(new Intent(Shop.this.getActivity(),
						BuyAudio.class));
			} else {
				Utils.showAlert(getActivity(),
						R.string.no_internet_connection_message);
			}
		} else if (v == bucks) {
			if (Utils.isNetworkAvailable(getActivity())) {
				startActivity(new Intent(Shop.this.getActivity(),
						BucksActivity.class));
			} else {
				Utils.showAlert(getActivity(),
						R.string.no_internet_connection_message);
			}
		} else if (v == coins) {
			if (Utils.isNetworkAvailable(getActivity())) {
				startActivity(new Intent(Shop.this.getActivity(),
						CoinsActivity.class));
			} else {
				Utils.showAlert(getActivity(),
						R.string.no_internet_connection_message);
			}
		} else if (v == course) {
			if (Utils.isNetworkAvailable(getActivity())) {
				startActivity(new Intent(Shop.this.getActivity(),
						BuyCourse.class));
			} else {
				Utils.showAlert(getActivity(),
						R.string.no_internet_connection_message);
			}
		} else if (v == ebook) {
			if (Utils.isNetworkAvailable(getActivity())) {
				startActivity(new Intent(Shop.this.getActivity(),
						BuyEbooks.class));
			} else {
				Utils.showAlert(getActivity(),
						R.string.no_internet_connection_message);
			}
		} else if (v == game) {

		} else if (v == backBtn) {
			getActivity().onBackPressed();
		}
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Get Coins Task Completed:" + response);
		if (response == null || response.equals("")) {
			Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT)
					.show();
			return;
		} else {
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.has("coins")) {
					JSONArray array = jsonObject.getJSONArray("coins");
					int amount = 0;
					for (int i = 0; i < array.length(); i++) {
						amount += new JSONObject(array.get(i).toString())
								.getInt("brain_bucks_point");
					}
					totalCoins.setText(String.valueOf(amount));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
