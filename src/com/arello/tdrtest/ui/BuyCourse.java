package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Categories;
import com.arello.tdrtest.model.Products;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class BuyCourse extends FragmentActivity implements
		BackGroundTaskListener, OnClickListener {

	Categories[] categories, customCategory;

	ArrayAdapter<String> spinnerAdapter;
	TextView txtBrainBucks, txtCoin;
	String brainBucks = "0", coins = "0";
	int categoryPos = 0;
	ArrayList<Products> products, customProducts;
	ExpandableListView expandableListView;
	ExpandableListAdapter adapter;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	// Button first, second, third, fourth;
	// LinearLayout layout;
	TextView noData;
	EditText edit;
	int lastPos = 0;
	int lastClick = 0;
	Button spinner;
	String selSlug = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.buy_course);
		edit = (EditText) findViewById(R.id.editText1);
		if (edit != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
		}
		edit.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					GetCustomProducts(selSlug);
				}
				return false;
			}
		});
		GetPoints();
	}

	public void GetCategories() {

		new BackGroudTask(BuyCourse.this, "Getting categories",
				Constants.CUSTOM_CATEGORY_URL, WebServiceHandler.GET, null,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						try {
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							customCategory = new Categories[jsonObject
									.getJSONArray("product_categories")
									.length()];
							customCategory = mapper.readValue(jsonObject
									.getJSONArray("product_categories")
									.toString(), Categories[].class);
							spinnerAdapter = new ArrayAdapter<String>(
									getApplicationContext(),
									R.layout.spinner_item, R.id.categoryName);
							for (Categories category : customCategory) {
								spinnerAdapter.add(category.getName().replace(
										"&amp;", "&"));
							}
							IntializeComponent();
							// spinner.setAdapter(spinnerAdapter);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
	}

	public void GetPoints() {
		List<NameValuePair> pair1 = new ArrayList<NameValuePair>();
		pair1.add(new BasicNameValuePair("user_id", Utils.getSharedPreferences(
				Constants.USER_ID, getApplicationContext())));
		new BackGroudTask(BuyCourse.this, "Getting categories",
				Constants.GET_BRAIN_COIN_URL, WebServiceHandler.GET, pair1,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						if (response == null || response.equals("")) {
							Toast.makeText(getApplicationContext(),
									"Server Error.", Toast.LENGTH_SHORT).show();
							return;
						} else {
							try {
								JSONObject jsonObject = new JSONObject(response);
								if (jsonObject.has("PointsData")) {
									brainBucks = jsonObject.getJSONObject(
											"PointsData").getString(
											"brain_bucks_point");
									coins = jsonObject.getJSONObject(
											"PointsData").getString("points");
								} else {
									brainBucks = "0";
									coins = "0";
								}
								GetCategories();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
	}

	public void IntializeComponent() {
		noData = (TextView) findViewById(R.id.noData);
		txtBrainBucks = (TextView) findViewById(R.id.txtBucks);
		txtCoin = (TextView) findViewById(R.id.txtCoin);
		txtBrainBucks.setSelected(true);
		txtCoin.setSelected(true);
		spinner = (Button) findViewById(R.id.spinner1);
		spinner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dialog dialog;
				AlertDialog.Builder builder = new AlertDialog.Builder(
						BuyCourse.this);
				builder.setSingleChoiceItems(spinnerAdapter, -1,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								selSlug = customCategory[whichButton].getSlug();
								spinner.setText(customCategory[whichButton]
										.getName());
								GetCustomProducts(selSlug);
								dialog.cancel();
							}
						});
				dialog = builder.create();
				dialog.show();
			}
		});
		txtBrainBucks.setText(brainBucks);
		txtCoin.setText(coins);
		txtBrainBucks.setSelected(true);
		txtCoin.setSelected(true);

		expandableListView = (ExpandableListView) findViewById(R.id.lvExp);

		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration
				.createDefault(getApplicationContext()));
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.product_icon)
				.showImageForEmptyUri(R.drawable.product_icon)
				.showImageOnFail(R.drawable.product_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();
		GetProducts();
	}

	public void GetCustomProducts(String slug) {
		System.out.println("drop down slug:" + slug);
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[product_cat] ", "courses"));
		pair.add(new BasicNameValuePair("filter[q]", edit.getText().toString()));
		System.out.println("Search Text:" + edit.getText().toString());
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(BuyCourse.this, "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) { // TODO
						try {
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							products = new ArrayList<Products>();
							products = mapper
									.readValue(
											jsonObject.getJSONArray("products")
													.toString(),
											TypeFactory.defaultInstance()
													.constructCollectionType(
															List.class,
															Products.class));
							if (products.size() > 0) {
								noData.setVisibility(View.GONE);
								expandableListView.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyCourse.this, products);
								expandableListView.setAdapter(adapter);
							} else {
								expandableListView.setVisibility(View.GONE);
								noData.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyCourse.this, products);
								expandableListView.setAdapter(adapter);
							}
						} catch (Exception e) { // TODO Auto-generated catch
												// block
							e.printStackTrace();
						}
					}
				});
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(response);
			ObjectMapper mapper = new ObjectMapper();
			categories = mapper.readValue(
					jsonObject.getJSONArray("product_categories").toString(),
					Categories[].class);
			GetCategories();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public class ExpandableListAdapter extends BaseExpandableListAdapter {

		private Context _context;
		ArrayList<Products> products;

		public ExpandableListAdapter(Context context,
				ArrayList<Products> products) {
			this._context = context;
			this.products = products;
		}

		@Override
		public Products getChild(int groupPosition, int childPosititon) {
			return this.products.get(groupPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_item, null);
			}
			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Products getGroup(int groupPosition) {
			return this.products.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return this.products.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, final ViewGroup parent) {
			String headerTitle = products.get(groupPosition).getTitle();
			// String price = products.get(groupPosition).getPrice();
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_group, null);
			}
			RatingBar bar = (RatingBar) convertView
					.findViewById(R.id.ratingBar1);
			bar.setRating(products.get(groupPosition).getAverage_rating());
			final LinearLayout layout = (LinearLayout) convertView
					.findViewById(R.id.descLayout);
			layout.setTag("layout" + String.valueOf(groupPosition));
			TextView lblTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			lblTitle.setText(headerTitle);
			ImageView img = (ImageView) convertView
					.findViewById(R.id.priceimage);
			TextView lblPrice = (TextView) convertView
					.findViewById(R.id.productPrice);
			if (products.get(groupPosition).getPoints_for()
					.equals("brainbucks")) {
				img.setBackground(getResources().getDrawable(R.drawable.bucks));
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else if (products.get(groupPosition).getPoints_for()
					.equals("coins")) {
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else {
				lblPrice.setText("0");
			}
			lblPrice.setSelected(true);

			String desc = products.get(groupPosition).getDescription();
			TextView description = (TextView) convertView
					.findViewById(R.id.productLongDesc);
			description.setText(desc);
			final Button expand = (Button) convertView
					.findViewById(R.id.productExpand);
			expand.setTag("btn" + String.valueOf(groupPosition));
			expand.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (layout.getVisibility() == View.VISIBLE) {
						layout.setVisibility(View.GONE);
						expand.setBackground(getResources().getDrawable(
								R.drawable.down));
						Animation animation = AnimationUtils.loadAnimation(
								getApplicationContext(), R.anim.layout_anim_up);
						layout.startAnimation(animation);
					} else {
						expand.setBackground(getResources().getDrawable(
								R.drawable.up));
						layout.setVisibility(View.VISIBLE);
						Animation animation = AnimationUtils.loadAnimation(
								getApplicationContext(), R.anim.layout_anim);

						layout.startAnimation(animation);
					}
					try {
						if (lastPos != groupPosition) {
							LinearLayout lay = (LinearLayout) parent
									.findViewWithTag("layout"
											+ String.valueOf(lastPos));
							lay.setVisibility(View.GONE);
							Button btn = (Button) parent.findViewWithTag("btn"
									+ String.valueOf(lastPos));
							btn.setBackground(getResources().getDrawable(
									R.drawable.down));
							Animation animation1 = AnimationUtils
									.loadAnimation(getApplicationContext(),
											R.anim.layout_anim_up);
							lay.startAnimation(animation1);
							lastPos = groupPosition;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			final ImageView pic = (ImageView) convertView
					.findViewById(R.id.productImage);
			String url = products.get(groupPosition).getFeatured_src()
					.replace("https", "http");
			imageLoader.displayImage(url, pic, options,
					new SimpleImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							pic.setImageBitmap(loadedImage);
						}
					}, new ImageLoadingProgressListener() {

						@Override
						public void onProgressUpdate(String imageUri,
								View view, int current, int total) {

						}
					});
			convertView.setTag(groupPosition);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public void GetProducts() {
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[product_cat] ", "courses"));
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(BuyCourse.this, "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						try {
							JSONObject jsonObject = new JSONObject(response);
							ObjectMapper mapper = new ObjectMapper();
							products = new ArrayList<Products>();
							products = mapper
									.readValue(
											jsonObject.getJSONArray("products")
													.toString(),
											TypeFactory.defaultInstance()
													.constructCollectionType(
															List.class,
															Products.class));
							if (products.size() > 0) {
								noData.setVisibility(View.GONE);
								expandableListView.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyCourse.this, products);
								expandableListView.setAdapter(adapter);
							} else {
								expandableListView.setVisibility(View.GONE);
								noData.setVisibility(View.VISIBLE);
								adapter = new ExpandableListAdapter(
										BuyCourse.this, products);
								expandableListView.setAdapter(adapter);
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}
					}
				});
	}

}
