package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;

public class Registeration extends TDRBaseActivity implements OnClickListener,
		BackGroundTaskListener {

	Button submit;
	EditText editUname, editPassword, editFirstName, editLastName, editEmail,
			editCountry, editState;

	@Override
	protected void onCreate(Bundle savedState) {
		super.onCreate(savedState);
		setContentView(R.layout.register_screen);
		intializeComponent();
	}

	public void intializeComponent() {
		editUname = (EditText) findViewById(R.id.editUsername);
		editEmail = (EditText) findViewById(R.id.editEmail);
		editPassword = (EditText) findViewById(R.id.editPassword);
		editFirstName = (EditText) findViewById(R.id.editFirstName);
		editLastName = (EditText) findViewById(R.id.editLastName);
		editCountry = (EditText) findViewById(R.id.editCountry);
		editState = (EditText) findViewById(R.id.editState);
		submit = (Button) findViewById(R.id.btnSubmit);
		submit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == submit) {
			if (editUname.getText().toString() == null
					|| editUname.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter Username.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editPassword.getText().toString() == null
					|| editPassword.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter Password.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editFirstName.getText().toString() == null
					|| editFirstName.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter First Name.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editLastName.getText().toString() == null
					|| editLastName.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter Last Name.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editEmail.getText().toString() == null
					|| editEmail.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter email.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (!Utils.isValidEmail(editEmail.getText().toString())) {
				Toast.makeText(getApplicationContext(), "Enter Valid Email.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editState.getText().toString() == null
					|| editState.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter City.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editCountry.getText().toString() == null
					|| editCountry.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter Country.",
						Toast.LENGTH_LONG).show();
				return;
			} else {
				// new LoginTask().execute();
				List<NameValuePair> pair = new ArrayList<NameValuePair>();
				pair.add(new BasicNameValuePair("username", editUname.getText()
						.toString()));
				pair.add(new BasicNameValuePair("user_pass", editPassword
						.getText().toString()));
				pair.add(new BasicNameValuePair("first_name", editFirstName
						.getText().toString()));
				pair.add(new BasicNameValuePair("last_name", editLastName
						.getText().toString()));
				pair.add(new BasicNameValuePair("email", editEmail.getText()
						.toString()));
				pair.add(new BasicNameValuePair("country", editCountry
						.getText().toString()));
				pair.add(new BasicNameValuePair("state", editState.getText()
						.toString()));
				if (Utils.isNetworkAvailable(getApplicationContext())) {
					new BackGroudTask(Registeration.this, "Registering user!!",
							Constants.SERVER_URL + Constants.REGISTRATION_URL,
							WebServiceHandler.GET, pair, this);
				} else {
					Toast.makeText(getApplicationContext(),
							"Please connect to internet.", Toast.LENGTH_LONG)
							.show();
					return;
				}
			}
		}
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Registration Task Completed:" + response);

		if (response == null || response.equals("")) {
			Toast.makeText(getApplicationContext(),
					"Server Error.Please Try again after some time.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.getString("flag").equals("0")) {
					JSONArray array = jsonObject.getJSONArray("msg");
					Toast.makeText(getApplicationContext(), array.getString(0),
							Toast.LENGTH_SHORT).show();
				} else if (jsonObject.getString("flag").equals("1")) {
					Toast.makeText(getApplicationContext(),
							"You are successfully registered.",
							Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(Registeration.this,
							ThoughtDRMainActivity.class);
					startActivity(intent);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
