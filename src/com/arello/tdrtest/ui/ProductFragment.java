package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Products;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class ProductFragment extends Fragment implements BackGroundTaskListener {

	String slug = "";
	View rootView;
	ArrayList<Products> products;
	ExpandableListView expandableListView;
	ExpandableListAdapter adapter;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	int groupPos = 0;

	public ProductFragment(String slug) {
		// TODO Auto-generated constructor stub
		this.slug = slug;
		System.out.println("slug:" + this.slug);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.product_list_fragment, container,
				false);
		IntializeComponent();
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.product_icon)
				.showImageForEmptyUri(R.drawable.product_icon)
				.showImageOnFail(R.drawable.product_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("filter[product_cat] ", slug));
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(ProductFragment.this.getActivity(), "Please wait!!",
				Constants.PRODUCTS_URL, WebServiceHandler.GET, pair, this);
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		try {
			System.out.println("Product Task Completed for slug:" + slug
					+ ":Response:" + response);
			JSONObject jsonObject = new JSONObject(response);
			ObjectMapper mapper = new ObjectMapper();
			products = new ArrayList<Products>();
			products = mapper.readValue(jsonObject.getJSONArray("products")
					.toString(), TypeFactory.defaultInstance()
					.constructCollectionType(List.class, Products.class));
			if (products.size() > 0) {
				adapter = new ExpandableListAdapter(getActivity(), products);
				expandableListView.setAdapter(adapter);
			} else {
				LayoutInflater infalInflater = (LayoutInflater) this
						.getActivity().getSystemService(
								Context.LAYOUT_INFLATER_SERVICE);
				View convertView = infalInflater.inflate(R.layout.empty_view,
						null);
				expandableListView.setEmptyView(convertView);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void IntializeComponent() {
		expandableListView = (ExpandableListView) rootView
				.findViewById(R.id.lvExp);
	}

	public class ExpandableListAdapter extends BaseExpandableListAdapter {

		private Context _context;
		ArrayList<Products> products;

		public ExpandableListAdapter(Context context,
				ArrayList<Products> products) {
			this._context = context;
			this.products = products;
		}

		@Override
		public Products getChild(int groupPosition, int childPosititon) {
			return this.products.get(groupPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_item, null);
			}
			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Products getGroup(int groupPosition) {
			return this.products.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return this.products.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, final ViewGroup parent) {
			String headerTitle = products.get(groupPosition).getTitle();
			// String price = products.get(groupPosition).getPrice();
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_group, null);
			}
			RatingBar bar = (RatingBar) convertView
					.findViewById(R.id.ratingBar1);
			bar.setRating(products.get(groupPosition).getAverage_rating());
			final LinearLayout layout = (LinearLayout) convertView
					.findViewById(R.id.descLayout);
			// layout.setTag(groupPosition);
			TextView lblTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			lblTitle.setText(headerTitle);
			ImageView img = (ImageView) convertView
					.findViewById(R.id.priceimage);
			TextView lblPrice = (TextView) convertView
					.findViewById(R.id.productPrice);
			if (products.get(groupPosition).getPoints_for()
					.equals("brainbucks")) {
				img.setBackground(getResources().getDrawable(R.drawable.bucks));
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else if (products.get(groupPosition).getPoints_for()
					.equals("coins")) {
				lblPrice.setText(products.get(groupPosition).getPoints());
			} else {
				lblPrice.setText("0");
				// lblPrice.setText(Html.fromHtml("$" + price.split("\\.")[0]
				// + ".<sup><small>" + price.split("\\.")[1]
				// + "</small></sup>"));
				// lblPrice.setTextColor(Color.YELLOW);
			}
			lblPrice.setSelected(true);

			String desc = products.get(groupPosition).getDescription();

			TextView description = (TextView) convertView
					.findViewById(R.id.productLongDesc);
			description.setText(desc);

			final Button expand = (Button) convertView
					.findViewById(R.id.productExpand);
			// expand.setTag("btn" + groupPosition);
			expand.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (layout.getVisibility() == View.VISIBLE) {
						layout.setVisibility(View.GONE);
						expand.setBackground(getResources().getDrawable(
								R.drawable.down));
					} else {
						expand.setBackground(getResources().getDrawable(
								R.drawable.up));
						layout.setVisibility(View.VISIBLE);
					}
				}
			});
			final ImageView pic = (ImageView) convertView
					.findViewById(R.id.productImage);
			String url = products.get(groupPosition).getFeatured_src()
					.replace("https", "http");
			imageLoader.displayImage(url, pic, options,
					new SimpleImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {

						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							pic.setImageBitmap(loadedImage);
						}
					}, new ImageLoadingProgressListener() {

						@Override
						public void onProgressUpdate(String imageUri,
								View view, int current, int total) {

						}
					});
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}
}
