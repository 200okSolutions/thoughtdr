package com.arello.tdrtest.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.dialogs.SelectCategoryDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog.OnHideDialogListener;
import com.arello.tdrtest.uihelpers.adapters.MusicListAdapter;

public class AddAffirmationsActivity extends TDRBaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, OnItemClickListener
{
	TDRListView mListView;
	MusicListAdapter mAdapter;
	
	SelectCategoryDialog mSelectCategoryDialog;
	Music trackToAdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_affirmations_layout);
		mListView = (TDRListView) findViewById(R.id.music_list);
		mAdapter = new MusicListAdapter(this, null, true, false, mListView);
		mAdapter.setSelectedItems(getTDRApplication().getDataManager().getAllMusic());
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		
		mSelectCategoryDialog = new SelectCategoryDialog(this);
		
		mSelectCategoryDialog.setOnHideListenet(new OnHideDialogListener() {
			@Override
			public void onHide() {
				Affirmation aff = new Affirmation();
				String affName = trackToAdd.getName();
				if (affName == null || affName.trim().length() == 0) {
					affName = "New affirmation " + String.valueOf(getTDRApplication().getDataManager().getAffirmationsCount() + 1);
				}
				aff.setName(affName);
				aff.setSoundPath(trackToAdd.getSoundPath());
				aff.setCategory(mSelectCategoryDialog.getResult());
				aff.setSortOrder(0);
				getTDRApplication().getDataManager().addAffirmation(aff);
				finish();
			}
		});
		
		getSupportLoaderManager().initLoader(0, null, this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mAdapter.stopPlay();
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		String select = "(" + Media.IS_PODCAST + " =0)";
		String[] proj = { MediaStore.Audio.Media._ID,
				MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.TITLE, 
				MediaStore.Audio.Media.SIZE,  MediaStore.Audio.Media.ARTIST };
		return new CursorLoader(this, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, select, null, Media.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}
	
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	@Override
	public void onItemClick(AdapterView<?> listView, View view, int i, long id)
	{
		Cursor c = (Cursor) mAdapter.getItem(i);
		trackToAdd = Music.musicFromCursor(c);
		mSelectCategoryDialog.showDialog();
	}
}
