package com.arello.tdrtest.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.TDRApplication;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.service.PlayService;
import com.arello.tdrtest.service.PlayService.OnCurrentAffirmationChangedListener;
import com.arello.tdrtest.service.PlayService.OnCurrentMusicChangedListener;
import com.arello.tdrtest.service.PlayService.OnHibernationCoundDownListener;
import com.arello.tdrtest.service.PlayService.OnServiceStopListener;
import com.arello.tdrtest.uihelpers.Utils;

public class PlayActivity extends Activity implements
		OnCurrentAffirmationChangedListener, OnCurrentMusicChangedListener,
		OnSeekBarChangeListener, OnServiceStopListener,
		OnHibernationCoundDownListener {
	Handler mHandler = new Handler();
	Runnable mTimerTask = new Runnable() {

		@Override
		public void run() {
			updateTime();
			mHandler.postDelayed(mTimerTask, 1000);
		}
	};

	Runnable waitServiceTask = new Runnable() {

		@Override
		public void run() {
			if (getTDRApplication().getPlayService() == null) {
				mHandler.postDelayed(waitServiceTask, 100);
			} else {
				PlayActivity.this.registerListeners();
			}
		}
	};

	long snoozeTime;

	Runnable snoozeTimerTask = new Runnable() {

		@Override
		public void run() {
			updateSnoozeTime();
			snoozeTime--;
			mHandler.postDelayed(snoozeTimerTask, 1000);
		}
	};

	Runnable returnFromSnoozeTask = new Runnable() {

		@Override
		public void run() {
			returnFromSnooze();
		}
	};

	boolean mWithMusic;

	TextView mAffirmationTitle;
	TextView mMusicTitle;
	SeekBar mAffirmationVolume;
	SeekBar mMusicVolume;
	boolean mSnoozed = false;
	Button mSnoozeButton;
	View mSnoozeView;
	ImageView mOnAirView;
	boolean isHibernate;
	private boolean mRestartSoundPlayer = false;
	private boolean mRestartMusicPlayer = false;
	private TextView mCountDown;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setResult(RESULT_OK);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.play_layout);
		findViewById(R.id.stop_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						int result = Utils.SetRewardsPoint(
								getApplicationContext(),
								System.currentTimeMillis());
						if (result == 1 || result == 2) {
							AlertDialog dialog = new AlertDialog.Builder(
									PlayActivity.this)
									.setTitle(
											PlayActivity.this.getResources()
													.getString(
															R.string.app_name))
									.setNeutralButton(R.string.close_alert,
											null).create();
							if (result == 1) {
								dialog.setMessage("Got 8 points.");
							} else {
								dialog.setMessage("Got 100 points.");
							}
							dialog.setOnDismissListener(new OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
									// TODO Auto-generated method stub
									dialog.cancel();
									getTDRApplication().stopService();
									finish();
								}
							});
							getTDRApplication().stopService();
							finish();
							// dialog.show();
						} else {
							getTDRApplication().stopService();
							finish();
						}
					}
				});
		mOnAirView = (ImageView) findViewById(R.id.on_air_image);
		mWithMusic = getIntent().getBooleanExtra(Constants.START_WITH_MUSIC,
				false);
		if (!mWithMusic) {
			findViewById(R.id.music_info_layout).setVisibility(View.GONE);
		}

		PlayService playService = getTDRApplication().getPlayService();
		if (null != playService) {
			// was playing already
			if (playService.isMusicPlayerStopped()) {
				((ImageButton) findViewById(R.id.music_pause_button))
						.setImageResource(R.drawable.play_button);
			} else {
				((ImageButton) findViewById(R.id.music_pause_button))
						.setImageResource(R.drawable.pause_button);
			}

			if (playService.isSoundPlayerStopped()) {
				((ImageButton) findViewById(R.id.affirmation_pause_button))
						.setImageResource(R.drawable.play_button);
			} else {
				((ImageButton) findViewById(R.id.affirmation_pause_button))
						.setImageResource(R.drawable.pause_button);
			}
		}

		boolean dayMode = getIntent()
				.getBooleanExtra(Constants.DAY_MODE, false);

		getTDRApplication().startService(mWithMusic, dayMode);
		mAffirmationTitle = (TextView) findViewById(R.id.affirmation_title);
		mCountDown = (TextView) findViewById(R.id.count_down);
		mMusicTitle = (TextView) findViewById(R.id.music_title);
		mAffirmationVolume = (SeekBar) findViewById(R.id.affirmation_volume);
		mMusicVolume = (SeekBar) findViewById(R.id.music_volume);
		mAffirmationVolume.setProgress((int) (getTDRApplication().getSettings()
				.getAffirmationVolume() * 100));
		mMusicVolume.setProgress((int) (getTDRApplication().getSettings()
				.getMusicVolume() * 100));
		mAffirmationVolume.setOnSeekBarChangeListener(this);
		mMusicVolume.setOnSeekBarChangeListener(this);
		mSnoozeButton = (Button) findViewById(R.id.snooze_button);
		mSnoozeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				mSnoozed = !mSnoozed;

				if (mSnoozed) {
					goToSnooze();
				} else {
					returnFromSnooze();
				}
			}
		});
		mSnoozeView = findViewById(R.id.snooze_view);
		findViewById(R.id.music_prev_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (getTDRApplication().getPlayService() != null) {
							getTDRApplication().getPlayService().playPrev();
						}
					}
				});
		findViewById(R.id.music_next_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (getTDRApplication().getPlayService() != null) {
							getTDRApplication().getPlayService().playNext();
						}
					}
				});
		findViewById(R.id.music_pause_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						if (getTDRApplication().getPlayService() != null) {
							if (getTDRApplication().getPlayService()
									.toggleMusicPlayer()) {
								((ImageButton) findViewById(R.id.music_pause_button))
										.setImageResource(R.drawable.play_button);
							} else {
								((ImageButton) findViewById(R.id.music_pause_button))
										.setImageResource(R.drawable.pause_button);
							}

						}
					}
				});
		if (!dayMode) {
			findViewById(R.id.affirmation_player_controls).setVisibility(
					View.GONE);
		}
		findViewById(R.id.affirmation_prev_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (getTDRApplication().getPlayService() != null) {
							getTDRApplication().getPlayService()
									.playPrevAffirmation();
						}
					}
				});
		findViewById(R.id.affirmation_next_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (getTDRApplication().getPlayService() != null) {
							getTDRApplication().getPlayService()
									.playNextAffirmation();
						}
					}
				});
		findViewById(R.id.affirmation_pause_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						if (getTDRApplication().getPlayService() != null) {
							if (getTDRApplication().getPlayService()
									.toggleAffirmation()) {
								((ImageButton) findViewById(R.id.affirmation_pause_button))
										.setImageResource(R.drawable.play_button);
							} else {
								((ImageButton) findViewById(R.id.affirmation_pause_button))
										.setImageResource(R.drawable.pause_button);
							}

						}
					}
				});
	}

	public void goToSnooze() {
		try {
			Settings settings = new Settings(getApplicationContext());
			int time = settings.getHybernationTime();// 15
			mSnoozed = true;
			mSnoozeButton.setText(R.string.resume);
			getTDRApplication().getPlayService().snooze();
			mHandler.postDelayed(returnFromSnoozeTask, time * 60000);
			snoozeTime = (settings.getHybernationTime() * 60000) / 1000;
			updateSnoozeTime();
			Utils.hideViewWithAnimation(mOnAirView, R.anim.alpha_fade_out);
			Utils.showViewWithAnimation(mSnoozeView, R.anim.alpha_fade_in);
			mSnoozeView.setVisibility(View.VISIBLE);
			mHandler.post(snoozeTimerTask);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void returnFromSnooze() {
		try {
			mSnoozed = false;
			mSnoozeButton.setText(R.string.snooze);
			getTDRApplication().getPlayService().returnFromSnooze();
			Utils.showViewWithAnimation(mOnAirView, R.anim.alpha_fade_in);
			Utils.hideViewWithAnimation(mSnoozeView, R.anim.alpha_fade_out);
			mHandler.removeCallbacks(snoozeTimerTask);
			mHandler.removeCallbacks(returnFromSnoozeTask);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void registerListeners() {
		getTDRApplication().getPlayService().setServiceStopListener(this);
		Utils.showViewWithAnimation(mOnAirView, R.anim.alpha_fade_in);
		getTDRApplication().getPlayService()
				.setOnCurrentAffirmationChangedListener(this);
		getTDRApplication().getPlayService().setOnHibernationCoundDownListener(
				this);
		onAffirmationChanged(getTDRApplication().getPlayService()
				.getCurrentAffirmation());
		float av = (float) mAffirmationVolume.getProgress() / 100;
		float mv = (float) mMusicVolume.getProgress() / 100;
		float ssv = getTDRApplication().getPlayService().getSleepSoundVolume();
		getTDRApplication().getPlayService().changeVolume(av, mv, ssv);
		if (mWithMusic) {
			getTDRApplication().getPlayService()
					.setOnCurrentMusicChangedListenr(this);
			onMusicChanged(getTDRApplication().getPlayService()
					.getCurrentMusic());
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		mHandler.post(mTimerTask);
		mHandler.post(waitServiceTask);
	}

	@Override
	protected void onResume() {
		super.onResume();

		PlayService playService = getTDRApplication().getPlayService();

		if (playService != null && playService.isSnoozed()) {
			// mHandler.post(snoozeTimerTask);
		}

		if (playService != null && mRestartSoundPlayer
				&& playService.isSoundPlayerStopped()) {
			// playService.toggleAffirmation();
			mRestartSoundPlayer = false;
		}
		if (playService != null && mRestartMusicPlayer
				&& playService.isMusicPlayerStopped() && mWithMusic) {
			// playService.toggleMusicPlayer();
			mRestartMusicPlayer = false;
		}
	}

	@Override
	protected void onPause() {
		PlayService playService = getTDRApplication().getPlayService();
		if (playService != null && !playService.isSoundPlayerStopped()) {
			// playService.toggleAffirmation();
			mRestartSoundPlayer = true;
		}
		if (playService != null && !playService.isMusicPlayerStopped()
				&& mWithMusic) {
			// playService.toggleMusicPlayer();
			mRestartMusicPlayer = true;
		}
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mHandler.removeCallbacks(waitServiceTask);
		mHandler.removeCallbacks(mTimerTask);
		// mHandler.removeCallbacksAndMessages(null);
	}

	@Override
	public void onBackPressed() {
		getTDRApplication().stopService();
		super.onBackPressed();
	}

	protected void updateTime() {
		Time now = new Time();
		now.setToNow();

		if (getTDRApplication().getSettings().get24HChecked()) {
			((TextView) findViewById(R.id.time_text)).setText(now
					.format("%H:%M"));
		} else {
			((TextView) findViewById(R.id.time_text)).setText(now
					.format("%I:%M %P"));
		}
	}

	protected void updateSnoozeTime() {
		((TextView) findViewById(R.id.snooze_timer)).setText(String.format(
				"%02d:%02d", snoozeTime / 60, snoozeTime % 60));
	}

	protected TDRApplication getTDRApplication() {
		return ((TDRApplication) getApplication());
	}

	@Override
	public void onMusicChanged(Music currentMusic) {
		mMusicTitle.setText(currentMusic.getName()
				+ (currentMusic.getArtist() != null ? " - "
						+ currentMusic.getArtist() : ""));
	}

	@Override
	public void onAffirmationChanged(Affirmation currentAffirmation) {

		if (currentAffirmation == null) {
			if (getTDRApplication().getPlayService().isSleepSoundPlayed()) {
				mAffirmationVolume.setProgress((int) (getTDRApplication()
						.getPlayService().getSleepSoundVolume() * 100));
				mAffirmationTitle.setText(getResources().getString(
						R.string.hibernation));
			} else {
				mAffirmationTitle.setText(getResources().getString(
						R.string.hibernation));
			}
			System.out.println("complete list.");
		} else {
			mCountDown.setVisibility(View.GONE);
			mAffirmationVolume.setProgress((int) (getTDRApplication()
					.getPlayService().getSoundVolume() * 100));
			mAffirmationTitle.setText(currentAffirmation.getName());
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int value, boolean arg2) {
		float av = (float) mAffirmationVolume.getProgress() / 100;
		float mv = (float) mMusicVolume.getProgress() / 100;

		if (getTDRApplication().getPlayService() != null) {
			PlayService service = getTDRApplication().getPlayService();
			float ssv = service.getSleepSoundVolume();
			if (service.isSleepSoundPlayed()) {
				ssv = av;
				av = service.getSoundVolume();
			}
			getTDRApplication().getSettings().setMusicVolume(mv);
			getTDRApplication().getSettings().setAffirmationVolume(av);
			getTDRApplication().getSettings().setSleepSoundsVolume(ssv);
			getTDRApplication().getPlayService().changeVolume(av, mv, ssv);
		} else {
			getTDRApplication().getSettings().setMusicVolume(mv);
			getTDRApplication().getSettings().setAffirmationVolume(av);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
	}

	@Override
	public void onServiceStopped() {
		getTDRApplication().stopService();
		setResult(RESULT_CANCELED);
		finish();
	}

	@Override
	public void onHibernationCoundDown(int left, int max) {
		// mCountDown.setVisibility(View.GONE);
		// int result = max - left;
		// sec += Integer.toString(secC);
		/*
		 * if (result >= 60) { min = "01:"; result -= 60; } else { min = "00:";
		 * }
		 * 
		 * if (result < 10) { sec = "0" + result; } else { sec = "" + result; }
		 */
		// mCountDown.setText(min + ":" + sec);

		// @author:Kaushal Madani
		int result = max;
		int minC = result / 60;
		int secC = result % 60;

		String min = Integer.toString(minC);

		mCountDown.setText(String.format("%02d:%02d", Integer.parseInt(min),
				secC));
	}

	@Override
	public void onHibernationStart() {
		System.out.println("hibernation start");
		mCountDown.setVisibility(View.VISIBLE);
		isHibernate = true;
	}

	@Override
	public void onHibernationStop() {
		System.out.println("hibernation stop");
		mCountDown.setVisibility(View.GONE);
		isHibernate = false;
	}
}
