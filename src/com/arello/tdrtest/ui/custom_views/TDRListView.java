package com.arello.tdrtest.ui.custom_views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.*;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import com.arello.tdrtest.uihelpers.Utils;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public class TDRListView extends ListView implements OnGestureListener,
		OnDoubleTapListener {

	private View mDragView;
	private WindowManager mWindowManager;
	private WindowManager.LayoutParams mWindowParams;
	private int mDragPos; // which item is being dragged
	private int mFirstDragPos; // where was the dragged item originally
	private int mDragPoint; // at what offset inside the item did the user grab
							// it
	private int mCoordOffset; // the difference between screen coordinates and
								// coordinates in this view
	private MoveListener mMoveListener;
	private RemoveListener mRemoveListener;
	private DoubleClickListener mDoubleClickListener;
	private int mHeight;
	private GestureDetector mGestureDetector;
	private Bitmap mDragBitmap;
	private final int mTouchSlop;
	private View mDraggedCell;
	private int mUpperBound;
	private int mLowerBound;
	private Handler mHandler;
	private int mLastDragY;
	private Runnable mScrollUpTask = new Runnable() {

		@Override
		public void run() {
			if (mLastDragY != -1 && mLastDragY < mDraggedCell.getHeight()) {
				setSelectionFromTop(getFirstVisiblePosition() - 1, 0);
				postDelayed(mScrollUpTask, 300);
			}
		}
	};
	private Runnable mScrollDownTask = new Runnable() {

		@Override
		public void run() {
			if (mLastDragY != -1
					&& mLastDragY > (mHeight - mDraggedCell.getHeight())) {
				setSelectionFromTop(getFirstVisiblePosition() + 1, 0);
				postDelayed(mScrollDownTask, 300);
			}
		}
	};

	public TDRListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
		mGestureDetector = new GestureDetector(this);
		mGestureDetector.setOnDoubleTapListener(this);
		setSelector(android.R.color.transparent);
		setCacheColorHint(Color.TRANSPARENT);
		setDividerHeight(0);

		mWindowParams = new WindowManager.LayoutParams();
		mWindowParams.gravity = Gravity.TOP;
		mWindowParams.x = 0;

		mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
		mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
		mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
		mWindowParams.format = PixelFormat.TRANSLUCENT;
		mWindowParams.windowAnimations = 0;
	}

	/*
	 * Restore size and visibility for all listitems
	 */
	private void unExpandViews(boolean deletion) {
		int position = getFirstVisiblePosition();
		int y = getChildAt(0).getTop();
		setAdapter(getAdapter());
		setSelectionFromTop(position, y);
		// int count = getChildCount();
		// for (int i = 0;i < count; i++) {
		// View v = getChildAt(i);
		// if (v == null) {
		// if (deletion) {
		// // HACK force update of mItemCount
		// int position = getFirstVisiblePosition();
		// int y = getChildAt(0).getTop();
		// setAdapter(getAdapter());
		// setSelectionFromTop(position, y);
		// // end hack
		// }
		// } else {
		// v.setVisibility(View.VISIBLE);
		// v.clearAnimation();
		// }
		// }
	}

	LinkedList<Integer> animatedItems = new LinkedList<Integer>();

	protected void doExpansion(int itemnum) {
		int from = mFirstDragPos < itemnum ? mFirstDragPos : itemnum;
		from = from > getFirstVisiblePosition() ? from
				: getFirstVisiblePosition();
		from -= getFirstVisiblePosition();

		int to = mFirstDragPos > itemnum ? mFirstDragPos : itemnum;
		to = to < getLastVisiblePosition() ? to : getLastVisiblePosition();
		to -= getFirstVisiblePosition();
		float offset = (mFirstDragPos > itemnum ? mDraggedCell.getHeight()
				: -mDraggedCell.getHeight());

		View viewToAnimate = null;
		float fromY, toY;
		final int fItem;
		if ((mDragPos - itemnum) * (mFirstDragPos - itemnum) >= 0
				&& itemnum != mFirstDragPos) {
			fromY = 0;
			toY = offset;
			viewToAnimate = getChildAt(itemnum - getFirstVisiblePosition());
			fItem = itemnum;
		} else {
			viewToAnimate = getChildAt(mDragPos - getFirstVisiblePosition());
			fromY = itemnum != mFirstDragPos || (mDragPos - itemnum) > 0 ? offset
					: -offset;
			toY = 0;
			fItem = mDragPos;
		}
		if (viewToAnimate != null) {
			TranslateAnimation anim = new TranslateAnimation(
					Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
					Animation.ABSOLUTE, fromY, Animation.ABSOLUTE, toY);

			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					animatedItems.remove(new Integer(fItem));
				}
			});
			anim.setFillAfter(true);
			anim.setDuration(300);

			viewToAnimate.clearAnimation();
			viewToAnimate.startAnimation(anim);
			animatedItems.remove(new Integer(fItem));
			animatedItems.add(new Integer(fItem));
		}

		for (int i = 0; i < getChildCount(); i++) {
			int realPos = i + getFirstVisiblePosition();
			View child = getChildAt(i);
			child.setVisibility((realPos == mFirstDragPos ? View.INVISIBLE
					: View.VISIBLE));

			if (realPos == mFirstDragPos
					|| !mMoveListener.canMove(mFirstDragPos, realPos)) {
				child.clearAnimation();
				continue;
			} else if (animatedItems.contains(new Integer(realPos))) {
				continue;
			}

			if (i < from || i > to) {
				child.clearAnimation();
				animatedItems.remove(new Integer(realPos));
			} else {
				TranslateAnimation cellAnim = new TranslateAnimation(
						Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
						Animation.ABSOLUTE, offset, Animation.ABSOLUTE, offset);
				cellAnim.setFillAfter(true);
				cellAnim.setDuration(0);
				child.startAnimation(cellAnim);

			}
		}
		layoutChildren();
	}

	AtomicBoolean scrollLock = new AtomicBoolean(false);
	boolean isUpScrolling = false;
	boolean isDownScrolling = false;

	private void adjustScrollBounds(int y) {
		if (y >= mHeight / 3) {
			mUpperBound = mHeight / 3;
		}
		if (y <= mHeight * 2 / 3) {
			mLowerBound = mHeight * 2 / 3;
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		mGestureDetector.onTouchEvent(ev);
		mLastDragY = -1;
		int action = ev.getAction();
		if (mMoveListener != null && mDragView != null) {
			switch (action) {
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				stopDragging();
				if (mMoveListener != null && mDragPos >= 0
						&& mDragPos < getCount()) {
					mMoveListener.move(mFirstDragPos, mDragPos);
				}
				unExpandViews(false);
				scrollLock.set(false);

				break;

			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_MOVE:
				int x = (int) ev.getX();
				int y = (int) ev.getY();
				Rect r = new Rect();
				getHitRect(r);
				if (y < r.top)
					y = r.top;
				if (y > r.bottom)
					y = r.bottom;
				mLastDragY = y;
				dragView(x, y);
				int itemnum = getChildIndexAtY(y);
				if (itemnum >= 0) {
					if (action == MotionEvent.ACTION_DOWN
							|| itemnum != mDragPos) {

						if (mMoveListener.canMove(mFirstDragPos, itemnum)) {
							if (itemnum > mDragPos)
								for (int i = mDragPos + 1; i <= itemnum; i++) {
									doExpansion(i);
									mDragPos = i;
								}
							else
								for (int i = mDragPos - 1; i >= itemnum; i--) {
									doExpansion(i);
									mDragPos = i;
								}
						}
						if (y > mHeight - mDraggedCell.getHeight()
								&& !isDownScrolling) {
							isDownScrolling = true;
							postDelayed(mScrollDownTask, 300);
						} else {
							isDownScrolling = false;
						}

						if (y < mDraggedCell.getHeight() && !isUpScrolling) {
							isUpScrolling = true;
							postDelayed(mScrollUpTask, 300);
						} else {
							isUpScrolling = false;
						}
					}
				}
				break;
			}
			return true;
		}
		return super.onTouchEvent(ev);
	}

	/*
	 * Initiates vertical dragging for a bitmap
	 */
	private void startDragging(Bitmap bm, int y) {
		mWindowParams.y = y - mDragPoint + mCoordOffset;

		ImageView v = new ImageView(getContext());

		v.setBackgroundColor(Color.TRANSPARENT);
		v.setImageBitmap(bm);

		if (mDragBitmap != null) {
			mDragBitmap.recycle();
		}
		mDragBitmap = bm;

		mWindowManager = (WindowManager) getContext()
				.getSystemService("window");
		mWindowManager.addView(v, mWindowParams);
		mDragView = v;

	}

	@Override
	public boolean performItemClick(View view, int position, long id) {
		return false;
	}

	public void showDeleteButton(int y) {
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		params.gravity = Gravity.TOP;
		params.x = 0;
		params.y = y;

		params.height = WindowManager.LayoutParams.WRAP_CONTENT;
		params.width = WindowManager.LayoutParams.WRAP_CONTENT;
		params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
		params.format = PixelFormat.TRANSLUCENT;
		params.windowAnimations = 0;

		mWindowManager = (WindowManager) getContext()
				.getSystemService("window");
		// mWindowManager.addView(v, params);
	}

	/*
	 * Moves the dragged view to the given coordinates
	 */
	private void dragView(int x, int y) {
		mWindowParams.y = y - mDragPoint + mCoordOffset;
		mWindowManager.updateViewLayout(mDragView, mWindowParams);
	}

	/*
	 * Terminates dragging process
	 */
	private void stopDragging() {
		WindowManager wm = (WindowManager) getContext().getSystemService(
				"window");
		wm.removeView(mDragView);
		mDragView = null;
		mDraggedCell = null;
		/*
		 * if (mDragBitmap != null) { mDragBitmap.recycle(); mDragBitmap = null;
		 * }
		 */
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		if (e1 != null && e2 != null && velocityX > 1000
				&& mRemoveListener != null) {
			int y = (int) e1.getY();
			try {
				int cellindex = getChildIndexAtY(y);
				if (cellindex == getChildIndexAtY((int) e2.getY())) {
					mRemoveListener.remove(cellindex);
				}
			} catch (IndexOutOfBoundsException ex) {

			}
		}
		return false;
	}

	@Override
	public void onLongPress(MotionEvent ev) {
		if (mMoveListener != null) {
			int x = (int) ev.getX();
			int y = (int) ev.getY();
			int itemnum = getChildIndexAtY(y);
			if (itemnum == AdapterView.INVALID_POSITION) {
				return;
			}
			View item = getChildAtPosition(itemnum);
			if (item == null)
				return;

			mDragPoint = y - item.getTop();
			mCoordOffset = ((int) ev.getRawY()) - y;

			if (mMoveListener.canMove(itemnum, itemnum)) {
				Bitmap bitmap = Utils.drawViewToBitmap(item);
				startDragging(bitmap, y);
				mMoveListener.moveStarted(itemnum);
				mDraggedCell = item;
				mDraggedCell.setVisibility(INVISIBLE);
				mDragPos = itemnum;
				mFirstDragPos = mDragPos;
				mHeight = getHeight();
				int touchSlop = mTouchSlop;
				mUpperBound = Math.min(y - touchSlop, mHeight * 1 / 3);
				mLowerBound = Math.max(y + touchSlop, mHeight * 2 / 3);
				return;
			}
			mDragView = null;
		}
	}

	protected int getChildIndexAtY(int y) {
		if (mDraggedCell != null && y >= mDraggedCell.getTop()
				&& y <= mDraggedCell.getBottom())
			return mFirstDragPos;
		return pointToPosition(getLeft() + 1, y);
	}

	protected View getChildAtPosition(int itemnum) {
		if (itemnum == AdapterView.INVALID_POSITION) {
			return null;
		}
		return (View) getChildAt(itemnum - getFirstVisiblePosition());
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		if (getOnItemClickListener() != null) {
			int itemnum = getChildIndexAtY((int) e.getY());
			getOnItemClickListener().onItemClick(TDRListView.this,
					getChildAtPosition(itemnum), itemnum,
					getAdapter().getItemId(itemnum));
		}
		return false;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		if (mDoubleClickListener != null) {
			int itemnum = getChildIndexAtY((int) e.getY());
			mDoubleClickListener.onDoubleClick(itemnum,
					getChildAtPosition(itemnum));
		}
		return false;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent arg0) {
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent arg0) {
		return true;
	}

	public void setOnDoubleClickListener(DoubleClickListener l) {
		mDoubleClickListener = l;
	}

	public void setMoveListener(MoveListener l) {
		mMoveListener = l;
	}

	public void setRemoveListener(RemoveListener l) {
		mRemoveListener = l;
	}

	public interface DoubleClickListener {
		void onDoubleClick(int index, View view);
	}

	public interface MoveListener {
		void move(int from, int to);

		void moveStarted(int from);

		boolean canMove(int from, int to);
	}

	public interface RemoveListener {
		void remove(int which);
	}
}