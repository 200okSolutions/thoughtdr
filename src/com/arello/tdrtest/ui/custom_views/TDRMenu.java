package com.arello.tdrtest.ui.custom_views;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;

import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.thoughtdr.R;

public class TDRMenu extends TDRBaseDialog {
	protected static final int MENU_SHADOW_WIDTH = 5;
	protected static final int ANIMATION_DURATION = 300;

	ListView mMenuListView;
	ImageView mMenuShadow;

	ArrayList<String> mMenuItemsStrings = new ArrayList<String>();
	ArrayList<OnClickListener> mMenuClickListeners = new ArrayList<OnClickListener>();

	RelativeLayout mRootLayout;
	Point mLastShowPoint;
	
	public TDRMenu(TDRBaseActivity _activity)
	{
		super(_activity, false, true);
		mRootLayout = new RelativeLayout(mActivity);
	}

	protected void createMenu()
	{
		mMenuListView = new ListView(mActivity);
		mMenuListView.setDivider(new ColorDrawable(Color.GRAY));
		mMenuListView.setDividerHeight(1);
		mMenuListView.setAdapter(new ArrayAdapter<String>(mActivity, R.layout.menu_item_layout, mMenuItemsStrings));

		mMenuListView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				if (mMenuClickListeners.get(arg2) != null)
				{
					arg1.setTag(mMenuItemsStrings.get(arg2));
					mMenuClickListeners.get(arg2).onClick(arg1);
				}
				hideMenu();
			}
		});

		DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
		int listWidth = metrics.widthPixels / 2;

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(listWidth, LayoutParams.WRAP_CONTENT);
		params.setMargins(MENU_SHADOW_WIDTH, MENU_SHADOW_WIDTH, 0, 0);

		mMenuListView.setLayoutParams(params);
		if (mMenuShadow != null)
		{
			((BitmapDrawable) mMenuShadow.getDrawable()).getBitmap().recycle();
			mMenuShadow.setImageBitmap(null);
		}
		mMenuShadow = createShadowForMenu();
		mRootLayout.addView(mMenuShadow);
		mRootLayout.addView(mMenuListView);
	}

	public boolean isMenuVisible()
	{
		return mMenuListView != null && mRootLayout.getParent() != null;
	}

	@Override
	public void hideDialog() {
		if (!isMenuVisible() || mAnimationInProccess)
			return;
		super.hideDialog();
	}
	
	@Override
	protected void removeViews() {
		super.removeViews();
		DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
		float animationAnchorX = metrics.widthPixels/2 > mLastShowPoint.x ? 0.2f : 0.8f;
		float animationAnchorY = metrics.heightPixels/2 > mLastShowPoint.y ? 0.0f : 1.0f;
		Utils.startScaleAnimation(mRootLayout, mLastShowPoint, 1.0f, 0.3f, 1.0f, 0.0f, animationAnchorX, animationAnchorY, new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				mAnimationInProccess = true;
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				mAnimationInProccess = false;
				mActivity.getLayoutRoot().removeView(mRootLayout);
			}
		});
	}
	
	public void hideMenu()
	{
		hideDialog();
	}
	
	@Override
	public void showDialog() {
		super.showDialog();
		DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
		float animationAnchorX = metrics.widthPixels/2 > mLastShowPoint.x ? 0.2f : 0.8f;
		float animationAnchorY = metrics.heightPixels/2 > mLastShowPoint.y ? 0.0f : 1.0f;
		Utils.startScaleAnimation(mRootLayout, mLastShowPoint, 0.3f, 1.0f, 0.0f, 1.0f, animationAnchorX, animationAnchorY, new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				mAnimationInProccess = true;
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				mAnimationInProccess = false;
			}
		});
	}
	
	@Override
	protected void addViews() {
		super.addViews();
		mActivity.getLayoutRoot().addView(mRootLayout);
	}

	public void showMenu(Point coords)
	{
		RelativeLayout.LayoutParams params =
				new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
		int showPointX = metrics.widthPixels/2 > coords.x ? coords.x : coords.x - metrics.widthPixels/2;
		int showPointY = metrics.heightPixels/2 > coords.y ? coords.y : coords.y - metrics.heightPixels/2;
		
		params.setMargins(showPointX - MENU_SHADOW_WIDTH , showPointY - MENU_SHADOW_WIDTH , 0, 0);

		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

		mRootLayout.setLayoutParams(params);
		
		mLastShowPoint = coords;
		
		if (isMenuVisible() || mAnimationInProccess)
		{
			return;
		}
		if (mMenuListView == null)
		{
			createMenu();
		}
		showDialog();
	}

	public void addMenuItem(String text, OnClickListener clickListener)
	{
		mMenuListView = null;
		mMenuItemsStrings.add(text);
		mMenuClickListeners.add(clickListener);
	}

	public ImageView createShadowForMenu()
	{
		BlurMaskFilter blurFilter = new BlurMaskFilter(MENU_SHADOW_WIDTH, BlurMaskFilter.Blur.OUTER);
		Paint shadowPaint = new Paint();
		shadowPaint.setMaskFilter(blurFilter);

		int[] offsetXY = new int[2];

		Bitmap viewPicture = Bitmap.createBitmap(mMenuListView.getLayoutParams().width, ((int) mActivity.getResources()
		                                                                                              .getDimension(
				                                                                                              R.dimen.action_menu_item_height) +
		                                                                                mMenuListView
				                                                                                .getDividerHeight()) *
		                                                                               mMenuItemsStrings.size() -
		                                                                               mMenuListView.getDividerHeight(),
		                                         Bitmap.Config.ARGB_8888);
		Canvas canva = new Canvas(viewPicture);
		canva.drawColor(Color.BLACK);
		Bitmap shadowImage = viewPicture.extractAlpha(shadowPaint, offsetXY);

		ImageView image = new ImageView(mActivity);
		image.setImageBitmap(shadowImage);
		RelativeLayout.LayoutParams params =
				new RelativeLayout.LayoutParams(viewPicture.getWidth() + 2 * MENU_SHADOW_WIDTH,
				                                viewPicture.getHeight() + 2 * MENU_SHADOW_WIDTH);

		image.setLayoutParams(params);
		return image;
	}
}
