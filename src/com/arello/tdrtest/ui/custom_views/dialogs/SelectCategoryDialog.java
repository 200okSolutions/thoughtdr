package com.arello.tdrtest.ui.custom_views.dialogs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ArrayAdapter;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.ui.TDRBaseActivity;

import java.util.List;

public class SelectCategoryDialog extends TDRBaseDialog implements OnClickListener
{
	AlertDialog.Builder mDialogBuilder;
	List<Category> mCatList;
	Category mSelectedCategory;

	private EnterDialog mNewCategoryDialog;
	private Category mNewCategory;

	public SelectCategoryDialog(TDRBaseActivity context)
	{
		super(context, true, false);
		mCatList = mActivity.getTDRApplication().getDataManager().getAllCategories();

		mNewCategory = new Category();
		mNewCategory.setTitle(context.getString(R.string.new_category));
		mCatList.add(mNewCategory);

		mNewCategoryDialog = new EnterDialog(context, R.string.enter_category_name);
		mNewCategoryDialog.setEmptyError(R.string.empty_category_name_alert);
		mNewCategoryDialog.setOnEditorActionListener(new EnterDialog.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction()
			{

				Category newCategory = Category.createCategory(mNewCategoryDialog.getValue());
				mActivity.getTDRApplication().getDataManager().addNewCategory(newCategory);
				mSelectedCategory = newCategory;
				if (hideListener != null)
				{
					hideListener.onHide();
				}

				mNewCategoryDialog.dismiss();

				return false;
			}
		});
	}

	@Override
	public void showDialog()
	{
		mDialogBuilder = new AlertDialog.Builder(mActivity);
		mDialogBuilder
				.setAdapter(new ArrayAdapter<Category>(mActivity, R.layout.menu_item_layout, R.id.menu_item, mCatList),
				            this);
		mDialogBuilder.show();
	}

	@Override
	public void hideDialog()
	{

	}

	public Category getResult()
	{
		return mSelectedCategory;
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1)
	{
		mSelectedCategory = mCatList.get(arg1);
		if (mSelectedCategory == mNewCategory)
		{
			mNewCategoryDialog.show();
		}
		else if (hideListener != null)
		{
			hideListener.onHide();
		}
	}
}
