package com.arello.tdrtest.ui.custom_views.dialogs;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.uihelpers.Utils;

/**
 * Date: 18.05.12 Time: 17:41
 * 
 * @author Yuri Shmakov
 */
public class EnterDialog implements OnClickListener {
	private Context mContext;
	private Dialog mDialog;
	private TextView mEnterField;
	private OnDismissListener mDismissListener;
	private OnSubmitListener mSubmitListener;
	private OnDeleteListener mDeleteListener;
	private OnEditorActionListener mEditorActionListener;
	private int mEmptyError = 0;
	private Map<Pattern, Integer> mRules;
	private int mTheme = 0;

	public EnterDialog(Context context, int title) {
		this(context, context.getString(title));
	}

	public EnterDialog(Context context, int title, int theme) {
		this(context, context.getString(title), theme);
	}

	public EnterDialog(Context context, String title, int theme) {
		mTheme = theme;
		mContext = context;

		if (mTheme == 0) {
			mDialog = new Dialog(mContext, R.style.Theme_ThoughtDR_Dialog);
		} else {
			Log.e("EnterDialog", "using theme");
			mDialog = new Dialog(mContext, mTheme);
		}

		mDialog.setContentView(R.layout.enter_dialog_one);

		((TextView) mDialog.findViewById(R.id.title_text)).setText(title);
		mDialog.findViewById(R.id.delete_aff_button).setOnClickListener(this);
		mDialog.findViewById(R.id.delete_aff_button).setVisibility(View.GONE);
		mDialog.findViewById(R.id.submit).setOnClickListener(this);
		mDialog.findViewById(R.id.submit).setVisibility(View.GONE);

		mEnterField = (TextView) mDialog.findViewById(R.id.edit_text);
		mEnterField.getLayoutParams().width = context.getResources()
				.getDisplayMetrics().widthPixels
				- Utils.pixelsFromDp(context, 40);

		mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialogInterface) {
				// Delay need for slow handsets
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						InputMethodManager inputMethodManager = (InputMethodManager) mContext
								.getSystemService(Context.INPUT_METHOD_SERVICE);

						if (inputMethodManager != null) {
							inputMethodManager.toggleSoftInput(0,
									InputMethodManager.SHOW_IMPLICIT);
						}

						// Need for correct edit field position
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								String prevValue = mEnterField.getText()
										.toString();
								mEnterField.setText(prevValue + " ");
								mEnterField.setText(prevValue);
							}
						}, 260);
					}

				}, 100);
			}
		});

		mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialogInterface) {
				if (mEnterField != null) {
					mEnterField.setText("");
				}

				if (mDismissListener != null) {
					mDismissListener.onDismiss();
				}
			}
		});

		mEnterField
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int i,
							KeyEvent keyEvent) {
						int errorStringId = 0;
						if (mEmptyError > 0 && getValue().length() < 1) {
							errorStringId = mEmptyError;
						} else if (mRules != null) {
							for (Pattern pattern : mRules.keySet()) {
								if (!pattern.matcher(getValue()).matches()) {
									errorStringId = mRules.get(pattern);
									break;
								}
							}
						}

						if (errorStringId > 0) {
							AlertDialog dialog = new AlertDialog.Builder(
									mContext)
									.setMessage(
											mContext.getString(errorStringId))
									.setCancelable(true)
									.setNeutralButton(
											mContext.getString(R.string.close_alert),
											null).create();
							dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(
										DialogInterface dialogInterface) {
									InputMethodManager imm = (InputMethodManager) mContext
											.getSystemService(mContext.INPUT_METHOD_SERVICE);
									imm.showSoftInput(mEnterField,
											InputMethodManager.SHOW_IMPLICIT);
								}
							});
							dialog.show();

							return true;
						}

						return mEditorActionListener != null ? mEditorActionListener
								.onEditorAction() : false;
					}
				});
	}

	public EnterDialog(Context context, String title) {
		mContext = context;

		if (mTheme == 0) {
			mDialog = new Dialog(mContext, R.style.Theme_ThoughtDR_Dialog);
		} else {
			Log.e("EnterDialog", "using theme");
			mDialog = new Dialog(mContext, mTheme);
		}

		mDialog.setContentView(R.layout.enter_dialog_one);

		((TextView) mDialog.findViewById(R.id.title_text)).setText(title);
		mDialog.findViewById(R.id.delete_aff_button).setOnClickListener(this);
		mDialog.findViewById(R.id.delete_aff_button).setVisibility(View.GONE);
		mDialog.findViewById(R.id.submit).setOnClickListener(this);
		mDialog.findViewById(R.id.submit).setVisibility(View.GONE);

		mEnterField = (TextView) mDialog.findViewById(R.id.edit_text);
		mEnterField.getLayoutParams().width = context.getResources()
				.getDisplayMetrics().widthPixels
				- Utils.pixelsFromDp(context, 40);

		mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialogInterface) {
				// Delay need for slow handsets
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						InputMethodManager inputMethodManager = (InputMethodManager) mContext
								.getSystemService(Context.INPUT_METHOD_SERVICE);

						if (inputMethodManager != null) {
							inputMethodManager.toggleSoftInput(0,
									InputMethodManager.SHOW_IMPLICIT);
						}

						// Need for correct edit field position
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								String prevValue = mEnterField.getText()
										.toString();
								mEnterField.setText(prevValue + " ");
								mEnterField.setText(prevValue);
							}
						}, 260);
					}

				}, 100);
			}
		});

		mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialogInterface) {
				if (mEnterField != null) {
					mEnterField.setText("");
				}

				if (mDismissListener != null) {
					mDismissListener.onDismiss();
				}
			}
		});

		mEnterField
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int i,
							KeyEvent keyEvent) {
						int errorStringId = 0;
						if (mEmptyError > 0 && getValue().length() < 1) {
							errorStringId = mEmptyError;
						} else if (mRules != null) {
							for (Pattern pattern : mRules.keySet()) {
								if (!pattern.matcher(getValue()).matches()) {
									errorStringId = mRules.get(pattern);
									break;
								}
							}
						}

						if (errorStringId > 0) {
							AlertDialog dialog = new AlertDialog.Builder(
									mContext)
									.setMessage(
											mContext.getString(errorStringId))
									.setCancelable(true)
									.setNeutralButton(
											mContext.getString(R.string.close_alert),
											null).create();
							dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(
										DialogInterface dialogInterface) {
									InputMethodManager imm = (InputMethodManager) mContext
											.getSystemService(mContext.INPUT_METHOD_SERVICE);
									imm.showSoftInput(mEnterField,
											InputMethodManager.SHOW_IMPLICIT);
								}
							});
							dialog.show();

							return true;
						}

						return mEditorActionListener != null ? mEditorActionListener
								.onEditorAction() : false;
					}
				});
	}

	public void hideKeyboard() {
		((InputMethodManager) mContext
				.getSystemService(Activity.INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(mEnterField.getWindowToken(),
						InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	public void setOnEditorActionListener(OnEditorActionListener listener) {
		mEditorActionListener = listener;
	}

	public void setOnDismissListener(OnDismissListener listener) {
		mDismissListener = listener;
	}

	public void setValue(String value) {
		mEnterField.setText(value);
	}

	public String getValue() {
		return mEnterField.getText().toString().trim();
	}

	public void setEmptyError(int emptyError) {
		mEmptyError = emptyError;
	}

	public void setRules(Map<Pattern, Integer> patterns) {
		mRules = patterns;
	}

	public void addRule(Pattern pattern, int error) {
		if (mRules == null) {
			mRules = new HashMap<Pattern, Integer>();
		}

		mRules.put(pattern, error);
	}

	public void setInputType(int inputType) {
		mEnterField.setRawInputType(inputType);
		mEnterField.setInputType(inputType);
	}

	public void showDeleteButton() {
		mDialog.findViewById(R.id.delete_aff_button)
				.setVisibility(View.VISIBLE);
	}

	public void showSubmitButton() {
		mDialog.findViewById(R.id.submit).setVisibility(View.VISIBLE);
	}

	public void show() {
		mDialog.findViewById(R.id.delete_aff_button).setVisibility(View.GONE);
		mDialog.findViewById(R.id.submit).setVisibility(View.GONE);
		mDialog.show();
	}

	public void dismiss() {
		mDialog.dismiss();
	}

	public void cancel() {
		mDialog.cancel();
	}

	public interface OnDismissListener {
		public void onDismiss();
	}

	public interface OnEditorActionListener {
		public boolean onEditorAction();
	}

	public interface OnSubmitListener {
		public void onSubmit();
	}

	public interface OnDeleteListener {
		public void onDelete();
	}

	public OnDeleteListener getDeleteListener() {
		return mDeleteListener;
	}

	public void setDeleteListener(OnDeleteListener deleteListener) {
		mDeleteListener = deleteListener;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.delete_aff_button) {
			if (mDeleteListener != null) {
				mDeleteListener.onDelete();
			}
		} else if (v.getId() == R.id.submit) {
			if (mSubmitListener != null) {
				mSubmitListener.onSubmit();
			}
		}
	}

	public Button getDeleteButton() {
		return (Button) mDialog.findViewById(R.id.delete_aff_button);
	}

	public OnSubmitListener getOnSubmitListener() {
		return mSubmitListener;
	}

	public void setOnSubmitListener(OnSubmitListener submitListener) {
		mSubmitListener = submitListener;
	}

}
