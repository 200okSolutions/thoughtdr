package com.arello.tdrtest.ui.custom_views.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.ui.TDRBaseActivity;

import java.util.regex.Pattern;

public class EnterRecordNameDialog extends TDRBaseDialog {
	View mRootView;
	public EditText mTextEdit;
	private int mEmptyErrorStringId;
	private Pattern mPattern;
	private int mPatternError;

	public EnterRecordNameDialog(final TDRBaseActivity context, int titleStringResId, int emptyErrorStringId) {
		this(context);
		((TextView)mRootView.findViewById(R.id.title_text)).setText(titleStringResId);
		mEmptyErrorStringId = emptyErrorStringId;
	}
	
	public EnterRecordNameDialog(final TDRBaseActivity context) {
		super(context, false, false);

		mEmptyErrorStringId = R.string.empty_affirmation_name_alert;

		mRootView = View.inflate(mActivity, R.layout.enter_affirmation_name_dialog, null);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		mRootView.setLayoutParams(params);
		mTextEdit = (EditText) mRootView.findViewById(R.id.affirmation_name_text_edit);
		mTextEdit.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2)
			{
				int errorStringId = 0;
				if (getResult().length() == 0)
				{
					errorStringId = mEmptyErrorStringId;
				}
				else if (null != mPattern && !mPattern.matcher(getResult()).matches())
				{
					errorStringId = mPatternError;
				}

				if (errorStringId > 0)
				{
					AlertDialog dialog = new AlertDialog.Builder(context).setMessage(context.getString
						(errorStringId))
					       .setCancelable(true)
					       .setNeutralButton(context.getString(R.string.close_alert), null)
					       .create();
					dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
					{
						@Override
						public void onDismiss(DialogInterface dialogInterface)
						{
							InputMethodManager imm = (InputMethodManager) context
									.getSystemService(context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(mTextEdit, InputMethodManager.SHOW_IMPLICIT);
						}
					});
					dialog.show();

					return false;
				}

				hideDialog();
				return true;
			}
		});
	}
	
	public void setText(String text) {
		mTextEdit.setText(text);
	}

	public void setPattern(Pattern pattern, int patternError)
	{
		mPattern = pattern;
		mPatternError = patternError;
	}

	@Override
	public void showDialog() {
		super.showDialog();
		mActivity.getLayoutRoot().addView(mRootView);
		mRootView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.alpha_fade_in));
		mTextEdit.requestFocus();
		((InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE)).showSoftInput(mTextEdit, 0);
	}
	
	@Override
	public void hideDialog() {
		super.hideDialog();
		((InputMethodManager)mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mTextEdit.getWindowToken(), 0);
		mTextEdit.clearFocus();
		mRootView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.alpha_fade_out));
	}
	
	@Override
	protected void removeViews() {
		super.removeViews();
		mActivity.getLayoutRoot().removeView(mRootView);
	}
	
	public String getResult() {
		return mTextEdit.getText().toString().trim();
	}
}
