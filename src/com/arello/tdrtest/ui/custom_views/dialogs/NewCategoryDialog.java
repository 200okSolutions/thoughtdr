package com.arello.tdrtest.ui.custom_views.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;

import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.thoughtdr.R;

public class NewCategoryDialog extends TDRBaseDialog {
	Context mContext;
	AlertDialog.Builder mDialogBuilder;
	EditText mTextEdit;
	public NewCategoryDialog(TDRBaseActivity context) {
		super(context, true, false);
		mContext = context;
	}

	@Override
	public void showDialog() {
		mDialogBuilder = new AlertDialog.Builder(mContext);
		mDialogBuilder.setMessage(mContext.getResources().getString(R.string.enter_category_name));  
		mDialogBuilder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				if (hideListener != null)
					hideListener.onHide();
			}
		});
		mDialogBuilder.setNegativeButton(android.R.string.cancel, null);
		mDialogBuilder.setView(mTextEdit = new EditText(mContext));
		mDialogBuilder.show();
	}
	
	@Override
	public void hideDialog() {
		
	}
	
	public String getResult() {
		return mTextEdit.getText().toString();
	}
}
