package com.arello.tdrtest.ui.custom_views.dialogs;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;

import com.arello.tdrtest.ui.TDRBaseActivity;

public class TDRBaseDialog {
	public final static int ANIMATION_DURATION = 300;
	protected TDRBaseActivity mActivity;
	Button mFullscreenButton;
	AlphaAnimation mShowAnimation;
	AlphaAnimation mHideAnimation;

	protected boolean mAnimationInProccess = false;

	public TDRBaseDialog(TDRBaseActivity _activity, boolean withFade,
			boolean withExitOnFreeSpaceTap) {
		mActivity = _activity;
		mFullscreenButton = new Button(mActivity);
		mFullscreenButton.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		if (withExitOnFreeSpaceTap)
			mFullscreenButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					hideDialog();
				}
			});
		mFullscreenButton.setBackgroundResource(0);

		if (withFade) {
			mFullscreenButton.setBackgroundColor(Color.BLACK);
			mShowAnimation = new AlphaAnimation(0.0f, 0.7f);
			mShowAnimation.setFillAfter(true);
			mShowAnimation.setDuration(ANIMATION_DURATION);
			mShowAnimation.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation arg0) {
					mAnimationInProccess = true;
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
				}

				@Override
				public void onAnimationEnd(Animation arg0) {
					mAnimationInProccess = false;
				}
			});
			mHideAnimation = new AlphaAnimation(0.7f, 0.0f);
			mHideAnimation.setDuration(ANIMATION_DURATION);
			mHideAnimation.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					mAnimationInProccess = true;
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					mAnimationInProccess = false;
					new Handler().post(new Runnable() {
						
						@Override
						public void run() {
							removeViews();
						}
					});
					
				}
			});
			mFullscreenButton.setEnabled(withExitOnFreeSpaceTap);
		}
	}
	
	protected void addViewToRoot(View view) {
		if (view.getParent() == null)
			mActivity.getLayoutRoot().addView(view);
	}

	protected void addViews() {
		addViewToRoot(mFullscreenButton);
	}

	protected void removeViews() {
		mFullscreenButton.clearAnimation();
		mActivity.getLayoutRoot().removeView(mFullscreenButton);
		if (hideListener != null)
			hideListener.onHide();
	}

	public void hideDialog() {
		if (mAnimationInProccess)
			return;
		if (mHideAnimation != null) {
			mFullscreenButton.startAnimation(mHideAnimation);
		} else
			removeViews();
	}

	public void showDialog() {
		if (mAnimationInProccess)
			return;
		addViews();
		if (mShowAnimation != null) {
			mFullscreenButton.startAnimation(mShowAnimation);
		}
	}

	public boolean isDialogVisible() {
		return mFullscreenButton.getParent() != null;
	}

	OnHideDialogListener hideListener;

	public interface OnHideDialogListener {
		public void onHide();
	}

	public void setOnHideListenet(OnHideDialogListener _listener) {
		hideListener = _listener;
	}
}
