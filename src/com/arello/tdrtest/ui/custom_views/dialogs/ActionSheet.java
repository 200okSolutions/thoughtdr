package com.arello.tdrtest.ui.custom_views.dialogs;

import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.tdrtest.uihelpers.Utils;

public class ActionSheet extends TDRBaseDialog implements OnClickListener
{
	public static final int DESTRUCTIVE_BUTTON_INDEX = -1;

	int mResult;

	View mRootView;

	public ActionSheet(TDRBaseActivity context)
	{
		super(context, true, false);
		mRootView = View.inflate(mActivity, R.layout.action_sheet_layout, null);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		mRootView.setLayoutParams(params);
	}

	public ActionSheet setTitle(int resId)
	{
		((TextView) mRootView.findViewById(R.id.title_text)).setVisibility(View.VISIBLE);
		((TextView) mRootView.findViewById(R.id.title_text)).setText(resId);
		return this;
	}

	public ActionSheet setDestructiveButtonTitle(int resId)
	{
		Button destructiveButton = (Button) mRootView.findViewById(R.id.destructive_button);
		destructiveButton.setVisibility(View.VISIBLE);
		destructiveButton.setText(resId);
		destructiveButton.setTag(new Integer(DESTRUCTIVE_BUTTON_INDEX));
		destructiveButton.setOnClickListener(this);
		return this;
	}

	public ActionSheet setOtherButtons(int[] resIds)
	{
		for (int i = 0; i < resIds.length; ++i)
		{
			Button newButton = (Button) View.inflate(mActivity, R.layout.action_sheet_button, null);
			newButton.setText(resIds[i]);
			newButton.setTag(new Integer(i));
			newButton.setOnClickListener(this);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(Utils.pixelsFromDp(mActivity, 3), Utils.pixelsFromDp(mActivity, 3), Utils.pixelsFromDp(mActivity, 3), Utils.pixelsFromDp(mActivity, 3));
			newButton.setLayoutParams(params);
			((ViewGroup) mRootView.findViewById(R.id.buttons_container)).addView(newButton);
		}
		return this;
	}

	@Override
	public void showDialog()
	{
		super.showDialog();
		mRootView.setVisibility(View.VISIBLE);
		addViewToRoot(mRootView);
		mRootView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.action_sheet_show));
	}

	@Override
	protected void removeViews()
	{
		super.removeViews();
		mRootView.clearAnimation();
		mRootView.setVisibility(View.INVISIBLE);
		new Handler().post(new Runnable()
		{

			@Override
			public void run()
			{
				mActivity.getLayoutRoot().removeView(mRootView);
			}
		});

	}

	@Override
	public void hideDialog()
	{
		super.hideDialog();
		mRootView.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.action_sheet_hide));
	}

	@Override
	public void onClick(View button)
	{
		mResult = ((Integer) button.getTag());
		hideDialog();
	}

	public int getResult()
	{
		return mResult;
	}
}
