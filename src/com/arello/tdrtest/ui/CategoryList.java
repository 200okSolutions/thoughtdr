package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Categories;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.adapters.CategoriesListAdapter;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CategoryList extends TDRBaseActivity implements
		BackGroundTaskListener, OnItemClickListener {

	ListView categoryListView;
	Categories[] categories;
	CategoriesListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_list);
		categoryListView = (ListView) findViewById(R.id.categoryListView);
		categoryListView.setOnItemClickListener(this);
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("consumer_key", Constants.CONSUMER_KEY));
		pair.add(new BasicNameValuePair("consumer_secret",
				Constants.CONSUMER_SECRET));
		new BackGroudTask(CategoryList.this, "Getting categories",
				Constants.CATEGORY_URL, WebServiceHandler.GET, pair, this);
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Category task completed:" + response);
		try {
			JSONObject jsonObject = new JSONObject(response);
			ObjectMapper mapper = new ObjectMapper();
			categories = mapper.readValue(
					jsonObject.getJSONArray("product_categories").toString(),
					Categories[].class);
			adapter = new CategoriesListAdapter(getApplicationContext(),
					R.layout.category_title, categories);
			categoryListView.setAdapter(adapter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (categories[position].getCount().equals("0")) {
			Toast.makeText(getApplicationContext(), "No products available.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			startActivity(new Intent(CategoryList.this, ProductList.class)
					.putExtra("slug", categories[position].getSlug()));
		}
	}
}
