package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.model.Bucks;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.thoughtdr.R;
import com.arello.thoughtdr.util.IabHelper;
import com.arello.thoughtdr.util.IabResult;
import com.arello.thoughtdr.util.Inventory;
import com.arello.thoughtdr.util.Purchase;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class BucksActivity extends TDRBaseActivity implements
		OnItemClickListener, BackGroundTaskListener {

	GridView bucksListView;
	Settings settings;
	ArrayList<Bucks> bucksArrayList;
	BucksAdapter adapter;
	String strBucks, strCoins;
	TextView txtCoins, txtBucks;
	IabHelper mHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bucks_list);
		bucksListView = (GridView) findViewById(R.id.coinBuckGrid);
		txtCoins = (TextView) findViewById(R.id.txtCoin);
		txtBucks = (TextView) findViewById(R.id.txtBucks);
		new BackGroudTask(BucksActivity.this, "", Constants.BUCKS_URL,
				WebServiceHandler.GET, null, this);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		ConnectGoogleAPI();
	}

	public void ConnectGoogleAPI() {
		String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjF/Z0uzO+2SQQD2zZVK09kvt6lRXc9vZPV85R1gwAYB9g2ftPuJf+55xwYxspZrTmcauOE+BB+z9W4eF46gyBtGdtzwZ0NWBCLGvtynON2MUrhDqbF6g/zoeDfAcmA90nbCt3CvQrJT5b/Wc2o/rmoP/VgpZKylcLpmyHoaC4um87sK3NqvcjBFAeArNMsp0YAriEr7K3UkzDPaP1IytR+LUxMF0b82khPLm4Lidlr490ycPOXUCoARYde9iAN4oPgoJpJ2CjCItM+YcVdVpQUpua3kKeILB7A6QJCkkck6/Y3gBY4i8MovfN6yUlFgiwQ3zFqFqmM4wc/grrZUfsQIDAQAB";
		mHelper = new IabHelper(this, base64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					Log.d("ThoughtDr", "In-app Billing setup failed: " + result);
				} else {
					Log.d("ToughtDr", "In-app Billing is set up OK");
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mHelper != null)
			mHelper.dispose();
		mHelper = null;
	}

	public void GetPoints() {
		List<NameValuePair> pair1 = new ArrayList<NameValuePair>();
		pair1.add(new BasicNameValuePair("user_id", Utils.getSharedPreferences(
				Constants.USER_ID, getApplicationContext())));
		new BackGroudTask(BucksActivity.this, "Getting categories",
				Constants.GET_BRAIN_COIN_URL, WebServiceHandler.GET, pair1,
				new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						System.out.println("Task Completed:" + response);
						if (response == null || response.equals("")) {
							Toast.makeText(getApplicationContext(),
									"Server Error.", Toast.LENGTH_SHORT).show();
							return;
						} else {
							try {
								JSONObject jsonObject = new JSONObject(response);
								if (jsonObject.has("PointsData")) {
									strCoins = jsonObject.getJSONObject(
											"PointsData").getString(
											"brain_bucks_point");
									strBucks = jsonObject.getJSONObject(
											"PointsData").getString("points");
								} else {
									strBucks = "0";
									strCoins = "0";
								}
								txtBucks.setText(strCoins);
								txtCoins.setText(strBucks);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Bucks Task Completed:" + response);
		if (response == null || response.equals("")) {
			Toast.makeText(getApplicationContext(), "No data available.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.getString("flag").equals("1")) {
					bucksArrayList = new ArrayList<Bucks>();
					ObjectMapper objectMapper = new ObjectMapper();
					bucksArrayList = objectMapper.readValue(
							jsonObject.getJSONArray("BucksData").toString(),
							TypeFactory.defaultInstance()
									.constructCollectionType(List.class,
											Bucks.class));
					adapter = new BucksAdapter(getApplicationContext(),
							R.layout.buck_item, bucksArrayList);
					bucksListView.setAdapter(adapter);
					GetPoints();
				} else {
					Toast.makeText(getApplicationContext(), "Server Error..",
							Toast.LENGTH_SHORT).show();
					return;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	static class Holder {

		TextView textCategoriesName, amountNew, price, percen,
				includeFree = null;
		ImageView bestDealTag, popularTag;
		LinearLayout layoutToChange;
		FrameLayout perLayout, buyNow;
	}

	public class BucksAdapter extends ArrayAdapter<Bucks> {

		private ArrayList<Bucks> bucksArrayList;
		private LayoutInflater inflater = null;

		public BucksAdapter(Context context, int resource,
				ArrayList<Bucks> bucksArrayList) {
			super(context, resource, bucksArrayList);

			this.bucksArrayList = bucksArrayList;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			final Holder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.buck_item, null);
				holder = new Holder();
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.amountNew = detail(
					convertView,
					R.id.coinBuckAmountNew,
					String.valueOf(
							this.bucksArrayList.get(position).getOffer_coins())
							.equals("0") ? String.valueOf(this.bucksArrayList
							.get(position).getCoins()) : String
							.valueOf(this.bucksArrayList.get(position)
									.getOffer_coins()));
			holder.amountNew.setSelected(true);
			holder.price = (TextView) convertView.findViewById(R.id.coinPrice);
			holder.price.setText("$"
					+ this.bucksArrayList.get(position).getPrice());
			holder.popularTag = (ImageView) convertView
					.findViewById(R.id.popularTag);
			holder.bestDealTag = (ImageView) convertView
					.findViewById(R.id.bestDeal);
			holder.layoutToChange = (LinearLayout) convertView
					.findViewById(R.id.bgToChange);
			holder.perLayout = (FrameLayout) convertView
					.findViewById(R.id.percentageLayout);
			holder.buyNow = (FrameLayout) convertView.findViewById(R.id.side);
			holder.includeFree = (TextView) convertView
					.findViewById(R.id.includefreeView);

			if (this.bucksArrayList.get(position).getPopular().equals("1")) {
				holder.bestDealTag.setVisibility(View.GONE);
				holder.layoutToChange.setBackground(getResources().getDrawable(
						R.drawable.green_box));
				holder.perLayout.setVisibility(View.INVISIBLE);
				holder.includeFree.setVisibility(View.GONE);
				holder.popularTag.setVisibility(View.VISIBLE);
			} else if (this.bucksArrayList.get(position).getBest_deal()
					.equals("1")) {
				holder.bestDealTag.setVisibility(View.VISIBLE);
				holder.layoutToChange.setBackground(getResources().getDrawable(
						R.drawable.green_box));
				holder.perLayout.setVisibility(View.INVISIBLE);
				holder.popularTag.setVisibility(View.INVISIBLE);
				holder.includeFree.setVisibility(View.GONE);
			} else {
				if (!String.valueOf(
						this.bucksArrayList.get(position).getOffer_coins())
						.equals("0")) {
					double diff = Double.parseDouble(this.bucksArrayList.get(
							position).getOffer_coins())
							- Double.parseDouble(this.bucksArrayList.get(
									position).getCoins());
					double divide = diff
							/ Double.parseDouble(this.bucksArrayList.get(
									position).getCoins());
					double value = divide * 100;
					holder.percen = detail(convertView, R.id.percentage, "+"
							+ String.valueOf((int) value) + "%");
					holder.percen.setRotation(-45);
					if (this.bucksArrayList.get(position).getIncluded_free()
							.equals("0")) {
						holder.includeFree.setVisibility(View.GONE);
					} else {
						holder.includeFree.setVisibility(View.VISIBLE);
					}
				} else {
					holder.perLayout.setVisibility(View.GONE);
				}
				holder.popularTag.setVisibility(View.INVISIBLE);
				holder.bestDealTag.setVisibility(View.INVISIBLE);
				holder.layoutToChange.setBackground(getResources().getDrawable(
						R.drawable.black_box));
			}
			holder.buyNow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						BuyProduct("test1");
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SendIntentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			return convertView;
		}

		private TextView detail(View v, int resId, String text) {
			TextView tv = (TextView) v.findViewById(resId);
			tv.setText(text);
			return tv;
		}
	}

	public void BuyProduct(String productName) throws RemoteException,
			SendIntentException {
		mHelper.launchPurchaseFlow(this, "premium", 10001,
				mPurchaseFinishedListener, "mypurchasetoken");
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				// Handle error
				Toast.makeText(getApplicationContext(), "Item not purchased",
						Toast.LENGTH_SHORT).show();
				System.out.println("Error");
				return;
			} else if (purchase.getSku().equals("premium")) {
				System.out.println("Purchased");
				consumeItem();
				Toast.makeText(getApplicationContext(), "Item purchased",
						Toast.LENGTH_SHORT).show();
			}

		}
	};

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (!mHelper.handleActivityResult(arg0, arg1, arg2)) {
			super.onActivityResult(arg0, arg1, arg2);
		}

	}

	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}

	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {

			if (result.isFailure()) {
				// Handle failure
				System.out.println("error");
			} else {
				mHelper.consumeAsync(inventory.getPurchase("test1"),
						mConsumeFinishedListener);
			}
		}
	};
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {

			if (result.isSuccess()) {
				System.out.println("Purchase successsful.");
			} else {
				// handle error
				System.out.println("Purchase successsful.");
			}
		}
	};
}
