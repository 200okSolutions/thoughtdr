package com.arello.tdrtest.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.model.Products;

public class Cart extends TDRBaseActivity implements OnItemClickListener {

	ListView productListView;
	ProducstListAdapter adapter;
	String slug;
	Settings settings;
	ArrayList<Products> arrayList;
	HashMap<Integer, Products> cart;
	boolean isCartChange = false;
	TextView amount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cart);
		productListView = (ListView) findViewById(R.id.cartView);
		productListView.setOnItemClickListener(this);
		settings = new Settings(getApplicationContext());
		amount = (TextView) findViewById(R.id.totalAmount);
		cart = settings.getCart();
		arrayList = new ArrayList<Products>();
		arrayList.addAll(cart.values());
		SetAmount();
		adapter = new ProducstListAdapter(getApplicationContext(),
				R.layout.category_title, arrayList);
		productListView.setAdapter(adapter);
	}

	public void SetAmount() {
		double total = 0;
		for (Products products : cart.values()) {
			total += Double.valueOf(products.getPrice());
		}
		amount.setText("$" + String.valueOf(total));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	static class Holder {

		TextView textCategoriesName = null;
		Button buttton = null;
	}

	public class ProducstListAdapter extends ArrayAdapter<Products> {

		private Context context;
		ArrayList<Products> arrayList;
		private LayoutInflater inflater = null;

		public ProducstListAdapter(Context context, int resource,
				ArrayList<Products> arrayList) {
			super(context, resource, arrayList);

			this.context = context;
			this.arrayList = arrayList;

			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@SuppressWarnings("unused")
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			final Holder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.category_title, null);
				holder = new Holder();
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.textCategoriesName = detail(
					convertView,
					R.id.categoryTitle,
					String.valueOf(this.arrayList.get(position).getTitle()
							+ "\n" + this.arrayList.get(position).getPrice()));
			holder.buttton = (Button) convertView.findViewById(R.id.coinBuckPrice);
			holder.buttton.setVisibility(View.VISIBLE);
			holder.buttton.setText("Remove");

			holder.buttton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						// cart.remove(Integer.parseInt(arrayList.get(position)
						// .getId()));
						settings.removeProduct(Integer.parseInt(arrayList.get(
								position).getId()));
						arrayList.remove(position);
						isCartChange = true;
						if (arrayList.isEmpty()) {
							onBackPressed();
						} else {
							adapter.notifyDataSetChanged();
							productListView.invalidate();
							SetAmount();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			return convertView;

		}

		private TextView detail(View v, int resId, String text) {
			TextView tv = (TextView) v.findViewById(resId);
			tv.setText(text);
			return tv;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		System.out.println("on back pressed:" + isCartChange);
		finish();
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("change", isCartChange);
		setResult(RESULT_OK, intent);
		super.finish();
	}

}
