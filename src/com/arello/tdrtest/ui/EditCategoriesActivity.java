package com.arello.tdrtest.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.TDRDataManager.OnDataChangedListener;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.TDRListView.RemoveListener;
import com.arello.tdrtest.ui.custom_views.dialogs.ActionSheet;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog.OnHideDialogListener;

import java.util.List;

public class EditCategoriesActivity extends TDRBaseActivity
		implements OnItemClickListener, RemoveListener, OnDataChangedListener
{
	TDRListView mListView;
	List<Category> mCategories;
	Category mEditedCategory;
	ActionSheet mDeleteActionSheet;
	Category mCategoryToDelete;

	private EnterDialog mEnterNewCategoryDialog;
	private EnterDialog mEnterCategoryDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_categories);
		mCategories = getTDRApplication().getDataManager().getEditableCategories();
		mListView = (TDRListView) findViewById(R.id.cat_list);
		mListView.setAdapter(new CategoryAdapter());// ArrayAdapter<Category>(this,
		// R.layout.affirmation_cell,
		// R.id.title_text,
		// mCategories));
		mListView.setOnItemClickListener(this);
		mListView.setRemoveListener(this);
		getTDRApplication().getDataManager().setOnCategoryChangedLisener(this);

		mEnterNewCategoryDialog = new EnterDialog(this, R.string.enter_category_name);
		mEnterNewCategoryDialog.setEmptyError(R.string.empty_category_name_alert);
		mEnterNewCategoryDialog.setOnEditorActionListener(new EnterDialog.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction()
			{
				getTDRApplication().getDataManager()
						.addNewCategory(Category.createCategory(mEnterNewCategoryDialog.getValue()));

				mEnterNewCategoryDialog.dismiss();

				return false;
			}
		});
		findViewById(R.id.add_new_button).setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				mEnterNewCategoryDialog.show();
			}
		});

		mEnterCategoryDialog = new EnterDialog(this, R.string.enter_category_name);
		mEnterCategoryDialog.setEmptyError(R.string.empty_category_name_alert);
		mEnterCategoryDialog.setOnEditorActionListener(new EnterDialog.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction()
			{
				getTDRApplication().getDataManager().renameCategory(mEditedCategory, mEnterCategoryDialog.getValue());

				mEnterCategoryDialog.dismiss();

				return false;
			}
		});

		mDeleteActionSheet = new ActionSheet(this).setDestructiveButtonTitle(R.string.delete)
		                                          .setOtherButtons(new int[]{android.R.string.cancel});

		mDeleteActionSheet.setOnHideListenet(new OnHideDialogListener()
		{

			@Override
			public void onHide()
			{
				if (mDeleteActionSheet.getResult() == ActionSheet.DESTRUCTIVE_BUTTON_INDEX)
				{
					getTDRApplication().getDataManager().removeCategory(mCategoryToDelete);
				}
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> listView, View view, int itemIndex, long itemId)
	{
		mEditedCategory = (Category) listView.getAdapter().getItem(itemIndex);
		mEnterCategoryDialog.setValue(mEditedCategory.getTitle());
		mEnterCategoryDialog.show();
	}

	@Override
	public void remove(int which)
	{
		mCategoryToDelete = mCategories.get(which);
		mDeleteActionSheet.showDialog();
	}

	@Override
	public void onDataChanged()
	{
		mCategories = getTDRApplication().getDataManager().getEditableCategories();
		mListView.setAdapter(new ArrayAdapter<Category>(this, R.layout.affirmation_cell, R.id.title_text, mCategories));
	}

	public class CategoryAdapter extends ArrayAdapter<Category>
	{
		public CategoryAdapter()
		{
			super(EditCategoriesActivity.this, R.layout.affirmation_cell, R.id.title_text,
			      EditCategoriesActivity.this.mCategories);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			convertView = super.getView(position, convertView, parent);
			// EditText titleText =
			// (EditText)convertView.findViewById(R.id.title_text);
			// convertView.setTag(getItem(position));
			return convertView;
		}
	}

}
