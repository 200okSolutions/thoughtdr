package com.arello.tdrtest.ui;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.thoughtdr.GCMTask;
import com.arello.thoughtdr.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class Login extends Activity implements OnClickListener,
		BackGroundTaskListener, ConnectionCallbacks, OnConnectionFailedListener {

	ImageButton fbLogin, twitterLogin, googleLogin;
	EditText editEmail, editPassword;
	TextView forgotPass, register;
	CheckBox rememberMe;
	CallbackManager callbackManager;
	ProgressDialog dialog;
	Twitter twitter;
	RequestToken requestToken = null;
	AccessToken accessToken;
	String TWITTER_CONSUMER_KEY = "3y4yFoEshObo1f61t9CEQ7MQT";
	String TWITTER_CONSUMER_SECRET = "6mIDnY2yVtgGGKAQIXrF6EnSHMcqqnHMJX7UGYqgBEz7Tyh2dJ";
	Dialog auth_dialog;
	WebView web;
	String oauth_url, oauth_verifier, profile_url;
	String access_token, secret;
	SharedPreferences pref;
	GoogleApiClient mGoogleApiClient;
	ConnectionResult mConnectionResult;
	private boolean mIntentInProgress;
	private boolean mSignInClicked;
	private boolean isGetProfile = false;
	Dialog registrationDialog;
	Button submit;
	GCMTask task;

	@Override
	protected void onCreate(Bundle savedState) {
		super.onCreate(savedState);
		FacebookSdk.sdkInitialize(getApplicationContext());
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		System.out.println("height:" + height + "\nwidth" + width);
		if (height >= 1440) {
			setContentView(R.layout.large_login_screen);
		} else {
			setContentView(R.layout.login_screen);
		}

		PackageInfo info;
		try {
			info = getPackageManager().getPackageInfo("com.arello.tdrtest",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				Log.e("hash key", something);
				System.out.println("hash key:" + something);
			}
		} catch (NameNotFoundException e1) {
			Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}
		task = new GCMTask(getApplicationContext());
		task.registerClient();
		registrationId = Utils.getSharedPreferences(Constants.REGISTRATION_ID,
				getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		pref = getPreferences(0);
		if (Utils.getSharedPreferences(Constants.IS_LOGGED_IN,
				getApplicationContext()) != null) {
			if (Utils.getSharedPreferences(Constants.IS_LOGGED_IN,
					getApplicationContext()).equals("0")) {
				intializeComponent();
			} else {
				Intent intent = new Intent(Login.this,
						ThoughtDRMainActivity.class);
				startActivity(intent);
				finish();
			}
		} else {
			intializeComponent();
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	String registrationId;

	public void getRegistrationId() {
		if (registrationId == null || registrationId.equals("")) {
			System.out.println("Registration Id is null in Login.");
			if (task.checkPlayServices()) {
				System.out.println("Google Play Services Available.");
				if (Utils.isNetworkAvailable(getApplicationContext())) {
					registrationId = task.registerClient();
				} else {
					Toast.makeText(getApplicationContext(),
							"Please conenct to internet.", Toast.LENGTH_LONG)
							.show();
					return;
				}
			} else {
				System.out.println("Google Play Services not Available.");
				Toast.makeText(getApplicationContext(),
						"Update google services.", Toast.LENGTH_LONG).show();
				return;
			}
		}
		if (registrationId == null || registrationId.equals("")) {
			registrationId = Utils.getSharedPreferences(
					Constants.REGISTRATION_ID, getApplicationContext());
			if (registrationId == null || registrationId.equals("")) {
				while (registrationId == null || registrationId.equals("")) {
					registrationId = Utils.getSharedPreferences(
							Constants.REGISTRATION_ID, getApplicationContext());
					if (registrationId != null)
						break;
					System.out.println("Loop");
				}
			}
			registerDevice();
		} else {
			registerDevice();
		}
	}

	public void registerDevice() {
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("user_id", Utils.getSharedPreferences(
				Constants.USER_ID, getApplicationContext())));
		pair.add(new BasicNameValuePair("device_platform", "android"));
		pair.add(new BasicNameValuePair("device_regis_id", registrationId));
		new BackGroudTask(Login.this, "", Constants.REGISTER_DEVICE_ID,
				WebServiceHandler.GET, pair, new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						try {
							System.out.println("response of device id:"
									+ response);
							JSONObject object = new JSONObject(response);
							if (object.getString("flag").equals("1")) {
								ChangeActivity();
							} else {
								Toast.makeText(getApplicationContext(),
										"Server Error.", Toast.LENGTH_SHORT)
										.show();
								return;
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
	}

	public void ChangeActivity() {
		Intent intent = new Intent(Login.this, ThoughtDRMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void IntializeFaceBook() {
		List<String> permessions = new ArrayList<String>();
		permessions.add("email");
		permessions.add("public_profile");
		LoginManager.getInstance().logInWithReadPermissions(this, permessions);
		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {

					@Override
					public void onSuccess(LoginResult result) {
						// TODO Auto-generated method stub
						result.getAccessToken();
						GraphRequest request = GraphRequest.newMeRequest(
								result.getAccessToken(),
								new GraphRequest.GraphJSONObjectCallback() {

									@Override
									public void onCompleted(JSONObject object,
											GraphResponse response) {
										// TODO Auto-generated method stub
										try {
											String name = object
													.getString("name");
											String email = object
													.getString("email");
											String id = object.getString("id");
											if (dialog.isShowing()) {
												dialog.cancel();
											}
											RegisterUsingSocialMedia("fb", id,
													name, email, " ", " ");
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								});
						Bundle parameters = new Bundle();
						parameters.putString("fields",
								"id,name,email,gender, birthday");
						request.setParameters(parameters);
						request.executeAsync();
					}

					@Override
					public void onError(FacebookException error) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub

					}
				});
	}

	public void intializeComponent() {
		editEmail = (EditText) findViewById(R.id.editUSername);
		editPassword = (EditText) findViewById(R.id.editPassowrd);
		submit = (Button) findViewById(R.id.btnLogin);
		submit.setOnClickListener(this);
		forgotPass = (TextView) findViewById(R.id.txtForgotPass);
		register = (TextView) findViewById(R.id.btnRegister);
		fbLogin = (ImageButton) findViewById(R.id.fbLogin);
		fbLogin.setOnClickListener(this);
		twitterLogin = (ImageButton) findViewById(R.id.twitterLogin);
		twitterLogin.setOnClickListener(this);
		googleLogin = (ImageButton) findViewById(R.id.googleLogin);
		googleLogin.setOnClickListener(this);
		forgotPass.setOnClickListener(this);
		register.setOnClickListener(this);
		rememberMe = (CheckBox) findViewById(R.id.isRemember);
		if (Utils.getSharedPreferences(Constants.IS_REMEMBER,
				getApplicationContext()) != null) {
			if (Utils.getSharedPreferences(Constants.IS_REMEMBER,
					getApplicationContext()).equals("1")) {
				editEmail.setText(Utils.getSharedPreferences(
						Constants.USER_NAME, getApplicationContext()));
				editPassword.setText(Utils.getSharedPreferences(
						Constants.PASSWORD, getApplicationContext()));
				rememberMe.setChecked(true);
			}
		}
	}

	private void showWaitdialog() {
		dialog = new ProgressDialog(Login.this);
		dialog.setMessage("Please wait!!!");
		dialog.setCancelable(false);
		dialog.show();
	}

	public void twitterLogin() {
		twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
		new TokenGet().execute();
	}

	public void RegisterUsingSocialMedia(String socialType, String id,
			String name, String email, String uname, String country) {
		List<NameValuePair> pair = new ArrayList<NameValuePair>();
		pair.add(new BasicNameValuePair("social_type", socialType));
		pair.add(new BasicNameValuePair("social_id", id));
		pair.add(new BasicNameValuePair("name", name));
		pair.add(new BasicNameValuePair("email", email));
		pair.add(new BasicNameValuePair("username", uname));
		pair.add(new BasicNameValuePair("country", country));
		new BackGroudTask(Login.this, "", Constants.SOCIAL_MEDIA_URL,
				WebServiceHandler.GET, pair, new BackGroundTaskListener() {

					@Override
					public void onCompleteTask(String response) {
						// TODO Auto-generated method stub
						System.out.println("social media task completed:"
								+ response);
						try {
							String id = "";
							JSONObject jsonObject = new JSONObject(response);
							if (jsonObject.getString("flag").equals("1")) {
								id = jsonObject.getString("UserId");
							} else if (jsonObject.getString("flag").equals("3")) {
								JSONObject object = jsonObject
										.getJSONObject("userdetails");
								id = object.getString("ID");
							} else if (jsonObject.getString("flag").equals("2")) {
								JSONObject object = jsonObject
										.getJSONObject("userdetails");
								id = object.getString("ID");
							} else {
								Toast.makeText(getApplicationContext(),
										"Server Error.", Toast.LENGTH_SHORT)
										.show();
								return;
							}
							Utils.saveSharedPreferences(Constants.USER_ID, id,
									getApplicationContext());
							Utils.saveSharedPreferences(Constants.IS_LOGGED_IN,
									"1", getApplicationContext());
							getRegistrationId();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});

	}

	private class TokenGet extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... args) {
			try {
				requestToken = twitter.getOAuthRequestToken();
				oauth_url = requestToken.getAuthorizationURL();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return oauth_url;
		}

		@Override
		protected void onPostExecute(String oauth_url) {
			if (oauth_url != null) {
				auth_dialog = new Dialog(Login.this);
				auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				auth_dialog.setContentView(R.layout.twitter_dialog);
				web = (WebView) auth_dialog.findViewById(R.id.webView1);
				web.getSettings().setJavaScriptEnabled(true);
				web.loadUrl(oauth_url + "&force_login=true");
				web.setWebViewClient(new WebViewClient() {
					boolean authComplete = false;

					@Override
					public void onPageStarted(WebView view, String url,
							Bitmap favicon) {
						super.onPageStarted(view, url, favicon);
					}

					@Override
					public void onPageFinished(WebView view, String url) {
						super.onPageFinished(view, url);
						if (url.contains("oauth_verifier")
								&& authComplete == false) {
							authComplete = true;
							Uri uri = Uri.parse(url);
							oauth_verifier = uri
									.getQueryParameter("oauth_verifier");
							auth_dialog.dismiss();
							new AccessTokenGet().execute();
						} else if (url.contains("denied")) {
							auth_dialog.dismiss();
							Toast.makeText(getApplicationContext(),
									"Sorry !, Permission Denied",
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				auth_dialog.show();
				auth_dialog.setCancelable(true);
			} else {
				Toast.makeText(getApplicationContext(),
						"Sorry !, Network Error or Invalid Credentials",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private class AccessTokenGet extends AsyncTask<String, String, Boolean> {

		String name = "";
		String id = "";
		String screenName = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(Login.this);
			dialog.setMessage("Please wait...");
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setIndeterminate(true);
			dialog.show();
		}

		@Override
		protected Boolean doInBackground(String... args) {
			try {
				accessToken = twitter.getOAuthAccessToken(requestToken,
						oauth_verifier);
				SharedPreferences.Editor edit = pref.edit();
				edit.putString("ACCESS_TOKEN", accessToken.getToken());
				edit.putString("ACCESS_TOKEN_SECRET",
						accessToken.getTokenSecret());
				access_token = accessToken.getToken();
				secret = accessToken.getTokenSecret();
				User user = twitter.showUser(accessToken.getUserId());
				name = user.getName();
				id = String.valueOf(user.getId());
				screenName = user.getScreenName();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean response) {
			System.out.println("on post execute.");
			dialog.cancel();
			auth_dialog.cancel();
			RegisterUsingSocialMedia("tw", id, name, "-", screenName, "-");
		}
	}

	private void showDialog() {
		registrationDialog = new Dialog(Login.this,
				R.style.Theme_ThoughtDR_DialogCustomAnimation);
		registrationDialog.setContentView(R.layout.enter_dialog);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = registrationDialog.getWindow();
		lp.copyFrom(window.getAttributes());
		// This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		registrationDialog.show();
		final EditText emailEdit = (EditText) registrationDialog
				.findViewById(R.id.edit_text);
		final Button submit, cancel;
		submit = (Button) registrationDialog.findViewById(R.id.btnSubmit);
		cancel = (Button) registrationDialog.findViewById(R.id.cancel);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (emailEdit.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Please enter email.", Toast.LENGTH_SHORT).show();
					return;
				} else {
					String email = emailEdit.getText().toString();
					if (Utils.isValidEmail(email)) {
						List<NameValuePair> pair = new ArrayList<NameValuePair>();
						pair.add(new BasicNameValuePair("email", email));
						if (Utils.isNetworkAvailable(getApplicationContext())) {
							new BackGroudTask(Login.this, "Sending Request!!!",
									Constants.SERVER_URL
											+ Constants.FORGOT_PASSWORD_URL,
									WebServiceHandler.GET, pair,
									new BackGroundTaskListener() {

										@Override
										public void onCompleteTask(
												String response) { // TODO
											System.out
													.println("Forgot Password Task Completed:"
															+ response);
											if (response == null
													|| response.equals("")) {
												Toast.makeText(
														getApplicationContext(),
														"Server Error.",
														Toast.LENGTH_SHORT)
														.show();
												return;
											} else {
												try {
													JSONObject jsonObject = new JSONObject(
															response);
													if (jsonObject.getString(
															"flag").equals("0")) {
														JSONArray array = jsonObject
																.getJSONArray("msg");
														Toast.makeText(
																getApplicationContext(),
																array.getString(0),
																Toast.LENGTH_SHORT)
																.show();
														return;
													} else {
														Toast.makeText(
																getApplicationContext(),
																jsonObject
																		.getString("msg"),
																Toast.LENGTH_SHORT)
																.show();
													}
												} catch (JSONException e) { // TODO
																			// Auto-generated
																			// catch
																			// //
																			// block
													e.printStackTrace();
												}
											}
										}
									});
						} else {
							Toast.makeText(getApplicationContext(),
									"Please connect to internet..",
									Toast.LENGTH_SHORT).show();
						}
						registrationDialog.cancel();
					} else {
						Toast.makeText(getApplicationContext(),
								"Please enter valid email.", Toast.LENGTH_SHORT)
								.show();
					}
				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				registrationDialog.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (arg0 == 111) {
			if (arg1 != RESULT_OK) {
				mSignInClicked = false;
			}
			mIntentInProgress = false;
			if (!mGoogleApiClient.isConnecting()) {
				System.out.println("not connected!!");
				mGoogleApiClient.connect();
				showWaitdialog();
			} else {
				getProfileInformation();
			}
		} else if (arg1 == RESULT_OK) {
			showWaitdialog();
			callbackManager.onActivityResult(arg0, arg1, arg2);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == submit) {
			if (editEmail.getText().toString() == null
					|| editEmail.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter username.",
						Toast.LENGTH_LONG).show();
				return;
			} else if (editPassword.getText().toString() == null
					|| editPassword.getText().toString().equals("")) {
				Toast.makeText(getApplicationContext(), "Enter Password.",
						Toast.LENGTH_LONG).show();
				return;
			} else {
				if (Utils.isNetworkAvailable(getApplicationContext())) {
					List<NameValuePair> pair = new ArrayList<NameValuePair>();
					pair.add(new BasicNameValuePair("username", editEmail
							.getText().toString()));
					pair.add(new BasicNameValuePair("password", editPassword
							.getText().toString()));
					new BackGroudTask(Login.this, "Authenticating user!!",
							Constants.SERVER_URL + Constants.LOGIN_URL,
							WebServiceHandler.GET, pair, this);
				} else {
					Toast.makeText(getApplicationContext(),
							"Please connect to internet.", Toast.LENGTH_LONG)
							.show();
					return;
				}
			}
		} else if (v == forgotPass) {
			showDialog();
		} else if (v == register) {
			// startActivity(new Intent(Login.this, Registeration.class));
			registrationDialog = new Dialog(Login.this,
					R.style.Theme_ThoughtDR_DialogCustomAnimation);
			registrationDialog.setContentView(R.layout.register_screen);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			Window window = registrationDialog.getWindow();
			lp.copyFrom(window.getAttributes());
			// This makes the dialog take up the full width
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
			window.setAttributes(lp);
			registrationDialog.show();
			Registration();
		} else if (v == fbLogin) {
			try {
				if (Utils.isNetworkAvailable(getApplicationContext())) {
					IntializeFaceBook();
				} else {
					Toast.makeText(getApplicationContext(),
							"Please Connect To Internet.", Toast.LENGTH_SHORT)
							.show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (v == twitterLogin) {
			LoginManager.getInstance().logOut();
			twitterLogin();
		} else if (v == googleLogin) {
			if (Utils.isNetworkAvailable(getApplicationContext())) {
				if (mGoogleApiClient.isConnected()) {
					getProfileInformation();
				} else {
					signInWithGplus();
					mGoogleApiClient.connect();
				}
			} else {
				Toast.makeText(getApplicationContext(),
						"Please connect to internet", Toast.LENGTH_SHORT)
						.show();
			}

		}
	}

	public void Registration() {
		Button submit, cancel;
		final EditText editUname, editPassword, editFirstName, editLastName, editEmail, editCountry, editState;
		editUname = (EditText) registrationDialog
				.findViewById(R.id.editUsername);
		editEmail = (EditText) registrationDialog.findViewById(R.id.editEmail);
		editPassword = (EditText) registrationDialog
				.findViewById(R.id.editPassword);
		editFirstName = (EditText) registrationDialog
				.findViewById(R.id.editFirstName);
		editLastName = (EditText) registrationDialog
				.findViewById(R.id.editLastName);
		editCountry = (EditText) registrationDialog
				.findViewById(R.id.editCountry);
		editState = (EditText) registrationDialog.findViewById(R.id.editState);
		submit = (Button) registrationDialog.findViewById(R.id.btnSubmit);
		cancel = (Button) registrationDialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				registrationDialog.cancel();
			}
		});
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (editUname.getText().toString() == null
						|| editUname.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter Username.",
							Toast.LENGTH_LONG).show();
					return;
				} else if (editPassword.getText().toString() == null
						|| editPassword.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter Password.",
							Toast.LENGTH_LONG).show();
					return;
				} else if (editFirstName.getText().toString() == null
						|| editFirstName.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Enter First Name.", Toast.LENGTH_LONG).show();
					return;
				} else if (editLastName.getText().toString() == null
						|| editLastName.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter Last Name.",
							Toast.LENGTH_LONG).show();
					return;
				} else if (editEmail.getText().toString() == null
						|| editEmail.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter email.",
							Toast.LENGTH_LONG).show();
					return;
				} else if (!Utils.isValidEmail(editEmail.getText().toString())) {
					Toast.makeText(getApplicationContext(),
							"Enter Valid Email.", Toast.LENGTH_LONG).show();
					return;
				} else if (editCountry.getText().toString() == null
						|| editCountry.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter Country.",
							Toast.LENGTH_LONG).show();
					return;
				} else if (editState.getText().toString() == null
						|| editState.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "Enter State.",
							Toast.LENGTH_LONG).show();
					return;
				} else {
					// new LoginTask().execute();
					List<NameValuePair> pair = new ArrayList<NameValuePair>();
					pair.add(new BasicNameValuePair("username", editUname
							.getText().toString()));
					pair.add(new BasicNameValuePair("user_pass", editPassword
							.getText().toString()));
					pair.add(new BasicNameValuePair("first_name", editFirstName
							.getText().toString()));
					pair.add(new BasicNameValuePair("last_name", editLastName
							.getText().toString()));
					pair.add(new BasicNameValuePair("email", editEmail
							.getText().toString()));
					pair.add(new BasicNameValuePair("country", editCountry
							.getText().toString()));
					pair.add(new BasicNameValuePair("state", editState
							.getText().toString()));
					if (Utils.isNetworkAvailable(getApplicationContext())) {
						new BackGroudTask(Login.this, "Registering user!!",
								Constants.SERVER_URL
										+ Constants.REGISTRATION_URL,
								WebServiceHandler.GET, pair,
								new BackGroundTaskListener() {

									@Override
									public void onCompleteTask(String response) {
										// TODO Auto-generated method stub
										System.out
												.println("Registration Task Completed:"
														+ response);

										if (response == null
												|| response.equals("")) {
											Toast.makeText(
													getApplicationContext(),
													"Server Error.Please Try again after some time.",
													Toast.LENGTH_SHORT).show();
											return;
										} else {
											try {
												JSONObject jsonObject = new JSONObject(
														response);
												if (jsonObject
														.getString("flag")
														.equals("0")) {
													JSONArray array = jsonObject
															.getJSONArray("msg");
													Toast.makeText(
															getApplicationContext(),
															array.getString(0),
															Toast.LENGTH_SHORT)
															.show();
												} else if (jsonObject
														.getString("flag")
														.equals("1")) {
													registrationDialog.cancel();
													Toast.makeText(
															getApplicationContext(),
															"You are successfully registered.",
															Toast.LENGTH_SHORT)
															.show();
													getRegistrationId();
												}
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
									}
								});
					} else {
						Toast.makeText(getApplicationContext(),
								"Please connect to internet.",
								Toast.LENGTH_LONG).show();
						return;
					}
				}
			}
		});
	}

	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String personName = currentPerson.getDisplayName();
				String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
				Log.e("Google Login:", "Name: " + personName + ", email: "
						+ email + " id:" + currentPerson.getId());
				if (mGoogleApiClient.isConnected()) {
					Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
					mGoogleApiClient.disconnect();
				}
				RegisterUsingSocialMedia("google", currentPerson.getId(),
						personName, email, " ", " ");
			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void signInWithGplus() {
		if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			resolveSignInError();
		}
	}

	private void resolveSignInError() {
		try {
			if (mConnectionResult.hasResolution()) {
				try {
					mIntentInProgress = true;
					mConnectionResult.startResolutionForResult(this, 111);
				} catch (SendIntentException e) {
					mIntentInProgress = false;
					mGoogleApiClient.connect();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Task Completed:" + response);
		if (response == null || response.equals("")) {
			Toast.makeText(getApplicationContext(),
					"Server Error.Please Try again after some time.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.getString("flag").equals("0")) {
					Toast.makeText(getApplicationContext(),
							jsonObject.getString("msg"), Toast.LENGTH_SHORT)
							.show();
					return;
				} else if (jsonObject.getString("flag").equals("1")) {
					JSONObject object = jsonObject.getJSONObject("userdetails");
					String user_id = object.getString("ID");
					if (rememberMe.isChecked()) {
						Utils.saveSharedPreferences(Constants.IS_REMEMBER, "1",
								getApplicationContext());
						Utils.saveSharedPreferences(Constants.USER_NAME,
								editEmail.getText().toString(),
								getApplicationContext());
						Utils.saveSharedPreferences(Constants.PASSWORD,
								editPassword.getText().toString(),
								getApplicationContext());
					} else {
						Utils.saveSharedPreferences(Constants.IS_REMEMBER, "0",
								getApplicationContext());
					}
					Utils.saveSharedPreferences(Constants.USER_ID, user_id,
							getApplicationContext());
					Utils.saveSharedPreferences(Constants.IS_LOGGED_IN, "1",
							getApplicationContext());
					System.out.println("ID:" + user_id);
					// getRegistrationId();
					ChangeActivity();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		if (!arg0.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this, 0)
					.show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = arg0;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		if (dialog != null) {
			if (dialog.isShowing()) {
				dialog.cancel();
				getProfileInformation();
			}
		}
		System.out.println("connected..");
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}

}
