package com.arello.tdrtest.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.uihelpers.PlayerHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA. User: nicholas_rash Date: 15.07.13 Time: 15:12 To
 * change this template use File | Settings | File Templates.
 */

public class EditSleepSoundsActivity extends TDRBaseActivity {
	private ExpandableListView listView;
	PlayerHelper mPlayerHelper = new PlayerHelper();
	List<SleepMusic> mSleepMusicList;
	Button buySleepSoundsButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_sleep_sounds);

		listView = (ExpandableListView) findViewById(R.id.listT);

		mSleepMusicList = getTDRApplication().getDataManager()
				.getAllSleepMusic();

		ArrayList<ArrayList<List<SleepMusic>>> groups = new ArrayList<ArrayList<List<SleepMusic>>>();
		ArrayList<List<SleepMusic>> children1 = new ArrayList<List<SleepMusic>>();
		children1.add(mSleepMusicList);
		groups.add(children1);

		ExpListAdapter adapter = new ExpListAdapter(getApplicationContext(),
				groups);
		listView.setAdapter(adapter);

	}

	public void goToBuySleepSounds() {
		startActivity(new Intent(getApplicationContext(),
				BuySleepSoundsActivity.class));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPlayerHelper.stopPlay();
	}

	private class ExpListAdapter extends BaseExpandableListAdapter implements
			PlayerHelper.PlayerHelperStateChangeListener,
			RadioGroup.OnCheckedChangeListener {
		SleepMusic mPlayedAffirmation;
		View mPlayedView;
		boolean mAnimationInProgress = false;

		private ArrayList<ArrayList<List<SleepMusic>>> mGroups;
		private Context mContext;

		@Override
		public void playerStarted() {
			notifyDataSetChanged();
		}

		@Override
		public void playerStopped() {
			if (mPlayedView != null) {
				mPlayedView.findViewById(R.id.timer_text).setVisibility(
						View.GONE);
				mPlayedView.findViewById(R.id.cell_right_icon).setSelected(
						false);
				mPlayedView = null;
			}
			// notifyDataSetChanged();
			mPlayedAffirmation = null;
		}

		@Override
		public void progressChanged(int progress, int duration) {
			notifyDataSetChanged();
		}

		public ExpListAdapter(Context context,
				ArrayList<ArrayList<List<SleepMusic>>> groups) {
			mContext = context;
			mGroups = groups;
		}

		@Override
		public int getGroupCount() {
			return mGroups.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return mGroups.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return mGroups.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return mSleepMusicList.get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			RadioGroup onOff;

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.group_view, null);

				final Settings settings = new Settings(mContext);

				boolean chcked = settings.getSleepMusicChecked();

				onOff = (RadioGroup) convertView.findViewById(R.id.rad_group);

				onOff.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						buySleepSoundsButton = (Button) findViewById(R.id.buy_sleep_sounds_btn);
						buySleepSoundsButton.setVisibility(View.GONE);

						switch (checkedId) {
						case R.id.right_butt:

							listView.expandGroup(0);
							settings.setSleepMusicChecked(true);

							break;

						case R.id.left_butt:

							listView.collapseGroup(0);
							mPlayerHelper.stopPlay();
							settings.setSleepMusicChecked(false);

							break;

						}

						buySleepSoundsButton
								.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View arg0) {
										mPlayerHelper.stopPlay();
										goToBuySleepSounds();
										View view = findViewById(R.id.cell_right_icon);
										if (view != null) {
											view.setSelected(false);
										}
									}
								});
					}
				});

				onOff.check(chcked ? R.id.right_butt : R.id.left_butt);

			} else {
				onOff = (RadioGroup) convertView.findViewById(R.id.rad_group);
			}

			TextView textGroup = (TextView) convertView
					.findViewById(R.id.textGroup);
			textGroup.setText("Sleep Sounds");

			// Switch cbCheckBox =
			// (Switch)convertView.findViewById(R.id.sleep_sounds_checkboX);

			/*
			 * cbCheckBox.setOnCheckedChangeListener(new
			 * CompoundButton.OnCheckedChangeListener() {
			 * 
			 * @Override public void onCheckedChanged(CompoundButton buttonView,
			 * boolean isChecked) { buySleepSoundsButton = (Button)
			 * findViewById(R.id.buy_sleep_sounds_btn);
			 * buySleepSoundsButton.setVisibility(View.INVISIBLE);
			 * 
			 * // TODO Auto-generated method stub if (buttonView.isChecked()) {
			 * listView.expandGroup(0);
			 * buySleepSoundsButton.setVisibility(View.VISIBLE); } else {
			 * listView.collapseGroup(0); mPlayerHelper.stopPlay(); }
			 * 
			 * buySleepSoundsButton.setOnClickListener(new
			 * View.OnClickListener() {
			 * 
			 * @Override public void onClick(View arg0) {
			 * mPlayerHelper.stopPlay(); goToBuySleepSounds(); View view =
			 * findViewById(R.id.cell_right_icon); view.setSelected(false); }
			 * });
			 * 
			 * } });
			 */
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			if (groupPosition != 0) {
				throw new IllegalStateException(
						"Only in Sleep sounds may be childs");
			}

			convertView = createViewFromResource(groupPosition, childPosition,
					convertView, parent);
			final View wasView = convertView;
			SleepMusic data = (SleepMusic) getChild(groupPosition,
					childPosition);
			convertView.setTag(data);
			((TextView) convertView.findViewById(R.id.category_name_text))
					.setText(data.getCategory());
			CheckBox checkMark = (CheckBox) convertView
					.findViewById(R.id.cell_left_icon);
			checkMark.setChecked(false);

			SleepMusic aff = (SleepMusic) convertView.getTag();

			convertView.findViewById(R.id.cell_right_icon).setOnClickListener(
					new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							View baseView = (View) v.getParent();
							SleepMusic aff = (SleepMusic) baseView.getTag();

							if (!v.isSelected()) {
								if (!TextUtils.isEmpty(aff.getSoundPath())) {
									mPlayerHelper.startPlay(aff.getSoundPath());
								} else {
									try {
										mPlayerHelper.startPlayAss(getAssets()
												.openFd(aff.getAssetPath())
												.getFileDescriptor());
									} catch (IOException e) {
									}
								}
							} else {
								mPlayerHelper.stopPlay();
							}

							v.setSelected(!v.isSelected());
						}
					});

			if (convertView.isSelected()) {
				mPlayedView = wasView;
				TextView tv = (TextView) mPlayedView
						.findViewById(R.id.timer_text);
				tv.setText(String.format(
						"%02d:%02d | %02d:%02d",
						mPlayerHelper.getCurrentProgress() / 60,
						(mPlayerHelper.getCurrentProgress() > 0 ? mPlayerHelper
								.getCurrentProgress() : 0) % 60, mPlayerHelper
								.getSongDuration() / 60, mPlayerHelper
								.getSongDuration() % 60));
				if (tv.getVisibility() != View.VISIBLE) {
					tv.setVisibility(View.VISIBLE);
				}
				mPlayedView.findViewById(R.id.cell_right_icon)
						.setSelected(true);
			} else {
				convertView.findViewById(R.id.cell_right_icon).setSelected(
						false);
				convertView.findViewById(R.id.timer_text).setVisibility(
						View.GONE);
			}

			checkMark
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton view,
								boolean isChecked) {
							SleepMusic data = (SleepMusic) ((View) view
									.getParent()).getTag();
							if (isChecked != data.getIsSelected()) {
								getTDRApplication()
										.getDataManager()
										.setSelectionSleepMusic(data, isChecked);
							}
						}
					});

			return convertView;
		}

		private View createViewFromResource(int groupPos, int position,
				View convertView, ViewGroup parent) {
			View view;
			TextView text;

			view = getLayoutInflater().inflate(R.layout.sleep_music_cell, null,
					false);
			view.setLayoutParams(new AbsListView.LayoutParams(
					AbsListView.LayoutParams.MATCH_PARENT, getResources()
							.getDimensionPixelSize(R.dimen.sleep_music_width)));

			try {
				text = (TextView) view.findViewById(R.id.title_text);
				// text.setPadding(0, 20, 0, 0);
			} catch (ClassCastException e) {
				Log.e("ArrayAdapter",
						"You must supply a resource ID for a TextView");
				throw new IllegalStateException(
						"ArrayAdapter requires the resource ID to be a TextView",
						e);
			}

			Object item = getChild(groupPos, position);
			if (item != null && text != null) {
				if (item instanceof CharSequence) {
					text.setText((CharSequence) item);
				} else {
					text.setText(item.toString());
				}
			}

			return view;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			int groupPos = (Integer) group.getTag();

			if (groupPos == 1) {
				getTDRApplication().getSettings().setSingleMode(
						checkedId == R.id.left_butt);
			}

			if (groupPos == 1) {
				getTDRApplication().getSettings().set24HChecked(
						checkedId == R.id.right_butt);
			}
		}
	}
}