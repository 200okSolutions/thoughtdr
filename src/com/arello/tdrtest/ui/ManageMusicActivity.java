package com.arello.tdrtest.ui;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.Loader.OnLoadCompleteListener;
import android.widget.ListView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.uihelpers.adapters.MusicListAdapter;

public class ManageMusicActivity extends TDRBaseActivity implements LoaderManager.LoaderCallbacks<Cursor>
{
	class AddAllTracks extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog dialog;
		Loader<Cursor> cursor;
		Cursor musicCursor;

		public void executeTask()
		{

			cursor = onCreateLoader(0, null);
			cursor.registerListener(1, new OnLoadCompleteListener<Cursor>()
			{
				@Override
				public void onLoadComplete(Loader<Cursor> loader, Cursor result)
				{
					if (result.moveToFirst())
					{
						musicCursor = result;
						execute();
					}
				}
			});
			cursor.startLoading();
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialog = ProgressDialog.show(ManageMusicActivity.this, "", getResources().getString(R.string.adding_tracks), true);
			mAddAllTracksTask = this;
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			do
			{
				Music m = Music.musicFromCursor(musicCursor);
				if (mAdapter.getSelectedMusic(m) == null)
				{
					mAdapter.selectItem(m);
				}
				if (isCancelled())
				{
					return null;
				}
			}
			while (musicCursor.moveToNext());
			return null;
		}

		@Override
		protected void onCancelled()
		{
			super.onCancelled();
			dialog.dismiss();
			mAddAllTracksTask = null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
			mAdapter.notifyDataSetChanged();
			dialog.dismiss();
			mAddAllTracksTask = null;
		}
	}

	TDRListView mListView;
	MusicListAdapter mAdapter;
	AddAllTracks mAddAllTracksTask;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_music_layout);
		mListView = (TDRListView) findViewById(R.id.music_list);
		mAdapter = new MusicListAdapter(this, null, true, true, mListView);
		mAdapter.setSelectedItems(getTDRApplication().getDataManager().getAllMusic());
		mListView.setAdapter(mAdapter);
		mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		getSupportLoaderManager().initLoader(0, null, this);
/*		findViewById(R.id.add_all_tracks_button).setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				new AddAllTracks().executeTask();
			}
		});
*/	}

	@Override
	protected void onStop()
	{
		super.onStop();
		if (mAddAllTracksTask != null)
		{
			mAddAllTracksTask.cancel(true);
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		mAdapter.stopPlay();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1)
	{
		String select = "(" + Media.IS_MUSIC + " =1)";
		String[] proj = { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.ARTIST };
		return new CursorLoader(this, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, select, null, Media.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		mAdapter.swapCursor(null);
	}
}
