package com.arello.tdrtest.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.PurchaseRecord;
import com.arello.tdrtest.purchase.PurchaseManager;
import com.arello.tdrtest.purchase.PurchaseManager.PurchaseManagerDelegate;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.tdrtest.uihelpers.adapters.PurchasesAdapter;

public class BuySleepSoundsActivity extends TDRBaseActivity implements PurchaseManagerDelegate, OnItemClickListener
{

    int mRequestCode;
    Integer mRecordPosition;
    TDRListView mPurchasesList;
    PurchaseManager mPurchaseManager;
    PurchasesAdapter mPurchasesAdapter;

    @Override
    protected void onCreate(Bundle savedState)
    {
        super.onCreate(savedState);
        setContentView(R.layout.purchases_list_layout);
        mPurchasesList = (TDRListView) findViewById(R.id.purchases_list);
        mPurchaseManager = new PurchaseManager(this);
        mPurchaseManager.setDelegate(this);
        mPurchasesList.setOnItemClickListener(this);
        mPurchaseManager.receivePurchaseList();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onProductsListReceived()
    {
        hideProgress();
        mPurchasesAdapter = new PurchasesAdapter(mPurchaseManager.getSleepSoundsList());
        mPurchasesList.setAdapter(mPurchasesAdapter);
    }

    @Override
    public void onProductsListLoadingStarted()
    {
        showProgress();
    }

    @Override
    public void onProductsListLoadingCanceled()
    {
		hideProgress();
    }

    @Override
    public void onProductsListLoadingFailed(String errorMessage)
    {
        Utils.showAlert(this, errorMessage);
        hideProgress();
    }

    @Override
    public void onItemClick(AdapterView<?> listView, View itemView, int position, long id)
    {
        mRecordPosition = position;

        Intent intent = new Intent(this, PurchaseInfoActivity.class);
        intent.putExtra(PurchaseInfoActivity.PURCHASE_TYPE, false);
        intent.putExtra(PurchaseInfoActivity.PURCHASE_ITEM, mPurchaseManager.getSleepSoundsList().get(position));
        startActivityForResult(intent, mRequestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && mRecordPosition != null &&
                mPurchaseManager.getAffirmationsList() != null && mPurchasesAdapter != null)
        {
            mPurchasesAdapter.setItem(mRecordPosition,
                    (PurchaseRecord) data.getSerializableExtra(PurchaseInfoActivity.PURCHASE_ITEM));
            mPurchasesAdapter.notifyDataSetChanged();
        }
    }
}
