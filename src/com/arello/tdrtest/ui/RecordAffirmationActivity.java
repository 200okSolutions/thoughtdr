package com.arello.tdrtest.ui;

import android.content.res.ColorStateList;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.ui.custom_views.dialogs.ActionSheet;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.SelectCategoryDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog.OnHideDialogListener;
import com.arello.tdrtest.uihelpers.Utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class RecordAffirmationActivity extends TDRBaseActivity implements
		OnClickListener {
	private MediaRecorder mRecorder;
	private String mFileName;
	private EnterDialog mRecordNameDialog;
	private String mRecordName;

	Button mRecordButton;
	Animation mPulseAnimation;
	ImageView mRecordingImage;
	ColorStateList mButtonDefaultTextColors;
	ActionSheet mActionSheet;
	SelectCategoryDialog mSelectCategoryDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.record_layout);

		initRecorder();

		mRecordButton = (Button) findViewById(R.id.record_button);
		mRecordButton.setSelected(false);
		mRecordButton.setOnClickListener(this);
		((TextView) findViewById(R.id.timer_text)).setText("00:00");
		mPulseAnimation = AnimationUtils.loadAnimation(this,
				R.anim.alpha_pulse_animation);
		mRecordingImage = (ImageView) findViewById(R.id.recording_image);
		mButtonDefaultTextColors = mRecordButton.getTextColors();
		mActionSheet = new ActionSheet(this);
		mActionSheet.setDestructiveButtonTitle(android.R.string.cancel);
		mActionSheet.setOtherButtons(new int[] { R.string.save });
		mActionSheet.setOnHideListenet(new OnHideDialogListener() {

			@Override
			public void onHide() {
				if (mActionSheet.getResult() == ActionSheet.DESTRUCTIVE_BUTTON_INDEX) {
					finish();
				} else {
					mRecordNameDialog.show();
				}
			}
		});

		mRecordNameDialog = new EnterDialog(this, R.string.record_name);
		mRecordNameDialog.setEmptyError(R.string.empty_affirmation_name_alert);
		mRecordNameDialog.setValue("New affirmation "
				+ String.valueOf(getTDRApplication().getDataManager()
						.getAffirmationsCount() + 1));

		mSelectCategoryDialog = new SelectCategoryDialog(this);

		mSelectCategoryDialog.setOnHideListenet(new OnHideDialogListener() {
			@Override
			public void onHide() {
				Affirmation aff = new Affirmation();
				String affName = mRecordName;
				if (affName == null || affName.trim().length() == 0) {
					affName = "New affirmation "
							+ String.valueOf(getTDRApplication()
									.getDataManager().getAffirmationsCount() + 1);
				}
				aff.setName(affName);
				aff.setSoundPath(mFileName);
				aff.setCategory(mSelectCategoryDialog.getResult());
				aff.setSortOrder(0);
				getTDRApplication().getDataManager().addAffirmation(aff);
				finish();
			}
		});

		mRecordNameDialog
				.setOnEditorActionListener(new EnterDialog.OnEditorActionListener() {
					@Override
					public boolean onEditorAction() {
						mRecordName = mRecordNameDialog.getValue();

						mRecordNameDialog.dismiss();

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								mSelectCategoryDialog.showDialog();
							}
						}, 333);

						return false;
					}
				});
	}

	boolean isRecordingInProccess = false;
	int time = 0;
	Handler timer;
	Runnable timerTask = new Runnable() {

		@Override
		public void run() {
			time += 1;
			updateTimeLabel();
			timer.postDelayed(timerTask, 1000);
		}
	};

	@Override
	public void onClick(View clickView) {
		if (!mRecordButton.isEnabled())
			return;

		isRecordingInProccess = !isRecordingInProccess;

		if (isRecordingInProccess) {
			mRecordingImage.setVisibility(View.VISIBLE);
			if (mRecordingImage.getAnimation() == null)
				mRecordingImage.startAnimation(mPulseAnimation);
			mRecordButton
					.setBackgroundResource(R.drawable.red_button_bg_selector);
			mRecordButton.setTextColor(getResources().getColor(
					android.R.color.white));
			mRecordButton.setPadding(Utils.pixelsFromDp(this, 4),
					Utils.pixelsFromDp(this, 8), Utils.pixelsFromDp(this, 4),
					Utils.pixelsFromDp(this, 10));
			mRecordButton.setText(getResources().getString(R.string.stop));
			startRecording();
		} else {
			mRecordButton.setEnabled(false);
			mRecordingImage.clearAnimation();
			mRecordingImage.setVisibility(View.INVISIBLE);
			mRecordButton.setBackgroundResource(R.drawable.button_bg_selector);
			mRecordButton.setTextColor(mButtonDefaultTextColors);
			mRecordButton.setPadding(Utils.pixelsFromDp(this, 4),
					Utils.pixelsFromDp(this, 8), Utils.pixelsFromDp(this, 4),
					Utils.pixelsFromDp(this, 10));
			mRecordButton.setText(getResources().getString(R.string.record));
			stopRecording();
			// mRecordNameDialog.showDialog();
			mActionSheet.showDialog();
		}
	}

	protected void updateTimeLabel() {
		TextView tv = (TextView) findViewById(R.id.timer_text);
		tv.setText(String.format("%02d:%02d", time / 60, time % 60));
	}

	@Override
	protected void onPause() {
		if (isRecordingInProccess) {
			// Need for right activity finishing
			mRecordingImage.clearAnimation();
			stopRecording();
			File f = new File(mFileName);
			if (f.exists()) {
				f.delete();
			}
			mFileName = null;
		}
		finish();

		super.onPause();
	}

	private void startRecording() {
		mRecorder.start();

		timer = new Handler();
		time = -1;
		timer.postDelayed(timerTask, 0);

		isRecordingInProccess = true;
	}

	private void stopRecording() {
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		isRecordingInProccess = false;
		timer.removeCallbacks(timerTask);
	}

	private void initRecorder() {
		mFileName = Utils.getCachePath(this) + "/"
				+ UUID.randomUUID().toString() + ".m4a";
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mRecorder.setOutputFile(mFileName);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		try {
			mRecorder.prepare();
		} catch (IOException e) {
			Log.e(this.getClass().getName(), "prepare() failed");
		}
	}
}
