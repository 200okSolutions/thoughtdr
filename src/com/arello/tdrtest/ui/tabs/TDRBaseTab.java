package com.arello.tdrtest.ui.tabs;

import com.arello.tdrtest.TDRApplication;
import com.arello.tdrtest.ui.TDRBaseActivity;

import android.support.v4.app.Fragment;

public class TDRBaseTab extends Fragment {
	
	public TDRBaseActivity getTDRActivity() {
		return (TDRBaseActivity) super.getActivity();
	}

	public TDRApplication getTDRApplication() {
		return getTDRActivity().getTDRApplication();
	}
}
