package com.arello.tdrtest.ui.tabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.TDRDataManager.OnDataChangedListener;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.ui.ManageMusicActivity;
import com.arello.tdrtest.ui.PlayActivity;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.TDRListView.DoubleClickListener;
import com.arello.tdrtest.ui.custom_views.TDRListView.MoveListener;
import com.arello.tdrtest.ui.custom_views.TDRListView.RemoveListener;
import com.arello.tdrtest.ui.custom_views.dialogs.ActionSheet;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog.OnHideDialogListener;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.tdrtest.uihelpers.adapters.MusicListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class DayTab extends TDRBaseTab implements
		LoaderManager.LoaderCallbacks<Cursor>, RemoveListener,
		OnDataChangedListener, MoveListener, DoubleClickListener,
		OnItemClickListener {
	TDRListView mListView;
	MusicListAdapter mAdapter;
	Button mMusicButton;
	ActionSheet mDeleteActionSheet;
	int mMusicIdToDelete;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.tab_day, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		view.findViewById(R.id.add_tracks_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getTDRActivity(),
								ManageMusicActivity.class);
						startActivity(intent);
					}
				});
		mMusicButton = (Button) view.findViewById(R.id.music_button);
		mMusicButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getTDRApplication().getDataManager().getNightAffirmations()
						.size() > 0) {
					Intent intent = new Intent(getTDRActivity(),
							PlayActivity.class);
					intent.putExtra(Constants.START_WITH_MUSIC, true);
					intent.putExtra(Constants.DAY_MODE, true);
					startActivity(intent);
				} else {
					Utils.showAlert(getActivity(),
							R.string.no_affirmations_alert);
				}
			}
		});

		view.findViewById(R.id.start_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (getTDRApplication().getDataManager()
								.getNightAffirmations().size() > 0) {
							Intent intent = new Intent(getTDRActivity(),
									PlayActivity.class);
							intent.putExtra(Constants.DAY_MODE, true);
							startActivity(intent);
						} else {
							Utils.showAlert(getActivity(),
									R.string.no_affirmations_alert);
						}
					}

				});
		view.findViewById(R.id.clear_all_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View view) {
						AlertDialog.Builder ad = new AlertDialog.Builder(
								getActivity());
						ad.setTitle("Warning!");
						ad.setMessage("Are you sure you want to delete all of the tracks?");
						ad.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// To change body of implemented methods
										// use File | Settings | File Templates.
										getTDRApplication().getDataManager()
												.clearMusic();
									}
								});
						ad.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
									}
								});
						ad.setIcon(R.drawable.caution_sign);
						ad.show();
					}
				});

		mListView = (TDRListView) view.findViewById(R.id.music_list);
		mAdapter = new MusicListAdapter(getTDRActivity(), null, true, false,
				mListView);
		mListView.setAdapter(mAdapter);
		mListView.setRemoveListener(this);
		mListView.setMoveListener(this);
		mListView.setOnDoubleClickListener(this);
		mListView.setOnItemClickListener(this);
		getTDRActivity().getSupportLoaderManager().initLoader(0, null, this);

		checkMusicButtonState();

		mDeleteActionSheet = new ActionSheet(getTDRActivity())
				.setDestructiveButtonTitle(R.string.delete).setOtherButtons(
						new int[] { android.R.string.cancel });

		mDeleteActionSheet.setOnHideListenet(new OnHideDialogListener() {

			@Override
			public void onHide() {
				if (mDeleteActionSheet.getResult() == ActionSheet.DESTRUCTIVE_BUTTON_INDEX) {
					getTDRApplication().getDataManager().deleteMusicWithId(
							mMusicIdToDelete);
				}
			}
		});
	}

	protected void checkMusicButtonState() {
		Utils.setViewIsEnabled(mMusicButton, getTDRApplication()
				.getDataManager().getAllMusic().size() != 0);
	}

	@Override
	public void onPause() {
		super.onPause();
		mAdapter.stopPlay();
	}

	@Override
	public void onResume() {
		super.onResume();
		getTDRActivity().getSupportLoaderManager().restartLoader(0, null, this);
		getTDRActivity().getTDRApplication().getDataManager()
				.setOnMusicChangedListener(this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return new AsyncTaskLoader<Cursor>(getTDRActivity()) {

			@Override
			public Cursor loadInBackground() {
				return getTDRActivity().getTDRApplication().getDataManager()
						.getAllMusicAsCursor();
			}

			@Override
			protected void onStartLoading() {
				forceLoad();
			}
		};
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	@Override
	public void remove(int which) {
		Cursor item = (Cursor) mAdapter.getItem(which);
		// getTDRActivity().getTDRApplication().getDataManager()
		// .deleteMusicWithId(item.getInt(item.getColumnIndex(Media._ID)));
		mMusicIdToDelete = item.getInt(item.getColumnIndex(Media._ID));
		mDeleteActionSheet.showDialog();
	}

	@Override
	public void onDataChanged() {
		getTDRActivity().getSupportLoaderManager().restartLoader(0, null, this);
		checkMusicButtonState();
	}

	@Override
	public void move(int from, int to) {
		Music mus = Music.musicFromCursor((Cursor) mAdapter.getItem(from));
		getTDRApplication().getDataManager().deleteMusic(mus);
		mus.setSortOrder(to);
		getTDRActivity().getTDRApplication().getDataManager().addMusic(mus);
	}

	@Override
	public void onDoubleClick(int index, View view) {

	}

	@Override
	public boolean canMove(int from, int to) {
		return true;
	}

	@Override
	public void moveStarted(int from) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Cursor item = (Cursor) mAdapter.getItem(position);
		// getTDRActivity().getTDRApplication().getDataManager()
		// .deleteMusicWithId(item.getInt(item.getColumnIndex(Media._ID)));
		mMusicIdToDelete = item.getInt(item.getColumnIndex(Media._ID));
		mDeleteActionSheet.showDialog();
	}
}
