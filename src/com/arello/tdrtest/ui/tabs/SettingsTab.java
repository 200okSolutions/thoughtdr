package com.arello.tdrtest.ui.tabs;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.data.purchase.FileItem;
import com.arello.tdrtest.purchase.DownloadManager;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTask;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTaskListener;
import com.arello.tdrtest.purchase.PurchaseManager;
import com.arello.tdrtest.purchase.SubscribeManager;
import com.arello.tdrtest.ui.BuySleepSoundsActivity;
import com.arello.tdrtest.ui.EditCategoriesActivity;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.uihelpers.Patterns;
import com.arello.tdrtest.uihelpers.PlayerHelper;
import com.arello.tdrtest.uihelpers.PlayerHelper.PlayerHelperStateChangeListener;
import com.arello.tdrtest.uihelpers.Utils;

public class SettingsTab extends TDRBaseTab implements OnCheckedChangeListener,
		PurchaseManager.PurchaseManagerDelegate {
	RadioGroup mAffirmationMode;
	RadioGroup m24hMode;
	TDRListView mListView;
	List<SleepMusic> mSleepMusicList;
	CheckBox mSleepModeCheckBox;

	private SharedPreferences mSharedPreferences;
	private SharedPreferences.Editor mEditor;
	private SubscribeManager mSubscribeManager;
	private PurchaseManager mPurchaseManager;
	private Button mDownloadFree;
	private Context mContext;
	private SleepMusicAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.tab_settings, null);
	}

	public Button cretateBuyButton(Context context) {
		Button buySleepSoundsButton = (Button) Button.inflate(getActivity(),
				R.layout.buy_sleep_sounds_button, null);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.setMargins(12, 12, 12, 12);
		buySleepSoundsButton.setLayoutParams(params);
		buySleepSoundsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SettingsTab.this.goToBuySleepSounds();
			}
		});
		return buySleepSoundsButton;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mContext = view.getContext();

		mSharedPreferences = mContext.getSharedPreferences("main",
				Context.MODE_PRIVATE);
		mEditor = mSharedPreferences.edit();

		mPurchaseManager = new PurchaseManager(mContext, true);
		mPurchaseManager.setDelegate(this);

		mDownloadFree = (Button) view.findViewById(R.id.download_free);

		configDownloadFreeButton();

		view.findViewById(R.id.edit_cats_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Intent editCats = new Intent(getTDRActivity(),
								EditCategoriesActivity.class);
						startActivity(editCats);
					}
				});

		mAffirmationMode = (RadioGroup) view
				.findViewById(R.id.affirmation_mode_radio_group);
		m24hMode = (RadioGroup) view.findViewById(R.id._24h_radio_group);
		mListView = (TDRListView) view.findViewById(R.id.sleep_music_list);
		RelativeLayout relLayout = new RelativeLayout(mContext);
		relLayout.setLayoutParams(new ListView.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		System.out.println("getc child.");
		relLayout.addView(cretateBuyButton(mContext));

		mListView.addFooterView(relLayout);

		mSleepModeCheckBox = (CheckBox) view
				.findViewById(R.id.sleep_music_checkbox);
		mSleepModeCheckBox
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						getTDRApplication().getSettings().setSleepMusicChecked(
								isChecked);
						if (isChecked) {
							Utils.showViewWithAnimation(mListView,
									R.anim.alpha_fade_in);
						} else {
							Utils.goneViewWithAnimation(mListView,
									R.anim.alpha_fade_out);
						}
					}
				});
	}

	public void goToBuySleepSounds() {
		startActivity(new Intent(getTDRActivity(), BuySleepSoundsActivity.class));
	}

	@Override
	public void onResume() {
		super.onResume();
		mSleepMusicList = getTDRApplication().getDataManager()
				.getAllSleepMusic();
		mAdapter = new SleepMusicAdapter();

		mListView.setAdapter(mAdapter);
		// mSleepModeCheckBox.setChecked(getTDRApplication().getSettings().getSleepMusicChecked());
		// ///////////////////////////////////////////
		mListView.setVisibility((getTDRApplication().getSettings()
				.getSleepMusicChecked() ? View.VISIBLE : View.GONE));

		mAffirmationMode
				.check(getTDRApplication().getSettings().isSingleMode() ? R.id.single_button
						: R.id.continuous_button);
		mAffirmationMode.setOnCheckedChangeListener(this);

		m24hMode.check(getTDRApplication().getSettings().get24HChecked() ? R.id._24h_button
				: R.id._12h_button);
		m24hMode.setOnCheckedChangeListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		mAdapter.stopPlay();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mAdapter.stopPlay();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mAdapter.stopPlay();
	}

	@Override
	public void onPause() {
		super.onPause();
		mAdapter.stopPlay();
		mAffirmationMode.setOnCheckedChangeListener(null);
		m24hMode.setOnCheckedChangeListener(null);
	}

	@Override
	public void onCheckedChanged(RadioGroup arg0, int item) {
		if (arg0.getId() == R.id.affirmation_mode_radio_group) {
			getTDRApplication().getSettings().setSingleMode(
					item == R.id.single_button);
		}

		if (arg0.getId() == R.id._24h_radio_group) {
			getTDRApplication().getSettings().set24HChecked(
					item == R.id._24h_button);
		}
	}

	public class SleepMusicAdapter extends ArrayAdapter<SleepMusic> implements
			PlayerHelperStateChangeListener {
		SleepMusic mPlayedAffirmation;
		View mPlayedView;
		boolean mAnimationInProgress = false;
		PlayerHelper mPlayerHelper = new PlayerHelper();

		@Override
		public void playerStarted() {
			notifyDataSetChanged();
		}

		public void stopPlay() {
			mPlayerHelper.stopPlay();
		}

		@Override
		public void playerStopped() {
			if (mPlayedView != null) {
				mPlayedView.findViewById(R.id.timer_text).setVisibility(
						View.GONE);
				mPlayedView.findViewById(R.id.cell_right_icon).setSelected(
						false);
				mPlayedView = null;
			}
			// notifyDataSetChanged();
			mPlayedAffirmation = null;
		}

		@Override
		public void progressChanged(int progress, int duration) {
			notifyDataSetChanged();
		}

		public SleepMusicAdapter() {
			super(getActivity(), R.layout.sleep_music_cell, R.id.title_text,
					mSleepMusicList);

			mPlayerHelper.setStateChangedListener(this);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final View wasView = null;// convertView;

			convertView = super.getView(position, convertView, parent);
			SleepMusic data = getItem(position);
			convertView.setTag(data);
			((TextView) convertView.findViewById(R.id.category_name_text))
					.setText(data.getCategory());
			CheckBox checkMark = (CheckBox) convertView
					.findViewById(R.id.cell_left_icon);
			checkMark.setChecked(data.getIsSelected());

			SleepMusic aff = (SleepMusic) convertView.getTag();

			convertView.findViewById(R.id.cell_right_icon).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							if (mAnimationInProgress) {
								return;
							}

							mPlayedView = wasView;
							View baseView = (View) v.getParent();
							SleepMusic aff = (SleepMusic) baseView.getTag();
							boolean newAff = (aff != mPlayedAffirmation);
							mPlayerHelper.stopPlay();

							if (!v.isSelected() && newAff) {
								v.setSelected(true);
								mPlayedAffirmation = aff;

								if (!TextUtils.isEmpty(aff.getSoundPath())) {
									mPlayerHelper.startPlay(aff.getSoundPath());
								} else {
									try {
										mPlayerHelper.startPlayAss(getActivity()
												.getAssets()
												.openFd(aff.getAssetPath())
												.getFileDescriptor());
									} catch (IOException e) {
									}
								}

							}
						}
					});

			/*
			 * if (wasView != null) { if (mPlayedAffirmation == aff) {
			 * mPlayedView = wasView; TextView tv = (TextView)
			 * mPlayedView.findViewById(R.id.timer_text);
			 * tv.setText(String.format("%02d:%02d | %02d:%02d",
			 * mPlayerHelper.getCurrentProgress() / 60,
			 * (mPlayerHelper.getCurrentProgress() > 0 ?
			 * mPlayerHelper.getCurrentProgress() : 0) % 60,
			 * mPlayerHelper.getSongDuration() / 60,
			 * mPlayerHelper.getSongDuration() % 60)); if (tv.getVisibility() !=
			 * View.VISIBLE) { tv.setVisibility(View.VISIBLE); }
			 * mPlayedView.findViewById(R.id.cell_right_icon).setSelected(true);
			 * } else {
			 * convertView.findViewById(R.id.cell_right_icon).setSelected
			 * (false);
			 * convertView.findViewById(R.id.timer_text).setVisibility(View
			 * .GONE); } }
			 */
			checkMark
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton view,
								boolean isChecked) {
							SleepMusic data = (SleepMusic) ((View) view
									.getParent()).getTag();
							if (isChecked != data.getIsSelected()) {
								getTDRApplication()
										.getDataManager()
										.setSelectionSleepMusic(data, isChecked);
							}
						}
					});
			return convertView;
		}
	}

	private void configDownloadFreeButton() {
		if (mSharedPreferences.getInt("starts", -1) >= 0) {
			mDownloadFree.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					showSubscribeDialog();
				}
			});
		} else {
			mDownloadFree.setText(R.string.download);
			mDownloadFree.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					startDownloadFree();
				}
			});

		}
	}

	private void showSubscribeDialog() {
		final EnterDialog emailEnterDialog = new EnterDialog(mContext,
				R.string.enter_email);
		emailEnterDialog.setEmptyError(R.string.empty_email_alert);
		emailEnterDialog.addRule(Patterns.EMAIL_ADDRESS,
				R.string.invalid_email_alert);
		emailEnterDialog.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		emailEnterDialog
				.setOnEditorActionListener(new EnterDialog.OnEditorActionListener() {
					@Override
					public boolean onEditorAction() {
						SubscribeManager.OnSubsribeListener onSubsribeListener = new SubscribeManager.OnSubsribeListener() {
							@Override
							public void onSubscribe() {
								configDownloadFreeButton();

								mDownloadFree.performClick();
							}
						};

						mSubscribeManager = new SubscribeManager(mContext,
								emailEnterDialog, onSubsribeListener);
						mSubscribeManager.execute(emailEnterDialog.getValue());

						return false;
					}
				});

		emailEnterDialog.show();
	}

	private void startDownloadFree() {
		mPurchaseManager.receivePurchaseList();
	}

	@Override
	public void onProductsListReceived() {
		if (mPurchaseManager.getAffirmationsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getAffirmationsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new AffirmationDownloadTask(
						fileItem));
			}
		}

		if (mPurchaseManager.getSleepSoundsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getSleepSoundsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new SleepSoundsDownloadTask(
						fileItem));
			}
		}
	}

	@Override
	public void onProductsListLoadingStarted() {
		Toast.makeText(mContext, R.string.downloading_free_affirmations,
				Toast.LENGTH_LONG).show();

		if (mDownloadFree != null) {
			mDownloadFree.setEnabled(false);
		}
	}

	@Override
	public void onProductsListLoadingFailed(String errorMessage) {
	}

	@Override
	public void onProductsListLoadingCanceled() {

	}

	public class DownloadManagerLoadFileTask extends
			DownloadManager.DownloadManagerTask {

		public DownloadManagerLoadFileTask(FileItem item) {
			super(item.getFilePath());
			setTag(item);
		}

		@Override
		protected String getFilePath() {
			return Utils.getCachePath(getActivity()) + "/"
					+ Uri.parse(getUrl()).getLastPathSegment();
		}

	}

	class AffirmationDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public AffirmationDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<Affirmation> freeAffirmation = getTDRApplication()
						.getDataManager().getAffirmationsWithProductId(
								"freeAffirmation");

				for (Affirmation a : freeAffirmation) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager().deleteAffirmation(
								a);
						break;
					}
				}

				Affirmation aff = new Affirmation();
				aff.setName(fileItem.getTitle());
				Category cat = getTDRApplication().getDataManager()
						.getCategoryWitnName(fileItem.getCategory());
				if (cat == null) {
					cat = Category.createCategory(fileItem.getCategory());
					getTDRApplication().getDataManager().addNewCategory(cat);
				}
				aff.setCategory(cat);
				aff.setProductId("freeAffirmation");
				aff.setSoundPath(destinationFile);
				getTDRApplication().getDataManager().addAffirmation(aff);
			}
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}
	}

	class SleepSoundsDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public SleepSoundsDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<SleepMusic> freeSleepMusic = getTDRApplication()
						.getDataManager().getSleepSoundsWithProductId(
								"freeSleepSounds");

				for (SleepMusic a : freeSleepMusic) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager()
								.deleteSleepSound(a);
						break;
					}
				}

				SleepMusic sleepMusic = new SleepMusic();
				sleepMusic.setName(fileItem.getTitle());
				sleepMusic.setProductId("freeSleepSounds");
				sleepMusic.setSoundPath(destinationFile);
				sleepMusic.setCategory(fileItem.getCategory());
				getTDRApplication().getDataManager().addSleepSound(sleepMusic);
			}
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}
	}
}
