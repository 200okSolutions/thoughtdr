package com.arello.tdrtest.ui.tabs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.data.purchase.FileItem;
import com.arello.tdrtest.data.purchase.WebServiceHandler;
import com.arello.tdrtest.purchase.BackGroudTask;
import com.arello.tdrtest.purchase.BackGroundTaskListener;
import com.arello.tdrtest.purchase.DownloadManager;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTask;
import com.arello.tdrtest.purchase.DownloadManager.DownloadManagerTaskListener;
import com.arello.tdrtest.purchase.PurchaseManager;
import com.arello.tdrtest.purchase.SubscribeManager;
import com.arello.tdrtest.ui.BuySleepSoundsActivity;
import com.arello.tdrtest.ui.ChangePassword;
import com.arello.tdrtest.ui.EditCategoriesActivity;
import com.arello.tdrtest.ui.EditSleepSoundsActivity;
import com.arello.tdrtest.ui.Login;
import com.arello.tdrtest.ui.ThoughtDRMainActivity;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog.OnDeleteListener;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog.OnSubmitListener;
import com.arello.tdrtest.uihelpers.Patterns;
import com.arello.tdrtest.uihelpers.PlayerHelper;
import com.arello.tdrtest.uihelpers.PlayerHelper.PlayerHelperStateChangeListener;
import com.arello.tdrtest.uihelpers.Utils;

/**
 * 05.04.2013
 * 
 * @author denis.mirochnik
 */
public class SettingsTabNew extends TDRBaseTab implements
		PurchaseManager.PurchaseManagerDelegate, BackGroundTaskListener {
	private ExpandableListView mListView;
	List<SleepMusic> mSleepMusicList;
	private SharedPreferences mSharedPreferences;
	private SubscribeManager mSubscribeManager;
	private Button mDownloadFree;
	private PurchaseManager mPurchaseManager;
	private Context mContext;
	private MyAdapter mAdapter;
	Dialog dialog1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.tab_settings_new, container, false);
	}

	public Button cretateBuyButton(Context context) {
		Button buySleepSoundsButton = (Button) Button.inflate(getActivity(),
				R.layout.buy_sleep_sounds_button, null);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.setMargins(12, 12, 12, 12);
		buySleepSoundsButton.setLayoutParams(params);
		buySleepSoundsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				goToBuySleepSounds();
			}
		});
		return buySleepSoundsButton;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mContext = view.getContext();

		mListView = (ExpandableListView) view.findViewById(R.id.list);

		mSleepMusicList = getTDRApplication().getDataManager()
				.getAllSleepMusic();

		Log.e("SettingsTabNew", "" + mSleepMusicList.size());

		View footer = getActivity().getLayoutInflater().inflate(
				R.layout.tab_settings_footer, null);

		mSharedPreferences = mContext.getSharedPreferences("main",
				Context.MODE_PRIVATE);
		// mEditor = mSharedPreferences.edit();

		mPurchaseManager = new PurchaseManager(mContext, true);
		mPurchaseManager.setDelegate(this);

		mDownloadFree = (Button) footer.findViewById(R.id.subscribe_butt);
		mDownloadFree.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showSubscribeDialog();
			}
		});
		// configDownloadFreeButton();

		mListView.addFooterView(footer);
		// mAdapter = new MyAdapter();
		// mListView.setAdapter(mAdapter);
	}

	public void goToBuySleepSounds() {
		startActivity(new Intent(getTDRActivity(), BuySleepSoundsActivity.class));
	}

	@Override
	public void onResume() {
		super.onResume();
		mSleepMusicList = getTDRApplication().getDataManager()
				.getAllSleepMusic();
		mAdapter = new MyAdapter();

		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onStop() {
		super.onStop();
		mAdapter.stopPlay();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mAdapter.stopPlay();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mAdapter.stopPlay();
	}

	@Override
	public void onPause() {
		super.onPause();
		mAdapter.stopPlay();
	}

	public void changePasswordDialog() {
		final Dialog registrationDialog;
		registrationDialog = new Dialog(SettingsTabNew.this.getActivity(),
				R.style.Theme_ThoughtDR_DialogCustomAnimation);
		registrationDialog.setContentView(R.layout.change_password_screen);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = registrationDialog.getWindow();
		lp.copyFrom(window.getAttributes());
		// This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		registrationDialog.show();
		final Button submit, cancel;
		cancel = (Button) registrationDialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				registrationDialog.cancel();
			}
		});
		final EditText editcurrentPass, editNewPass, editconfirm;
		editcurrentPass = (EditText) registrationDialog
				.findViewById(R.id.editCurrent);
		editNewPass = (EditText) registrationDialog.findViewById(R.id.editNew);
		editconfirm = (EditText) registrationDialog
				.findViewById(R.id.editConfirm);
		submit = (Button) registrationDialog.findViewById(R.id.btnSubmitChange);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String newPass = editNewPass.getText().toString();
				String confirmPass = editconfirm.getText().toString();
				if (editcurrentPass.getText().toString() == null
						|| editcurrentPass.getText().toString().equals("")) {
					Toast.makeText(SettingsTabNew.this.getActivity(),
							"Enter current Password.", Toast.LENGTH_SHORT)
							.show();
					return;
				} else if (editNewPass.getText().toString() == null
						|| editNewPass.getText().toString().equals("")) {
					Toast.makeText(SettingsTabNew.this.getActivity(),
							"Enter New Password.", Toast.LENGTH_SHORT).show();
					return;
				} else if (editconfirm.getText().toString() == null
						|| editconfirm.getText().toString().equals("")) {
					Toast.makeText(SettingsTabNew.this.getActivity(),
							"Enter Confirm Password.", Toast.LENGTH_SHORT)
							.show();
					return;
				} else if (!newPass.equals(confirmPass)) {
					Toast.makeText(SettingsTabNew.this.getActivity(),
							"New Password and confirm password doesn't match.",
							Toast.LENGTH_SHORT).show();
					return;
				} else {
					List<NameValuePair> pair = new ArrayList<NameValuePair>();
					pair.add(new BasicNameValuePair("old_password",
							editcurrentPass.getText().toString()));
					pair.add(new BasicNameValuePair("new_password", editNewPass
							.getText().toString()));
					pair.add(new BasicNameValuePair("user_id", Utils
							.getSharedPreferences(Constants.USER_ID,
									SettingsTabNew.this.getActivity())));
					if (Utils.isNetworkAvailable(SettingsTabNew.this
							.getActivity())) {
						new BackGroudTask(SettingsTabNew.this.getActivity(),
								"Sending Request!!!", Constants.SERVER_URL
										+ Constants.CHANGE_PASSWORD_URL,
								WebServiceHandler.GET, pair,
								new BackGroundTaskListener() {

									@Override
									public void onCompleteTask(String response) {
										// TODO Auto-generated method stub
										System.out
												.println("Change Pass Task Completed:"
														+ response);
										if (response == null
												|| response.equals("")) {
											Toast.makeText(
													SettingsTabNew.this
															.getActivity(),
													"Server Error.",
													Toast.LENGTH_SHORT).show();
											return;
										} else {
											JSONObject jsonObject;
											try {
												jsonObject = new JSONObject(
														response);
												if (jsonObject
														.getString("flag")
														.equals("1")) {
													if (Utils
															.getSharedPreferences(
																	Constants.IS_REMEMBER,
																	SettingsTabNew.this
																			.getActivity()) != null) {
														if (Utils
																.getSharedPreferences(
																		Constants.IS_REMEMBER,
																		SettingsTabNew.this
																				.getActivity())
																.equals("1")) {
															Utils.saveSharedPreferences(
																	Constants.PASSWORD,
																	editNewPass
																			.getText()
																			.toString(),
																	SettingsTabNew.this
																			.getActivity());
														}
													}
													Toast.makeText(
															SettingsTabNew.this
																	.getActivity(),
															jsonObject
																	.getString("msg"),
															Toast.LENGTH_SHORT)
															.show();
													registrationDialog.cancel();
												} else {
													Toast.makeText(
															SettingsTabNew.this
																	.getActivity(),
															jsonObject
																	.getString("msg"),
															Toast.LENGTH_SHORT)
															.show();
													return;
												}
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

										}
									}
								});
					} else {
						Toast.makeText(SettingsTabNew.this.getActivity(),
								"Plese connect to internet.",
								Toast.LENGTH_SHORT).show();
						return;
					}
				}
			}
		});
	}

	public class MyAdapter extends BaseExpandableListAdapter implements
			PlayerHelperStateChangeListener, OnCheckedChangeListener,
			android.widget.CompoundButton.OnCheckedChangeListener {
		SleepMusic mPlayedAffirmation;
		View mPlayedView;
		boolean mAnimationInProgress = false;
		PlayerHelper mPlayerHelper = new PlayerHelper();

		public static final int GROUP_AFF_MODE = 0;
		public static final int GROUP_EDIT_AFF = 1;
		public static final int GROUP_SLEEP_SOUND = 2;
		public static final int GROUP_CLOCK_MODE = 3;
		public static final int GROUP_HIBERNATION_FEATURE = 4;
		public static final int VERSION = 5;
		public static final int CHANGE_PASS = 6;
		public static final int LOGOUT = 7;

		@Override
		public void playerStarted() {
			notifyDataSetChanged();
		}

		public void stopPlay() {
			mPlayerHelper.stopPlay();
		}

		@Override
		public void playerStopped() {
			if (mPlayedView != null) {
				mPlayedView.findViewById(R.id.timer_text).setVisibility(
						View.GONE);
				mPlayedView.findViewById(R.id.cell_right_icon).setSelected(
						false);
				mPlayedView = null;
			}
			// notifyDataSetChanged();
			mPlayedAffirmation = null;
		}

		@Override
		public void progressChanged(int progress, int duration) {
			notifyDataSetChanged();
		}

		public MyAdapter() {
			mPlayerHelper.setStateChangedListener(this);
		}

		private View createViewFromResource(int groupPos, int position,
				View convertView, ViewGroup parent) {
			View view;
			TextView text;

			view = getActivity().getLayoutInflater().inflate(
					R.layout.sleep_music_cell, null, false);

			try {
				text = (TextView) view.findViewById(R.id.title_text);
			} catch (ClassCastException e) {
				Log.e("ArrayAdapter",
						"You must supply a resource ID for a TextView");
				throw new IllegalStateException(
						"ArrayAdapter requires the resource ID to be a TextView",
						e);
			}

			Object item = getChild(groupPos, position);
			if (item != null && text != null) {
				if (item instanceof CharSequence) {
					text.setText((CharSequence) item);
				} else {
					text.setText(item.toString());
				}
			}

			return view;
		}

		@Override
		public int getGroupCount() {
			return 8;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			if (groupPosition == GROUP_SLEEP_SOUND) {
				return mSleepMusicList.size() + 1;
			}
			return 0;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return new Object();
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			if (groupPosition != GROUP_SLEEP_SOUND) {
				throw new IllegalStateException(
						"Only in Sleep sounds may be childs");
			}

			return mSleepMusicList.get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return groupPosition * 20 + childPosition;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			switch (groupPosition) {
			case GROUP_AFF_MODE:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings_rad_group, parent, false);

				break;

			case GROUP_EDIT_AFF:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings_butt, parent, false);

				break;

			case GROUP_SLEEP_SOUND:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings, parent, false);

				break;

			case GROUP_CLOCK_MODE:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings_rad_group, parent, false);

				break;
			case GROUP_HIBERNATION_FEATURE:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.group_view, parent, false);

				break;
			case VERSION:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.version_layout, parent, false);

				break;
			case CHANGE_PASS:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings, parent, false);

				break;
			case LOGOUT:

				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.item_settings, parent, false);

				break;

			}

			switch (groupPosition) {
			case GROUP_AFF_MODE:

			{
				((TextView) convertView.findViewById(R.id.text))
						.setText(R.string.play_mode);
				((RadioButton) convertView.findViewById(R.id.left_butt))
						.setText(R.string.single_mode);
				((RadioButton) convertView.findViewById(R.id.right_butt))
						.setText(R.string.continuous);

				RadioGroup radGroup = (RadioGroup) convertView
						.findViewById(R.id.rad_group);

				radGroup.setTag(groupPosition);

				radGroup.check(getTDRApplication().getSettings().isSingleMode() ? R.id.left_butt
						: R.id.right_butt);
				radGroup.setOnCheckedChangeListener(this);

			}
				break;

			case GROUP_EDIT_AFF:

				((TextView) convertView.findViewById(R.id.text))
						.setText(R.string.edit_categories);
				View butt = convertView.findViewById(R.id.button);
				butt.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent editCats = new Intent(getTDRActivity(),
								EditCategoriesActivity.class);
						startActivity(editCats);
					}
				});

				break;

			case GROUP_SLEEP_SOUND:

				((TextView) convertView.findViewById(R.id.text))
						.setText(R.string.sleep_sound);
				Button bButton = (Button) convertView
						.findViewById(R.id.sleep_btn);

				bButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						Intent editSleepSounds = new Intent(getTDRActivity(),
								EditSleepSoundsActivity.class);
						startActivity(editSleepSounds);
					}
				});

				break;
			case CHANGE_PASS:

				((TextView) convertView.findViewById(R.id.text))
						.setText("Change Password");
				Button cButton = (Button) convertView
						.findViewById(R.id.sleep_btn);
				cButton.setText("Change");
				cButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						/*
						 * Intent editSleepSounds = new Intent(getTDRActivity(),
						 * ChangePassword.class);
						 * startActivity(editSleepSounds);
						 */
						changePasswordDialog();
					}
				});

				break;
			case LOGOUT:

				((TextView) convertView.findViewById(R.id.text))
						.setText("Logout");
				Button lButton = (Button) convertView
						.findViewById(R.id.sleep_btn);
				lButton.setText("Logout");
				lButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						Utils.saveSharedPreferences(Constants.IS_LOGGED_IN,
								"0", getActivity());
						Intent editSleepSounds = new Intent(getTDRActivity(),
								Login.class);
						editSleepSounds.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(editSleepSounds);
						getActivity().finish();
					}
				});

				break;
			case GROUP_CLOCK_MODE:

				((TextView) convertView.findViewById(R.id.text))
						.setText("Clock mode");
				((RadioButton) convertView.findViewById(R.id.left_butt))
						.setText("12hr");
				((RadioButton) convertView.findViewById(R.id.right_butt))
						.setText("24hr");

				RadioGroup radGroup = (RadioGroup) convertView
						.findViewById(R.id.rad_group);

				radGroup.setTag(groupPosition);

				radGroup.check(getTDRApplication().getSettings()
						.get24HChecked() ? R.id.right_butt : R.id.left_butt);
				radGroup.setOnCheckedChangeListener(this);

				break;

			case GROUP_HIBERNATION_FEATURE:
				RadioGroup onOff;

				onOff = (RadioGroup) convertView.findViewById(R.id.rad_group);
				final Settings setting = new Settings(getActivity());
				onOff.check(setting.IS_HIBERNATE_ACTIVE() ? R.id.right_butt
						: R.id.left_butt);
				onOff.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						switch (checkedId) {
						case R.id.right_butt:
							setting.setIS_HIBERNATE_ACTIVE(true);
							break;

						case R.id.left_butt:
							setting.setIS_HIBERNATE_ACTIVE(false);
							break;
						}
					}
				});

				TextView textGroup = (TextView) convertView
						.findViewById(R.id.textGroup);
				textGroup.setText("Turn Hibernate on/off");

				break;

			case VERSION:
				try {
					PackageInfo pInfo = getActivity().getPackageManager()
							.getPackageInfo(getActivity().getPackageName(), 0);
					TextView versionName = (TextView) convertView
							.findViewById(R.id.txtVersionName);
					versionName.setText(pInfo.versionName);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			if (groupPosition != GROUP_SLEEP_SOUND) {
				throw new IllegalStateException(
						"Only in Sleep sounds may be childs");
			}
			System.out.println("get child.");
			if (childPosition == mSleepMusicList.size()) {
				RelativeLayout relLayout = new RelativeLayout(mContext);
				relLayout.setLayoutParams(new ListView.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				relLayout.addView(cretateBuyButton(mContext));

				return relLayout;
			}

			convertView = createViewFromResource(groupPosition, childPosition,
					convertView, parent);
			final View wasView = convertView;
			SleepMusic data = (SleepMusic) getChild(groupPosition,
					childPosition);
			convertView.setTag(data);
			((TextView) convertView.findViewById(R.id.category_name_text))
					.setText(data.getCategory());
			CheckBox checkMark = (CheckBox) convertView
					.findViewById(R.id.cell_left_icon);
			checkMark.setChecked(data.getIsSelected());

			SleepMusic aff = (SleepMusic) convertView.getTag();

			convertView.findViewById(R.id.cell_right_icon).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							if (mAnimationInProgress) {
								return;
							}

							mPlayedView = wasView;
							View baseView = (View) v.getParent();
							SleepMusic aff = (SleepMusic) baseView.getTag();
							boolean newAff = (aff != mPlayedAffirmation);
							mPlayerHelper.stopPlay();

							if (!v.isSelected() && newAff) {
								v.setSelected(true);
								mPlayedAffirmation = aff;

								if (!TextUtils.isEmpty(aff.getSoundPath())) {
									mPlayerHelper.startPlay(aff.getSoundPath());
								} else {
									try {
										mPlayerHelper.startPlayAss(getActivity()
												.getAssets()
												.openFd(aff.getAssetPath())
												.getFileDescriptor());
									} catch (IOException e) {
									}
								}
							}
						}
					});

			if (wasView != null) {
				if (mPlayedAffirmation == aff) {
					mPlayedView = wasView;
					TextView tv = (TextView) mPlayedView
							.findViewById(R.id.timer_text);
					tv.setText(String.format(
							"%02d:%02d | %02d:%02d",
							mPlayerHelper.getCurrentProgress() / 60,
							(mPlayerHelper.getCurrentProgress() > 0 ? mPlayerHelper
									.getCurrentProgress() : 0) % 60,
							mPlayerHelper.getSongDuration() / 60, mPlayerHelper
									.getSongDuration() % 60));
					if (tv.getVisibility() != View.VISIBLE) {
						tv.setVisibility(View.VISIBLE);
					}
					mPlayedView.findViewById(R.id.cell_right_icon).setSelected(
							true);
				} else {
					convertView.findViewById(R.id.cell_right_icon).setSelected(
							false);
					convertView.findViewById(R.id.timer_text).setVisibility(
							View.GONE);
				}
			}

			checkMark
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton view,
								boolean isChecked) {
							SleepMusic data = (SleepMusic) ((View) view
									.getParent()).getTag();
							if (isChecked != data.getIsSelected()) {
								getTDRApplication()
										.getDataManager()
										.setSelectionSleepMusic(data, isChecked);
							}
						}
					});
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			int groupPos = (Integer) group.getTag();

			if (groupPos == GROUP_AFF_MODE) {
				getTDRApplication().getSettings().setSingleMode(
						checkedId == R.id.left_butt);
			}

			if (groupPos == GROUP_CLOCK_MODE) {
				getTDRApplication().getSettings().set24HChecked(
						checkedId == R.id.right_butt);
			}
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			getTDRApplication().getSettings().setSleepMusicChecked(isChecked);

			if (isChecked) {
				mListView.expandGroup(GROUP_AFF_MODE);
			} else {
				mListView.collapseGroup(GROUP_AFF_MODE);
			}
		}

	}

	private void configDownloadFreeButton() {
		showSubscribeDialog();
		/*
		 * if (mSharedPreferences.getInt("starts", -1) >= 0) {
		 * mDownloadFree.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View view) { showSubscribeDialog(); }
		 * }); } else { mDownloadFree.setText(R.string.download);
		 * mDownloadFree.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View view) { // startDownloadFree(); }
		 * });
		 */

	}

	@SuppressLint("NewApi")
	public void showSubscribeDialog() {
		dialog1 = new Dialog(SettingsTabNew.this.getActivity(),
				R.style.Theme_ThoughtDR_DialogCustom);
		dialog1.setContentView(R.layout.subscribe_dialog);
		dialog1.show();
		Button submit, cancel;
		final EditText fname, lname, email, phone, country;
		fname = (EditText) dialog1.findViewById(R.id.fname);
		lname = (EditText) dialog1.findViewById(R.id.lname);
		email = (EditText) dialog1.findViewById(R.id.email);
		phone = (EditText) dialog1.findViewById(R.id.phone);
		country = (EditText) dialog1.findViewById(R.id.country);
		submit = (Button) dialog1.findViewById(R.id.submit);
		cancel = (Button) dialog1.findViewById(R.id.delete_aff_button);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (fname.getText().toString() == null
						|| fname.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Please enter first name",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (lname.getText().toString() == null
						|| lname.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Please enter last name",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (email.getText().toString() == null
						|| email.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Please enter email",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (!Utils.isValidEmail(email.getText().toString())) {
					Toast.makeText(getActivity(), "Please enter valid email.",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (phone.getText().toString() == null
						|| phone.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Please enter phone",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (phone.getText().toString().length() < 10) {
					Toast.makeText(getActivity(), "Please enter valid phone",
							Toast.LENGTH_SHORT).show();
					return;
				} else if (country.getText().toString() == null
						|| country.getText().toString().equals("")) {
					Toast.makeText(getActivity(), "Please enter country",
							Toast.LENGTH_SHORT).show();
					return;
				} else {
					// dialog1.cancel();
					List<NameValuePair> pair = new ArrayList<NameValuePair>();
					pair.add(new BasicNameValuePair("first_name", fname
							.getText().toString()));
					pair.add(new BasicNameValuePair("last_name", lname
							.getText().toString()));
					pair.add(new BasicNameValuePair("email", email.getText()
							.toString()));
					pair.add(new BasicNameValuePair("phone_no", phone.getText()
							.toString()));
					pair.add(new BasicNameValuePair("country", country
							.getText().toString()));
					new BackGroudTask(SettingsTabNew.this.getActivity(),
							"Subscribing!!", Constants.NEWS_LETTER_URL,
							WebServiceHandler.GET, pair, SettingsTabNew.this);
				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.cancel();
			}
		});
	}

	private void showSubscribeDialog1() {
		final EnterDialog emailEnterDialog = new EnterDialog(mContext,
				R.string.enter_email, R.style.Theme_ThoughtDR_DialogCenter);
		emailEnterDialog.setEmptyError(R.string.empty_email_alert);
		emailEnterDialog.addRule(Patterns.EMAIL_ADDRESS,
				R.string.invalid_email_alert);
		emailEnterDialog.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		emailEnterDialog.setOnSubmitListener(new OnSubmitListener() {

			@Override
			public void onSubmit() {
				SubscribeManager.OnSubsribeListener onSubsribeListener = new SubscribeManager.OnSubsribeListener() {

					@Override
					public void onSubscribe() {
						configDownloadFreeButton();

						mDownloadFree.performClick();
					}
				};

				mSubscribeManager = new SubscribeManager(mContext,
						emailEnterDialog, onSubsribeListener);
				mSubscribeManager.execute(emailEnterDialog.getValue());
			}
		});

		emailEnterDialog
				.setOnEditorActionListener(new EnterDialog.OnEditorActionListener() {

					@Override
					public boolean onEditorAction() {
						SubscribeManager.OnSubsribeListener onSubsribeListener = new SubscribeManager.OnSubsribeListener() {

							@Override
							public void onSubscribe() {
								configDownloadFreeButton();

							}
						};

						mSubscribeManager = new SubscribeManager(mContext,
								emailEnterDialog, onSubsribeListener);
						mSubscribeManager.execute(emailEnterDialog.getValue());

						return false;
					}
				});

		emailEnterDialog.setDeleteListener(new OnDeleteListener() {

			@Override
			public void onDelete() {
				emailEnterDialog.dismiss();
			}
		});

		emailEnterDialog.show();
		emailEnterDialog.showDeleteButton();
		emailEnterDialog.getDeleteButton().setText("Cancel");
		emailEnterDialog.showSubmitButton();
	}

	private void startDownloadFree() {
		mPurchaseManager.receivePurchaseList();
	}

	@Override
	public void onProductsListReceived() {
		if (mPurchaseManager.getAffirmationsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getAffirmationsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new AffirmationDownloadTask(
						fileItem));
			}
		}

		if (mPurchaseManager.getSleepSoundsList().size() > 0) {
			for (FileItem fileItem : mPurchaseManager.getSleepSoundsList()
					.get(0).getItems()) {
				DownloadManager.addNewLoadTask(new SleepSoundsDownloadTask(
						fileItem));
			}
		}
	}

	@Override
	public void onProductsListLoadingStarted() {
		Toast.makeText(mContext, R.string.downloading_free_affirmations,
				Toast.LENGTH_LONG).show();

		if (mDownloadFree != null) {
			mDownloadFree.setEnabled(false);
		}
	}

	@Override
	public void onProductsListLoadingFailed(String errorMessage) {
	}

	@Override
	public void onProductsListLoadingCanceled() {
	}

	public class DownloadManagerLoadFileTask extends
			DownloadManager.DownloadManagerTask {

		public DownloadManagerLoadFileTask(FileItem item) {
			super(item.getFilePath());
			setTag(item);
		}

		@Override
		protected String getFilePath() {
			return Utils.getCachePath(getActivity()) + "/"
					+ Uri.parse(getUrl()).getLastPathSegment();
		}

	}

	class AffirmationDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public AffirmationDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<Affirmation> freeAffirmation = getTDRApplication()
						.getDataManager().getAffirmationsWithProductId(
								"freeAffirmation");

				for (Affirmation a : freeAffirmation) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager().deleteAffirmation(
								a);
						break;
					}
				}

				Affirmation aff = new Affirmation();
				aff.setName(fileItem.getTitle());
				Category cat = getTDRApplication().getDataManager()
						.getCategoryWitnName(fileItem.getCategory());
				if (cat == null) {
					cat = Category.createCategory(fileItem.getCategory());
					getTDRApplication().getDataManager().addNewCategory(cat);
				}
				aff.setCategory(cat);
				aff.setProductId("freeAffirmation");
				aff.setSoundPath(destinationFile);
				getTDRApplication().getDataManager().addAffirmation(aff);
			}
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}
	}

	class SleepSoundsDownloadTask extends DownloadManagerLoadFileTask implements
			DownloadManagerTaskListener {
		public SleepSoundsDownloadTask(FileItem item) {
			super(item);
			setListener(this);
		}

		@Override
		public void onDownloadComplete(String originalUri,
				String destinationFile, DownloadManagerTask task) {
			FileItem fileItem = (FileItem) task.getTag();

			if (originalUri.equals(fileItem.getFilePath())) {
				List<SleepMusic> freeSleepMusic = getTDRApplication()
						.getDataManager().getSleepSoundsWithProductId(
								"freeSleepSounds");

				for (SleepMusic a : freeSleepMusic) {
					if (a.getName().equals(fileItem.getTitle())) {
						getTDRApplication().getDataManager()
								.deleteSleepSound(a);
						break;
					}
				}

				SleepMusic sleepMusic = new SleepMusic();
				sleepMusic.setName(fileItem.getTitle());
				sleepMusic.setProductId("freeSleepSounds");
				sleepMusic.setSoundPath(destinationFile);
				sleepMusic.setCategory(fileItem.getCategory());
				getTDRApplication().getDataManager().addSleepSound(sleepMusic);
			}
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}

		@Override
		public void onDownloadStarted(String originalUri,
				DownloadManagerTask task) {
		}

		@Override
		public void onDownloadFailed(String origianlUri, String errorMessage,
				DownloadManagerTask task) {
			if (DownloadManager.getQueueSize() == 0 && mDownloadFree != null) {
				mDownloadFree.setEnabled(true);
			}
		}
	}

	@Override
	public void onCompleteTask(String response) {
		// TODO Auto-generated method stub
		System.out.println("Subscriiton Task Completed:" + response);
		if (response == null || response.equals("")) {

		} else {
			try {
				JSONObject jsonObject = new JSONObject(response);
				if (jsonObject.getString("flag").equals("1")) {
					dialog1.cancel();
					Toast.makeText(getActivity(), jsonObject.getString("msg"),
							Toast.LENGTH_SHORT).show();
					return;
				} else if (jsonObject.getString("flag").equals("0")) {
					Toast.makeText(getActivity(), jsonObject.getString("msg"),
							Toast.LENGTH_SHORT).show();
					return;
				} else {
					Toast.makeText(getActivity(),
							"Server Error.Please try again after some time.",
							Toast.LENGTH_SHORT).show();
					return;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
