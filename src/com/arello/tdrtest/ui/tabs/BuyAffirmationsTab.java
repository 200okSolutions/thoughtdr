package com.arello.tdrtest.ui.tabs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.PurchaseRecord;
import com.arello.tdrtest.purchase.PurchaseManager;
import com.arello.tdrtest.purchase.PurchaseManager.PurchaseManagerDelegate;
import com.arello.tdrtest.ui.PurchaseInfoActivity;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.tdrtest.uihelpers.adapters.PurchasesAdapter;

public class BuyAffirmationsTab extends TDRBaseTab implements PurchaseManagerDelegate, OnItemClickListener {

	int mRequestCode;
	Integer mRecordPosition;
	TDRListView mPurchasesList;
	PurchaseManager mPurchaseManager;
	PurchasesAdapter mPurchasesAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.purchases_list_layout, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mPurchasesList = (TDRListView) view.findViewById(R.id.purchases_list);
		mPurchaseManager = new PurchaseManager(this.getActivity() , this);
		mPurchaseManager.setDelegate(this);
		mPurchasesList.setOnItemClickListener(this);
		mPurchaseManager.receivePurchaseList();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onProductsListReceived() {
		getTDRActivity().hideProgress();
		mPurchasesAdapter = new PurchasesAdapter(mPurchaseManager.getAffirmationsList());
		mPurchasesList.setAdapter(mPurchasesAdapter);
	}

	@Override
	public void onProductsListLoadingStarted() {
		getTDRActivity().showProgress();
	}

	@Override
	public void onProductsListLoadingFailed(String errorMessage) {
		Utils.showAlert(this.getActivity(), errorMessage);
		getTDRActivity().hideProgress();
	}

    @Override
    public void onProductsListLoadingCanceled() {
        getTDRActivity().hideProgress();
    }

	@Override
	public void onItemClick(AdapterView<?> listView, View itemView, int position, long id) {
		mRecordPosition = position;

		Intent intent = new Intent(getActivity(), PurchaseInfoActivity.class);
		intent.putExtra(PurchaseInfoActivity.PURCHASE_TYPE, true);
		intent.putExtra(PurchaseInfoActivity.PURCHASE_ITEM, mPurchaseManager.getAffirmationsList().get(position));
		startActivityForResult(intent, mRequestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && mRecordPosition != null &&
		    mPurchaseManager.getAffirmationsList() != null && mPurchasesAdapter != null)
		{
			mPurchasesAdapter.setItem(mRecordPosition,
			                          (PurchaseRecord) data.getSerializableExtra(PurchaseInfoActivity.PURCHASE_ITEM));
			mPurchasesAdapter.notifyDataSetChanged();
		}
	}

   public boolean onBackPressed(){
       return mPurchaseManager.cancelPurchaseListReceiving();
   }
}
