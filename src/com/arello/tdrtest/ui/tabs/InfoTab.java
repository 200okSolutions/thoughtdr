package com.arello.tdrtest.ui.tabs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.uihelpers.adapters.DetailChild;
import com.arello.tdrtest.uihelpers.adapters.DetailGroup;
import com.arello.tdrtest.uihelpers.adapters.HelpListAdapter;
import com.arello.tdrtest.uihelpers.adapters.impl.SimpleChild;
import com.arello.tdrtest.uihelpers.adapters.impl.SimpleGroup;

import java.util.ArrayList;
import java.util.List;

public class InfoTab extends TDRBaseTab {

	private Context mContext;
	private ExpandableListView mHelpList;
	private List<DetailGroup> mHelpListGroups;
	private WebView mVideo;
	private String mVideoHtml;
	private ViewGroup mVideoWrapper;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	    return inflater.inflate(R.layout.tab_info, null);
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mContext = getActivity();

		mHelpList = (ExpandableListView) view.findViewById(R.id.help_list);

		initVideo();

		initHelpList();

		showHelpList();
	}

	private void showHelpList()
	{
		HelpListAdapter listAdapter = new HelpListAdapter(mContext, mHelpListGroups);
		mHelpList.setAdapter(listAdapter);

		mHelpList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener()
		{
			@Override
			public void onGroupCollapse(int i)
			{
				if (i == 0)
				{
					mVideo.stopLoading();
					mVideoWrapper.removeView(mVideo);
					mVideo.loadData("", "text/html", null);
				}
			}
		});

		mHelpList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener()
		{
			@Override
			public void onGroupExpand(int i)
			{
				if (i == 0)
				{
					mVideo.loadData(mVideoHtml, "text/html", null);
					mVideoWrapper.addView(mVideo, 0);
				}
			}
		});
	}

	private void initHelpList()
	{
		mHelpListGroups = new ArrayList<DetailGroup>();

		//mHelpListGroups.add(getHelpVideoGroup());
     //   TextView mTextView = new TextView(getActivity().getApplicationContext());
     //   mTextView.setText(R.string.head_string);
        mHelpListGroups.add(getTextGroup(R.string.head_string, R.string.head_string));

        mHelpListGroups.add(getTextGroup(R.string.getting_started_group, R.string.getting_started));
		mHelpListGroups.add(getTextGroup(R.string.night_help_group, R.string.night_help));
		mHelpListGroups.add(getTextGroup(R.string.manage_help_group, R.string.manage_help));
		mHelpListGroups.add(getTextGroup(R.string.day_help_group, R.string.day_help));
		mHelpListGroups.add(getTextGroup(R.string.settings_help_group, R.string.settings_help));
		mHelpListGroups.add(getTextGroup(R.string.buy_help_group, R.string.buy_help));
		/*mHelpListGroups.add(getTextGroup(R.string.day_help_group, R.string.day_help));
		mHelpListGroups.add(getTextGroup(R.string.manage_help_group, R.string.manage_help));
		mHelpListGroups.add(getTextGroup(R.string.record_help_group, R.string.record_help));
		mHelpListGroups.add(getTextGroup(R.string.settings_help_group, R.string.settings_help));*/
	}

	private DetailGroup getHelpVideoGroup()
	{
		View groupView = LayoutInflater.from(mContext).inflate(R.layout.help_group, null);
		groupView.findViewById(R.id.video).setVisibility(View.VISIBLE);
		((TextView) groupView.findViewById(R.id.name)).setText(getString(R.string.help_video_group));

		DetailGroup group = new SimpleGroup(groupView);
		List<DetailChild> groupChildren = new ArrayList<DetailChild>();
		group.setChildren(groupChildren);

		groupChildren.add(new SimpleChild(mVideoWrapper));

		return group;
	}

	private void initVideo()
	{
		mVideoWrapper = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.help_video_child, null);

		final View progress = mVideoWrapper.findViewById(R.id.loading);

		mVideo = new WebView(mContext);
		mVideo.getSettings().setUserAgentString("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.36 (KHTML, " +
		                                       "like Gecko) Chrome/13.0.766.0 Safari/534.36");
		// Need for old devices (x.x < 2.2)
		//mVideo.getSettings().setPluginsEnabled(true);
		mVideo.getSettings().setJavaScriptEnabled(true);
		mVideo.setWebViewClient(new WebViewClient()
		{
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				progress.setVisibility(View.VISIBLE);
			}
			public void onPageFinished(WebView view, String url) {
				progress.setVisibility(View.GONE);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				return false;
			}
		});

		mVideoHtml = "<object><param name=\"movie\" value=\"http://www.youtube" +
		             ".com/v/Vw4KVoEVcr0?version=3&feature=player_detailpage\"><param name=\"allowFullScreen\" " +
		             "value=\"true\"><param name=\"allowScriptAccess\" value=\"always\"><embed src=\"http://www" +
		             ".youtube.com/v/Vw4KVoEVcr0?version=3&feature=player_detailpage\" " +
		             "type=\"application/x-shockwave-flash\" allowfullscreen=\"true\" " +
		             "allowScriptAccess=\"always\"></object>";

		mVideoHtml = "<!DOCTYPE html>\n" +
		             "<html>\n" +
		             "<head>\n" +
		             "<meta charset=utf-8 />\n" +
		             "<title>JS Bin</title>\n" +
		             "<!--[if IE]>\n" +
		             "  <script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script>\n" +
		             "<![endif]-->\n" +
		             "<style>\n" +
		             " </style>\n" +
		             "</head>\n" +
		             "<body>\n" +
		             "  <iframe src=\"http://www.youtube.com/embed/Vw4KVoEVcr0\" frameborder=\"0\" allowfullscreen></iframe>\n" +
		             "</iframe>\n" +
		             "</body>\n" +
		             "</html>";
	}

	private DetailGroup getTextGroup(int groupTitle, int groupContent)
	{
		View groupView = LayoutInflater.from(mContext).inflate(R.layout.help_group, null);
        if(groupTitle == R.string.head_string){
            ((TextView) groupView.findViewById(R.id.name)).setLinkTextColor(Color.RED);
            ((TextView) groupView.findViewById(R.id.name)).setAutoLinkMask(Linkify.ALL);
            ((TextView) groupView.findViewById(R.id.name)).setTextSize(15);
            ((TextView) groupView.findViewById(R.id.name)).setText(groupTitle);
            ((TextView) groupView.findViewById(R.id.name)).setFocusable(true);
        }
        else
        {
            ((TextView) groupView.findViewById(R.id.name)).setText(groupTitle);
        }

		DetailGroup group = new SimpleGroup(groupView);
		List<DetailChild> groupChildren = new ArrayList<DetailChild>();
		group.setChildren(groupChildren);

		groupChildren.add(new SimpleChild(getChildView(groupContent)));

		return group;
	}

	private View getChildView(int resId)
	{
		TextView view = (TextView) LayoutInflater.from(mContext).inflate(R.layout.help_text_child, null);

        view.setLinkTextColor(Color.RED);

        view.setAutoLinkMask(Linkify.ALL);
		view.setText(Html.fromHtml(getString(resId)));

        view.setFocusable(true);

		return view;
	}

	@Override
	public void onPause()
	{
		mVideo.stopLoading();
		mVideo.loadData("", "text/html", null);

		super.onPause();
	}
}