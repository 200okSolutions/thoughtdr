package com.arello.tdrtest.ui.tabs;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.ui.ManageAffirmationActivity;
import com.arello.tdrtest.ui.PlayActivity;
import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.TDRMenu;
import com.arello.tdrtest.uihelpers.Utils;
import com.arello.tdrtest.uihelpers.adapters.AffirmationListAdapter;

public class NightTab extends TDRBaseTab {

	TDRMenu mOptionsMenu;
	Button mSetupButton;
	String[] mMenuItems;
	AffirmationListAdapter mListAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.tab_night, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mSetupButton = (Button) view.findViewById(R.id.setup_button);
		mSetupButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Settings settings = new Settings(getActivity());

				if (!settings.getHibTimePopupWas()) {
					settings.setHibTimePopupWas(true);
				}

				// Toast.makeText(getActivity(), R.string.the_first_hibernation,
				// Toast.LENGTH_SHORT).show();
				mOptionsMenu.showMenu(new Point(mSetupButton.getRight() - 15,
						mSetupButton.getBottom() - 5));

			}
		});

		mOptionsMenu = new TDRMenu((TDRBaseActivity) getActivity());

		mMenuItems = getResources().getStringArray(R.array.quick_options);

		for (String item : mMenuItems) {
			if (item.equals(mMenuItems[mMenuItems.length - 1])) {
				continue;
			}

			mOptionsMenu.addMenuItem(item, new OnClickListener() {

				@Override
				public void onClick(View v) {
					String option = (String) v.getTag();
					selectOption(option);
					// getTDRApplication().getSettings().setSingleMode(false);
					try {
						int mins = Integer.parseInt(option.split("\\ ")[0]);
						getTDRApplication().getSettings().setHybernationTime(
								mins);
					} catch (NumberFormatException e) {
						if (option.equals(mMenuItems[0])) {
							getTDRApplication().getSettings()
									.setHybernationTime(0);
						} else if (option
								.equals(mMenuItems[mMenuItems.length - 1])) {
							getTDRApplication().getSettings().setSingleMode(
									true);
						}
					}
					Settings settings = new Settings(getActivity());
					if (settings.IS_HIBERNATE_ACTIVE()) {
						if (showFirstHibernationDialog(getActivity())) {
							AlertDialog.Builder ad = new AlertDialog.Builder(
									getActivity());
							ad.setTitle("Information!");
							ad.setMessage(R.string.the_first_hibernation);
							ad.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// startPlayActivity();
										}
									});
							ad.setIcon(R.drawable.caution_sign);
							ad.show();
						}
					}
				}
			});
		}

		mListAdapter = new AffirmationListAdapter(
				((TDRBaseActivity) getActivity()), getAffirmationList(), false);

		((TDRListView) view.findViewById(R.id.aff_list))
				.setAdapter(mListAdapter);
		((TDRListView) view.findViewById(R.id.aff_list))
				.setOnItemClickListener(mListAdapter);
		view.findViewById(R.id.manage_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View view) {
						startActivity(new Intent(getTDRActivity(),
								ManageAffirmationActivity.class));
					}
				});
		view.findViewById(R.id.start_button).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						onStartClick();
					}
				});
	}

	@Override
	public void onResume() {
		super.onResume();

		int hybernationTime = getTDRApplication().getSettings()
				.getHybernationTime();
		boolean isSingleMode = getTDRApplication().getSettings().isSingleMode();

		/*
		 * if (isSingleMode) { mSetupButton.setText("Single"); } else
		 */
		{
			for (String item : mMenuItems) {
				if ((hybernationTime == 0 && item.equals(mMenuItems[0]))
						|| (item.startsWith(String.valueOf(hybernationTime)))) {
					selectOption(item);
				}
			}
		}

		mListAdapter.setData(getAffirmationList());
		mListAdapter.notifyDataSetChanged();
	}

	protected void selectOption(String text) {
		mSetupButton.setText(text);
	}

	protected List<Affirmation> getAffirmationList() {
		return getTDRActivity().getTDRApplication().getDataManager()
				.getNightAffirmations();
	}

	protected void onManageAffirmationStart() {
		getTDRActivity().getTDRApplication().getDataManager().editModeDay = false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_CANCELED) {
			// getActivity().finish();
		}
	}

	protected void onStartClick() {
		if (getTDRApplication().getDataManager().getNightAffirmations().size() > 0) {
			// startActivity(intent);
			long time = System.currentTimeMillis();
			Utils.saveSharedPreferences(Constants.START_TIME,
					String.valueOf(time), getActivity());
			startPlayActivity();
		} else {
			Utils.showAlert(getActivity(), R.string.no_affirmations_alert);
		}
	}

	private void startPlayActivity() {
		Intent intent = new Intent(getActivity(), PlayActivity.class);
		startActivityForResult(intent, 100);
	}

	private static final String THOUGHT_DR_FIRST_HIBERNATION_PREF = "THOUGHT_DR_FIRST_HIBERNATION_PREF";
	private static final String THOUGHT_DR_FIRST_HIBERNATION_PREF_KEY = "THOUGHT_DR_FIRST_HIBERNATION_PREF_KEY";

	private SharedPreferences getPref(Context context) {
		return context.getSharedPreferences(THOUGHT_DR_FIRST_HIBERNATION_PREF,
				Context.MODE_PRIVATE);
	}

	private SharedPreferences.Editor getEditor(Context context) {
		return getPref(context).edit();
	}

	private boolean showFirstHibernationDialog(Context context) {
		boolean show = getPref(context).getBoolean(
				THOUGHT_DR_FIRST_HIBERNATION_PREF_KEY, true);

		if (show) {
			getEditor(context).putBoolean(
					THOUGHT_DR_FIRST_HIBERNATION_PREF_KEY, false).commit();
		}
		return show;
	}

}
