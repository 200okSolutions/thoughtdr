package com.arello.tdrtest.ui;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.TDRApplication;

public class TDRBaseActivity extends FragmentActivity {
	RelativeLayout mRootLayout;
	View mContent;
	View mProgress;

	@Override
	public void setContentView(int layoutResID) {
		mRootLayout = new RelativeLayout(this);
		mRootLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		setContentView(mRootLayout);

		getWindow().setBackgroundDrawableResource(R.drawable.background);

		LayoutInflater inflater = getLayoutInflater();

		mProgress = inflater.inflate(R.layout.progress_layout, null);
		mContent = inflater.inflate(layoutResID, null);

		mContent.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		RelativeLayout.LayoutParams bgParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		bgParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		/*
		 * ImageView bgImage = new ImageView(this);
		 * bgImage.setScaleType(ScaleType.CENTER);
		 * bgImage.setImageResource(R.drawable.background);
		 * bgImage.setLayoutParams(bgParams);
		 */

		mProgress.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		// mRootLayout.addView(bgImage);
		mRootLayout.addView(mContent);
		mRootLayout.addView(mProgress);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public RelativeLayout getLayoutRoot() {
		return mRootLayout;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	}

	public void showProgress() {
		if (null != mProgress) {
			mProgress.setVisibility(View.VISIBLE);
		}
	}

	public void hideProgress() {
		if (null != mProgress) {
			mProgress.setVisibility(View.GONE);
		}
	}

	protected void showAlert(int message, int positiveButton) {
		showAlert(getString(message), positiveButton);
	}

	protected void showAlert(String message, int positiveButton) {
		new AlertDialog.Builder(this).setTitle(R.string.app_name)
				.setMessage(message).setPositiveButton(positiveButton, null)
				.show();
	}

	public TDRApplication getTDRApplication() {
		return (TDRApplication) super.getApplication();
	}
}
