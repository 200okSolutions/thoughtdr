package com.arello.tdrtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.TDRDataManager.OnDataChangedListener;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.ui.custom_views.TDRListView;
import com.arello.tdrtest.ui.custom_views.TDRListView.DoubleClickListener;
import com.arello.tdrtest.ui.custom_views.dialogs.ActionSheet;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog;
import com.arello.tdrtest.ui.custom_views.dialogs.EnterDialog.OnDeleteListener;
import com.arello.tdrtest.ui.custom_views.dialogs.TDRBaseDialog.OnHideDialogListener;
import com.arello.tdrtest.uihelpers.adapters.AffirmationListAdapter;
import com.arello.tdrtest.uihelpers.adapters.UITableViewAdapter.IndexPath;
import com.arello.tdrtest.uihelpers.adapters.UITableViewAdapter.OnItemClickListener;
import com.arello.tdrtest.uihelpers.adapters.UITableViewAdapter.OnMoveCellLisener;
import com.arello.tdrtest.uihelpers.adapters.UITableViewAdapter.OnRemoveCellListener;

public class ManageAffirmationActivity extends TDRBaseActivity implements
		OnDataChangedListener, DoubleClickListener, OnMoveCellLisener,
		OnRemoveCellListener, OnItemClickListener,
		ExpandableListView.OnGroupClickListener,
		ExpandableListView.OnGroupCollapseListener, OnDeleteListener {
	private AffirmationListAdapter mListAdapter;
	private EnterDialog mEnterRecordName;

	TDRListView mListView;
	Affirmation mEditedAffirmation;
	Affirmation mAffirmationToDelete;
	ActionSheet mDeleteActionSheet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_affirmation_layout);

		findViewById(R.id.new_affirmation_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						mListAdapter.stopPlay();
						startActivity(new Intent(
								ManageAffirmationActivity.this,
								RecordAffirmationActivity.class));
					}
				});
		mListView = (TDRListView) findViewById(R.id.aff_list);

		mListView.setOnDoubleClickListener(this);

		mEnterRecordName = new EnterDialog(this, R.string.record_name);
		mEnterRecordName.setEmptyError(R.string.empty_affirmation_name_alert);
		mEnterRecordName
				.setOnEditorActionListener(new EnterDialog.OnEditorActionListener() {
					@Override
					public boolean onEditorAction() {
						getTDRApplication().getDataManager()
								.renameAffirmation(mEditedAffirmation,
										mEnterRecordName.getValue());

						mEnterRecordName.dismiss();

						return false;
					}
				});

		mDeleteActionSheet = new ActionSheet(this);
		mDeleteActionSheet.setTitle(R.string.delete_warning);
		mDeleteActionSheet.setDestructiveButtonTitle(R.string.delete);
		mDeleteActionSheet
				.setOtherButtons(new int[] { android.R.string.cancel });
		mDeleteActionSheet.setOnHideListenet(new OnHideDialogListener() {

			@Override
			public void onHide() {
				if (mDeleteActionSheet.getResult() == ActionSheet.DESTRUCTIVE_BUTTON_INDEX) {
					getTDRApplication().getDataManager().deleteAffirmation(
							mAffirmationToDelete);
				}
			}
		});

		findViewById(R.id.add_affirmations_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						mListAdapter.stopPlay();
						startActivity(new Intent(
								ManageAffirmationActivity.this,
								AddAffirmationsActivity.class));
					}
				});
	}

	@Override
	protected void onResume() {
		super.onResume();
		getTDRApplication().getDataManager()
				.setOnAffirmationDataChangedListener(this);

		mListAdapter = new AffirmationListAdapter(this, getTDRApplication()
				.getDataManager().getAllAffirmation(), true);
		mListAdapter.setOnMoveCellListener(this);
		mListAdapter.setOnRemoveCellListener(this);
		mListAdapter.setOnItemClickListener(this);
		mListView.setMoveListener(mListAdapter);
		mListView.setOnItemClickListener(mListAdapter);
		mListView.setRemoveListener(mListAdapter);
		mListView.setAdapter(mListAdapter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mListAdapter.stopPlay();
		mListAdapter.commitSortOrder();
		getTDRApplication().getDataManager()
				.setOnAffirmationDataChangedListener(null);
	}

	@Override
	public void onDataChanged() {
		mListAdapter.setData(getTDRApplication().getDataManager()
				.getAllAffirmation());
		mListAdapter.notifyDataSetChanged();
		mListView.forceLayout();
	}

	@Override
	public void onDoubleClick(int index, View view) {
	}

	@Override
	public void moveCell(IndexPath from, IndexPath to) {
		Affirmation aff = mListAdapter.getItem(from);
		getTDRApplication().getDataManager().deleteAffirmation(aff);
		if (from.section != to.section) {
			Category cat = mListAdapter.getCategory(to.section);
			aff.setCategory(cat);
		}
		aff.setSortOrder(to.row);
		getTDRApplication().getDataManager().addAffirmation(aff);
	}

	@Override
	public void removeCell(IndexPath cell) {
		mAffirmationToDelete = mListAdapter.getItem(cell);
		mDeleteActionSheet.showDialog();
	}

	@Override
	public void onItemClick(AdapterView<?> view, View itemView, IndexPath path) {
		if (path.row == -1) {
			getTDRApplication().getDataManager().hideUnhideCategory(
					mListAdapter.getCategory(path.section));
			mListAdapter.notifyDataSetChanged();
		} else {
			Affirmation aff = mListAdapter.getItem(path);
			if (aff.getProductId() == null) {
				mEditedAffirmation = aff;
				mEnterRecordName.setValue(aff.getName());
				mEnterRecordName.setDeleteListener(this);
				mEnterRecordName.show();
				mEnterRecordName.showDeleteButton();
			} else {
				mAffirmationToDelete = aff;
				mDeleteActionSheet.showDialog();
			}
		}
	}

	@Override
	public void moveStarted(IndexPath from) {
		mListAdapter.stopPlay();
	}

	@Override
	public void onConfigurationChanged(
			android.content.res.Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onGroupClick(ExpandableListView listView, View view, int i,
			long l) {
		return false;
	}

	@Override
	public void onGroupCollapse(int i) {

	}

	@Override
	public void onDelete() {
		getTDRApplication().getDataManager().deleteAffirmation(
				mEditedAffirmation);

		mEnterRecordName.dismiss();
	}
}
