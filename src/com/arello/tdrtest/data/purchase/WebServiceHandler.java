package com.arello.tdrtest.data.purchase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class WebServiceHandler {

	public final static int GET = 1;
	public final static int POST = 2;
	public final static int IMAGEPOST = 3;
	static DefaultHttpClient httpClient = new DefaultHttpClient();
	static HttpEntity httpEntity = null;
	static HttpResponse httpResponse = null;

	public static String makeServiceCall(String url, int method,
			List<NameValuePair> params) throws ConnectTimeoutException,
			UnsupportedEncodingException, IOException {
		String response = null;
		// http client
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpEntity httpEntity = null;
		HttpResponse httpResponse = null;

		// Checking http request method type
		if (method == POST) {
			HttpPost httpPost = new HttpPost(url);
			// adding post params
			if (params != null) {
				httpPost.setEntity(new UrlEncodedFormEntity(params));
			}

			httpResponse = httpClient.execute(httpPost);

		} else if (method == GET) {
			// appending params to url
			if (params != null) {
				String paramString = URLEncodedUtils.format(params, "utf-8");
				url += "?" + paramString;
			}
			HttpGet httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

		}
		httpEntity = httpResponse.getEntity();
		response = EntityUtils.toString(httpEntity);
		httpResponse.getEntity().consumeContent();
		return response;
	}
}