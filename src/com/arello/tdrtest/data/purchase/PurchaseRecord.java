package com.arello.tdrtest.data.purchase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PurchaseRecord implements Serializable {
	private String productID;
	private String title;
	private String description;
	private String value;
	private List<FileItem> items;
	private boolean purchased;
	private boolean downloaded;

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<FileItem> getItems() {
		return items;
	}

	public void setItems(List<FileItem> items) {
		this.items = items;
	}

	public boolean isPurchased() {
		return purchased;
	}

	public void setPurchased(boolean purchased) {
		this.purchased = purchased;
	}

	public boolean isDownloaded() {
		return downloaded;
	}

	public void setDownloaded(boolean downloaded) {
		this.downloaded = downloaded;
	}

	public static PurchaseRecord createPurchaseRecordFromMap(
			Map<String, Object> map) {
		PurchaseRecord record = new PurchaseRecord();
		record.setProductID(((String) map.get("productID_android")));
		record.setTitle(((String) map.get("title")));
		record.setDescription(((String) map.get("description")));
		record.setValue(((String) map.get("value")));

		List<FileItem> items = new ArrayList<FileItem>();
		List<Map<String, Object>> files = (List<Map<String, Object>>) map
				.get("files");
		for (Map<String, Object> file : files) {
			items.add(FileItem.createItemFromMap(file));
		}

		record.setItems(items);

		return record;
	}
}
