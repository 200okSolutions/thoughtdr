package com.arello.tdrtest.data.purchase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class WebServiceHandlerSecure {

	public final static int GET = 1;
	public final static int POST = 2;
	public final static int IMAGEPOST = 3;
	static HttpClient httpClient = getHttpsClient(new DefaultHttpClient());
	static HttpEntity httpEntity = null;
	static HttpResponse httpResponse = null;

	public static String makeServiceCall(String url, int method,
			List<NameValuePair> params) throws ConnectTimeoutException,
			UnsupportedEncodingException, IOException {
		String response = null;
		// http client
		// DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpEntity httpEntity = null;
		HttpResponse httpResponse = null;

		// Checking http request method type
		if (method == POST) {
			HttpPost httpPost = new HttpPost(url);
			// adding post params
			if (params != null) {
				httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			}

			httpResponse = httpClient.execute(httpPost);

		} else if (method == GET) {
			// appending params to url
			if (params != null) {
				String paramString = URLEncodedUtils.format(params, "utf-8");
				url += "?" + paramString;
			}
			HttpGet httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

		}
		httpEntity = httpResponse.getEntity();
		response = EntityUtils.toString(httpEntity);
		httpResponse.getEntity().consumeContent();
		return response;
	}

	public static HttpClient getHttpsClient(HttpClient client) {
		try {
			X509TrustManager x509TrustManager = new X509TrustManager() {

				@Override
				public void checkClientTrusted(
						java.security.cert.X509Certificate[] chain,
						String authType)
						throws java.security.cert.CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public void checkServerTrusted(
						java.security.cert.X509Certificate[] chain,
						String authType)
						throws java.security.cert.CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					// TODO Auto-generated method stub
					return null;
				}
			};

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext
					.init(null, new TrustManager[] { x509TrustManager }, null);
			SSLSocketFactory sslSocketFactory = new ExSSLSocketFactory(
					sslContext);
			sslSocketFactory
					.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager clientConnectionManager = client
					.getConnectionManager();
			SchemeRegistry schemeRegistry = clientConnectionManager
					.getSchemeRegistry();
			schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));
			return new DefaultHttpClient(clientConnectionManager,
					client.getParams());
		} catch (Exception ex) {
			return null;
		}
	}
}