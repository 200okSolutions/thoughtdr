package com.arello.tdrtest.data.purchase;

import java.io.Serializable;
import java.util.Map;

import com.arello.tdrtest.Constants;

public class FileItem implements Serializable {
	private String title;
	private String category;
	private String filePath;
	private String identifier;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public static FileItem createItemFromMap(Map<String, Object> map) {
		FileItem item = new FileItem();
		item.setTitle((String) map.get("title"));
		item.setFilePath(Constants.PURCHASE_FTP_BASE_DIRECTORY
				+ (String) map.get("filename"));
		item.setCategory((String) map.get("category"));
		return item;
	}

}
