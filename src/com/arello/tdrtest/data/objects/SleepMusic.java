package com.arello.tdrtest.data.objects;

import com.arellomobile.android.libs.cache.ormlite.field.DatabaseField;

public class SleepMusic
{
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	String name;
	@DatabaseField
	int sortOrder;
	@DatabaseField
	String soundPath;
	@DatabaseField
	boolean isSelected;
	@DatabaseField
	String category;
	@DatabaseField
	private String productId;

	@DatabaseField
	private String mAssetPath;

	public int getId()
	{
		return id;
	}

	public void setId(int value)
	{
		id = value;
	}

	public String getSoundPath()
	{
		return soundPath;
	}

	public void setSoundPath(String value)
	{
		soundPath = value;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String value)
	{
		name = value;
	}

	public int getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(int value)
	{
		sortOrder = value;
	}

	public boolean getIsSelected()
	{
		return isSelected;
	}

	public void setIsSelected(boolean value)
	{
		isSelected = value;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String value)
	{
		category = value;
	}

	public static SleepMusic createSleepMusic(String name, String categoryName, String fileName)
	{
		SleepMusic sleepMusic = new SleepMusic();
		sleepMusic.setName(name);
		sleepMusic.setSoundPath(fileName);
		sleepMusic.setCategory(categoryName);
		return sleepMusic;
	}

	public static SleepMusic createSleepMusic(String name, String categoryName, String fileName, String assetname)
	{
		SleepMusic sleepMusic = new SleepMusic();
		sleepMusic.setName(name);
		sleepMusic.setSoundPath(fileName);
		sleepMusic.setAssetPath(assetname);
		sleepMusic.setCategory(categoryName);
		return sleepMusic;
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public String getAssetPath()
	{
		return mAssetPath;
	}

	public void setAssetPath(String assetPath)
	{
		mAssetPath = assetPath;
	}

}
