package com.arello.tdrtest.data.objects;

import android.database.Cursor;
import android.provider.MediaStore.Audio.Media;

import com.arellomobile.android.libs.cache.ormlite.field.DatabaseField;
import com.arellomobile.android.libs.cache.ormlite.table.DatabaseTable;

import java.util.Map;

@DatabaseTable(tableName = Music.MUSIC_TABLE_NAME)
public class Music {
	public final static String MUSIC_TABLE_NAME = "MusicTable"; 
	public final static String SORT_ORDER = "SortOrder";
	@DatabaseField(generatedId = true, columnName = Media._ID)
	int id;
	@DatabaseField(columnName = Media.TITLE)
	String name;
	@DatabaseField(columnName = SORT_ORDER)
	int sortOrder;
	@DatabaseField(columnName = Media.DATA)
	String soundPath;
	@DatabaseField(columnName = Media.SIZE)
	long size;
	@DatabaseField(columnName = Media.ARTIST)
	String artist;
	
	public int getId() {
		return id;
	}
	
	public void setId(int value) {
		id = value;
	}
	
	public String getSoundPath() {
		return soundPath;
	}
	
	public void setSoundPath(String value) {
		soundPath = value;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String value) {
		name = value.trim();
	}
	
	public int getSortOrder() {
		return sortOrder;
	}
	
	public void setSortOrder(int value) {
		sortOrder = value;
	}
	
	public long getSize() {
		return size;
	}
	
	public void setSize(long value) {
		size = value;
	}
	
	public void setValuesFromCursor(Cursor cursor) {
		setId(cursor.getInt(cursor.getColumnIndex(Media._ID)));
		setSoundPath(cursor.getString(cursor.getColumnIndex(Media.DATA)));
		setName(cursor.getString(cursor.getColumnIndex(Media.TITLE)));
		String artist = cursor.getString(cursor.getColumnIndex(Media.ARTIST));
		if (artist == null || artist.equals("<unknown>")) {
			setArtist(null);
		} else {
			setArtist(cursor.getString(cursor.getColumnIndex(Media.ARTIST)));
		}
		setSize(cursor.getLong(cursor.getColumnIndex(Media.SIZE)));
		int sortOrderIndex = cursor.getColumnIndex(SORT_ORDER);
		if (sortOrderIndex != -1)
			setSortOrder(cursor.getInt(sortOrderIndex));
	}
	
	public static Music musicFromCursor(Cursor cursor) {
		Music music = new Music();
		music.setValuesFromCursor(cursor);
		return music;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
}
