package com.arello.tdrtest.data.objects;

import com.arellomobile.android.libs.cache.ormlite.field.DatabaseField;
import com.arellomobile.android.libs.cache.ormlite.table.DatabaseTable;

@DatabaseTable
public class Affirmation {
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	String name;
	@DatabaseField
	String soundPath;
	@DatabaseField
	int sortOrder;
	@DatabaseField
	boolean playNight;
	@DatabaseField(foreign = true)
	Category category;
	@DatabaseField
	int playlistSortOrder;
	@DatabaseField
	private String productId;

	public int getId() {
		return id;
	}

	public void setId(int value) {
		id = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getSoundPath() {
		return soundPath;
	}

	public void setSoundPath(String value) {
		soundPath = value;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int value) {
		sortOrder = value;
	}

	public boolean getPlayNight() {
		return playNight;
	}

	public void setPlayNight(boolean value) {
		playNight = value;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category value) {
		category = value;
	}

	public int getPlaylistSortOrder() {
		return playlistSortOrder;
	}

	public void setPlaylistSortOrder(int value) {
		playlistSortOrder = value;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Affirmation) {
			return ((Affirmation) o).getId() == id;
		}
		return false;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
}
