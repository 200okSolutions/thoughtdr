package com.arello.tdrtest.data.objects;

import com.arellomobile.android.libs.cache.ormlite.field.DatabaseField;
import com.arellomobile.android.libs.cache.ormlite.table.DatabaseTable;

@DatabaseTable
public class Category implements Comparable<Category> {
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	String title;
	@DatabaseField
	boolean hidden;
	
	public int getId() {
		return id;
	}
	
	public void setId(int value) {
		id = value;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String value) {
		title = value;
	}
	
	public boolean getHidden() {
		return hidden;
	}
	
	public void setHidden(boolean value) {
		hidden = value;
	}
	
	public static Category createCategory(String title) {
		Category cat = new Category();
		cat.setTitle(title);
		cat.setHidden(false);
		return cat;
	}
	
	@Override
	public String toString() {
		return getTitle();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Category) {
			return ((Category)o).getId() == id;
		}
		return false;
	}

	@Override
	public int compareTo(Category another) {
		return id - another.id;
	}
}
