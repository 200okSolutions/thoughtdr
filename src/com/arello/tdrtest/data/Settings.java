package com.arello.tdrtest.data;

import java.util.HashMap;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.model.Products;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {
	static final String HYBERNATION_TIME_KEY = "HYBERNATION_TIME";
	static final int DEFAULT_HYBERNATION_TIME = 90;
	static final String MUSIC_VOLUME_KEY = "MUSIC_VOLUME_KEY";
	static final float DEFAULT_MUSIC_VOLUME = 0.7f;
	static final String SLEEP_SOUNDS_VOLUME_KEY = "SLEEP_SOUNDS_VOLUME_KEY";
	static final float DEFAULT_SLEEP_SOUNDS_VOLUME = 0.7f;
	static final String AFFIRMATION_VOLUME_KEY = "AFFIRMATION_VOLUME_KEY";
	static final float DEFAULT_AFFIRMATION_VOLUME = 0.4f;
	static final String SINGLE_MODE = "SINGLE_MODE_KEY";
	static final boolean SINGLE_MODE_DEFAULT = false;
	static final String SLEEP_MUSIC_CHECKED = "SLEEP_MUSIC_CHECKED_KEY";
	static final boolean SLEEP_MUSIC_CHECKED_DEFAULT = false;

	static final String HIB_TIME_POPUP_WAS = "HIB_TIME_POPUP_WAS";
	static final boolean HIB_TIME_POPUP_WAS_DEFAULT = false;

	static final String _24H_CHECKED = "24H_CHECKED_KEY";
	static final boolean _25H_CHECKED_DEFAULT = false;

	static final String IS_HIBERNATE_ACTIVE_KEY = "IS_HIBERNATE_ACTIVE";
	static final boolean IS_HIBERNATE_ACTIVE = true;

	SharedPreferences mPrefs;

	public Settings(Context context) {
		mPrefs = context.getSharedPreferences(
				getClass().getPackage().getName(), Context.MODE_PRIVATE);
	}

	public boolean getHibTimePopupWas() {
		return mPrefs
				.getBoolean(HIB_TIME_POPUP_WAS, HIB_TIME_POPUP_WAS_DEFAULT);
	}

	public void setHibTimePopupWas(boolean value) {
		mPrefs.edit().putBoolean(HIB_TIME_POPUP_WAS, value).commit();
	}

	public int getHybernationTime() {
		return mPrefs.getInt(HYBERNATION_TIME_KEY, DEFAULT_HYBERNATION_TIME);
	}

	public void setHybernationTime(int time) {
		mPrefs.edit().putInt(HYBERNATION_TIME_KEY, time).commit();
	}

	public void setMusicVolume(float volume) {
		mPrefs.edit().putFloat(MUSIC_VOLUME_KEY, volume).commit();
	}

	public float getMusicVolume() {
		return mPrefs.getFloat(MUSIC_VOLUME_KEY, DEFAULT_MUSIC_VOLUME);
	}

	public void setSleepSoundsVolume(float volume) {
		mPrefs.edit().putFloat(SLEEP_SOUNDS_VOLUME_KEY, volume).commit();
	}

	public float getSleepSoundsVolume() {
		return mPrefs.getFloat(SLEEP_SOUNDS_VOLUME_KEY,
				DEFAULT_SLEEP_SOUNDS_VOLUME);
	}

	public void setAffirmationVolume(float volume) {
		mPrefs.edit().putFloat(AFFIRMATION_VOLUME_KEY, volume).commit();
	}

	public float getAffirmationVolume() {
		return mPrefs.getFloat(AFFIRMATION_VOLUME_KEY,
				DEFAULT_AFFIRMATION_VOLUME);
	}

	public boolean isSingleMode() {
		System.out.println("single mode:"
				+ mPrefs.getBoolean(SINGLE_MODE, SINGLE_MODE_DEFAULT));
		return mPrefs.getBoolean(SINGLE_MODE, SINGLE_MODE_DEFAULT);
	}

	public void setSingleMode(boolean value) {
		System.out.println("single mode:" + value);
		mPrefs.edit().putBoolean(SINGLE_MODE, value).commit();
	}

	public boolean getSleepMusicChecked() {
		return mPrefs.getBoolean(SLEEP_MUSIC_CHECKED,
				SLEEP_MUSIC_CHECKED_DEFAULT);
	}

	public void setSleepMusicChecked(boolean value) {
		mPrefs.edit().putBoolean(SLEEP_MUSIC_CHECKED, value).commit();
	}

	public boolean get24HChecked() {
		return mPrefs.getBoolean(_24H_CHECKED, _25H_CHECKED_DEFAULT);
	}

	public void set24HChecked(boolean value) {
		mPrefs.edit().putBoolean(_24H_CHECKED, value).commit();
	}

	public boolean IS_HIBERNATE_ACTIVE() {
		return mPrefs.getBoolean(IS_HIBERNATE_ACTIVE_KEY, IS_HIBERNATE_ACTIVE);
	}

	public void setIS_HIBERNATE_ACTIVE(boolean value) {
		mPrefs.edit().putBoolean(IS_HIBERNATE_ACTIVE_KEY, value).commit();
	}

	public HashMap<Integer, Products> getCart() {
		return Constants.cart;
	}

	public void addProductToCart(int id, Products products) {
		Constants.cart.put(id, products);
	}

	public boolean isCartContainsProduct(int id) {
		return Constants.cart.containsKey(id);
	}

	public void removeProduct(int id) {
		Constants.cart.remove(id);
	}
}
