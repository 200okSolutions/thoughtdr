package com.arello.tdrtest.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arellomobile.android.libs.cache.cache.BasicOrmLiteOpenHelper;
import com.arellomobile.android.libs.cache.ormlite.dao.Dao;
import com.arellomobile.android.libs.cache.ormlite.support.ConnectionSource;
import com.arellomobile.android.libs.cache.ormlite.table.TableUtils;
import com.arellomobile.android.libs.system.log.LogUtils;

import java.sql.SQLException;

public class DatabaseHelper extends BasicOrmLiteOpenHelper {

	private static final String DATABASE_NAME = "ThoughtDR.db";
	private static final int DATABASE_VERSION = 1;

	private Dao<Affirmation, Integer> affDao;
	private Dao<Music, Integer> musicDao;
	private Dao<Category, Integer> categoriesDao;
	private Dao<SleepMusic, Integer> sleepMusicDao;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
		super.onCreate(database, connectionSource);
		try {
			log.info("onCreate");
			TableUtils.createTable(connectionSource, Affirmation.class);
			TableUtils.createTable(connectionSource, Music.class);
			TableUtils.createTable(connectionSource, Category.class);
			TableUtils.createTable(connectionSource, SleepMusic.class);
		} catch (SQLException e) {
			log.severe(LogUtils.getErrorReport("Can't create database", e));
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database,
			ConnectionSource connectionSource, int oldVersion, int newVersion) {
		super.onUpgrade(database, connectionSource, oldVersion, newVersion);
		try {
			log.info("onUpgrade");
			TableUtils.dropTable(connectionSource, Affirmation.class, true);
			TableUtils.dropTable(connectionSource, Music.class, true);
			TableUtils.dropTable(connectionSource, Category.class, true);
			TableUtils.dropTable(connectionSource, SleepMusic.class, true);
			onCreate(database, connectionSource);
		} catch (SQLException e) {
			log.severe(LogUtils.getErrorReport("Can't drop databases", e));
			throw new RuntimeException(e);
		}
	}

	public Dao<Affirmation, Integer> getAffirmationDao() throws SQLException {
		if (affDao == null) {
			affDao = getDao(Affirmation.class);
		}
		return affDao;
	}

	public Dao<Music, Integer> getMusicDao() throws SQLException {
		if (musicDao == null) {
			musicDao = getDao(Music.class);
		}
		return musicDao;
	}

	public Dao<Category, Integer> getCategoryDao() throws SQLException {
		if (categoriesDao == null) {
			categoriesDao = getDao(Category.class);
		}
		return categoriesDao;
	}

	public Dao<SleepMusic, Integer> getSleepMusicDao() throws SQLException {
		if (sleepMusicDao == null) {
			sleepMusicDao = getDao(SleepMusic.class);
		}
		return sleepMusicDao;
	}
}
