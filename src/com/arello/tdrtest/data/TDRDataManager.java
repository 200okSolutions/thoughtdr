package com.arello.tdrtest.data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore.Audio.Media;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arellomobile.android.libs.cache.cache.ORMLiteBasicCache;
import com.arellomobile.android.libs.cache.ormlite.android.AndroidCompiledStatement;
import com.arellomobile.android.libs.cache.ormlite.dao.CloseableIterator;
import com.arellomobile.android.libs.cache.ormlite.misc.TransactionManager;
import com.arellomobile.android.libs.cache.ormlite.stmt.StatementBuilder.StatementType;
import com.arellomobile.android.libs.cache.ormlite.table.DatabaseTableConfig;

public class TDRDataManager extends ORMLiteBasicCache<DatabaseHelper> {
	public static int DEFAULT_CATEGORY_ID = 1;
	public boolean editModeDay = true;

	public TDRDataManager(Context context) {
		super(context, new DatabaseHelper(context), false);
		try {
			if (databaseHelper.getCategoryDao().queryForAll().size() == 0) {
				for (Category cat : Constants.defaultCategories()) {
					databaseHelper.getCategoryDao().create(cat);
				}
			}
			if (databaseHelper.getSleepMusicDao().queryForAll().size() == 0) {
				for (SleepMusic sm : Constants.defaultSleepMusics(context)) {
					databaseHelper.getSleepMusicDao().create(sm);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addSleepSound(SleepMusic sleepMisic) {
		try {
			databaseHelper.getSleepMusicDao().create(sleepMisic);
			notifySleepMisicChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Affirmation> getAllAffirmation() {
		try {
			return databaseHelper.getAffirmationDao().query(
					databaseHelper.getAffirmationDao().queryBuilder()
							.orderBy("sortOrder", false).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Affirmation>();
	}

	public List<Affirmation> getNightAffirmations() {
		try {
			return databaseHelper.getAffirmationDao().query(
					databaseHelper.getAffirmationDao().queryBuilder()
							.orderBy("playlistSortOrder", true).where()
							.eq("playNight", true).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Affirmation>();
	}

	public void addAffirmation(Affirmation affirmation) {
		try {
			if (affirmation.getCategory() == null) {
				affirmation.setCategory(defaultCategory());
			}

			CloseableIterator<Affirmation> iterator = databaseHelper
					.getAffirmationDao()
					.iterator(
							databaseHelper
									.getAffirmationDao()
									.queryBuilder()
									.where()
									.ge("sortOrder", affirmation.getSortOrder())
									.and()
									.eq("category_id",
											affirmation.getCategory())
									.prepare());
			while (iterator.hasNext()) {
				Affirmation aff = iterator.next();
				aff.setSortOrder(aff.getSortOrder() + 1);
				databaseHelper.getAffirmationDao().update(aff);
			}

			databaseHelper.getAffirmationDao().create(affirmation);
			notifyAffirmationsChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteAffirmation(Affirmation affirmation) {

		try {
			CloseableIterator<Affirmation> iterator = databaseHelper
					.getAffirmationDao()
					.iterator(
							databaseHelper
									.getAffirmationDao()
									.queryBuilder()
									.where()
									.gt("sortOrder", affirmation.getSortOrder())
									.and()
									.eq("category_id",
											affirmation.getCategory())
									.prepare());
			while (iterator.hasNext()) {
				Affirmation aff = iterator.next();
				aff.setSortOrder(aff.getSortOrder() - 1);
				databaseHelper.getAffirmationDao().update(aff);
			}

			databaseHelper.getAffirmationDao().delete(affirmation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		notifyAffirmationsChanged();
	}

	public void deleteSleepSound(SleepMusic sleepMusic) {

		try {
			databaseHelper.getSleepMusicDao().delete(sleepMusic);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		notifySleepMisicChanged();
	}

	public void flagAffirmation(Affirmation affirmation, boolean play) {
		try {
			affirmation.setPlayNight(play);
			databaseHelper.getAffirmationDao().update(affirmation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean isAffirmationChecked(Affirmation aff) {
		return aff.getPlayNight();
	}

	public void addMusicWithCheck(Music music) {
		try {
			if (databaseHelper
					.getMusicDao()
					.query(databaseHelper
							.getMusicDao()
							.queryBuilder()
							.where()
							.eq(Media.SIZE, music.getSize())
							.and()
							.eq(Media.DATA,
									music.getSoundPath().replace("'", "\\'"))
							.prepare()).size() == 0) {
				databaseHelper.getMusicDao().create(music);
			}
			notifyMusicChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addMusic(final Music music) {
		try {
			new TransactionManager(databaseHelper.getConnectionSource())
					.callInTransaction(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							CloseableIterator<Music> iterator = databaseHelper
									.getMusicDao()
									.iterator(
											databaseHelper
													.getMusicDao()
													.queryBuilder()
													.where()
													.ge(Music.SORT_ORDER,
															music.getSortOrder())
													.prepare());
							while (iterator.hasNext()) {
								Music aff = iterator.next();
								aff.setSortOrder(aff.getSortOrder() + 1);
								databaseHelper.getMusicDao().update(aff);
							}

							databaseHelper.getMusicDao().create(music);
							return null;
						}
					});
			notifyMusicChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearMusic() {
		try {
			databaseHelper.getMusicDao().delete(
					databaseHelper.getMusicDao().deleteBuilder().prepare());
			notifyMusicChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteMusic(final Music music) {
		try {
			databaseHelper.getMusicDao().delete(music);

			new TransactionManager(databaseHelper.getConnectionSource())
					.callInTransaction(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							CloseableIterator<Music> iterator = databaseHelper
									.getMusicDao()
									.iterator(
											databaseHelper
													.getMusicDao()
													.queryBuilder()
													.where()
													.gt(Music.SORT_ORDER,
															music.getSortOrder())
													.prepare());
							while (iterator.hasNext()) {
								Music ms = iterator.next();
								ms.setSortOrder(ms.getSortOrder() - 1);
								databaseHelper.getMusicDao().update(ms);
							}
							return null;
						}
					});

			notifyMusicChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getAffirmationsCount() {
		try {
			return (int) databaseHelper
					.getConnectionSource()
					.getReadOnlyConnection()
					.queryForLong(
							"SELECT COUNT(*) FROM "
									+ DatabaseTableConfig.fromClass(
											databaseHelper
													.getConnectionSource(),
											Affirmation.class).getTableName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void deleteMusicWithId(int id) {
		try {
			Music toDelete = databaseHelper.getMusicDao().queryForId(id);
			if (toDelete != null) {
				deleteMusic(toDelete);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Music> getAllMusic() {
		try {
			return databaseHelper.getMusicDao().query(
					databaseHelper.getMusicDao().queryBuilder()
							.orderBy(Music.SORT_ORDER, true).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Music>();
	}

	public Cursor getAllMusicAsCursor() {
		try {
			Cursor cursor = ((AndroidCompiledStatement) databaseHelper
					.getConnectionSource()
					.getReadOnlyConnection()
					.compileStatement(
							String.format("SELECT * FROM %s ORDER BY %s",
									Music.MUSIC_TABLE_NAME, Music.SORT_ORDER),
							StatementType.SELECT, null, null)).getCursor();

			return cursor;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void swapMusics(Music music1, Music music2) {
		try {
			int sort = music1.getSortOrder();
			music1.setSortOrder(music2.getSortOrder());
			music2.setSortOrder(sort);
			databaseHelper.getMusicDao().update(music1);
			databaseHelper.getMusicDao().update(music2);
			notifyMusicChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Music getMusicWithSortIndex(int sortIndex) {
		try {
			return databaseHelper.getMusicDao().queryForFirst(
					databaseHelper.getMusicDao().queryBuilder().where()
							.eq("sortOrder", sortIndex).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Category defaultCategory() {
		try {
			return databaseHelper.getCategoryDao().queryForId(
					DEFAULT_CATEGORY_ID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Category> getAllCategories() {
		try {
			return databaseHelper.getCategoryDao().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Category>();
	}

	public void renameAffirmation(Affirmation affiramtion, String name) {
		affiramtion.setName(name);
		try {
			databaseHelper.getAffirmationDao().update(affiramtion);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void renameCategory(Category category, String name) {
		category.setTitle(name);
		try {
			databaseHelper.getCategoryDao().update(category);
			notifyCategoriesChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Category> getEditableCategories() {
		try {
			return databaseHelper.getCategoryDao().query(
					databaseHelper.getCategoryDao().queryBuilder().where()
							.ne("id", DEFAULT_CATEGORY_ID).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Category>();
	}

	public void removeCategory(Category category) {
		try {
			int itemsInDefault = databaseHelper
					.getAffirmationDao()
					.query(databaseHelper.getAffirmationDao().queryBuilder()
							.where().eq("category_id", DEFAULT_CATEGORY_ID)
							.prepare()).size();
			CloseableIterator<Affirmation> iterator = databaseHelper
					.getAffirmationDao().iterator(
							databaseHelper.getAffirmationDao().queryBuilder()
									.where().eq("category_id", category)
									.prepare());
			while (iterator.hasNext()) {
				Affirmation aff = iterator.next();
				aff.setSortOrder(itemsInDefault++);
				aff.getCategory().setId(DEFAULT_CATEGORY_ID);
				databaseHelper.getAffirmationDao().update(aff);
			}
			databaseHelper.getCategoryDao().delete(category);
			notifyCategoriesChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addNewCategory(Category category) {
		try {
			databaseHelper.getCategoryDao().create(category);
			notifyCategoriesChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<SleepMusic> getAllSleepMusic() {
		try {
			return databaseHelper.getSleepMusicDao().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<SleepMusic>();
	}

	public List<SleepMusic> getSelectedSleepMusics() {
		try {
			return databaseHelper.getSleepMusicDao().query(
					databaseHelper.getSleepMusicDao().queryBuilder().where()
							.eq("isSelected", true).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<SleepMusic>();
	}

	public void hideUnhideCategory(Category category) {
		category.setHidden(!category.getHidden());
		try {
			databaseHelper.getCategoryDao().update(category);
			notifyCategoriesChanged();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setSelectionSleepMusic(SleepMusic sleepMusic, boolean isSelected) {
		sleepMusic.setIsSelected(isSelected);
		try {
			databaseHelper.getSleepMusicDao().update(sleepMusic);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setPlayListSortOrderForAffirmation(Affirmation affirmation,
			int sortOrder) {
		affirmation.setPlaylistSortOrder(sortOrder);
		try {
			databaseHelper.getAffirmationDao().update(affirmation);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Affirmation> getAffirmationsWithProductId(String productId) {
		try {
			return databaseHelper.getAffirmationDao().query(
					databaseHelper.getAffirmationDao().queryBuilder().where()
							.eq("productId", productId).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Affirmation>();
	}

	public List<SleepMusic> getSleepSoundsWithProductId(String productId) {
		try {
			return databaseHelper.getSleepMusicDao().query(
					databaseHelper.getSleepMusicDao().queryBuilder().where()
							.eq("productId", productId).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<SleepMusic>();
	}

	public Category getCategoryWitnName(String name) {
		try {
			List<Category> results = databaseHelper.getCategoryDao().query(
					databaseHelper.getCategoryDao().queryBuilder().where()
							.like("title", name).prepare());
			if (results.size() > 0) {
				return results.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Affirmation> getPurchasedAffirmationsWithId(String purchaseId) {
		try {
			return databaseHelper.getAffirmationDao().query(
					databaseHelper.getAffirmationDao().queryBuilder().where()
							.eq("productId", purchaseId).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Affirmation>();
	}

	public List<SleepMusic> getPurchasedSleepSoundsWithId(String purchaseId) {
		try {
			return databaseHelper.getSleepMusicDao().query(
					databaseHelper.getSleepMusicDao().queryBuilder().where()
							.eq("productId", purchaseId).prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<SleepMusic>();
	}

	protected void notifyAffirmationsChanged() {
		if (affirmationDataChangedListener != null) {
			affirmationDataChangedListener.onDataChanged();
		}
	}

	protected void notifyMusicChanged() {
		if (musicChangedListener != null) {
			musicChangedListener.onDataChanged();
		}
	}

	protected void notifyCategoriesChanged() {
		if (categoriesChangedLisener != null) {
			categoriesChangedLisener.onDataChanged();
		}
	}

	protected void notifySleepMisicChanged() {
		if (onSleepMisicChangeListener != null) {
			onSleepMisicChangeListener.onDataChanged();
		}
	}

	public interface OnDataChangedListener {
		void onDataChanged();
	}

	OnDataChangedListener onSleepMisicChangeListener;

	public void setOnSleepMisicChangeListener(
			OnDataChangedListener onSleepMisicChangeListener) {
		this.onSleepMisicChangeListener = onSleepMisicChangeListener;
	}

	OnDataChangedListener affirmationDataChangedListener;

	public void setOnAffirmationDataChangedListener(
			OnDataChangedListener listener) {
		affirmationDataChangedListener = listener;
	}

	OnDataChangedListener musicChangedListener;

	public void setOnMusicChangedListener(OnDataChangedListener lisener) {
		musicChangedListener = lisener;
	}

	OnDataChangedListener categoriesChangedLisener;

	public void setOnCategoryChangedLisener(OnDataChangedListener listener) {
		categoriesChangedLisener = listener;
	}
}
