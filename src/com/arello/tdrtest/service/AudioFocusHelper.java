package com.arello.tdrtest.service;

import android.content.Context;
import android.media.AudioManager;

public class AudioFocusHelper {
	public interface MusicFocusable {
		public void onFocusChanged(int focus);
	}

	protected interface FocusHelper {
		boolean requestFocus();

		boolean abandonFocus();
	}

	protected static class AndroidFocusHelper implements FocusHelper,
		AudioManager.OnAudioFocusChangeListener {
		MusicFocusable mFocusable;
		AudioManager mAM;

		public AndroidFocusHelper(Context context, MusicFocusable focusable) {
			mAM = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			mFocusable = focusable;
		}

		/** Requests audio focus. Returns whether request was successful or not. */
		@Override
		public boolean requestFocus() {
			return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mAM
					.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
							AudioManager.AUDIOFOCUS_GAIN);
		}

		/** Abandons audio focus. Returns whether request was successful or not. */
		@Override
		public boolean abandonFocus() {
			return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mAM
					.abandonAudioFocus(this);
		}

		@Override
		public void onAudioFocusChange(int focusChange) {
			mFocusable.onFocusChanged(focusChange);
		}
	}

	protected static class StubFocusHelper implements FocusHelper {
		MusicFocusable mFocusable;

		public StubFocusHelper(MusicFocusable focusable) {
			mFocusable = focusable;
		}

		@Override
		public boolean requestFocus() {
			return true;
		}

		@Override
		public boolean abandonFocus() {
			return true;
		}
	}

	public static FocusHelper createFocusHelper(Context context, MusicFocusable focusable) {
		if (android.os.Build.VERSION.SDK_INT >= 8) {
			return new AndroidFocusHelper(context, focusable);
		} else {
			return new StubFocusHelper(focusable);
		}
	}
}
