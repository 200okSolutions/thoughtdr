package com.arello.tdrtest.service;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.TDRApplication;
import com.arello.tdrtest.data.Settings;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.data.objects.SleepMusic;
import com.arello.tdrtest.service.AudioFocusHelper.MusicFocusable;
import com.arello.tdrtest.ui.PlayActivity;

public class PlayService extends Service implements MusicFocusable,
		OnCompletionListener {
	private PowerManager.WakeLock mWakeLock;

	public class PlayServiceBinder extends Binder {
		public PlayService getService() {
			return PlayService.this;
		}
	}

	protected final PlayServiceBinder mLocalBinder = new PlayServiceBinder();

	private int mCurrentAff = 0;

	Runnable mAfterSleepTask = new Runnable() {

		@Override
		public void run() {

			mFirstPlay = false;

			if (!mDayMode) {
				if (mSleepMusicList != null && mSleepMusicList.size() > 0) {
					mSleepMusicList.clear();
				}
			}
			mLastSleepStartTime = -1;
			if (refershSleepTime) {
				mSleepTime = (mDayMode ? 0 : getTDRApplication().getSettings()
						.getHybernationTime());
			}
			System.out.println("current aff:" + mCurrentAff);
			if (!mDayMode && getTDRApplication().getSettings().isSingleMode()
					&& mCurrentAff == Constants.MAX_AFFIRMATOIN_COUNT
					&& !mFirstPlay) {
				stopService();
				stopSelf();
				return;
			}

			if (mCurrentAff == Constants.MAX_AFFIRMATOIN_COUNT) {
				mCurrentAff = 0;
			}

			/*
			 * if (mContinueAfterSleep) { mContinueAfterSleep = false;
			 * 
			 * if (mSleepSoundsPlayer != null && mSleepSoundsPlayer.isPlaying())
			 * { mSleepSoundsPlayer.pause(); }
			 * 
			 * mSleepPlayerStopped = true; prepareSoundPlayer(); startPlay();
			 * 
			 * return; }
			 */if (mAffirmationList.size() > 0) {

				if (mDayMode) {
					for (Affirmation aff : mAffirmationList) {
						mAffirmationQueue.add(aff);
					}

					/*
					 * for (int i = 0; i < Constants.MAX_AFFIRMATOIN_COUNT; ++i)
					 * { Affirmation aff = mAffirmationList.get(i %
					 * mAffirmationList.size()); mAffirmationQueue.add(aff); }
					 */} else {
					Affirmation aff = mAffirmationList.get(mCurrentAff
							% mAffirmationList.size());
					for (int j = 0; j < Constants.REPEAT_AFFIRMATION_COUNT; ++j) {
						mAffirmationQueue.add(aff);
					}

					mCurrentAff++;
				}
				if (mSleepSoundsPlayer != null
						&& mSleepSoundsPlayer.isPlaying()) {
					mSleepSoundsPlayer.pause();
				}
				mSleepPlayerStopped = true;
				prepareSoundPlayer();
				startPlay();
			}
		}
	};

	NotificationManager mNotificationManager;
	Notification mNotification = null;

	AudioFocusHelper.FocusHelper mAudioFocusHelper = null;
	static final int NOTIFICATION_ID = 1;

	int mSleepTime = Constants.DEFAULT_SLEEP_TIME;
	MediaPlayer mSoundPlayer;

	MediaPlayer mMusicPlayer = null;
	MediaPlayer mSleepSoundsPlayer = null;
	int mCurrentMusic = 0;
	int mCurrentSleepSound = 0;
	List<Music> mMusicList;
	List<Affirmation> mAffirmationList;
	List<SleepMusic> mSleepMusicList;
	Handler mHandler = new Handler();
	LinkedList<Affirmation> mAffirmationQueue = new LinkedList<Affirmation>();

	float mMusicVolume = 0.99f;
	float mSoundVolume = 0.99f;
	private float mSleepSoundVolume = 0.99f;
	boolean mIsDuck = false;
	boolean mCanPlay = false;
	boolean mInitialized = false;
	Affirmation mCurrentAffirmation;
	long mSnoozeStartTime;
	long mLastSleepStartTime;
	boolean mSnoozed = false;
	boolean mMusicPlayerStopped = false;
	boolean mSoundPlayerStopped = false;
	boolean mSleepPlayerStopped = false;
	boolean mWithMusic = false;
	boolean mDayMode = false;
	boolean refershSleepTime = false;

	private boolean mFirstPlay;

	public boolean isSleepSoundPlayed() {
		return mSleepSoundsPlayer != null && mSleepSoundsPlayer.isPlaying();
	}

	public float getSoundVolume() {
		return mSoundVolume;
	}

	public float getMusicVolume() {
		return mMusicVolume;
	}

	public float getSleepSoundVolume() {
		return mSleepSoundVolume;
	}

	@Override
	public void onCreate() {
		Log.i(PlayService.class.toString(), "Creating Playback Service...");

		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		mAudioFocusHelper = AudioFocusHelper.createFocusHelper(
				getApplicationContext(), this);

	}

	@Override
	public IBinder onBind(Intent arg0) {
		startService(
				arg0 != null
						&& arg0.getBooleanExtra(Constants.START_WITH_MUSIC,
								false),
				arg0 != null && arg0.getBooleanExtra(Constants.DAY_MODE, false));
		return mLocalBinder;
	}

	@Override
	public void onFocusChanged(int focus) {

		mIsDuck = false;
		switch (focus) {
		case AudioManager.AUDIOFOCUS_GAIN:
			mCanPlay = true;
			startPlay();
			break;
		case AudioManager.AUDIOFOCUS_LOSS:
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			mCanPlay = false;
			pausePlay();
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			mCanPlay = true;
			mIsDuck = true;
			startPlay();
			break;

		default:
			break;
		}
		changeVolume(mSoundVolume, mMusicVolume, mSleepSoundVolume);
	}

	public void stopService() {
		if (!mInitialized) {
			return;
		}
		if (mMusicPlayer != null) {
			mMusicPlayer.release();
		}
		if (mSoundPlayer != null) {
			mSoundPlayer.release();
		}
		if (mSleepSoundsPlayer != null) {
			mSleepSoundsPlayer.release();
		}
		removeWakeLock();
		mHandler.removeCallbacksAndMessages(null);
		mAudioFocusHelper.abandonFocus();
		stopForeground(true);
		mInitialized = false;
		if (serviceStopListener.get() != null) {
			serviceStopListener.get().onServiceStopped();
		}
		System.out.println("stop service called.");
	}

	public void startService(boolean withMusic, boolean dayMode) {
		if (mInitialized) {
			return;
		}
		mWithMusic = withMusic;
		mDayMode = dayMode;
		initForeground(getResources().getString(R.string.app_name));
		setupPlayer(mWithMusic, mDayMode);
		if (mAudioFocusHelper.requestFocus()) {
			mCanPlay = true;
			changeVolume(getTDRApplication().getSettings()
					.getAffirmationVolume(), getTDRApplication().getSettings()
					.getMusicVolume(), getTDRApplication().getSettings()
					.getSleepSoundsVolume());
			startPlay();
		}
		mInitialized = true;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		startService(
				intent != null
						&& intent.getBooleanExtra(Constants.START_WITH_MUSIC,
								false),
				intent != null
						&& intent.getBooleanExtra(Constants.DAY_MODE, false));
		return START_STICKY;
	}

	/**
	 * Updates the notification.
	 */
	void updateNotification(String text) {
		PendingIntent pIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(getApplicationContext(),
						PlayActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

		String contentTitle = getApplicationContext().getResources().getString(
				R.string.app_name);
		mNotification.setLatestEventInfo(getApplicationContext(), contentTitle,
				text, pIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mNotification);
	}

	public void changeVolume(float sound, float music, float sleepSoundVolume) {
		mSoundVolume = sound;
		mMusicVolume = music;
		mSleepSoundVolume = sleepSoundVolume;
		if (mMusicPlayer != null) {
			if (!mIsDuck) {
				mMusicPlayer.setVolume(mMusicVolume, mMusicVolume);
			} else {
				mMusicPlayer.setVolume(mMusicVolume / 2, mMusicVolume / 2);
			}
		}
		if (mSoundPlayer != null) {
			if (!mIsDuck) {
				mSoundPlayer.setVolume(mSoundVolume, mSoundVolume);
			} else {
				mSoundPlayer.setVolume(mSoundVolume / 2, mSoundVolume / 2);
			}
		}
		if (mSleepSoundsPlayer != null) {
			if (!mIsDuck) {
				mSleepSoundsPlayer.setVolume(mSleepSoundVolume,
						mSleepSoundVolume);
			} else {
				mSleepSoundsPlayer.setVolume(mSleepSoundVolume / 2,
						mSleepSoundVolume / 2);
			}
		}
	}

	public void snooze() {
		if (mSnoozed) {
			return;
		}
		mSnoozeStartTime = System.currentTimeMillis();
		mSnoozed = true;
		pausePlay();
		mHandler.removeCallbacks(mAfterSleepTask);
		mHandler.removeCallbacks(mCountDown);
	}

	public boolean isSnoozed() {
		return mSnoozed;
	}

	public void returnFromSnooze() {
		try {
			if (!mSnoozed) {
				return;
			}
			mSnoozed = false;

			// Original
			// if (mLastSleepStartTime > 0) {
			// mHandler.postDelayed(
			// mAfterSleepTask,
			// ((mLastSleepStartTime + mSleepTime
			// * Constants.SLEEP_TIME_MULTIPLIER) - mSnoozeStartTime));
			// mWas--;

			if (isRuuning) {
				mHandler.postDelayed(mAfterSleepTask, Long.valueOf(mMax * 1000));
				mCountDown.run();
			} else {
				startPlay();
			}
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void pausePlay() {
		try {
			if (mMusicPlayer != null) {
				mMusicPlayer.pause();
			}
			if (mSoundPlayer != null) {
				mSoundPlayer.pause();
			}
			if (mSleepSoundsPlayer != null) {
				mSleepSoundsPlayer.pause();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}

	void startPlay() {
		if (!mCanPlay || mSnoozed) {
			System.out.println("first");
			return;
		}
		if (mMusicPlayer != null && !mMusicPlayer.isPlaying()
				&& !mMusicPlayerStopped) {
			initWakeLock();
			mMusicPlayer.start();
			System.out.println("second");
		}
		if (mSoundPlayer != null && !mSoundPlayer.isPlaying()
				&& !mSoundPlayerStopped) {
			initWakeLock();
			mSoundPlayer.start();
			System.out.println("third");
		}
		if (mSleepSoundsPlayer != null && !mSleepSoundsPlayer.isPlaying()
				&& !mSleepPlayerStopped) {
			initWakeLock();
			mSleepSoundsPlayer.start();
			System.out.println("fourth");
		}
	}

	private synchronized void initWakeLock() {
		if (null == mWakeLock) {
			PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
			mWakeLock = powerManager.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
		}
		mWakeLock.acquire();
	}

	private synchronized void removeWakeLock() {
		if (null != mWakeLock) {
			mWakeLock.release();
		}
	}

	void prepareMusicPlayer() {
		if (mMusicPlayer != null) {
			mMusicPlayer.release();
		}
		if (mMusicList.size() == 0) {
			return;
		}
		mMusicPlayer = new MediaPlayer();
		try {
			mMusicPlayer.setOnCompletionListener(this);
			if (mMusicChangeListener != null) {
				mMusicChangeListener.onMusicChanged(mMusicList
						.get(mCurrentMusic));
			}
			mMusicPlayer.setDataSource(mMusicList.get(mCurrentMusic)
					.getSoundPath());
			mMusicPlayer.prepare();
			changeVolume(mSoundVolume, mMusicVolume, mSleepSoundVolume);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void prepareSleepSoundsPlayer() {
		if (mSleepSoundsPlayer != null) {
			mSleepSoundsPlayer.release();
		}
		if (mSleepMusicList.size() == 0) {
			return;
		}
		mSleepSoundsPlayer = new MediaPlayer();
		try {
			mSleepSoundsPlayer.setOnCompletionListener(this);
			SleepMusic sleepMusic = mSleepMusicList.get(mCurrentSleepSound);

			if (!TextUtils.isEmpty(sleepMusic.getSoundPath())) {
				mSleepSoundsPlayer.setDataSource(sleepMusic.getSoundPath());
			} else {
				mSleepSoundsPlayer.setDataSource(getAssets().openFd(
						sleepMusic.getAssetPath()).getFileDescriptor());
			}

			mSleepSoundsPlayer.prepare();
			changeVolume(mSoundVolume, mMusicVolume, mSleepSoundVolume);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void prepareSoundPlayer() {
		if (mAffirmationQueue.size() == 0) {
			return;
		}
		if (mSoundPlayer != null) {
			mSoundPlayer.release();
		}
		mSoundPlayer = new MediaPlayer();
		try {
			mSoundPlayer.setOnCompletionListener(this);
			mCurrentAffirmation = mAffirmationQueue.peek();
			if (mAffirmationChangeListener != null) {
				mAffirmationChangeListener
						.onAffirmationChanged(mCurrentAffirmation);
			}
			updateNotification(mCurrentAffirmation.getName());
			mSoundPlayer.setDataSource(mAffirmationQueue.poll().getSoundPath());
			mSoundPlayer.prepare();
			changeVolume(mSoundVolume, mMusicVolume, mSleepSoundVolume);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {

			prepareSoundPlayer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void setupPlayer(boolean withMusic, boolean dayMode) {
		mCurrentAff = 0;
		refershSleepTime = true;
		mFirstPlay = true;
		if (withMusic) {
			mMusicList = getTDRApplication().getDataManager().getAllMusic();
			if (mMusicList.size() != 0) {
				prepareMusicPlayer();
			} else {
				mMusicPlayer = null;
			}
		} else {
			mMusicPlayer = null;
		}
		mAffirmationList = getTDRApplication().getDataManager()
				.getNightAffirmations();

		if (getTDRApplication().getSettings().getSleepMusicChecked()) {
			mSleepMusicList = getTDRApplication().getDataManager()
					.getSelectedSleepMusics();
		} else {
			mSleepMusicList = new ArrayList<SleepMusic>();
		}

		if (!getTDRApplication().getSettings().isSingleMode()) {
			if (!dayMode) {
				for (Affirmation aff : mAffirmationList) {
					mAffirmationQueue.add(aff);
				}
				if (getTDRApplication().getSettings().getSleepMusicChecked()) {
					mSleepMusicList = getTDRApplication().getDataManager()
							.getSelectedSleepMusics();
				} else {
					mSleepMusicList = new ArrayList<SleepMusic>();
				}
				prepareSoundPlayer();
			} else {
				prepareSoundPlayer();
				mSleepTime = 0;
				mAfterSleepTask.run();
			}
		} else {
			if (dayMode) {
				prepareSoundPlayer();
				mSleepTime = 0;
				mAfterSleepTask.run();
				return;
			} else {
				/*
				 * for (int i = 0; i < Constants.MAX_AFFIRMATOIN_COUNT; i++) {
				 * Affirmation aff = mAffirmationList.get(i %
				 * mAffirmationList.size());
				 * 
				 * mAffirmationQueue.add(aff); }
				 */
				for (Affirmation aff : mAffirmationList) {
					mAffirmationQueue.add(aff);
				}

				/*
				 * Affirmation aff = mAffirmationList.get(mCurrentAff %
				 * Constants.MAX_AFFIRMATOIN_COUNT);
				 * 
				 * for (int i = 0; i < Constants.REPEAT_AFFIRMATION_COUNT; ++i)
				 * { mAffirmationQueue.add(aff); }
				 * 
				 * mCurrentAff++;
				 */}
			mSleepTime = getTDRApplication().getSettings().getHybernationTime();
			prepareSoundPlayer();
		}
	}

	void initForeground(String text) {
		PendingIntent pIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(getApplicationContext(),
						PlayActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

		mNotification = new Notification();
		mNotification.tickerText = text;
		mNotification.icon = android.R.drawable.ic_media_play;
		mNotification.flags |= Notification.FLAG_ONGOING_EVENT;

		String contentTitle = getApplicationContext().getResources().getString(
				R.string.app_name);
		mNotification.setLatestEventInfo(getApplicationContext(), contentTitle,
				text, pIntent);

		startForeground(NOTIFICATION_ID, mNotification);
	}

	@Override
	public void onDestroy() {
		stopService();
		super.onDestroy();
	}

	protected TDRApplication getTDRApplication() {
		return ((TDRApplication) getApplication());
	}

	private int mMax;
	private int mWas;

	private long mWasTime = 0;
	boolean isRuuning = false;
	Runnable mCountDown = new Runnable() {

		@Override
		public void run() {
			long time = SystemClock.uptimeMillis();

			long corr = 0;
			if (mWasTime != 0) {
				corr = (time - mWasTime);
			}
			mWasTime = time;
			isRuuning = true;
			if (mHibernationCoundDownListener != null) {
				mHibernationCoundDownListener
						.onHibernationCoundDown(mWas, mMax);

				// Kaushal
				if (mMax >= 0) {
					System.out.println("max:" + mMax);
					mMax--;
					mHandler.postDelayed(mCountDown, 1000);
				}

				// Original
				/*
				 * if (mMax > mWas) { int millis =
				 * Constants.SLEEP_TIME_MULTIPLIER / 60;
				 * 
				 * int passed = (int) Math.floor(corr / millis); mWas += passed
				 * + (corr < millis ? 1 : 0); corr -= passed * millis; mWasTime
				 * -= corr; mMax--; mHandler.postDelayed(mCountDown, 1000); }
				 */
			}
		}
	};

	void startCoundDown(int max) {
		mHibernationCoundDownListener.onHibernationStart();

		mMax = max;
		// mMax = 10;
		mWas = 0;
		mWasTime = 0;

		mCountDown.run();
	}

	// private CountDownTimer mTimer

	void goToSleep() {
		System.out.println("sleep time:" + mSleepTime);
		if (mSleepTime != 0) {
			updateNotification(getResources().getString(R.string.hibernation));
			mLastSleepStartTime = System.currentTimeMillis();
			if (mSleepSoundsPlayer == null)// &&
											// !getTDRApplication().getSettings().isSingleMode())
			{
				prepareSleepSoundsPlayer();
				mSleepPlayerStopped = false;
				startPlay();
			}

			if (mAffirmationChangeListener != null) {
				mAffirmationChangeListener.onAffirmationChanged(null);
			}

			mHandler.postDelayed(mAfterSleepTask, mSleepTime
					* Constants.SLEEP_TIME_MULTIPLIER * 100);
			// mHandler.postDelayed(mAfterSleepTask, 10000);
			startCoundDown(mSleepTime * 60);
		} else {
			mAfterSleepTask.run();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		Log.e("PlayService", "onCompletion");

		if (mp == mSoundPlayer) {
			if (mAffirmationQueue.size() != 0) {
				/*
				 * if (!mDayMode &&
				 * !getTDRApplication().getSettings().isSingleMode() &&
				 * !mFirstPlay) { mSleepTime = Constants.DEFAULT_SLEEP_TIME;
				 * 
				 * return; }
				 */
				prepareSoundPlayer();
				startPlay();
			} else if (mDayMode) {
				mAfterSleepTask.run();
			} else {
				Settings settings = new Settings(getApplicationContext());
				if (!mDayMode && mFirstPlay) {
					if (settings.IS_HIBERNATE_ACTIVE()) {
						mSleepTime = Constants.DEFAULT_SLEEP_TIME;
					} else {
						mSleepTime = 0;
					}
				}
				mFirstPlay = false;
				mSoundPlayer = null;
				goToSleep();
			}
		} else if (mp == mMusicPlayer) {
			playNext();
		} else if (mp == mSleepSoundsPlayer) {
			if (mSleepMusicList.size() == 0) {
				return;
			}
			mCurrentSleepSound = (mCurrentSleepSound + 1)
					% mSleepMusicList.size();
			prepareSleepSoundsPlayer();
			startPlay();
		}
	}

	public Music getCurrentMusic() {
		return mMusicList.get(mCurrentMusic);
	}

	public Affirmation getCurrentAffirmation() {
		if (mAffirmationQueue.size() == 0) {
			if (mSoundPlayer != null && mSoundPlayer.isPlaying()) {
				if (mAffirmationList.size() > 0) {
					return mAffirmationList.get(mAffirmationList.size() - 1);
				}
			}
			return null;
		}
		return mCurrentAffirmation;
	}

	public void playNext() {
		mCurrentMusic = (mCurrentMusic + 1) % mMusicList.size();
		prepareMusicPlayer();
		startPlay();
	}

	public void playPrev() {
		if (--mCurrentMusic < 0) {
			mCurrentMusic = mMusicList.size() - 1;
		}
		prepareMusicPlayer();
		startPlay();
	}

	public boolean toggleMusicPlayer() {
		mMusicPlayerStopped = !mMusicPlayerStopped;
		if (mMusicPlayerStopped && mMusicPlayer.isPlaying()) {
			mMusicPlayer.pause();
		} else {
			startPlay();
		}
		return mMusicPlayerStopped;
	}

	public boolean isMusicPlayerStopped() {
		return mMusicPlayerStopped;
	}

	public void playNextAffirmation() {
		if (mAffirmationQueue.size() > 0) {
			while (mAffirmationQueue.peek() == mCurrentAffirmation) {
				mAffirmationQueue.poll();
			}
			if (mSoundPlayer != null) {
				mSoundPlayer.release();
			}
			onCompletion(mSoundPlayer);
		} else {
			setupPlayer(mWithMusic, mDayMode);
			startPlay();
		}
	}

	public void playPrevAffirmation() {
		if (mAffirmationQueue.size() > 0) {
			int affirmationIndex = mAffirmationQueue.size()
					/ Constants.REPEAT_AFFIRMATION_COUNT;
			affirmationIndex = (mWithMusic && mDayMode ? mAffirmationList
					.size() : Constants.MAX_AFFIRMATOIN_COUNT)
					- affirmationIndex - 1;
			affirmationIndex--;
			if (affirmationIndex < 0) {

				affirmationIndex += (mWithMusic && mDayMode ? mAffirmationList
						.size() : Constants.MAX_AFFIRMATOIN_COUNT);
				mAffirmationQueue.clear();
			} else {
				while (mAffirmationQueue.peek() == mCurrentAffirmation) {
					mAffirmationQueue.poll();
				}
				for (int i = 0; i < Constants.REPEAT_AFFIRMATION_COUNT; ++i) {
					mAffirmationQueue.add(0, mCurrentAffirmation);
				}
			}
			affirmationIndex = affirmationIndex % mAffirmationList.size();
			if (affirmationIndex < 0) {
				affirmationIndex = mAffirmationList.size() - 1;
			}
			for (int i = 0; i < Constants.REPEAT_AFFIRMATION_COUNT; ++i) {
				mAffirmationQueue
						.add(0, mAffirmationList.get(affirmationIndex));
			}
			if (mSoundPlayer != null) {
				mSoundPlayer.release();
			}
			onCompletion(mSoundPlayer);
		}
	}

	public boolean toggleAffirmation() {
		mSoundPlayerStopped = !mSoundPlayerStopped;
		if (mSoundPlayerStopped && mSoundPlayer.isPlaying()) {
			mSoundPlayer.pause();
		} else {
			startPlay();
		}
		return mSoundPlayerStopped;
	}

	public boolean isSoundPlayerStopped() {
		return mSoundPlayerStopped;
	}

	private OnCurrentMusicChangedListener mMusicChangeListener;
	private OnCurrentAffirmationChangedListener mAffirmationChangeListener;
	private OnHibernationCoundDownListener mHibernationCoundDownListener;

	public void setOnHibernationCoundDownListener(
			OnHibernationCoundDownListener hibernationCoundDownListener) {
		mHibernationCoundDownListener = hibernationCoundDownListener;
	}

	public void setOnCurrentMusicChangedListenr(OnCurrentMusicChangedListener l) {
		mMusicChangeListener = l;
	}

	public void setOnCurrentAffirmationChangedListener(
			OnCurrentAffirmationChangedListener l) {
		mAffirmationChangeListener = l;
	}

	public OnServiceStopListener getServiceStopListener() {
		return serviceStopListener.get();
	}

	public void setServiceStopListener(OnServiceStopListener serviceStopListener) {
		this.serviceStopListener = new WeakReference<OnServiceStopListener>(
				serviceStopListener);
	}

	public interface OnCurrentMusicChangedListener {
		void onMusicChanged(Music currentMusic);
	}

	public interface OnCurrentAffirmationChangedListener {
		void onAffirmationChanged(Affirmation currentAffirmation);
	}

	public interface OnHibernationCoundDownListener {
		void onHibernationCoundDown(int left, int max);

		void onHibernationStart();

		void onHibernationStop();
	}

	private WeakReference<OnServiceStopListener> serviceStopListener;

	public interface OnServiceStopListener {
		void onServiceStopped();
	}
}
