package com.arello.tdrtest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Coins {
	@JsonProperty("id")
	String id;
	@JsonProperty("name")
	String name;
	@JsonProperty("coins")
	String coins;
	@JsonProperty("offer_coins")
	String offer_coins;
	@JsonProperty("price")
	String price;
	@JsonProperty("best_deal")
	String best_deal;
	@JsonProperty("created_date")
	String created_date;
	@JsonProperty("included_free")
	String included_free;
	@JsonProperty("popular")
	String popular;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCoins() {
		return coins;
	}

	public void setCoins(String coins) {
		this.coins = coins;
	}

	public String getOffer_coins() {
		return offer_coins;
	}

	public void setOffer_coins(String offer_coins) {
		this.offer_coins = offer_coins;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getBest_deal() {
		return best_deal;
	}

	public void setBest_deal(String best_deal) {
		this.best_deal = best_deal;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getIncluded_free() {
		return included_free;
	}

	public void setIncluded_free(String included_free) {
		this.included_free = included_free;
	}

	public String getPopular() {
		return popular;
	}

	public void setPopular(String popular) {
		this.popular = popular;
	}

}
