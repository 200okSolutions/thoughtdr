package com.arello.tdrtest.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Products implements Serializable {
	@JsonProperty("title")
	String title;
	@JsonProperty("id")
	String id;
	@JsonProperty("price_html")
	String price_html;
	@JsonProperty("in_stock")
	String in_stock;
	@JsonProperty("description")
	String description;
	@JsonProperty("featured_src")
	String featured_src;
	@JsonProperty("price")
	String price;
	@JsonProperty("regular_price")
	String regular_price;
	@JsonProperty("sale_pricel")
	String sale_pricel;
	@JsonProperty("average_rating")
	float average_rating;
	@JsonProperty("points_for")
	String points_for;
	@JsonProperty("points")
	String points;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrice_html() {
		return price_html;
	}

	public void setPrice_html(String price_html) {
		this.price_html = price_html;
	}

	public String getIn_stock() {
		return in_stock;
	}

	public void setIn_stock(String in_stock) {
		this.in_stock = in_stock;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFeatured_src() {
		return featured_src;
	}

	public void setFeatured_src(String featured_src) {
		this.featured_src = featured_src;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRegular_price() {
		return regular_price;
	}

	public void setRegular_price(String regular_price) {
		this.regular_price = regular_price;
	}

	public String getSale_pricel() {
		return sale_pricel;
	}

	public void setSale_pricel(String sale_pricel) {
		this.sale_pricel = sale_pricel;
	}

	public float getAverage_rating() {
		return average_rating;
	}

	public void setAverage_rating(float average_rating) {
		this.average_rating = average_rating;
	}

	public String getPoints_for() {
		return points_for;
	}

	public void setPoints_for(String points_for) {
		this.points_for = points_for;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

}
