package com.arello.tdrtest.uihelpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;

public class Utils {
	public static ImageButton getButtonWithDrawableAndBackground(
			Activity activity, Drawable icon, int backgroundResource) {
		ImageButton actionButton = new ImageButton(activity);
		actionButton.setImageDrawable(icon);
		actionButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
		actionButton.setBackgroundResource(backgroundResource);
		actionButton.setPadding(8, 8, 8, 8);

		return actionButton;
	}

	static public String getContents(File aFile) {
		// ...checks on aFile are elided
		StringBuilder contents = new StringBuilder();

		try {
			BufferedReader input = new BufferedReader(new FileReader(aFile));
			try {
				String line = null;
				while ((line = input.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return contents.toString();
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null
				|| connectivityManager.getAllNetworkInfo() == null) {
			return false;
		}
		for (NetworkInfo info : connectivityManager.getAllNetworkInfo()) {
			if (info.getState() == NetworkInfo.State.CONNECTED) {
				return true;
			}
		}
		return false;
	}

	public static Bitmap drawViewToBitmap(View view) {
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas canva = new Canvas(bitmap);
		view.draw(canva);
		return bitmap;
	}

	public static void saveSharedPreferences(String key, String value,
			Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.APP_TITLE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
		Log.d(Constants.APP_TITLE, "Store: Key:" + key);
		Log.d(Constants.APP_TITLE, "Store: Value:" + value);
	}

	public static String getSharedPreferences(String key, Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.APP_TITLE, Context.MODE_PRIVATE);
		String value = sharedPreferences.getString(key, null);
		Log.d(Constants.APP_TITLE, "Get: Key: " + key);
		Log.d(Constants.APP_TITLE, "Get: Value: " + value);
		return value;
	}

	public static int SetRewardsPoint(Context context, long stopTime) {
		int result = 0;
		try {
			long startTime = Long.valueOf(getSharedPreferences(
					Constants.START_TIME, context));
			long totalTime = stopTime - startTime;
			System.out.println("Total Time Spent:" + totalTime);
			if (getSharedPreferences(Constants.IS_EIGTH_HOUR, context) == null) {
				if (totalTime > Constants.FIRST_STOP) {
					System.out.println("got points in first stop");
					saveSharedPreferences(Constants.IS_EIGTH_HOUR, "true",
							context);
					saveSharedPreferences(Constants.TOTAL_TIME,
							String.valueOf(0), context);
					result = 1;
				} else {
					saveSharedPreferences(Constants.IS_EIGTH_HOUR, "false",
							context);
					saveSharedPreferences(Constants.TOTAL_TIME,
							String.valueOf(totalTime), context);
				}
			} else {
				if (getSharedPreferences(Constants.IS_EIGTH_HOUR, context)
						.equals("true")) {
					if (totalTime > Constants.FIRST_STOP) {
						System.out.println("got points in first stop");
						saveSharedPreferences(Constants.IS_EIGTH_HOUR, "true",
								context);
						saveSharedPreferences(Constants.TOTAL_TIME,
								String.valueOf(0), context);
						result = 1;
					} else {
						saveSharedPreferences(Constants.IS_EIGTH_HOUR, "false",
								context);
						saveSharedPreferences(Constants.TOTAL_TIME,
								String.valueOf(totalTime), context);
					}
				} else {
					long savedTotalTime = Long.valueOf(getSharedPreferences(
							Constants.TOTAL_TIME, context));
					if (savedTotalTime + totalTime > Constants.SECOND_STOP) {
						System.out.println("got points in second stop");
						saveSharedPreferences(Constants.IS_EIGTH_HOUR, "true",
								context);
						saveSharedPreferences(Constants.TOTAL_TIME,
								String.valueOf(0), context);
						result = 1;
					} else {
						saveSharedPreferences(Constants.TOTAL_TIME,
								String.valueOf(savedTotalTime + totalTime),
								context);
					}
				}
			}
			if (getSharedPreferences(Constants.TOTAL_HOUR, context) == null) {
				saveSharedPreferences(Constants.TOTAL_HOUR,
						String.valueOf(totalTime), context);
			} else {
				long old = Long.valueOf(getSharedPreferences(
						Constants.TOTAL_HOUR, context));
				if ((old + totalTime) > Constants.LAST_STOP) {
					System.out.println("got 100 points");
					result = 2;
					saveSharedPreferences(Constants.TOTAL_HOUR,
							String.valueOf(0), context);
				} else {
					saveSharedPreferences(Constants.TOTAL_HOUR,
							String.valueOf(old + totalTime), context);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean isValidEmail(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static void startScaleAnimation(View view, Point coord, float from,
			float to, float fromAlpha, float toAlpha, float animationAnchorX,
			float animationAnchorY, AnimationListener listener) {
		ScaleAnimation scaleAnim = new ScaleAnimation(from, to, from, to,
				Animation.RELATIVE_TO_SELF, animationAnchorX,
				Animation.RELATIVE_TO_SELF, animationAnchorY);
		if (from < to) {
			scaleAnim
					.setInterpolator(new android.view.animation.OvershootInterpolator());
		} else {
			scaleAnim
					.setInterpolator(new android.view.animation.AccelerateInterpolator());
		}

		AnimationSet animSet = new AnimationSet(false);
		animSet.addAnimation(scaleAnim);
		animSet.addAnimation(new AlphaAnimation(fromAlpha, toAlpha));
		animSet.setDuration(300);
		if (listener != null) {
			animSet.setAnimationListener(listener);
		}
		view.startAnimation(animSet);
	}

	public static String getCachePath(Context context) {
		String fileStoragePrefix;
		if (!Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			fileStoragePrefix = "/data/data/" + context.getPackageName();
		} else {
			fileStoragePrefix = Environment.getExternalStorageDirectory()
					.getPath()
					+ "/Android/data/"
					+ context.getPackageName()
					+ "/files";
		}
		File f = new File(fileStoragePrefix);
		if (!f.exists()) {
			if (f.mkdirs()) {

				f = new File(fileStoragePrefix + "/.nomedia");
				try {
					if (!f.exists()) {
						f.createNewFile();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fileStoragePrefix;
	}

	public static void showAlert(Context context, String text) {
		showAlert(context, text, null);
	}

	public static void showAlert(Context context, String text,
			DialogInterface.OnDismissListener onDismissListener) {
		if (text.contains("I/O error during system call")) {
			return;
		}

		AlertDialog dialog = new AlertDialog.Builder(context)
				.setTitle(context.getResources().getString(R.string.app_name))
				.setMessage(text).setNeutralButton(R.string.close_alert, null)
				.create();
		dialog.setOnDismissListener(onDismissListener);
		dialog.show();
	}

	public static void showAlert(Context context, int stringResourceId) {
		showAlert(context, context.getResources().getString(stringResourceId));
	}

	/* animation show methods */

	public static void showViewWithAnimation(View view, Animation animation,
			AnimationListener listener) {
		if (view.getVisibility() == View.VISIBLE) {
			return;
		}
		view.setVisibility(View.VISIBLE);
		animation.setAnimationListener(listener);
		view.startAnimation(animation);
	}

	public static void showViewWithAnimation(View view, Animation animation) {
		showViewWithAnimation(view, animation, null);
	}

	public static void showViewWithAnimation(View view, int animationId) {
		showViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId));
	}

	public static void showViewWithAnimation(View view, int animationId,
			AnimationListener listener) {
		showViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId),
				listener);
	}

	/* animation gone methods */

	public static void goneViewWithAnimation(View view, Animation animation,
			AnimationListener listener) {
		if (view.getVisibility() == View.GONE
				|| view.getVisibility() == View.INVISIBLE) {
			return;
		}
		animation.setAnimationListener(new AnimationListenerWithBindView(view,
				listener) {

			@Override
			public void animationEndedWithView(View view) {
				view.setVisibility(View.GONE);
			}
		});
		view.startAnimation(animation);
	}

	public static void goneViewWithAnimation(View view, int animationId,
			AnimationListener listener) {
		goneViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId),
				listener);
	}

	public static void goneViewWithAnimation(View view, int animationId) {
		goneViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId),
				null);
	}

	public static void goneViewWithAnimation(View view, Animation animation) {
		goneViewWithAnimation(view, animation, null);
	}

	/* invisible methods */

	public static void hideViewWithAnimation(View view, Animation animation,
			AnimationListener listener) {
		if (view.getVisibility() == View.GONE
				|| view.getVisibility() == View.INVISIBLE) {
			return;
		}
		animation.setAnimationListener(new AnimationListenerWithBindView(view,
				listener) {

			@Override
			public void animationEndedWithView(View view) {
				view.setVisibility(View.INVISIBLE);
			}
		});
		view.startAnimation(animation);
	}

	public static void hideViewWithAnimation(View view, int animationId,
			AnimationListener listener) {
		hideViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId),
				listener);
	}

	public static void hideViewWithAnimation(View view, int animationId) {
		hideViewWithAnimation(view,
				AnimationUtils.loadAnimation(view.getContext(), animationId),
				null);
	}

	public static void hideViewWithAnimation(View view, Animation animation) {
		hideViewWithAnimation(view, animation, null);
	}

	protected static abstract class AnimationListenerWithBindView implements
			AnimationListener {
		View mView;
		AnimationListener mListener;

		public AnimationListenerWithBindView(View view,
				AnimationListener listener) {
			mView = view;
			mListener = listener;
		}

		public abstract void animationEndedWithView(View view);

		@Override
		public void onAnimationEnd(Animation animation) {
			if (mListener != null) {
				mListener.onAnimationEnd(animation);
			}
			animationEndedWithView(mView);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			if (mListener != null) {
				mListener.onAnimationRepeat(animation);
			}
		}

		@Override
		public void onAnimationStart(Animation animation) {
			if (mListener != null) {
				mListener.onAnimationStart(animation);
			}
		}

	}

	public static int pixelsFromDp(Context context, int dp) {
		return (int) (dp * context.getResources().getDisplayMetrics().density);
	}

	public static void setViewIsEnabled(View view, boolean isEnabled) {
		view.setEnabled(isEnabled);
		float resultAlpha = (isEnabled ? 1.0f : 0.5f);
		AlphaAnimation changeAlpha = new AlphaAnimation(resultAlpha,
				resultAlpha);
		changeAlpha.setFillAfter(true);
		view.startAnimation(changeAlpha);
	}
}
