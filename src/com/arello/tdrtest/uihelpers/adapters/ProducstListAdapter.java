package com.arello.tdrtest.uihelpers.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.model.Categories;
import com.arello.tdrtest.model.Products;

public class ProducstListAdapter extends ArrayAdapter<Products> {

	private Context context;
	private Products[] products;
	private LayoutInflater inflater = null;

	public ProducstListAdapter(Context context, int resource,
			Products[] products) {
		super(context, resource, products);

		this.context = context;
		this.products = products;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@SuppressWarnings("unused")
	private class Holder {

		TextView textCategoriesName = null;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = null;

		if (convertView == null) {
			try {
				holder = new Holder();
				convertView = inflater.inflate(R.layout.category_title, null);
				convertView.setTag(holder);
			} catch (Exception e) {
				System.out.println("Exception Ex : " + e.toString());
			}
		} else {
			try {
				holder = (Holder) convertView.getTag();
			} catch (Exception e) {
				System.out.println("Exception Ex : " + e.toString());
			}
		}
		holder.textCategoriesName = detail(
				convertView,
				R.id.categoryTitle,
				String.valueOf(this.products[position].getTitle() + "\n"
						+ products[position].getPrice()));
		return convertView;
	}

	private TextView detail(View v, int resId, String text) {
		TextView tv = (TextView) v.findViewById(resId);
		tv.setText(text);
		return tv;
	}
}
