package com.arello.tdrtest.uihelpers.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.purchase.PurchaseRecord;

import java.util.List;

public class PurchasesAdapter extends BaseAdapter {
	List<PurchaseRecord> mItemsList;

	public PurchasesAdapter(List<PurchaseRecord> records) {
		mItemsList = records;
	}

	@Override
	public int getCount() {
		return mItemsList.size();
	}

	@Override
	public Object getItem(int index) {
		return mItemsList.get(index);
	}

	public void setItem(int index, PurchaseRecord record) {
		mItemsList.set(index, record);
	}

	@Override
	public long getItemId(int index) {
		return index;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			view = View.inflate(parent.getContext(), R.layout.purchase_cell,
					null);
		}
		PurchaseRecord record = mItemsList.get(index);
		view.findViewById(R.id.checkmark).setVisibility(
				record.isPurchased() ? View.VISIBLE : View.GONE);
		((TextView) view.findViewById(R.id.title_text)).setText(record
				.getTitle());
		return view;
	}

}
