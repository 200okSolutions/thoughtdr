package com.arello.tdrtest.uihelpers.adapters.impl;

import android.content.Context;
import android.view.View;
import com.arello.tdrtest.uihelpers.adapters.DetailChild;
import com.arello.tdrtest.uihelpers.adapters.DetailGroup;

import java.util.List;

/**
 * Date: 17.04.12
 * Time: 14:42
 *
 * @author Yuri Shmakov
 */
public class SimpleGroup implements DetailGroup
{
	transient private View mGroupView;
	private List<DetailChild> mChildren;

	public SimpleGroup(View groupView)
	{
		mGroupView = groupView;
	}

	public void setGroupView(View groupView)
	{
		mGroupView = groupView;
	}

	@Override
	public View render(Context context)
	{
		if (null == mGroupView)
		{
			mGroupView = new View(context);
		}

		return mGroupView;
	}

	public List<DetailChild> getChildren()
	{
		return mChildren;
	}

	public void setChildren(List<DetailChild> children)
	{
		mChildren = children;
	}
}
