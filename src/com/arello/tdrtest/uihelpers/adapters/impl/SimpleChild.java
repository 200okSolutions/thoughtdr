package com.arello.tdrtest.uihelpers.adapters.impl;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.arello.tdrtest.uihelpers.adapters.DetailChild;

/**
 * Date: 17.04.12
 * Time: 14:48
 *
 * @author Yuri Shmakov
 */
public class SimpleChild implements DetailChild
{
	transient private View mChildView;

	public SimpleChild(View childView)
	{
		mChildView = childView;
	}

	@Override
	public View render(Context context)
	{
		if (null == mChildView)
		{
			mChildView = new View(context);
		}

		return mChildView;
	}

	@Override
	public Boolean onClick(Context context)
	{
		return false;
	}
}
