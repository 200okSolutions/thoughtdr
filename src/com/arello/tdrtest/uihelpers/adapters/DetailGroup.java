package com.arello.tdrtest.uihelpers.adapters;

import android.content.Context;
import android.view.View;

import java.io.Serializable;
import java.util.List;

/**
 * Date: 17.04.12
 * Time: 14:42
 *
 * @author Yuri Shmakov
 */
public interface DetailGroup extends Serializable
{
	View render(Context context);

	List<DetailChild> getChildren();

	void setChildren(List<DetailChild> children);
}
