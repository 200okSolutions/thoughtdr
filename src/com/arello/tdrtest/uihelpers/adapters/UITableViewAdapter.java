package com.arello.tdrtest.uihelpers.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import com.arello.tdrtest.ui.custom_views.TDRListView;

public abstract class UITableViewAdapter extends BaseAdapter implements
		TDRListView.MoveListener, TDRListView.RemoveListener,
		OnItemClickListener {
	public static class IndexPath {
		public IndexPath(int _section, int _row) {
			section = _section;
			row = _row;
		}

		public int section;
		public int row;
	}

	public static final int VIEW_TYPE_HEADER = 0;
	public static final int VIEW_TYPE_CELL = 1;

	public abstract int numberOfSections();

	public abstract int numberOfRowsInSection(int section);

	public abstract View viewForHeaderInSection(int section, View reuseView);

	public abstract View viewForCellAtPath(IndexPath indexPath, View reuseView);

	public int mSectionsCount;
	public int[] mRowsCount;

	@Override
	public int getCount() {
		mSectionsCount = numberOfSections();
		int counter = 0;
		mRowsCount = new int[mSectionsCount];
		for (int i = 0; i < mSectionsCount; ++i) {
			mRowsCount[i] = numberOfRowsInSection(i);
			counter += mRowsCount[i];
		}
		return counter + mSectionsCount;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	protected IndexPath indexPathForPosition(int position) {
		if (mSectionsCount > 0) {
			int prevCount = 0;
			for (int s = 0, row = mRowsCount[0] + 1; s < mSectionsCount; row += (mRowsCount[++s] + 1)) {
				if (position > prevCount && position < row)
					return new IndexPath(s, position - prevCount - 1);
				else if (position == prevCount)
					return new IndexPath(s, -1);
				else
					prevCount = row;
			}
		}
		return new IndexPath(-1, -1);
	}

	@Override
	public View getView(int position, View view, ViewGroup arg2) {
		IndexPath viewPath = indexPathForPosition(position);
		if (viewPath.row == -1)
			return viewForHeaderInSection(viewPath.section, view);
		else
			return viewForCellAtPath(viewPath, view);
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		IndexPath viewPath = indexPathForPosition(position);
		if (viewPath.row == -1)
			return VIEW_TYPE_HEADER;
		else
			return VIEW_TYPE_CELL;
	}

	@Override
	public void move(int from, int to) {
		if (mMovelCellListener != null && from != to) {
			IndexPath f = indexPathForPosition(from);
			IndexPath t = indexPathForPosition(to);
			if (t.row == -1) {
				if (from > to) {
					t.section -= 1;
					t.row = mRowsCount[t.section];
				} else {
					t.row = 0;
				}
			} else {
				if (from < to && f.section != t.section) {
					t.row += 1;
				}
			}
			mMovelCellListener.moveCell(f, t);
		}
	}

	@Override
	public boolean canMove(int from, int to) {
		IndexPath path = indexPathForPosition(from);
		if (path.row == -1)
			return false;
		else {
			if (to == 0)
				return false;
		}
		return true;
	}

	@Override
	public void remove(int which) {
		if (mRemoveCellListener == null)
			return;
		IndexPath path = indexPathForPosition(which);
		if (path.row != -1)
			mRemoveCellListener.removeCell(path);
	}

	@Override
	public void moveStarted(int from) {
		if (mMovelCellListener != null) {
			IndexPath f = indexPathForPosition(from);
			mMovelCellListener.moveStarted(f);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> view, View itemView, int position,
			long id) {
		if (mItemClickListener != null) {
			mItemClickListener.onItemClick(view, itemView,
					indexPathForPosition(position));
		}
	}

	public interface OnItemClickListener {
		public void onItemClick(AdapterView<?> view, View itemView,
				IndexPath path);
	}

	OnItemClickListener mItemClickListener;

	public void setOnItemClickListener(OnItemClickListener l) {
		mItemClickListener = l;
	}

	OnMoveCellLisener mMovelCellListener;

	public void setOnMoveCellListener(OnMoveCellLisener l) {
		mMovelCellListener = l;
	}

	OnRemoveCellListener mRemoveCellListener;

	public void setOnRemoveCellListener(OnRemoveCellListener l) {
		mRemoveCellListener = l;
	}

	public static interface OnMoveCellLisener {
		void moveCell(IndexPath from, IndexPath to);

		void moveStarted(IndexPath from);
	}

	public static interface OnRemoveCellListener {
		void removeCell(IndexPath cell);
	}
}
