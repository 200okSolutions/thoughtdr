package com.arello.tdrtest.uihelpers.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Music;
import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.tdrtest.uihelpers.PlayerHelper;
import com.arello.tdrtest.uihelpers.PlayerHelper.PlayerHelperStateChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MusicListAdapter extends CursorAdapter implements PlayerHelperStateChangeListener
{
	private boolean mEditable;
	private TDRBaseActivity mActivity;
	private List<Music> mSelectedItems = new ArrayList<Music>();
	private boolean mHavePlayed;
	private Integer mPlayedPosition;
	private String mPlayedTime;
	ListView mListView;

	public MusicListAdapter(Context context, Cursor c, int flags)
	{
		super(context, c, flags);
	}

	public MusicListAdapter(TDRBaseActivity context, Cursor c, boolean autoRequery, boolean _editable, ListView _listView)
	{
		super(context, c, autoRequery);
		mListView = _listView;
		mEditable = _editable;
		mActivity = context;
		mPlayerHelper.setStateChangedListener(this);
	}

	public void setSelectedItems(List<Music> items)
	{
		mSelectedItems = items;
	}

	PlayerHelper mPlayerHelper = new PlayerHelper();

	public Music getSelectedMusic(Music _music)
	{
		for (Music music : mSelectedItems)
		{
			if (music.getSize() == _music.getSize() && music.getSoundPath().equals(_music.getSoundPath()))
			{
				return music;
			}
		}
		return null;
	}

	public void stopPlay()
	{
		mPlayerHelper.stopPlay();
	}

	public void deselectItem(Music music)
	{
		mActivity.getTDRApplication().getDataManager().deleteMusicWithId(music.getId());
		mSelectedItems.remove(music);
	}

	public void selectItem(Music music)
	{
		music.setSortOrder(mSelectedItems.size());
		mActivity.getTDRApplication().getDataManager().addMusic(music);
		mSelectedItems.add(music);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
	{
		View view = View.inflate(context, R.layout.music_cell, null);
		view.findViewById(R.id.cell_right_icon).setVisibility(View.VISIBLE);

		Music music = new Music();
		music.setValuesFromCursor(cursor);

		if (mEditable)
		{
			view.findViewById(R.id.cell_left_icon).setVisibility(View.VISIBLE);
		}
		else
		{
			view.findViewById(R.id.cell_left_icon).setVisibility(View.GONE);
		}
		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		Music music = new Music();
		music.setValuesFromCursor(cursor);

		((TextView) view.findViewById(R.id.title_text)).setText(music.getName());

		((TextView) view.findViewById(R.id.description_text)).setText(music.getArtist() != null ? music.getArtist() : "");

		if (mEditable)
		{
			Music selectedMusic = getSelectedMusic(music);
			if (selectedMusic != null)
			{
				music.setId(selectedMusic.getId());
			}
			((CheckBox) view.findViewById(R.id.cell_left_icon)).setOnCheckedChangeListener(null);
			((CheckBox) view.findViewById(R.id.cell_left_icon)).setChecked(selectedMusic != null);
			view.findViewById(R.id.cell_left_icon).setTag(music);
			((CheckBox) view.findViewById(R.id.cell_left_icon)).setOnCheckedChangeListener(new OnCheckedChangeListener()
			{

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
				{
					Music music = (Music) buttonView.getTag();
					if (isChecked)
					{
						selectItem(music);
					}
					else
					{
						deselectItem(music);
					}
				}
			});
		}

		view.findViewById(R.id.cell_right_icon).setTag(music);
		view.findViewById(R.id.cell_right_icon).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				v.setSelected(!v.isSelected());
				if (v.isSelected())
				{
					Music music = (Music) v.getTag();
					mPlayerHelper.startPlay(music.getSoundPath());
					mHavePlayed = true;
				}
				else
				{
					stopPlay();
				}
			}
		});

		if (mHavePlayed && mPlayerHelper.getFileName().equals(music.getSoundPath()))
		{
			view.findViewById(R.id.timer_text).setVisibility(View.VISIBLE);
			view.findViewById(R.id.cell_right_icon).setSelected(true);
			((TextView) view.findViewById(R.id.timer_text)).setText(mPlayedTime);
			view.setTag(music);
		}
		else
		{
			view.findViewById(R.id.timer_text).setVisibility(View.GONE);
			view.findViewById(R.id.cell_right_icon).setSelected(false);
			view.setTag(null);
		}
	}

	@Override
	public void playerStarted()
	{
		notifyDataSetChanged();
	}

	@Override
	public void playerStopped()
	{
		mHavePlayed = false;

		View playedView = findPlayedView();

		if (null == playedView)
		{
			return;
		}

		playedView.findViewById(R.id.timer_text).setVisibility(View.GONE);
		playedView.findViewById(R.id.cell_right_icon).setSelected(false);
		playedView = null;
		mPlayedPosition = null;
	}

	@Override
	public int getViewTypeCount()
	{
		return 2;
	}

	@Override
	public int getItemViewType(int position)
	{
		if (!mHavePlayed)
			return super.getItemViewType(position);
		if (!mDataValid)
		{
			throw new IllegalStateException("this should only be called when the cursor is valid");
		}
		if (!mCursor.moveToPosition(position))
		{
			throw new IllegalStateException("couldn't move cursor to position " + position);
		}
		Music m = Music.musicFromCursor(mCursor);
		return m.getSoundPath().equals(mPlayerHelper.getFileName()) ? 1 : 0;
	}

	private View findPlayedView()
	{
		int childCount = mListView.getChildCount();
		for (int i = 0; i < childCount; ++i)
		{
			Music m = (Music) mListView.getChildAt(i).getTag();
			if (m != null && m.getSoundPath().equals(mPlayerHelper.getFileName()))
			{
				return mListView.getChildAt(i);
			}
		}
		return null;
	}

	@Override
	public void progressChanged(int progress, int duration)
	{

		mPlayedTime = String.format("%02d:%02d | %02d:%02d", progress / 60, progress % 60, duration / 60, duration % 60);

		View playedView = findPlayedView();

		if (null == playedView)
		{
			return;
		}

		TextView tv = (TextView) playedView.findViewById(R.id.timer_text);
		tv.setText(mPlayedTime);
		if (tv.getVisibility() != View.VISIBLE)
		{
			tv.setVisibility(View.VISIBLE);
		}
	}
}
