package com.arello.tdrtest.uihelpers.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.List;

/**
 * Date: 14.05.12
 * Time: 19:03
 *
 * @author Yuri Shmakov
 */
public class HelpListAdapter extends BaseExpandableListAdapter
{
	private Context mContext;
	private List<DetailGroup> mGroups;

	public HelpListAdapter(Context context, List<DetailGroup> groups)
	{
		mContext = context;
		mGroups = groups;
	}

	public DetailChild getChild(int groupPosition, int childPosition)
	{
		return mGroups.get(groupPosition).getChildren().get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	public int getChildrenCount(int groupPosition)
	{
		return mGroups.get(groupPosition).getChildren().size();
	}

	public DetailGroup getGroup(int groupPosition)
	{
		return mGroups.get(groupPosition);
	}

	public int getGroupCount()
	{
		return mGroups.size();
	}

	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
	{
		return getGroup(groupPosition).render(mContext);
	}

	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent)
	{
		return getChild(groupPosition, childPosition).render(mContext);
	}

	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}

	public boolean hasStableIds()
	{
		return true;
	}
}
