package com.arello.tdrtest.uihelpers.adapters;

import android.content.Context;
import android.view.View;

import java.io.Serializable;

/**
 * Date: 17.04.12
 * Time: 14:44
 *
 * @author Yuri Shmakov
 */
public interface DetailChild extends Serializable
{
	View render(Context context);

	Boolean onClick(Context context);
}
