package com.arello.tdrtest.uihelpers.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arello.thoughtdr.R;
import com.arello.tdrtest.model.Categories;

public class CategoriesListAdapter extends ArrayAdapter<Categories> {

	private Context context;
	private Categories[] CategoriesList;
	private LayoutInflater inflater = null;
	private CategoriesListAdapter CategoriesListAdapter;

	public CategoriesListAdapter(Context context, int resource,
			Categories[] CategoriesList) {
		super(context, resource, CategoriesList);

		this.context = context;
		this.CategoriesList = CategoriesList;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		CategoriesListAdapter = this;

	}

	@SuppressWarnings("unused")
	private class Holder {

		TextView textCategoriesName = null;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = null;

		if (convertView == null) {
			try {

				holder = new Holder();
				convertView = inflater.inflate(R.layout.category_title, null);
				convertView.setTag(holder);

			} catch (Exception e) {
				System.out.println("Exception Ex : " + e.toString());
			}
		}

		else {

			try {

				holder = (Holder) convertView.getTag();

			} catch (Exception e) {
				System.out.println("Exception Ex : " + e.toString());
			}
		}

		holder.textCategoriesName = detail(convertView, R.id.categoryTitle,
				String.valueOf(this.CategoriesList[position].getName()));

		return convertView;

	}

	private TextView detail(View v, int resId, String text) {
		TextView tv = (TextView) v.findViewById(resId);
		tv.setText(text);
		return tv;
	}
}
