package com.arello.tdrtest.uihelpers.adapters;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.arello.tdrtest.Constants;
import com.arello.thoughtdr.R;
import com.arello.tdrtest.data.objects.Affirmation;
import com.arello.tdrtest.data.objects.Category;
import com.arello.tdrtest.ui.TDRBaseActivity;
import com.arello.tdrtest.uihelpers.PlayerHelper;
import com.arello.tdrtest.uihelpers.PlayerHelper.PlayerHelperStateChangeListener;
import com.arello.tdrtest.uihelpers.Utils;

import java.util.*;

public class AffirmationListAdapter extends UITableViewAdapter
		implements PlayerHelperStateChangeListener, OnCheckedChangeListener
{
	public final static long MANAGE_AFFIRMATION_SECTION = 1;
	Map<Category, List<Affirmation>> mAffirmations = new TreeMap<Category, List<Affirmation>>();
	boolean mEditable = false;
	TDRBaseActivity mActivity;
	List<Category> mCategories;
	Map<Boolean, Drawable> mGroupBackground = new HashMap<Boolean, Drawable>();

	ArrayList<Affirmation> mSelectedAffirmations = new ArrayList<Affirmation>();

	public AffirmationListAdapter(TDRBaseActivity _activity, List<Affirmation> _affirmations, boolean _editable)
	{
		mGroupBackground.put(true, _activity.getResources().getDrawable(R.drawable.category_header_bg));
		mGroupBackground.put(false, _activity.getResources().getDrawable(R.drawable.category_header_locked_bg));
		mEditable = _editable;
		mActivity = _activity;
		mPlayerHelper.setStateChangedListener(this);
		if (_editable)
		{
			mCategories = mActivity.getTDRApplication().getDataManager().getAllCategories();
		}
		else
		{
			mCategories = new ArrayList<Category>();
			mCategories.add(new Category());
		}
		for (Category cat : mCategories)
		{
			mAffirmations.put(cat, new ArrayList<Affirmation>());
		}
		setData(_affirmations);
	}

	public Category getCategory(int index)
	{
		return mCategories.get(index);
	}

	public Affirmation getItem(IndexPath position)
	{
		return mAffirmations.get(mCategories.get(position.section)).get(position.row);
	}

	public void setData(List<Affirmation> _affirmations)
	{
		//		if (mPlayerHelper != null)
		//			mPlayerHelper.stopPlay();
		//		mPlayerHelper = new PlayerHelper();

		for (List<Affirmation> affList : mAffirmations.values())
		{
			affList.clear();
		}
		mSelectedAffirmations.clear();
		if (mEditable)
		{
			for (Affirmation aff : _affirmations)
			{
				if (mAffirmations.get(aff.getCategory()) != null)
				{
					mAffirmations.get(aff.getCategory()).add(aff);
				}
				if (aff.getPlayNight())
				{
					mSelectedAffirmations.add(aff);
				}
			}
			Collections.sort(mSelectedAffirmations, new Comparator<Affirmation>()
			{

				@Override
				public int compare(Affirmation arg0, Affirmation arg1)
				{
					return arg0.getPlaylistSortOrder() - arg1.getPlaylistSortOrder();
				}
			});
		}
		else
		{
			mAffirmations.put(new Category(), _affirmations);
		}
	}

	@Override
	public boolean areAllItemsEnabled()
	{
		return false;
	}

	@Override
	public boolean isEnabled(int position)
	{
		return false;
	}

	Affirmation mPlayedAffirmation;
	View mPlayedView;
	boolean mAnimationInProgress = false;
	PlayerHelper mPlayerHelper = new PlayerHelper();

	public void stopPlay()
	{
		mPlayerHelper.stopPlay();
	}

	boolean needHideKeyboard = false;

	protected View initView()
	{
		View view = View.inflate(mActivity, R.layout.affirmation_cell, null);
		if (mEditable)
		{
			view.findViewById(R.id.cell_left_icon).setVisibility(View.VISIBLE);
			view.findViewById(R.id.cell_right_icon).setVisibility(View.VISIBLE);
			view.findViewById(R.id.cell_right_icon).setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					if (mAnimationInProgress)
					{
						return;
					}
					View baseView = (View) v.getParent();
					Affirmation aff = (Affirmation) baseView.getTag();
					boolean newAff = (aff != mPlayedAffirmation);
					mPlayerHelper.stopPlay();

					if (!v.isSelected() && newAff)
					{
						v.setSelected(true);
						mPlayedAffirmation = aff;
						mPlayerHelper.startPlay(aff.getSoundPath());
					}
				}
			});
			CheckBox checkBox = (CheckBox) view.findViewById(R.id.cell_left_icon);
			checkBox.setOnCheckedChangeListener(this);
		}
		return view;
	}

	@Override
	public void playerStarted()
	{
		notifyDataSetChanged();
	}

	@Override
	public void playerStopped()
	{
		if (mPlayedView != null)
		{
			mPlayedView.findViewById(R.id.timer_text).setVisibility(View.GONE);
			mPlayedView.findViewById(R.id.cell_right_icon).setSelected(false);
			mPlayedView = null;
		}
		//		notifyDataSetChanged();
		mPlayedAffirmation = null;
	}

	@Override
	public void progressChanged(int progress, int duration)
	{
		notifyDataSetChanged();
		//		if (mPlayedView != null) {
		//			TextView tv = (TextView) mPlayedView.findViewById(R.id.timer_text);
		//			tv.setText(String.format("%02d:%02d | %02d:%02d", progress / 60,
		//					progress % 60, duration / 60, duration % 60));
		//			if (tv.getVisibility() != View.VISIBLE) {
		//				tv.setVisibility(View.VISIBLE);
		//			}
		//		}
	}

	@Override
	public int numberOfSections()
	{
		return mCategories.size();
	}

	@Override
	public int numberOfRowsInSection(int section)
	{
		Category cat = mCategories.get(section);
		if (!cat.getHidden())
		{
			return mAffirmations.get(cat).size();
		}
		return 0;
	}

	@Override
	public View viewForHeaderInSection(int section, View reuseView)
	{
		if (mEditable)
		{
			if (reuseView == null)
			{
				reuseView = View.inflate(mActivity, R.layout.affirmation_header, null);
			}

			TextView category = (TextView) reuseView.findViewById(R.id.category_header_title);
			ImageView arrow = (ImageView) reuseView.findViewById(R.id.arrow_image);

			Category cat = mCategories.get(section);
			boolean haveChild = mAffirmations.get(cat).size() > 0;

			category.setText(cat.getTitle());

			int alpha = haveChild ? 255 : 125;

			reuseView.setBackgroundResource(
					haveChild ? R.drawable.category_header_bg : R.drawable.category_header_locked_bg);
			arrow.setImageResource((haveChild && cat.getHidden()) ? R.drawable.right_arrow :
			                       (haveChild ? R.drawable.down_arrow : R.drawable.down_arrow_locked));
			category.setTextColor(category.getTextColors().withAlpha(alpha));


			return reuseView;
		}
		else
		{
			return new View(mActivity);
		}
	}

	@Override
	public int getViewTypeCount()
	{
		return super.getViewTypeCount() + 1;
	}

	@Override
	public int getItemViewType(int position)
	{
		int type = super.getItemViewType(position);
		if (type == VIEW_TYPE_CELL)
		{
			IndexPath indexPath = indexPathForPosition(position);
			Category cat = mCategories.get(indexPath.section);
			Affirmation item = mAffirmations.get(cat).get(indexPath.row);
			if (item == mPlayedAffirmation)
			{
				return VIEW_TYPE_CELL + 1;
			}
		}
		return type;
	}

	@Override
	public View viewForCellAtPath(IndexPath indexPath, View reuseView)
	{
		Category cat = mCategories.get(indexPath.section);
		Affirmation item = mAffirmations.get(cat).get(indexPath.row);

		//if (reuseView == mPlayedView )
		//	mPlayedView = null;

		if (null == reuseView)
		{
			reuseView = initView();
		}
		else
		{
			TextView titleText = (TextView) reuseView.findViewById(R.id.title_text);
			if (titleText == null)
			{
				reuseView = initView();
			}
		}
		reuseView.setTag(null);
		CheckBox checkBox = (CheckBox) reuseView.findViewById(R.id.cell_left_icon);
		checkBox.setChecked(mActivity.getTDRApplication().getDataManager().isAffirmationChecked(item));
		Utils.setViewIsEnabled(checkBox,
		                       mSelectedAffirmations.size() < Constants.MAX_AFFIRMATOIN_COUNT || checkBox.isChecked());

		if (mEditable)
		{
			TextView sortOrderTextView = (TextView) reuseView.findViewById(R.id.sort_order);
			int sortOrder = mSelectedAffirmations.indexOf(item);
			if (sortOrder == -1)
			{
				sortOrderTextView.setVisibility(View.INVISIBLE);
			}
			else
			{
				sortOrderTextView.setText(String.valueOf(sortOrder + 1));
				sortOrderTextView.setVisibility(View.VISIBLE);
			}
		}

		if (mPlayedAffirmation == item)
		{
			mPlayedView = reuseView;
			TextView tv = (TextView) mPlayedView.findViewById(R.id.timer_text);
			tv.setText(String.format("%02d:%02d | %02d:%02d", mPlayerHelper.getCurrentProgress() / 60,
			                         (mPlayerHelper.getCurrentProgress() > 0 ? mPlayerHelper.getCurrentProgress() : 0) %
			                         60, mPlayerHelper.getSongDuration() / 60, mPlayerHelper.getSongDuration() % 60));
			if (tv.getVisibility() != View.VISIBLE)
			{
				tv.setVisibility(View.VISIBLE);
			}
			mPlayedView.findViewById(R.id.cell_right_icon).setSelected(true);
		}
		else
		{
			reuseView.findViewById(R.id.cell_right_icon).setSelected(false);
			reuseView.findViewById(R.id.timer_text).setVisibility(View.GONE);
		}

		reuseView.setTag(item);
		((TextView) reuseView.findViewById(R.id.title_text)).setText(item.getName());

		return reuseView;
	}

	public void commitSortOrder()
	{
		for (int i = 0; i < mSelectedAffirmations.size(); ++i)
		{
			mActivity.getTDRApplication().getDataManager()
			         .setPlayListSortOrderForAffirmation(mSelectedAffirmations.get(i), i);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{

		Affirmation aff = (Affirmation) ((View) buttonView.getParent()).getTag();

		if (aff != null && isChecked != aff.getPlayNight())
		{
			mActivity.getTDRApplication().getDataManager().flagAffirmation(aff, isChecked);

			if (isChecked)
			{
				mSelectedAffirmations.add(aff);
			}
			else
			{
				mSelectedAffirmations.remove(aff);
			}
			notifyDataSetChanged();
		}
	}

}
