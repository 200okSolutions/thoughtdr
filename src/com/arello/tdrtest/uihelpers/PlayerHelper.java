package com.arello.tdrtest.uihelpers;

import java.io.FileDescriptor;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.util.Log;

public class PlayerHelper
{
	MediaPlayer mPlayer;
	String fileName;
	Handler mHandler = new Handler();
	int mTime = 0;
	Runnable mTimerTask = new Runnable()
	{

		@Override
		public void run()
		{
			mTime += 1;
			if (mStateListener != null)
			{
				int durationInSeconds = getSongDuration();
				mStateListener.progressChanged(mTime, durationInSeconds);
			}
			mHandler.postDelayed(mTimerTask, 1000);
		}
	};

	public void startPlay(String fileName)
	{
		if (mPlayer != null)
		{
			stopPlay();
		}
		try
		{
			mPlayer = new MediaPlayer();
			mPlayer.setDataSource(fileName);
			mPlayer.setOnCompletionListener(new OnCompletionListener()
			{

				@Override
				public void onCompletion(MediaPlayer arg0)
				{
					stopPlay();
				}
			});
			mPlayer.prepare();
			mPlayer.start();
			this.fileName = fileName;
			mTime = -1;
			mHandler.postDelayed(mTimerTask, 0);
			if (mStateListener != null)
			{
				mStateListener.playerStarted();
			}
		}
		catch (IOException e)
		{
			Log.e(getClass().getName(), "prepare() failed");
		}
	}

	public void startPlayAss(FileDescriptor fd)
	{
		if (mPlayer != null)
		{
			stopPlay();
		}
		try
		{
			mPlayer = new MediaPlayer();
			mPlayer.setDataSource(fd);
			mPlayer.setOnCompletionListener(new OnCompletionListener()
			{

				@Override
				public void onCompletion(MediaPlayer arg0)
				{
					stopPlay();
				}
			});
			mPlayer.prepare();
			mPlayer.start();
			fileName = fileName;
			mTime = -1;
			mHandler.postDelayed(mTimerTask, 0);
			if (mStateListener != null)
			{
				mStateListener.playerStarted();
			}
		}
		catch (IOException e)
		{
			Log.e(getClass().getName(), "prepare() failed");
		}
	}

	public int getSongDuration()
	{
		if (mPlayer != null)
		{
			return (int) Math.round((double) mPlayer.getDuration() / 1000);
		}
		return 0;
	}

	public int getCurrentProgress()
	{
		return mTime;
	}

	public void stopPlay()
	{
		if (mPlayer == null)
		{
			return;
		}
		mPlayer.stop();
		mPlayer.release();
		mPlayer = null;
		mHandler.removeCallbacks(mTimerTask);
		if (mStateListener != null)
		{
			mStateListener.playerStopped();
		}
	}

	PlayerHelperStateChangeListener mStateListener;

	public interface PlayerHelperStateChangeListener
	{
		void playerStarted();

		void playerStopped();

		void progressChanged(int progress, int duration);
	}

	public void setStateChangedListener(PlayerHelperStateChangeListener listener)
	{
		mStateListener = listener;
	}

	public String getFileName()
	{
		return fileName;
	}
}
