package com.arello.thoughtdr;

import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class GCMTask {

	Context context;
	private static final String PROJECT_ID = "1060076496302";

	private static final String TAG = "ThoughtDR";

	private String regId = "";

	IntentFilter gcmFilter;

	public GCMTask(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public String registerClient() {
		try {
			// Success! Do what you want
			GCMRegistrar.checkDevice(context);
			GCMRegistrar.checkManifest(context);
			regId = GCMRegistrar.getRegistrationId(context);
			if (regId.equals("")) {
				GCMRegistrar.register(context, PROJECT_ID);
				regId = GCMRegistrar.getRegistrationId(context);
			}
			System.out.println(TAG
					+ ":-Get The Registration Id From Registration Class.");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return regId;
	}

	public boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				Log.i(TAG, "This device is supported.");
			} else {
				Log.i(TAG, "This device is not supported.");
			}
			return false;
		}
		return true;
	}
}
