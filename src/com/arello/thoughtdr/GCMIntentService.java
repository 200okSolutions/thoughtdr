package com.arello.thoughtdr;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.arello.tdrtest.Constants;
import com.arello.tdrtest.uihelpers.Utils;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String PROJECT_ID = "14059071431";

	private static final String TAG = "GCMIntentService";

	public GCMIntentService() {
		super(PROJECT_ID);
		Log.d(TAG, "GCMIntentService init");
	}

	@Override
	protected void onError(Context ctx, String sError) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Error: " + sError);

	}

	@Override
	protected void onMessage(Context ctx, Intent intent) {

		Log.d(TAG, "Message Received");

		String message = intent.getStringExtra("message");

		sendGCMIntent(ctx, message);

	}

	private void sendGCMIntent(Context ctx, String message) {

		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("GCM_RECEIVED_ACTION");

		broadcastIntent.putExtra("gcm", message);

		ctx.sendBroadcast(broadcastIntent);

	}

	@Override
	protected void onRegistered(Context ctx, String regId) {
		// TODO Auto-generated method stub
		Log.d(TAG, regId);
		if (regId.equals("")) {
			Utils.saveSharedPreferences(Constants.REGISTRATION_ID, "1", ctx);
		} else {
			Utils.saveSharedPreferences(Constants.REGISTRATION_ID, regId, ctx);
		}
	}

	@Override
	protected void onUnregistered(Context ctx, String regId) {
		// TODO Auto-generated method stub
		// send notification to your server to remove that regId

	}

}
